interface CacheItem<V> {
	value: V;
	timestamp: number;
}

export class SimpleCache<K, V> {
	contents: Map<K, CacheItem<V>>;
	oldContents: this["contents"];
	cacheA: this["contents"];
	cacheB: this["contents"];
	maxAge: number;
	interval: ReturnType<typeof setInterval> | undefined;
	/** Max age in ms */
	constructor(maxAge: number) {
		this.maxAge = maxAge;
		this.cacheA = new Map<K, CacheItem<V>>();
		this.cacheB = new Map<K, CacheItem<V>>();
		this.contents = this.cacheA;
		this.oldContents = this.cacheB;
	}
	private toggle() {
		this.oldContents.clear();
		if (this.contents.size === 0 && this.interval !== undefined) {
			clearInterval(this.interval);
			this.interval = undefined;
		} else {
			const temp = this.oldContents;
			this.oldContents = this.contents;
			this.contents = temp;
		}
	}
	set(key: K, value: V): void {
		this.contents.set(key, {
			value,
			timestamp: Date.now(),
		});
		if (this.interval === undefined) {
			this.interval = setInterval(() => this.toggle(), this.maxAge);
		}
	}
	get(key: K): V | undefined {
		const item = this.contents.get(key) ?? this.oldContents.get(key);
		if (item === undefined || item.timestamp < Date.now() - this.maxAge) {
			return undefined;
		} else {
			return item.value;
		}
	}
}
