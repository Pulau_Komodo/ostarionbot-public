import { ChatClient } from "@twurple/chat";
import { checkSoonEvents } from "../calendar/event_checks.js";
import { db } from "../db.js";
import { log } from "../log.js";
import { onMessage } from "../onMessage.js";
import { twitchAuthProvider } from "./twitch_auth.js";

export class TwitchMessage {
	readonly network = "twitch";
	readonly channel: string;
	readonly userId: string;
	readonly content: string;
	readonly whisper: boolean;
	constructor(channel: string, author: string, content: string, whisper: boolean) {
		this.channel = channel;
		this.userId = author;
		this.content = content;
		this.whisper = whisper;
		this.channel = whisper ? author : channel;
	}
}

const qChannels = `
	SELECT domain
	FROM domain_config
	WHERE network = 'twitch' AND enabled = true
`;
const channels: string[] = db
	.prepare(qChannels)
	.all()
	.map((config) => (config as { domain: string }).domain);

export const twitchChat = new ChatClient({
	authProvider: twitchAuthProvider,
	channels,
});

let joinedCount = 0;
/** Whether events were checked */
let wereEventsChecked = false;

log("Connecting to Twitch chat");
twitchChat.connect();

setTimeout(() => {
	if (wereEventsChecked) return;
	checkSoonEvents("twitch", true);
	log(
		`Only ${joinedCount}/${channels.length} Twitch channels were joined even after 20 seconds, checking events anyway.`,
	);
	log(channels);
}, 20 * 1000);

twitchChat.onJoin((channel) => {
	joinedCount++;
	log(`Joined ${channel} (${joinedCount}/${channels.length})`);
	if (joinedCount === channels.length) {
		log(`${joinedCount}/${channels.length} Twitch channels joined, checking events.`);
		checkSoonEvents("twitch", true);
		wereEventsChecked = true;
	}
});

twitchChat.onConnect(async () => {
	log("Connected to Twitch chat");
	/*		setTimeout(() => {
		if (wereEventsChecked) return;
		checkSoonEvents("twitch", true);
		log(`Only ${joinedCount}/${channels.length} channels were joined even after 20 seconds, checking events anyway.`);
		log(channels);
	}, 20*1000);

	try {
		const joins = channels.map(channel => twitchChat.join(channel));
		const results = await Promise.allSettled(joins);
		joinedCount = results.reduce((accumulator, item) => {
			return accumulator + (item.status === "fulfilled" ? 1 : 0);
		}, 0);
	} catch (error) {
		console.log(error);
	}
	log(`${joinedCount}/${channels.length} channels joined, checking events.`);
	checkSoonEvents("twitch", true);
	wereEventsChecked = true;*/
});

twitchChat.onMessage((channel, user, message) => {
	const messageObject = new TwitchMessage(channel, user, message, false);
	void onMessage(messageObject);
});

// twitchChat.onWhisper((user, message) => {
// 	const messageObject = new TwitchMessage(user, user, message, true);
// 	void onMessage(messageObject);
// });
