import { CommandError } from "../Errors.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";
import { formatList } from "../formatList.js";
import { twitchApi } from "./twitchApi.js";
import { HelixStream } from "@twurple/api";
import { NBSP, VS16, WJ } from "../characters.js";
import { makeUnbreakable } from "../util.js";
import { ApplicationCommandOptionType } from "discord.js";

const getTopStreams = async (
	gameArg: string | undefined,
): Promise<{ streams: HelixStream[]; gameName: string | undefined }> => {
	if (!gameArg) {
		const { data } = await twitchApi.streams.getStreams({ limit: 20 });
		return { streams: data, gameName: undefined };
	}
	const game = await twitchApi.games.getGameByName(gameArg);
	if (game === null) {
		const { data: searchResults } = await twitchApi.search.searchCategories(gameArg);
		if (searchResults.length === 0) throw new CommandError("Game not found");
		let output = `Game not found. Did you mean `;
		const searchResultsText = searchResults.slice(0, 10).map(({ name }) => `"${name}"`);
		output += formatList(searchResultsText, { finalConnector: " or " });
		output += `?`;
		throw new CommandError(output);
	}

	const { data } = await game.getStreams({ limit: 20 });
	return { streams: data, gameName: game.name };
};

/** A user attempts to get the top Twitch streams, optionally filtered to a game */
const topStreams = async (gameArg: string | undefined, onDiscord: boolean): Promise<string> => {
	const { streams, gameName } = await getTopStreams(gameArg);
	const plural_s = streams.length === 1 ? "" : "s";
	const streamsText = gameName
		? `${gameName} stream${plural_s}`
		: `stream${plural_s} on all of Twitch`;
	if (streams.length === 0) return `There are no ${streamsText}.`;

	const topOnly = streams.length > 10 ? "top" : "only";
	const eye = `${onDiscord ? "\\" : ""}👁${VS16}`;
	const eggplant = onDiscord ? "\\🍆" : "🍆";
	const gamepad = onDiscord ? "\\🎮" : "🎮";
	const formattedStreams = streams
		.slice(0, 10)
		.map(({ userName, userDisplayName, viewers, gameName }) => {
			let output = ``;
			const linkName =
				userName.toLowerCase() === userDisplayName.toLowerCase()
					? userDisplayName
					: userName;
			if (gameArg) {
				output += onDiscord ? `<` : ``;
				output += `https://twitch.tv/${linkName}`;
				output += onDiscord ? `>` : ``;
			} else {
				output += onDiscord ? `**${linkName}**` : linkName;
				output += `${NBSP}${gamepad}${NBSP}${makeUnbreakable(gameName)}`;
			}
			const viewersIcon = gameName === "Pools, Hot Tubs, and Beaches" ? eggplant : eye;
			output += `${NBSP}(${viewers.toString()}${WJ}${viewersIcon})`;
			return output;
		});
	const count = streams.length === 1 ? "" : `${streams.slice(0, 10).length.toString()} `;
	let output = `The ${topOnly} ${count}${streamsText} `;
	output += formatList(formattedStreams, { opening: ["is ", "are "] });
	output += ".";
	return output;
};

const cTopStreams: CommandFunction = async (cCall) => {
	cCall.reply(await topStreams(cCall.argstring, cCall.network === "discord"));
};

const slashTopStreams: SlashCommandFunction = (interaction) => {
	const gameArg = interaction.options.getString("game");
	return topStreams(gameArg ?? undefined, true);
};

addCommand({
	name: "topstreams",
	run: cTopStreams,
	category: "twitch",
	usage: "[game]",
	description: "Gets the top 10 streams, optionally filtered to a game.",
	slash: {
		run: slashTopStreams,
		options: [
			{
				name: "game",
				type: ApplicationCommandOptionType.String,
				description: "The game to filter by",
			},
		],
	},
	heat: { fixed: 2 },
});
