import { ApplicationCommandOptionType, escapeMarkdown } from "discord.js";
import { stringifyInterval, stringifyIntervalShort } from "interval-conversions";
import { CommandError } from "../Errors.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";
import { twitchApi } from "./twitchApi.js";
import ISO6391 from "iso-639-1";

const streamInfo = async (channelName: string, onDiscord: boolean): Promise<string> => {
	const user = await twitchApi.users.getUserByName(channelName).catch(console.error);
	if (!user) throw new CommandError(`Channel "${channelName}" not found`);
	const stream = await twitchApi.streams.getStreamByUserName(channelName).catch(console.error);

	const linkName = user.displayName.toLowerCase() === user.name ? user.displayName : user.name;
	const link = onDiscord ? `<https://twitch.tv/${linkName}>` : `https://twitch.tv/${linkName}`;
	if (!stream) {
		return `${link} is offline.`;
	}

	const durationText = stringifyInterval(stream.startDate.getTime() - Date.now());
	const title = onDiscord ? escapeMarkdown(stream.title) : stream.title;
	const viewers = stream.viewers === 1 ? "viewer" : "viewers";
	return `${link} is streaming ${
		stream.gameName
	}. Live for ${durationText}. ${stream.viewers.toString()} ${viewers}. Title: "${title}"`;
};

const cStreamInfo: CommandFunction = async (cCall) => {
	if (!cCall.args[0]) throw new CommandError();
	const output = await streamInfo(cCall.args[0], cCall.network === "discord");
	cCall.reply(output);
};

const slashStreamInfo: SlashCommandFunction = (interaction) => {
	const channelName = interaction.options.getString("channel", true);
	return streamInfo(channelName, true);
};

addCommand({
	name: "streaminfo",
	run: cStreamInfo,
	category: "twitch",
	usage: "<channel>",
	description: "Get information about a Twitch stream.",
	aliases: ["si"],
	slash: {
		run: slashStreamInfo,
		options: [
			{
				name: "channel",
				type: ApplicationCommandOptionType.String,
				description: "The name of the channel",
				required: true,
			},
		],
	},
});

const userTypeRecord: Record<string, string> = {
	staff: "a Twitch staff member",
	admin: "a Twitch admin",
	// eslint-disable-next-line @typescript-eslint/naming-convention
	global_mod: "a global Twitch moderator",
	"": "",
};

const stringifyTypes = (type: string, broadcasterType: string): string => {
	if (!type && !broadcasterType) return "";
	let output = " (";
	if (type) {
		output += userTypeRecord[type];
		if (broadcasterType) output += ` and ${broadcasterType}`;
		output += ")";
		return output;
	}
	if (broadcasterType === "partner") {
		output += `a `;
	} else if (broadcasterType === "affiliate") {
		output += `an `;
	}
	output += `${broadcasterType})`;
	return output;
};

const channelInfo = async (channelName: string): Promise<string> => {
	const user = await twitchApi.users.getUserByName(channelName).catch(console.error);
	if (!user) throw new CommandError(`Channel "${channelName}" not found`);
	const channel = await twitchApi.channels.getChannelInfoById(user.id);
	if (!channel) throw new CommandError("Channel not found");

	const { broadcasterType, creationDate, description, displayName, id, name, type } = user;
	const { gameName, language, title } = channel;

	const nameText = displayName.toLowerCase() === name ? displayName : `${displayName} (${name})`;
	const userTypeText = stringifyTypes(type, broadcasterType);
	const descriptionText = description ? `description: "${description}"` : `no description`;
	const titleText = title ? `title: "${title}"` : `no title`;
	const categoryText = gameName ? `category: ${gameName}` : `no category`;
	const languageName = ISO6391.getName(language);
	const languageNativeName = ISO6391.getNativeName(language);
	const languageNames =
		languageName === languageNativeName
			? languageName
			: `${languageName} (${languageNativeName})`;
	const languageText = languageName !== "" ? `${languageNames}` : language;

	return `Channel info for ${nameText}${userTypeText}: id: ${id}, created at ${creationDate.toISOString()} (over ${stringifyIntervalShort(
		creationDate.getTime() - Date.now(),
		true,
	)} ago), ${descriptionText}, ${titleText}, ${categoryText}, language: ${languageText}`;
};

const cChannelInfo: CommandFunction = async (cCall) => {
	if (!cCall.args[0]) throw new CommandError();
	let output = await channelInfo(cCall.args[0]);

	if (cCall.network === "discord") output = escapeMarkdown(output);
	cCall.reply(output);
};

const slashChannelInfo: SlashCommandFunction = (interaction) => {
	const channelName = interaction.options.getString("channel", true);
	return channelInfo(channelName);
};

addCommand({
	name: "channelinfo",
	run: cChannelInfo,
	category: "twitch",
	usage: "<channel>",
	description: "Get information about a Twitch channel.",
	aliases: ["streaminfo2"],
	slash: {
		run: slashChannelInfo,
		options: [
			{
				name: "channel",
				type: ApplicationCommandOptionType.String,
				description: "The name of the channel",
				required: true,
			},
		],
	},
});
