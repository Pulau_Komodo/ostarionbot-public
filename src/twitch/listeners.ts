import { twitchApi } from "./twitchApi.js";
import { DirectConnectionAdapter, EventSubListener, EventSubStreamOnlineEvent } from "@twurple/eventsub";
import { queueMessage } from "../message.js";
import { CommandError } from "../Errors.js";
import { addCommand, CommandFunction } from "../commands.js";
import { stringifyInterval } from "interval-conversions";
import { PermissionFlagsBits, Permissions } from "discord.js";
import { Network } from "../common_types.js";

/*
const listener = new EventSubListener(
	twitchApi,
	new DirectConnectionAdapter({
		hostName: "???",
		sslCert: {
			key: "???",
			cert: "???"
		}
	}),
	"???"
);
listener.listen().catch(console.error);
*/
interface SubscriptionInfo {
	network: Network;
	channel: string;
	reason: "chatter" | "popular" | "admin";
}
/** A map with broadcaster IDs as keys and information on who is subscribed as values */
const subscribedStreams = new Map<string, SubscriptionInfo[]>();

const handleStreamOnlineEvent = async (event: EventSubStreamOnlineEvent): Promise<void> => {
	const subscribedChannels = subscribedStreams.get(event.broadcasterId);
	if (!subscribedChannels) {
		console.error(
			`Notified of channel "${event.broadcasterName}" going online with no subscriptions. ID: ${event.broadcasterId}`,
		);
		return;
	}
	for (const { network, channel } of subscribedChannels) {
		const linkName =
			event.broadcasterDisplayName.toLowerCase() === event.broadcasterName.toLowerCase()
				? event.broadcasterDisplayName
				: event.broadcasterName;
		const interval = stringifyInterval(event.startDate.getTime() - Date.now());
		const ago = event.startDate.getTime() <= Date.now() ? "ago" : "from now";
		let output = `https://twitch.tv/${linkName} just went live `;
		if (event.streamType === "playlist") {
			output += `with a playlist `;
		} else if (event.streamType === "premiere") {
			output += `with a premiere `;
		} else if (event.streamType === "rerun") {
			output += `with a rerun `;
		} else if (event.streamType === "watch_party") {
			output += `with a watch party `;
		}
		output += `${interval} ${ago}!`;
		queueMessage(network, channel, output);
	}
};

// listener.subscribeToStreamOnlineEvents("id", handleStreamOnlineEvent).catch(console.error);

const addSubscription = async (
	network: Network,
	channel: string,
	reason: "chatter" | "popular" | "admin",
	target: string,
): Promise<boolean> => {
	const user = await twitchApi.users.getUserByName(target).catch(console.log);
	if (!user) return false;
	console.log(`ID: ${user.id} Name: ${user.name}`);
	const twitchChannel = await twitchApi.channels.getChannelInfoById(user).catch(console.error);
	if (!twitchChannel) return false;
	console.log(`ID: ${twitchChannel.id} Name: ${twitchChannel.name}`);
	const subscriptionInfo = { network, channel, reason };
	const subscriptions = subscribedStreams.get(twitchChannel.id);
	if (subscriptions) {
		if (
			subscriptions.some((subscription) => {
				return (
					network === subscription.network &&
					channel === subscription.channel &&
					reason === subscription.reason
				);
			})
		)
			return false;
		subscriptions.push(subscriptionInfo);
	} else {
		subscribedStreams.set(twitchChannel.id, [subscriptionInfo]);
	}
	return true;
};

const cAddSubscription: CommandFunction = async (cCall) => {
	if (!cCall.args[0]) throw new CommandError();

	if (
		(cCall.network === "discord" &&
			!cCall.message.member.permissions.has(PermissionFlagsBits.Administrator)) ||
		(cCall.network === "twitch" && cCall.user.id !== cCall.channel.id.slice(1))
	)
		throw new CommandError("Admin only");

	const success = await addSubscription(cCall.network, cCall.channel.id, "admin", cCall.args[0]);
	if (success) cCall.reply("Success");
	else cCall.reply("Failed");
};

addCommand({
	name: "testsubscribe",
	run: cAddSubscription,
	category: "twitch",
	usage: "<username>",
	description:
		"Subcribes the channel to receive alerts when a stream goes online. (admin only and does not actually do anything)",
});
