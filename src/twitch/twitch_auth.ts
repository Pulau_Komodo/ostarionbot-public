import { AccessToken, RefreshingAuthProvider } from "@twurple/auth";
import { db } from "../db.js";
import { twitch as twitchConfig } from "../config.js";
const { clientId, clientSecret } = twitchConfig;

const queryTwitchAuth = (): string => {
	const qTwitchAuth = `
		SELECT value
		FROM global_variables WHERE key = 'twitchAccessToken'
	`;
	return (db.prepare(qTwitchAuth).get() as { value: string }).value;
};

const querySaveTwitchAuth = (accessToken: string): void => {
	const qSaveAccessToken = `
		UPDATE global_variables
		SET value = $accessToken
		WHERE key = 'twitchAccessToken'
	`;
	db.prepare(qSaveAccessToken).run({ accessToken });
};

const onRefresh = (_userId: string, token: AccessToken) => {
	querySaveTwitchAuth(JSON.stringify(token));
};

const tokenData = JSON.parse(queryTwitchAuth());

export const twitchAuthProvider = new RefreshingAuthProvider({
	clientId,
	clientSecret,
});

twitchAuthProvider.onRefresh(onRefresh);

twitchAuthProvider.addUser(twitchConfig.userId, tokenData, ["chat"]);
