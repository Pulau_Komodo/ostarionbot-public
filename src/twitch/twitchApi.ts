import { ApiClient } from "@twurple/api";
import { twitchAuthProvider } from "./twitch_auth.js";

export const twitchApi = new ApiClient({ authProvider: twitchAuthProvider });
