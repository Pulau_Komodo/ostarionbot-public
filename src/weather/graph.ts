import { execFile } from "child_process";
import { AttachmentBuilder } from "discord.js";
import { promisify } from "util";

const promiseExecFile = promisify(execFile);

type GraphType =
	| "hourly_pop"
	| "hourly_precipitation"
	| "hourly_temp"
	| "hourly_uvi"
	| "hourly_wind"
	| "daily_temp"
	| "minutely_precipitation";

type CompositeGraphType = "hourly_composite";

type Interval = "minute" | "hour" | "day";

/**
 * Generates a graph and puts it in an attachment. Data is an array of arrays. The first value of each array should be the UNIX timestamp in seconds. The others are the other values that belong to a particular point in time in the graph.
 */
export const graph = async (
	data: [number, ...number[]][],
	timezoneOffset: number,
	type: GraphType,
	interval: Interval,
): Promise<AttachmentBuilder> => {
	const args = data
		.flatMap((element) => {
			const date = new Date((element[0] + timezoneOffset) * 1000);
			let time;
			if (interval === "hour") {
				time = date.getUTCHours();
			} else if (interval === "day") {
				time = date.getUTCDate();
			} else {
				time = date.getUTCMinutes();
			}
			return [time, ...element.slice(1)];
		})
		.map((n) => n.toString());
	args.unshift(type);
	try {
		const result = await promiseExecFile("./graph.exe", args, { encoding: "buffer" });
		return new AttachmentBuilder(result.stdout, { name: `${type}_graph.png` });
	} catch (error) {
		console.error(error);
		return new AttachmentBuilder("./data/static/placeholder.png", { name: "error.png" });
	}
};

export const graphComposite = async (
	datas: [number, ...number[]][][],
	timezoneOffset: number,
	type: CompositeGraphType,
	interval: Interval,
): Promise<AttachmentBuilder> => {
	const args = datas.map((data) =>
		data
			.flatMap((element) => {
				const date = new Date((element[0] + timezoneOffset) * 1000);
				let time;
				if (interval === "hour") {
					time = date.getUTCHours();
				} else {
					time = date.getUTCDate();
				}
				return [time, ...element.slice(1)];
			})
			.map((n) => n.toString())
			.join(" "),
	);
	args.unshift(type);
	try {
		const result = await promiseExecFile("./graph.exe", args, { encoding: "buffer" });
		return new AttachmentBuilder(result.stdout, { name: `${type}_graph.png` });
	} catch (error) {
		console.error(error);
		return new AttachmentBuilder("./data/static/placeholder.png", { name: "error.png" });
	}
};
