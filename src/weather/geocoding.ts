import { CommandError } from "../Errors.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";
import { LOCATION_OPTION_REQUIRED, LOCATION_USAGE } from "./constants.js";
import { getUserLocation } from "./user_location.js";
import { lookUpCoords, lookUpSettlement } from "./API_calls.js";
import { round } from "./util.js";

export class Location {
	/** location name */
	readonly name?: string;
	/** country name, or code if name could not be found */
	readonly country?: string;
	readonly latitude: number;
	readonly longitude: number;
	readonly fromCoords: boolean;
	constructor(
		name: string | undefined,
		country: string | undefined,
		latitude: number,
		longitude: number,
		fromCoords: boolean,
	) {
		this.name = name;
		this.country = country;
		this.latitude = latitude;
		this.longitude = longitude;
		this.fromCoords = fromCoords;
	}
	toString(): string {
		if (this.name !== undefined) {
			return `${this.name}, ${this.country ?? "unknown"}`;
		} else {
			return `unnamed location at ${this.toCoordinateString()}`;
		}
	}
	toCoordinateString(): string {
		return `${this.latitude}, ${this.longitude}`;
	}
	toNameAndCoordinateString(): string {
		if (this.name !== undefined) {
			return `${this.toString()} at ${this.toCoordinateString()}`;
		} else {
			return `${this.toString()}`;
		}
	}
	/**
	 * Parses a user string to get a settlement name from coordinates, or coordinates from a settlement name. If the text is empty, but a user ID and domain are provided, tries to get it from the user location database. Throws CommandError if it fails.
	 */
	static async fromInputOrUser(input: string, user?: UserWithDomain): Promise<Location> {
		if (input === "") {
			return Location.fromUser(user);
		} else {
			return Location.fromInput(input);
		}
	}
	/** Throws a CommandError if the input looks like invalid coordinates or did not look like coordinates and could not be found as a settlement */
	static async fromInput(input: string): Promise<Location> {
		const loc = parseLocation(input);
		if (loc.settlement !== null) {
			const location = await lookUpSettlement(loc.settlement);
			if (!location) throw new CommandError("No such settlement found");
			return new Location(
				location.name,
				location.country,
				location.latitude,
				location.longitude,
				false,
			);
		}
		return this.fromCoordinates(loc.latitude, loc.longitude);
	}
	/** Throws a CommandError if user has no location set or something else goes wrong */
	static async fromUser(user?: UserWithDomain): Promise<Location> {
		if (user === undefined) throw new CommandError();
		const loc = await getUserLocation(user.user, user.domain);
		if (loc === false)
			throw new CommandError(
				"User has no location set, and must provide location on weather commands",
			);
		if (!loc)
			throw new CommandError("User has a location set, but that location appears invalid");
		return new Location(loc.name, loc.country, loc.latitude, loc.longitude, false);
	}
	static async fromCoordinates(latitude: number, longitude: number): Promise<Location> {
		const location = await lookUpCoords(latitude, longitude);
		if (!location) {
			return new Location(undefined, undefined, latitude, longitude, true);
		}
		return new Location(location.name, location.country, latitude, longitude, true);
	}
}

interface LocationBySettlement {
	settlement: string;
	latitude: null;
	longitude: null;
}
interface LocationByCoordinates {
	settlement: null;
	latitude: number;
	longitude: number;
}
export type LocationBySettlementOrCoordinates = LocationBySettlement | LocationByCoordinates;

export interface UserWithDomain {
	user: string;
	domain: string;
}

/**
 * Parses a user string presumed to contain a location. If it looks like coordinates, returns the coordinates, otherwise treats it like a city.
 *
 * Throws CommandError if coordinates are invalid or string is empty.
 */
export const parseLocation = (text: string): LocationBySettlementOrCoordinates => {
	const coordinates = parseCoordinates(text);
	if (!coordinates) {
		return { settlement: text, latitude: null, longitude: null };
	}
	const { latitude, longitude } = coordinates;
	if (latitude < -90 || latitude > 90) throw new CommandError("Latitude ranges from -90 to 90");
	if (longitude < -180 || longitude > 180)
		throw new CommandError("Longitude ranges from -180 to 180");
	return { settlement: null, latitude, longitude };
};

const DIRECTIONS = {
	N: { direction: "latitude", sign: 1 },
	S: { direction: "latitude", sign: -1 },
	E: { direction: "longitude", sign: 1 },
	W: { direction: "longitude", sign: -1 },
} as const;

const parseCoordinates = (text: string): { latitude: number; longitude: number } | undefined => {
	const SIMPLE_REGEX =
		/^([+-]?\s*(?:\d+(?:\.\d+)?|\.\d+))\s*,\s*([+-]?\s*(?:\d+(?:\.\d+)?|\.\d+))$/;
	const simpleMatch = text.match(SIMPLE_REGEX) as null | [string, string, string];
	if (simpleMatch) {
		return { latitude: Number(simpleMatch[1]), longitude: Number(simpleMatch[2]) };
	}
	const FANCIER_REGEX =
		/^(\d{1,3})°(\d{1,2})[\u2032'](\d{1,2})[″"]\s*([NESW])\s*(\d{1,3})°(\d{1,2})[\u2032'](\d{1,2})[″"]\s*([NESW])$/i;
	const fancierMatch = text.match(FANCIER_REGEX) as
		| null
		| [string, string, string, string, string, string, string, string, string];
	if (fancierMatch) {
		const firstDirection = fancierMatch[4].toUpperCase() as keyof typeof DIRECTIONS;
		const secondDirection = fancierMatch[8].toUpperCase() as keyof typeof DIRECTIONS;
		if (DIRECTIONS[firstDirection] === DIRECTIONS[secondDirection]) {
			throw new CommandError("Invalid combination of geographic axis directions");
		}
		const firstValues = fancierMatch.slice(1, 4).map(Number) as [number, number, number];
		const secondValues = fancierMatch.slice(5, 9).map(Number) as [number, number, number];
		const [latitudeValues, latitudeSign, longitudeValues, longitudeSign] =
			DIRECTIONS[firstDirection].direction === "latitude"
				? [
						firstValues,
						DIRECTIONS[firstDirection].sign,
						secondValues,
						DIRECTIONS[secondDirection].sign,
				  ]
				: [
						secondValues,
						DIRECTIONS[secondDirection].sign,
						firstValues,
						DIRECTIONS[firstDirection].sign,
				  ];
		const latitude =
			latitudeSign *
			round(latitudeValues[0] + latitudeValues[1] / 60 + latitudeValues[2] / 60 / 60, 4);
		const longitude =
			longitudeSign *
			round(longitudeValues[0] + longitudeValues[1] / 60 + longitudeValues[2] / 60 / 60, 4);
		return { latitude, longitude };
	}
};

const getCoords = async (locationArg: string): Promise<string> => {
	const location = await Location.fromInput(locationArg);
	if (location.name === undefined) {
		return `Did not find a settlement near ${location.toCoordinateString()}`;
	} else {
		return `${location.toString()} is at ${location.toCoordinateString()}`;
	}
};

const cGetCoords: CommandFunction = async (cCall) => {
	if (cCall.argstring === "") throw new CommandError();
	cCall.reply(await getCoords(cCall.argstring));
};

const slashGetCoords: SlashCommandFunction = (interaction) => {
	const location = interaction.options.getString("location", true);
	return getCoords(location);
};

addCommand({
	name: "coords",
	run: cGetCoords,
	category: "weather",
	usage: LOCATION_USAGE,
	description:
		"Finds the coordinates of the specified settlement, or the settlement at the specified coordinates.",
	slash: {
		run: slashGetCoords,
		options: [LOCATION_OPTION_REQUIRED],
	},
});
