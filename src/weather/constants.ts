import { ApplicationCommandOptionType } from "discord.js";

export const LOCATION_USAGE_WITH_USER_LOCATION = "<[settlement]> or <[latitude]>,<[longitude]>";
export const LOCATION_USAGE = "<settlement> or <latitude>,<longitude>";
// eslint-disable-next-line @typescript-eslint/naming-convention
export const LOCATION_OPTION = {
	name: "location",
	type: ApplicationCommandOptionType.String,
	description:
		"A place name or a set of coordinates. Required if the user has no personal location set.",
} as const;
// eslint-disable-next-line @typescript-eslint/naming-convention
export const LOCATION_OPTION_REQUIRED = {
	name: "location",
	type: ApplicationCommandOptionType.String,
	description: `A place name ("London" or "London, UK") or a set of coordinates ("51.5085, -0.1257")`,
	required: true,
} as const;
