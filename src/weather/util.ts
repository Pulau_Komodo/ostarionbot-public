import { addPattern, buildPatterns, createRoot, findPatterns } from "aho.js";
import { VS16 } from "../characters.js";

/* Takes a number and turns it into an ordinal (1 -> 1st). */
export const toOrdinal = (number: number): string => {
	const string = number.toString();
	const lastChar = string.slice(-1);
	const secondToLastChar = string.slice(-2, -1);
	if (lastChar === "1" && secondToLastChar !== "1") return `${string}st`;
	if (lastChar === "2" && secondToLastChar !== "1") return `${string}nd`;
	if (lastChar === "3" && secondToLastChar !== "1") return `${string}rd`;
	return `${string}th`;
};

/* Takes a fraction and turns it into a percentage (.5 -> 50%), rounded to the nearest percent. This avoids floating point nonsense. */
export const percentage = (number: number): number => {
	return Math.round(number * 100);
};

/* Takes an angle in degrees from 0 to 360 (where 0 is north and 90 is east) and turns it into a direction like "NNE". */
export const degreesToCardinal = (angle: number): string => {
	if (angle < 11.25) {
		return "N";
	} else if (angle < 33.75) {
		return "NNE";
	} else if (angle < 56.25) {
		return "NE";
	} else if (angle < 78.75) {
		return "ENE";
	} else if (angle < 101.25) {
		return "E";
	} else if (angle < 123.75) {
		return "ESE";
	} else if (angle < 146.25) {
		return "SE";
	} else if (angle < 168.75) {
		return "SSE";
	} else if (angle < 191.25) {
		return "S";
	} else if (angle < 213.75) {
		return "SSW";
	} else if (angle < 236.25) {
		return "SW";
	} else if (angle < 258.75) {
		return "WSW";
	} else if (angle < 281.25) {
		return "W";
	} else if (angle < 303.75) {
		return "WNW";
	} else if (angle < 326.25) {
		return "NW";
	} else if (angle < 348.75) {
		return "NNW";
	} else {
		return "N";
	}
};

/* Takes an angle in degrees from 0 to 360 (where 0 is north and 90 is east) and turns it into an emoji like "↗". */
export const degreesToEmoji = (angle: number): string => {
	if (angle < 22.5) {
		return `⬆${VS16}`;
	} else if (angle < 67.5) {
		return `↗${VS16}`;
	} else if (angle < 112.5) {
		return `➡${VS16}`;
	} else if (angle < 157.5) {
		return `↘${VS16}`;
	} else if (angle < 202.5) {
		return `⬇${VS16}`;
	} else if (angle < 247.5) {
		return `↙${VS16}`;
	} else if (angle < 292.5) {
		return `⬅${VS16}`;
	} else if (angle < 337.5) {
		return `↖${VS16}`;
	} else {
		return `⬆${VS16}`;
	}
};

const alertEmojis = {
	fire: "🔥",
	tornado: `🌪${VS16}`,
	flood: "🌊",
	thunder: `⛈${VS16}`,
	blizzard: `🌨${VS16}`,
	storm: "💨",
	snow: `🌨${VS16}`,
	heat: "🥵",
	"high-temperature": "🥵",
	"high temperature": "🥵",
	cold: "🥶",
	"low-temperature": "🥶",
	"low temperature": "🥶",
	rain: `🌧${VS16}`,
} as const;

const root = createRoot();
Object.keys(alertEmojis).forEach((p) => addPattern(root, p));
const built = buildPatterns(root);

/**
 * Try to find an emoji fitting to the alert based on its name. Case insensitive. Returns an empty string if no fitting emoji was found.
 *
 * I am reasonably sure clunky mapping and deduping loses me whatever tiny gains I get by using Aho Corasick.
 */
export const findAlertEmoji = (alert: string): string => {
	alert = alert.toLowerCase();
	const matches = findPatterns(built, alert);
	const emojis = [...matches].map(
		({ pattern }) => alertEmojis[[...pattern].join("") as keyof typeof alertEmojis],
	);
	return [...new Set(emojis)].join("");
};

export const capitalizeFirstChar = (text: string): string => {
	return `${text.slice(0, 1).toUpperCase()}${text.slice(1)}`;
};

export const round = (number: number, precision: number): number => {
	const precisionMultiplier = Math.pow(10, precision);
	return Math.round(number * precisionMultiplier) / precisionMultiplier;
};

export const DAYS_OF_WEEK = {
	0: "Sun",
	1: "Mon",
	2: "Tue",
	3: "Wed",
	4: "Thu",
	5: "Fri",
	6: "Sat",
} as const;
