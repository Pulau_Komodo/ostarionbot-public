import { AttachmentBuilder, TextChannel, ThreadChannel } from "discord.js";
import { NBSP } from "../../characters.js";
import { Network } from "../../common_types.js";
import { getMinutelyWeather } from "../API_calls.js";
import { Location } from "../geocoding.js";
import { CommandFunction, SlashCommandFunction, addCommand } from "../../commands.js";
import { CommandError } from "../../Errors.js";
import { canSendAttachment } from "../../permission checks.js";
import { LOCATION_OPTION, LOCATION_USAGE_WITH_USER_LOCATION } from "../constants.js";
import { MinutelyWeather } from "../API_interfaces.js";
import { graph } from "../graph.js";

/**
 * Generates a chart with amount of precipitation in hours
 */
export const minutelyPrecipitationGraph = async (
	minutelyWeather: MinutelyWeather[],
	timezoneOffset: number,
): Promise<AttachmentBuilder> => {
	return graph(
		formatMinutelyPrecipitationArgs(minutelyWeather),
		timezoneOffset,
		"minutely_precipitation",
		"minute",
	);
};

export const formatMinutelyPrecipitationArgs = (
	minutelyWeather: MinutelyWeather[],
): [number, number][] => {
	return minutelyWeather.map((minute) => [minute.dt, Math.round(minute.precipitation * 100)]);
};

const getMinutelyForecast = async (
	locationArg: string,
	userId: string,
	domain: string,
	network: Network,
	canAttach: boolean,
): Promise<string | { content: string; files: [AttachmentBuilder] }> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getMinutelyWeather(location.latitude, location.longitude);
	const bold = network === "discord" ? "**" : "";
	const escape = network === "discord" ? "\\" : "";

	const minutely = data.minutely?.slice(0, 60);

	if (minutely === undefined || minutely.length === 0) {
		return `No minutely data available for ${location.toString()}.`;
	}

	const sum = minutely.reduce((acc, curr) => acc + curr.precipitation, 0);

	if (sum === 0) {
		return `No precipitation is forecast for ${location.toString()} over the following 60 minutes.`;
	}

	const lead = `Precipitation forecast for ${location.toString()} over the following 60 minutes, in mm/h:`;

	if (canAttach) {
		const graph = await minutelyPrecipitationGraph(minutely, data.timezone_offset);
		return { content: lead, files: [graph] };
	} else {
		const output = minutely
			.map((minutely) => {
				const date = new Date((minutely.dt + data.timezone_offset) * 1000);
				const minute = date.getUTCMinutes().toString();
				return `${bold}${minute}${bold}:${NBSP}${minutely.precipitation}`;
			})
			.join(", ");
		return `${lead} ${output}`;
	}
};

const cMinutelyForecast: CommandFunction = async (cCall) => {
	const canSendAttachment = cCall.network === "discord" ? cCall.canReplyAttachment : false;
	const result = await getMinutelyForecast(
		cCall.argstring,
		cCall.user.id,
		cCall.domain,
		cCall.network,
		canSendAttachment,
	);
	if (typeof result === "string") {
		cCall.reply(result);
	} else if (cCall.network === "discord") {
		cCall.channel.send(result);
	}
};

const slashMinutelyForecast: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const channel = interaction.channel;
	if (!channel || !(channel instanceof TextChannel || channel instanceof ThreadChannel))
		throw new CommandError("Could not get channel");
	const canAttach = canSendAttachment(channel);
	const location = interaction.options.getString("location") ?? "";
	return getMinutelyForecast(
		location,
		interaction.user.id,
		interaction.guildId,
		"discord",
		canAttach,
	);
};

addCommand({
	name: "minutelyforecast",
	run: cMinutelyForecast,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description: "Gets the minutely precipitation forecast for the given location.",
	aliases: ["minutely"],
	slash: {
		run: slashMinutelyForecast,
		options: [LOCATION_OPTION],
	},
});
