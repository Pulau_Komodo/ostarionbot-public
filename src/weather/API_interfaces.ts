/* eslint-disable @typescript-eslint/naming-convention */
// One-call API

export interface WeatherResponse {
	/** latitude */
	lat: number;
	/** longitude */
	lon: number;
	/** timezone name */
	timezone: string;
	/** offset from UTC in seconds */
	timezone_offset: number;
	/** current weather */
	current: CurrentWeather;
	/** minutely forecast for 1 hour */
	minutely?: [MinutelyWeather, ...MinutelyWeather[]];
	/** hourly forecast for 48 hours */
	hourly: [HourlyWeather, ...HourlyWeather[]];
	/** daily forecast for 7 days */
	daily: [DailyWeather, ...DailyWeather[]];
	/** national weather alert data from major national weather warning systems */
	alerts?: Alert[];
}

type BaseWeather = Dt & {
	/** atmospheric pressure at sea level in hPa */
	pressure: number;
	/** relative humidity in % (0-100) */
	humidity: number;
	/** atmospheric temperature in °C below which dew can form, at current pressure and humidity */
	dew_point: number;
	/** UV index */
	uvi: number;
	/** cloudiness in % (0-100) */
	clouds: number;
	/** wind speed in m/s */
	wind_speed: number;
	/** wind direction in degrees (N is 0, E is 90) */
	wind_deg: number;
	/** wind gust speed in m/s */
	wind_gust?: number;
	weather: Weather[];
};

export interface Weather {
	/** weather condition id https://openweathermap.org/weather-conditions#Weather-Condition-Codes-2 */
	id: number;
	/** group of weather parameters (?) */
	main: string;
	/** weather condition description */
	description: string;
	/** weather icon id */
	icon: string;
}

interface CurrentHourlyShared {
	/** temperature in °C adjusted for human perception */
	feels_like: number;
	/** average visibility distance in m */
	visibility: number;
}

interface CurrentDailyShared {
	/** sunrise time */
	sunrise: number;
	/** sunset time */
	sunset: number;
}

interface Pop {
	/** probability of precipitation (0-1) */
	pop: number;
}

interface Dt {
	/** time of the forecast data, UNIX timestamp in seconds */
	dt: number;
}

type CurrentWeather = BaseWeather &
	CurrentHourlyShared &
	CurrentDailyShared & {
		/** temperature in °C */
		temp: number;
		rain?: {
			/** rainfall over the last hour, in mm */
			"1h"?: number;
		};
		snow?: {
			/** snowfall over the last hour, in mm (after melting?) */
			"1h"?: number;
		};
	};

export type MinutelyWeather = Dt & {
	/** precipitation in mm/h */
	precipitation: number;
};

export type HourlyWeather = BaseWeather &
	CurrentHourlyShared &
	Pop & {
		/** temperature in °C */
		temp: number;
		rain?: {
			/** rainfall over the (last? surely not) hour, in mm */
			"1h"?: number;
		};
		snow?: {
			/** snowfall over the (last? surely not) hour, in mm (after melting?) */
			"1h"?: number;
		};
	};

export type DailyWeather = BaseWeather &
	CurrentDailyShared &
	Pop & {
		/** time of moonrise */
		moonrise: number;
		/** time of moonset */
		moonset: number;
		/** moon phase from 0 to 1, where 0 and 1 are new moon, .25 is first quarter etc. */
		moon_phase: number;
		temp: {
			/** morning temperature in °C */
			morn: number;
			/** day temperature in °C */
			day: number;
			/** evening temperature in °C */
			eve: number;
			/** night temperature in °C */
			night: number;
			/** minimum temperature in °C */
			min: number;
			/** maximum temperature in °C */
			max: number;
		};
		feels_like: {
			/** morning temperature in °C adjusted for human perception */
			morn: number;
			/** day temperature in °C adjusted for human perception */
			day: number;
			/** evening temperature in °C adjusted for human perception */
			eve: number;
			/** night temperature in °C adjusted for human perception */
			night: number;
		};
		/** precipitation (or rainfall?) in mm */
		rain?: number;
		/** snowfall in mm */
		snow?: number;
	};

export interface Alert {
	/** name of the source https://openweathermap.org/api/one-call-api#listsource */
	sender_name: string;
	/** alert event name */
	event: string;
	/** time the alert starts */
	start: number;
	/** time the alert ends */
	end: number;
	/** description of the alert (can be quite long) */
	description: string;
}

// Geocoding API

export type GeocodingResponse = {
	/** name of the found location */
	name: string;
	/** local name strings on language code keys */
	local_names: unknown;
	/** latitude */
	lat: number;
	/** longitude */
	lon: number;
	/** country code of the location */
	country: string;
	/** state code of the location (US-only) */
	state?: string;
}[];

// Air pollution API

export interface AirPollutionResponse {
	/** "Coordinates from the specified location (latitude, longitude)"" */
	coord: [number, number];
	list: {
		/** "Date and time, Unix, UTC" */
		dt: number;
		main: {
			/** "Air Quality Index. Possible values: 1, 2, 3, 4, 5. Where 1 = Good, 2 = Fair, 3 = Moderate, 4 = Poor, 5 = Very Poor."" */
			aqi: number;
		};
		/** concentrations of pollutants, in μg/m3 */
		components: {
			/** carbon monoxide */
			co: number;
			/** nitrogen monoxide */
			no: number;
			/** nitrogen dioxide */
			no2: number;
			/** ozone */
			o3: number;
			/** sulphur dioxide */
			so2: number;
			/** fine particulate matter */
			pm2_5: number;
			/** coarse particulate matter */
			pm10: number;
			/** ammonia */
			nh3: number;
		};
	}[];
}
