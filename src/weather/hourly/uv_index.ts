import { AttachmentBuilder, TextChannel, ThreadChannel } from "discord.js";
import { VS16 } from "../../characters.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../../commands.js";
import { Network } from "../../common_types.js";
import { CommandError } from "../../Errors.js";
import { canSendAttachment } from "../../permission checks.js";
import { getHourlyWeather } from "../API_calls.js";
import { HourlyWeather } from "../API_interfaces.js";
import { LOCATION_OPTION, LOCATION_USAGE_WITH_USER_LOCATION } from "../constants.js";
import { Location } from "../geocoding.js";
import { graph } from "../graph.js";

/**
 * Generates a chart with UVI over hours
 */
export const hourlyUviGraph = async (
	hourlyWeather: HourlyWeather[],
	timezoneOffset: number,
): Promise<AttachmentBuilder> => {
	return graph(formatHourlyUviArgs(hourlyWeather), timezoneOffset, "hourly_uvi", "hour");
};

export const formatHourlyUviArgs = (hourlyWeather: HourlyWeather[]): [number, number][] => {
	return hourlyWeather.map((hour) => [hour.dt, Math.round(hour.uvi * 100)]);
};

export const getUviEmoji = (uvi: number, escape: string): string => {
	return uvi >= 11 ? `☠${VS16}` : uvi >= 8 ? `${escape}🛑` : uvi >= 6 ? `${escape}⚠${VS16}` : "";
};

const getHourlyUv = async (
	locationArg: string,
	userId: string,
	domain: string,
	network: Network,
	canAttach: boolean,
): Promise<string | { content: string; files: [AttachmentBuilder] }> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getHourlyWeather(location.latitude, location.longitude);
	const { hourly } = data;
	const processedHourly: {
		uvi: number;
		startTime: string;
		endTime: string | undefined;
	}[] = [];
	for (const hour of hourly.slice(0, 25)) {
		const time = new Date((hour.dt + data.timezone_offset) * 1000)
			.getUTCHours()
			.toString()
			.padStart(2, "0");
		if (processedHourly.length > 0) {
			const previousProcessed = processedHourly[processedHourly.length - 1]!;
			if (hour.uvi === 0 && previousProcessed.uvi === 0) {
				previousProcessed.endTime = time;
			} else {
				processedHourly.push({
					uvi: hour.uvi,
					startTime: time,
					endTime: undefined,
				});
			}
		} else {
			processedHourly.push({
				uvi: hour.uvi,
				startTime: time,
				endTime: undefined,
			});
		}
	}
	const text = `UV index in ${location.toString()} for the next 24 hours: ${processedHourly
		.map((hour) => {
			const time =
				hour.endTime !== undefined ? `${hour.startTime}–${hour.endTime}` : hour.startTime;
			const icon = getUviEmoji(hour.uvi, "");
			const bold = network === "discord" ? "**" : "";
			const uvi = hour.uvi === 0 ? "🌃" : `${bold}${Math.round(hour.uvi).toString()}${bold}`;
			return `${time}: ${icon}${uvi}`;
		})
		.join(", ")}`;

	if (canAttach) {
		const attachment = await hourlyUviGraph(data.hourly, data.timezone_offset);
		return { content: text, files: [attachment] };
	} else {
		return text;
	}
};

const cGetHourlyUv: CommandFunction = async (cCall) => {
	const canSendAttachment = cCall.network === "discord" ? cCall.canReplyAttachment : false;
	const result = await getHourlyUv(
		cCall.argstring,
		cCall.user.id,
		cCall.domain,
		cCall.network,
		canSendAttachment,
	);
	if (typeof result === "string") {
		cCall.reply(result);
	} else if (cCall.network === "discord") {
		cCall.replyWithAddition(result.content, result.files[0]);
	}
};

const slashGetHourlyUv: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const channel = interaction.channel;
	if (!channel || !(channel instanceof TextChannel || channel instanceof ThreadChannel))
		throw new CommandError("Could not get channel");
	const canAttach = canSendAttachment(channel);
	const location = interaction.options.getString("location") ?? "";
	return getHourlyUv(location, interaction.user.id, interaction.guildId, "discord", canAttach);
};

addCommand({
	name: "uv",
	run: cGetHourlyUv,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description: "Gets the hourly UV index for the next 24 hours in the given location.",
	aliases: ["uvi"],
	slash: {
		run: slashGetHourlyUv,
		options: [LOCATION_OPTION],
	},
});
