import { AttachmentBuilder, TextChannel, ThreadChannel } from "discord.js";
import { NBSP, VS16 } from "../../characters.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../../commands.js";
import { Network } from "../../common_types.js";
import { CommandError } from "../../Errors.js";
import { canSendAttachment } from "../../permission checks.js";
import { getHourlyWeather } from "../API_calls.js";
import { HourlyWeather } from "../API_interfaces.js";
import { LOCATION_OPTION, LOCATION_USAGE_WITH_USER_LOCATION } from "../constants.js";
import { Location } from "../geocoding.js";
import { graph } from "../graph.js";
import { round } from "../util.js";

/**
 * Generates a chart with temperature, feels-like temperature and wet bulb temperature over hours
 */
export const hourlyTempGraph = async (
	hourlyWeather: HourlyWeather[],
	timezoneOffset: number,
): Promise<AttachmentBuilder> => {
	return graph(
		formatHourlyTempArgs(hourlyWeather),
		timezoneOffset,
		"hourly_temp",
		"hour",
	);
};

export const formatHourlyTempArgs = (hourlyWeather: HourlyWeather[]): [number, number, number, number][] => {
	return hourlyWeather.map((hour) => [
		hour.dt,
		Math.round(hour.temp * 100),
		Math.round(hour.feels_like * 100),
		Math.round(hour.humidity),
	]);
};

/**
 * Calculates wet bulb temperature in °C given dry bulb temperature in °C and relative humidity * 100 (0-100).
 *
 * Supposedly this is only accurate for temperatures between -20 °C and 50 °C, and relative humidities between .05 and .99 (5 and 99).
 */
const wetBulbTemp = (temp: number, humidity: number): number => {
	return (
		temp * Math.atan(0.151977 * Math.sqrt(humidity + 8.313659)) +
		Math.atan(temp + humidity) -
		Math.atan(humidity - 1.676331) +
		0.00391838 * Math.pow(humidity, 1.5) * Math.atan(0.023101 * humidity) -
		4.686035
	);
};

const getHourlyTemp = async (
	locationArg: string,
	userId: string,
	domain: string,
	network: Network,
	canAttach: boolean,
): Promise<string | { content: string; files: [AttachmentBuilder] }> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getHourlyWeather(location.latitude, location.longitude);

	const lead = `Temperature forecast for ${location.toString()} over the following`;
	if (canAttach) {
		const attachment = await hourlyTempGraph(data.hourly, data.timezone_offset);
		return {
			content: `${lead} 48 hours: 🟥 temperature, 🟩 feels-like, 🟦 wet-bulb`,
			files: [attachment],
		};
	} else {
		const bold = network === "discord" ? "**" : "";
		const escape = network === "discord" ? "\\" : "";

		const output = data.hourly
			.slice(0, 25)
			.map((hourly) => {
				const date = new Date((hourly.dt + data.timezone_offset) * 1000);
				const hour = date.getUTCHours().toString();
				return `${bold}${hour}${bold}:${NBSP}${round(hourly.temp, 1)}°C${NBSP}${round(
					hourly.feels_like,
					1,
				)}${escape}👈°C${NBSP}${round(
					wetBulbTemp(hourly.temp, hourly.humidity),
					1,
				)}${escape}💧${escape}🌡${VS16}°C`;
			})
			.join(", ");
		return `${lead} 24 hours: ${output}`;
	}
};

const cHourlyTemp: CommandFunction = async (cCall) => {
	const canSendAttachment = cCall.network === "discord" ? cCall.canReplyAttachment : false;
	const result = await getHourlyTemp(
		cCall.argstring,
		cCall.user.id,
		cCall.domain,
		cCall.network,
		canSendAttachment,
	);
	if (typeof result === "string") {
		cCall.reply(result);
	} else if (cCall.network === "discord") {
		cCall.replyWithAddition(result.content, result.files[0]);
	}
};

const slashHourlyTemp: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const channel = interaction.channel;
	if (!channel || !(channel instanceof TextChannel || channel instanceof ThreadChannel))
		throw new CommandError("Could not get channel");
	const canAttach = canSendAttachment(channel);
	const location = interaction.options.getString("location") ?? "";
	return getHourlyTemp(location, interaction.user.id, interaction.guildId, "discord", canAttach);
};

addCommand({
	name: "temperatureforecast",
	run: cHourlyTemp,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description: "Gets the hourly temperature forecast for the given location.",
	aliases: ["temperature", "temps", "temp"],
	slash: {
		run: slashHourlyTemp,
		options: [LOCATION_OPTION],
	},
});
