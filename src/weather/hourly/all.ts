import { AttachmentBuilder, TextChannel, ThreadChannel } from "discord.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../../commands.js";
import { Network } from "../../common_types.js";
import { CommandError } from "../../Errors.js";
import { canSendAttachment } from "../../permission checks.js";
import { LOCATION_OPTION, LOCATION_USAGE_WITH_USER_LOCATION } from "../constants.js";
import { Location } from "../geocoding.js";
import { getHourlyWeather } from "../API_calls.js";
import { formatHourlyTempArgs } from "./temperature.js";
import { formatHourlyWindArgs } from "./wind.js";
import { formatHourlyUviArgs } from "./uv_index.js";
import { formatHourlyPopArgs, formatHourlyPrecipitationArgs } from "./rain.js";
import { graphComposite } from "../graph.js";

const getHourlyAll = async (
	locationArg: string,
	userId: string,
	domain: string,
	network: Network,
	canAttach: boolean,
): Promise<string | { content: string; files: AttachmentBuilder[] }> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getHourlyWeather(location.latitude, location.longitude);

	if (!canAttach) {
		throw new CommandError(
			"This channel does not allow embeds and this command has no no-embed fallback",
		);
	}

	const compositeGraph = await graphComposite([
		formatHourlyTempArgs(data.hourly),
		formatHourlyPopArgs(data.hourly),
		formatHourlyPrecipitationArgs(data.hourly),
		formatHourlyWindArgs(data.hourly),
		formatHourlyUviArgs(data.hourly),
	], data.timezone_offset, "hourly_composite", "hour");

	return {
		content: `Temperature (🟥temp, 🟩feels-like, 🟦wet-bulb), probability of precipitation, amount of precipitation (🟦rain, ⬜snow), wind and UVI forecasts for ${location.toString()} over the following 48 hours:`,
		files: [compositeGraph],
	};
};

const cHourlyAll: CommandFunction = async (cCall) => {
	const canSendAttachment = cCall.network === "discord" ? cCall.canReplyAttachment : false;
	const result = await getHourlyAll(
		cCall.argstring,
		cCall.user.id,
		cCall.domain,
		cCall.network,
		canSendAttachment,
	);
	if (typeof result === "string") {
		cCall.reply(result);
	} else if (cCall.network === "discord") {
		await cCall.channel.send(result);
	}
};

const slashHourlyAll: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const channel = interaction.channel;
	if (!channel || !(channel instanceof TextChannel || channel instanceof ThreadChannel))
		throw new CommandError("Could not get channel");
	const canAttach = canSendAttachment(channel);
	const location = interaction.options.getString("location") ?? "";
	return getHourlyAll(location, interaction.user.id, interaction.guildId, "discord", canAttach);
};

addCommand({
	name: "hourlyforecast",
	run: cHourlyAll,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description: "Gets the hourly forecast for the given location.",
	aliases: [],
	slash: {
		run: slashHourlyAll,
		options: [LOCATION_OPTION],
	},
});
