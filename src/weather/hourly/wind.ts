import { AttachmentBuilder, TextChannel, ThreadChannel } from "discord.js";
import { NBSP } from "../../characters.js";
import { CommandFunction, SlashCommandFunction, addCommand } from "../../commands.js";
import { Network } from "../../common_types.js";
import { CommandError } from "../../Errors.js";
import { canSendAttachment } from "../../permission checks.js";
import { getHourlyWeather } from "../API_calls.js";
import { HourlyWeather } from "../API_interfaces.js";
import { LOCATION_USAGE_WITH_USER_LOCATION, LOCATION_OPTION } from "../constants.js";
import { Location } from "../geocoding.js";
import { graph } from "../graph.js";
import { degreesToCardinal, round } from "../util.js";

/**
 * Generates a chart with wind speed and wind gust speed over hours
 */
export const hourlyWindGraph = async (
	hourlyWeather: HourlyWeather[],
	timezoneOffset: number,
): Promise<AttachmentBuilder> => {
	return graph(formatHourlyWindArgs(hourlyWeather), timezoneOffset, "hourly_wind", "hour");
};

export const formatHourlyWindArgs = (
	hourlyWeather: HourlyWeather[],
): [number, number, number, number][] => {
	return hourlyWeather.map((hour) => [
		hour.dt,
		Math.round(hour.wind_speed * 100),
		Math.round((hour.wind_gust ?? hour.wind_speed) * 100),
		Math.round(hour.wind_deg),
	]);
};

/**
 * Like "5m/s NNE (10m/s 💨)"
 */
const describeWind = (
	speed: number,
	angle: number,
	gust: number | undefined,
	escape: string,
): string => {
	const gusts = gust !== undefined ? `${NBSP}(${round(gust, 1)}m/s${NBSP}${escape}💨)` : "";
	if (speed === 0) {
		return `no${NBSP}${gusts}`;
	} else {
		return `${round(speed, 1)}m/s${NBSP}${degreesToCardinal(angle)}${gusts}`;
	}
};

const getWindForecast = async (
	locationArg: string,
	userId: string,
	domain: string,
	network: Network,
	canAttach: boolean,
): Promise<string | { content: string; files: [AttachmentBuilder] }> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getHourlyWeather(location.latitude, location.longitude);
	const bold = network === "discord" ? "**" : "";
	const escape = network === "discord" ? "\\" : "";

	const lead = `Wind forecast for ${location.toString()} over the following`;
	if (canAttach) {
		const attachment = await hourlyWindGraph(data.hourly, data.timezone_offset);
		return { content: `${lead} 48 hours, in m/s:`, files: [attachment] };
	} else {
		const output = data.hourly
			.slice(0, 25)
			.map((hourly) => {
				const date = new Date((hourly.dt + data.timezone_offset) * 1000);
				const hour = date.getUTCHours().toString();
				return `${bold}${hour}${bold}:${NBSP}${describeWind(
					hourly.wind_speed,
					hourly.wind_deg,
					hourly.wind_gust,
					escape,
				)}`;
			})
			.join(", ");
		return `${lead} 24 hours: ${output}`;
	}
};

const cWindForecast: CommandFunction = async (cCall) => {
	const canSendAttachment = cCall.network === "discord" ? cCall.canReplyAttachment : false;
	const result = await getWindForecast(
		cCall.argstring,
		cCall.user.id,
		cCall.domain,
		cCall.network,
		canSendAttachment,
	);
	if (typeof result === "string") {
		cCall.reply(result);
	} else if (cCall.network === "discord") {
		cCall.replyWithAddition(result.content, result.files[0]);
	}
};

const slashWindForecast: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const channel = interaction.channel;
	if (!channel || !(channel instanceof TextChannel || channel instanceof ThreadChannel))
		throw new CommandError("Could not get channel");
	const canAttach = canSendAttachment(channel);
	const location = interaction.options.getString("location") ?? "";
	return getWindForecast(
		location,
		interaction.user.id,
		interaction.guildId,
		"discord",
		canAttach,
	);
};

addCommand({
	name: "windforecast",
	run: cWindForecast,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description: "Gets the hourly wind forecast for the given location.",
	aliases: ["wind"],
	slash: {
		run: slashWindForecast,
		options: [LOCATION_OPTION],
	},
});
