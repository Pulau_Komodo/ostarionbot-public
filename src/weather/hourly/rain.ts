import { AttachmentBuilder, TextChannel, ThreadChannel } from "discord.js";
import { NBSP, VS16 } from "../../characters.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../../commands.js";
import { Network } from "../../common_types.js";
import { CommandError } from "../../Errors.js";
import { canSendAttachment } from "../../permission checks.js";
import { getHourlyWeather } from "../API_calls.js";
import { HourlyWeather } from "../API_interfaces.js";
import { LOCATION_OPTION, LOCATION_USAGE_WITH_USER_LOCATION } from "../constants.js";
import { Location } from "../geocoding.js";
import { graph } from "../graph.js";
import { percentage } from "../util.js";

/**
 * Generates a chart with probability of rain and snow over hours
 */
export const hourlyPopGraph = async (
	hourlyWeather: HourlyWeather[],
	timezoneOffset: number,
): Promise<AttachmentBuilder> => {
	return graph(
		formatHourlyPopArgs(hourlyWeather),
		timezoneOffset,
		"hourly_pop",
		"hour",
	);
};

export const formatHourlyPopArgs = (hourlyWeather: HourlyWeather[]): [number, number][] => {
	return hourlyWeather.map((hour) => [hour.dt, Math.round(hour.pop * 100)]);
};

/**
 * Generates a chart with probability of rain and snow over hours
 */
export const hourlyPrecipitationGraph = async (
	hourlyWeather: HourlyWeather[],
	timezoneOffset: number,
): Promise<AttachmentBuilder> => {
	return graph(
		formatHourlyPrecipitationArgs(hourlyWeather),
		timezoneOffset,
		"hourly_precipitation",
		"hour",
	);
};

export const formatHourlyPrecipitationArgs = (hourlyWeather: HourlyWeather[]): [number, number, number][] => {
	return hourlyWeather.map((hour) => [
		hour.dt,
		Math.round((hour?.rain?.["1h"] ?? 0) * 100),
		Math.round((hour?.snow?.["1h"] ?? 0) * 100),
	]);
}

/** with leading space if any content */
const describeRainAndSnow = (hour: HourlyWeather, escape: string): string => {
	const rain = Math.round(hour.rain?.["1h"] ?? 0);
	const snow = Math.round(hour.snow?.["1h"] ?? 0);
	if (rain > 0 && snow > 0) {
		return `${NBSP}(${rain}mm${NBSP}${escape}🌧${VS16}${NBSP}${snow}mm${NBSP}${escape}🌨${VS16})`;
	} else if (rain > 0) {
		return `${NBSP}(${rain}mm${NBSP}${escape}🌧${VS16})`;
	} else if (snow > 0) {
		return `${NBSP}(${snow}mm${NBSP}${escape}🌨${VS16})`;
	} else {
		return "";
	}
};

const getRainForecast = async (
	locationArg: string,
	userId: string,
	domain: string,
	network: Network,
	canAttach: boolean,
): Promise<string | { content: string; files: AttachmentBuilder[] }> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getHourlyWeather(location.latitude, location.longitude);

	const lead = `Rain forecast for ${location.toString()} over the following`;
	if (canAttach) {
		const chanceAttachment = await hourlyPopGraph(data.hourly, data.timezone_offset);
		const amountAttachment = await hourlyPrecipitationGraph(data.hourly, data.timezone_offset);
		return {
			content: `${lead} 48 hours: 🟦 rain, ⬜ snow`,
			files: [chanceAttachment, amountAttachment],
		};
	} else {
		const bold = network === "discord" ? "**" : "";
		const escape = network === "discord" ? "\\" : "";

		const output = data.hourly
			.slice(0, 25)
			.map((hourly) => {
				const date = new Date((hourly.dt + data.timezone_offset) * 1000);
				const hour = date.getUTCHours().toString();
				return `${bold}${hour}${bold}:${NBSP}${percentage(
					hourly.pop,
				)}%${describeRainAndSnow(hourly, escape)}`;
			})
			.join(", ");
		return `${lead} 24 hours: ${output}`;
	}
};

const cRainForecast: CommandFunction = async (cCall) => {
	const canSendAttachment = cCall.network === "discord" ? cCall.canReplyAttachment : false;
	const result = await getRainForecast(
		cCall.argstring,
		cCall.user.id,
		cCall.domain,
		cCall.network,
		canSendAttachment,
	);
	if (typeof result === "string") {
		cCall.reply(result);
	} else if (cCall.network === "discord") {
		cCall.channel.send(result);
	}
};

const slashRainForecast: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const channel = interaction.channel;
	if (!channel || !(channel instanceof TextChannel || channel instanceof ThreadChannel))
		throw new CommandError("Could not get channel");
	const canAttach = canSendAttachment(channel);
	const location = interaction.options.getString("location") ?? "";
	return getRainForecast(
		location,
		interaction.user.id,
		interaction.guildId,
		"discord",
		canAttach,
	);
};

addCommand({
	name: "rainforecast",
	run: cRainForecast,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description: "Gets the hourly rain forecast for the given location.",
	aliases: ["rain"],
	slash: {
		run: slashRainForecast,
		options: [LOCATION_OPTION],
	},
});
