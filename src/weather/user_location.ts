import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";
import { db } from "../db.js";
import { CommandError } from "../Errors.js";
import { LocationApiData, lookUpCoords, lookUpSettlement } from "./API_calls.js";
import { LOCATION_OPTION_REQUIRED, LOCATION_USAGE } from "./constants.js";
import { Location, LocationBySettlementOrCoordinates, parseLocation } from "./geocoding.js";

const queryUserLocation = (
	user: string,
	domain: string,
): LocationBySettlementOrCoordinates | undefined => {
	const query = `
		SELECT settlement, latitude, longitude
		FROM user_locations
		WHERE user = $user AND domain = $domain
	`;
	const result = db.prepare(query).get({ user, domain }) as
		| LocationBySettlementOrCoordinates
		| undefined;
	return result;
};

const querySetUserLocation = (
	user: string,
	domain: string,
	location: LocationBySettlementOrCoordinates,
) => {
	const qSetCoordinates = `
		INSERT INTO user_locations(user, domain, latitude, longitude)
		VALUES ($user, $domain, $latitude, $longitude)
	`;
	const qSetSettlement = `
		INSERT INTO user_locations(user, domain, settlement)
		VALUES ($user, $domain, $settlement)
	`;
	if (location.settlement !== null) {
		db.prepare(qSetSettlement).run({
			user,
			domain,
			settlement: location.settlement,
		});
	} else {
		db.prepare(qSetCoordinates).run({
			user,
			domain,
			latitude: location.latitude,
			longitude: location.longitude,
		});
	}
};

const queryUnsetUserLocation = (user: string, domain: string): boolean => {
	const qUnsetCoordinates = `
		DELETE FROM user_locations
		WHERE user = $user AND domain = $domain
	`;
	const result = db.prepare(qUnsetCoordinates).run({ user, domain });
	return result.changes > 0;
};

/** Returns Location on success, false on nothing set, and undefined on something set but couldn't look it up */
export const getUserLocation = async (
	user: string,
	domain: string,
): Promise<LocationApiData | false | undefined> => {
	const result = queryUserLocation(user, domain);
	if (!result) return false;
	if (result.settlement !== null) {
		console.log(result.settlement);
		return lookUpSettlement(result.settlement);
	} else {
		return lookUpCoords(result.latitude, result.longitude);
	}
};

const setLocation = async (
	locationArg: string,
	userId: string,
	domain: string,
): Promise<string> => {
	const location = await Location.fromInput(locationArg);
	if (location.fromCoords) {
		querySetUserLocation(userId, domain, {
			settlement: null,
			latitude: location.latitude,
			longitude: location.longitude,
		});
		return `User location set to ${location.toCoordinateString()} (${location.toString()})`;
	} else {
		querySetUserLocation(userId, domain, {
			settlement: locationArg,
			latitude: null,
			longitude: null,
		});
		return `User location set to ${locationArg} (${location.toNameAndCoordinateString()})`;
	}
};

const cSetLocation: CommandFunction = async (cCall) => {
	cCall.reply(await setLocation(cCall.argstring, cCall.user.id, cCall.domain));
};

const slashSetLocation: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const location = interaction.options.getString("location", true);
	return setLocation(location, interaction.user.id, interaction.guildId);
};

const unsetLocation = async (userId: string, domain: string): Promise<string> => {
	const wasDeleted = queryUnsetUserLocation(userId, domain);
	if (wasDeleted) {
		return "Successfully unset the user's location.";
	} else {
		return "User had no location set.";
	}
};

const cUnsetLocation: CommandFunction = async (cCall) => {
	cCall.reply(await unsetLocation(cCall.user.id, cCall.domain));
};

const slashUnsetLocation: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	return unsetLocation(interaction.user.id, interaction.guildId);
};

addCommand({
	name: "setlocation",
	run: cSetLocation,
	category: "weather",
	usage: LOCATION_USAGE,
	description: "Sets the user's default location for weather commands in the current domain.",
	slash: {
		run: slashSetLocation,
		options: [LOCATION_OPTION_REQUIRED],
	},
});

addCommand({
	name: "unsetlocation",
	run: cUnsetLocation,
	category: "weather",
	description:
		"Unsets the user's stored default location for weather commands in the current domain.",
	slash: {
		run: slashUnsetLocation,
	},
});
