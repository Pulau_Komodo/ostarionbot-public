import { addCommand, CommandFunction, SlashCommandFunction } from "../../commands.js";
import { Network } from "../../common_types.js";
import { CommandError } from "../../Errors.js";
import { getDailyWeather } from "../API_calls.js";
import { LOCATION_OPTION, LOCATION_USAGE_WITH_USER_LOCATION } from "../constants.js";
import { Location } from "../geocoding.js";
import { DAYS_OF_WEEK, percentage, toOrdinal } from "../util.js";
import { AttachmentBuilder, TextChannel, ThreadChannel } from "discord.js";
import { DailyWeather } from "../API_interfaces.js";
import { canSendAttachment } from "../../permission checks.js";
import { graph } from "../graph.js";

/**
 * Generates a chart with minimum and maximum temperature for a given number of days
 */
export const dailyTemperatureGraph = async (
	dailyWeather: DailyWeather[],
	timezoneOffset: number,
): Promise<AttachmentBuilder> => {
	return graph(
		dailyWeather.map((day) => [
			day.dt,
			Math.round(day.temp.min * 100),
			Math.round(day.temp.max * 100),
		]),
		timezoneOffset,
		"daily_temp",
		"day",
	);
};

const getDailyForecast = async (
	locationArg: string,
	userId: string,
	domain: string,
	network: Network,
	canAttach: boolean,
): Promise<string | { content: string; files: [AttachmentBuilder] }> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getDailyWeather(location.latitude, location.longitude);
	const bold = network === "discord" ? "**" : "";
	const escape = network === "discord" ? "\\" : "";

	const output = data.daily
		.map((day) => {
			const date_obj = new Date((day.dt + data.timezone_offset) * 1000);
			const date = date_obj.getUTCDate();
			const day_of_week = DAYS_OF_WEEK[date_obj.getUTCDay() as 0 | 1 | 2 | 3 | 4 | 5 | 6];
			return `${bold}${day_of_week} ${toOrdinal(date)}${bold}:\u00A0${Math.round(
				day.temp.min,
			)}–\u200D${Math.round(day.temp.max)}°C,\u00A0${
				day.humidity
			}%\u00A0${escape}💧\u200D,\u00A0${percentage(day.pop)}%\u00A0🌨\u200D🌧\u00A0chance`;
		})
		.join(", \n");

	const text = `Weather forecast of ${location.toString()}: \n${output}`;
	if (canAttach) {
		const attachment = await dailyTemperatureGraph(data.daily, data.timezone_offset);
		return { content: text, files: [attachment] };
	} else {
		return text;
	}
};

const cDailyForecast: CommandFunction = async (cCall) => {
	const canSendAttachment = cCall.network === "discord" ? cCall.canReplyAttachment : false;
	const result = await getDailyForecast(
		cCall.argstring,
		cCall.user.id,
		cCall.domain,
		cCall.network,
		canSendAttachment,
	);
	if (typeof result === "string") {
		cCall.reply(result);
	} else if (cCall.network === "discord") {
		cCall.replyWithAddition(result.content, result.files[0]);
	}
};

const slashDailyForecast: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const location = interaction.options.getString("location") ?? "";
	const channel = interaction.channel;
	if (!channel || !(channel instanceof TextChannel || channel instanceof ThreadChannel))
		throw new CommandError("Could not get channel");
	const canAttach = canSendAttachment(channel);
	return getDailyForecast(
		location,
		interaction.user.id,
		interaction.guildId,
		"discord",
		canAttach,
	);
};

addCommand({
	name: "dailyforecast",
	run: cDailyForecast,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description: "Gets the 7-day weather forecast at the given location.",
	aliases: ["forecast"],
	slash: {
		run: slashDailyForecast,
		options: [LOCATION_OPTION],
	},
});
