import { stringifyInterval } from "interval-conversions";
import { addCommand, CommandFunction, SlashCommandFunction } from "../../commands.js";
import { CommandError } from "../../Errors.js";
import { getCurrentAndDailyWeather } from "../API_calls.js";
import { LOCATION_OPTION, LOCATION_USAGE_WITH_USER_LOCATION } from "../constants.js";
import { Location } from "../geocoding.js";

const describeAlertDates = (start: number, end: number): string => {
	const startDate = new Date(start * 1000);
	const startText = `${startDate.getUTCFullYear()}-${(startDate.getUTCMonth() + 1)
		.toString()
		.padStart(2, "0")}-${startDate.getUTCDate().toString().padStart(2, "0")} ${startDate
		.getUTCHours()
		.toString()
		.padStart(2, "0")}:${startDate.getUTCMinutes().toString().padStart(2, "0")}:${startDate
		.getUTCSeconds()
		.toString()
		.padStart(2, "0")}`;
	const interval = stringifyInterval((end - start) * 1000, {
		startDate,
		thresholds: { seconds: true },
	});
	return `${startText}, lasting ${interval}`;
};

/** Crops and flattens */
const processDescription = (text: string, maxLength: number): string => {
	text = text
		.trim()
		.replace(/###/g, "")
		.replace(/(?:\r\n?|\n)+/g, " - ");

	if (text.length <= maxLength - 2) return `"${text}"`;

	const ending = ` (${text.length.toString()} characters)`;
	return `"${text.slice(0, maxLength - 2 - 1 - ending.length)}…"${ending}`;
};

const getAlerts = async (locationArg: string, userId: string, domain: string): Promise<string> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getCurrentAndDailyWeather(location.latitude, location.longitude);
	const { alerts } = data;
	if (alerts === undefined || alerts.length === 0) {
		return `No alerts for ${location.toString()}.`;
	}
	const alertText = alerts
		.map((alert) => {
			return `**${alert.event}**: ${processDescription(alert.description, 200)} issued by ${
				alert.sender_name
			} on ${describeAlertDates(alert.start, alert.end)}.`;
		})
		.join("\n");
	const s = alerts.length === 1 ? "" : "s";
	return `${alerts.length} alert${s} for ${location.toString()}: ${alertText}`;
};

const cAlerts: CommandFunction = async (cCall) => {
	cCall.reply(await getAlerts(cCall.argstring, cCall.user.id, cCall.domain));
};

const slashAlerts: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const location = interaction.options.getString("location") ?? "";
	return getAlerts(location, interaction.user.id, interaction.guildId);
};

addCommand({
	name: "weatheralerts",
	run: cAlerts,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description: "Gets any weather alerts issued for the given location.",
	aliases: ["weatheralert", "alerts", "alert"],
	slash: {
		run: slashAlerts,
		options: [LOCATION_OPTION],
	},
});
