import { VS16 } from "../../characters.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../../commands.js";
import { Network } from "../../common_types.js";
import { CommandError } from "../../Errors.js";
import { isNonEmptyArray } from "../../type_guards.js";
import { getAirPollution } from "../API_calls.js";
import { LOCATION_OPTION, LOCATION_USAGE_WITH_USER_LOCATION } from "../constants.js";
import { Location } from "../geocoding.js";
import { capitalizeFirstChar } from "../util.js";

type Pollutant = "no2" | "o3" | "pm10" | "pm2_5" | "co" | "no" | "so2" | "nh3";

const pollutants = {
	no2: {
		name: "nitrogen dioxide",
		symbol: "NO₂",
		thresholds: [50, 100, 200, 400],
	},
	o3: {
		name: "ozone",
		symbol: "O₃",
		thresholds: [60, 120, 180, 240],
	},
	pm10: {
		name: "coarse particulate matter",
		symbol: "PM₁₀",
		thresholds: [25, 50, 90, 180],
	},
	// eslint-disable-next-line @typescript-eslint/naming-convention
	pm2_5: {
		name: "fine particulate matter",
		symbol: "PM₂₋₅",
		thresholds: [15, 30, 55, 110],
	},
	co: { name: "carbon dioxide", symbol: "CO" },
	no: { name: "nitrogen monoxide", symbol: "NO" },
	so2: { name: "sulphur dioxide", symbol: "SO₂" },
	nh3: { name: "ammonia", symbol: "NH₃" },
} as const;

export const selectAirQualityEmoji = (
	concentration: number,
	thresholds: readonly [number, number, number, number],
): string => {
	const emojis = ["🙂", "😕", "😦", "😨", `☣${VS16}`] as const;
	const index = thresholds.findIndex((threshold) => concentration < threshold);
	const adjustedIndex = index === -1 ? 4 : (index as 0 | 1 | 2 | 3 | 4);
	return emojis[adjustedIndex];
};

const describePollutant = (pollutant: Pollutant, concentration: number, escape: string): string => {
	const pollutantObject = pollutants[pollutant];
	const roundedConcentration = Math.round(concentration);
	if ("thresholds" in pollutantObject) {
		const emoji = selectAirQualityEmoji(concentration, pollutantObject.thresholds);
		return `${pollutantObject.symbol}:\u00A0${roundedConcentration}μg/m³\u00A0${escape}${emoji}`;
	}
	return `${pollutantObject.symbol}:\u00A0${roundedConcentration}μg/m³`;
};

export const aqiThresholds = [2, 3, 4, 5] as const;

const airPollution = async (
	locationArg: string,
	userId: string,
	domain: string,
	network: Network,
): Promise<string> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getAirPollution(location.latitude, location.longitude);

	if (!isNonEmptyArray(data.list)) throw new Error("Unexpected result from weather API");

	const bold = network === "discord" ? "**" : "";
	const escape = network === "discord" ? "\\" : "";
	const aqi = data.list[0].main.aqi;
	let output = `${bold}AQI:\u00A0${aqi}\u00A0${escape}${selectAirQualityEmoji(
		aqi,
		aqiThresholds,
	)}${bold}`;
	const components = data.list[0].components;
	for (const [key] of Object.entries(pollutants) as Entries<typeof pollutants>) {
		output += `, ${describePollutant(key, components[key], escape)}`;
	}
	return `${capitalizeFirstChar(location.toString())}: ${output}`;
};

const cAirPollution: CommandFunction = async (cCall) => {
	cCall.reply(await airPollution(cCall.argstring, cCall.user.id, cCall.domain, cCall.network));
};

const slashAirPollution: SlashCommandFunction = async (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const location = interaction.options.getString("location") ?? "";
	return airPollution(location, interaction.user.id, interaction.guildId, "discord");
};

addCommand({
	name: "airpollution",
	run: cAirPollution,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description: "Gets the one hour average air pollution measured at a given location.",
	aliases: ["airquality", "aqi"],
	slash: {
		run: slashAirPollution,
		options: [LOCATION_OPTION],
	},
});
