import { addCommand, CommandFunction, SlashCommandFunction } from "../../commands.js";
import { CommandError } from "../../Errors.js";
import { getCurrentAndDailyWeather } from "../API_calls.js";
import { LOCATION_OPTION, LOCATION_USAGE_WITH_USER_LOCATION } from "../constants.js";
import { Location } from "../geocoding.js";
import { capitalizeFirstChar } from "../util.js";

/** Returns the emoji clock most appropriate to a date */
export const emojiClock = (date: Date): string => {
	const hour = (date.getUTCHours() % 12) + Math.floor(date.getUTCMinutes() / 30) / 2;
	// prettier-ignore
	const emojis: Record<number, string> = {
		0: "🕛", 0.5: "🕧",
		1: "🕐", 1.5: "🕜",
		2: "🕑", 2.5: "🕝",
		3: "🕒", 3.5: "🕞",
		4: "🕓", 4.5: "🕟",
		5: "🕔", 5.5: "🕠",
		6: "🕕", 6.5: "🕡",
		7: "🕖", 7.5: "🕢",
		8: "🕗", 8.5: "🕣",
		9: "🕘", 9.5: "🕤",
		10: "🕙", 10.5: "🕥",
		11: "🕚", 11.5: "🕦",
	};
	return emojis[hour] ?? "";
};

const stringifyTime = (time: number, addEmoji = false): string => {
	const date = new Date(time * 1000);
	return `${addEmoji ? emojiClock(date) : ""}${date
		.getUTCHours()
		.toString()
		.padStart(2, "0")}:${date.getUTCMinutes().toString().padStart(2, "0")}`;
};

const getSunsetAndRise = async (
	locationArg: string,
	userId: string,
	domain: string,
): Promise<string> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getCurrentAndDailyWeather(location.latitude, location.longitude);
	const { current, daily } = data;
	let output = `${stringifyTime(current.dt + data.timezone_offset, true)} `;
	if (current.dt < current.sunrise) {
		output += `🌅${stringifyTime(current.sunrise + data.timezone_offset)} 🌃${stringifyTime(
			current.sunset + data.timezone_offset,
		)}`;
	} else if (current.dt < current.sunset) {
		output += `🌃${stringifyTime(current.sunset + data.timezone_offset)} 🌅${stringifyTime(
			daily[1]!.sunrise + data.timezone_offset,
		)}`;
	} else {
		output += `🌅${stringifyTime(daily[1]!.sunrise + data.timezone_offset)} 🌃${stringifyTime(
			daily[1]!.sunset + data.timezone_offset,
		)}`;
	}
	return `${capitalizeFirstChar(location.toString())}: ${output}`;
};

const cSunsetAndRise: CommandFunction = async (cCall) => {
	cCall.reply(await getSunsetAndRise(cCall.argstring, cCall.user.id, cCall.domain));
};

const slashSunsetAndRise: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const location = interaction.options.getString("location") ?? "";
	return getSunsetAndRise(location, interaction.user.id, interaction.guildId);
};

addCommand({
	name: "sun",
	run: cSunsetAndRise,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description: "Gets the next sunrise and the next sunset time at the given location.",
	aliases: ["sunrise", "sunset"],
	slash: {
		run: slashSunsetAndRise,
		options: [LOCATION_OPTION],
	},
});
