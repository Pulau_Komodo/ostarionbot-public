import { Alert, DailyWeather, Weather } from "../API_interfaces.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../../commands.js";
import { Location } from "../geocoding.js";
import { getCurrentAndDailyWeather } from "../API_calls.js";
import { LOCATION_OPTION, LOCATION_USAGE_WITH_USER_LOCATION } from "../constants.js";
import {
	capitalizeFirstChar,
	degreesToCardinal,
	findAlertEmoji,
	round,
	toOrdinal,
} from "../util.js";
import { CommandError } from "../../Errors.js";
import { VS16 } from "../../characters.js";
import { log } from "../../log.js";

/** https://openweathermap.org/weather-conditions#Weather-Condition-Codes-2 */
const emojifyWeatherId = (id: number, day: boolean): string => {
	const group = Math.trunc(id / 100);
	if (group === 2) {
		return `⛈${VS16}`;
	} else if (group === 3 || group === 5) {
		return `🌧${VS16}`;
	} else if (group === 6) {
		return `🌨${VS16}`;
	} else if (
		id === 701 ||
		id === 711 ||
		id === 721 ||
		id === 731 ||
		id === 741 ||
		id === 751 ||
		id === 761 ||
		id === 762
	) {
		return `🌫${VS16}`;
	} else if (id === 771) {
		return "💨";
	} else if (id === 781) {
		return `🌪${VS16}`;
	} else if (id === 800) {
		if (day) {
			return `☀${VS16}`;
		} else {
			return "🌃";
		}
	} else if (group === 8) {
		return `☁${VS16}`;
	} else {
		log(`Did not have an emoji for weather id ${id}`);
		return "";
	}
};

/** contains leading space and trailing comma */
export const describeWeatherConditions = (weatherConditions: Weather[], day: boolean): string => {
	if (weatherConditions.length === 0) return "";
	return weatherConditions
		.map((weather) => ` ${emojifyWeatherId(weather.id, day)} ${weather.description},`)
		.join("");
};

const throwF = (error: Error) => {
	throw error;
};

const findExtremes = (days: DailyWeather[]): [DailyWeather, DailyWeather] => {
	let coldestDay = days[0] ?? throwF(new RangeError());
	let hottestDay = days[0] ?? throwF(new RangeError());
	for (const day of days.slice(1)) {
		if (day.temp.min < coldestDay.temp.min) {
			coldestDay = day;
		}
		if (day.temp.max > hottestDay.temp.max) {
			hottestDay = day;
		}
	}
	return [coldestDay, hottestDay];
};

const describeExtremes = (
	coldestDay: DailyWeather,
	hottestDay: DailyWeather,
	timezoneOffset: number,
): string => {
	const coldestDate = new Date((coldestDay.dt + timezoneOffset) * 1000).getDate();
	if (coldestDay === hottestDay) {
		return `7-day minimum: ${round(coldestDay.temp.min, 1)}°C, 7-day maximum: ${round(
			hottestDay.temp.max,
			1,
		)}°C, both on the ${toOrdinal(coldestDate)}.`;
	}
	const hottestDate = new Date((hottestDay.dt + timezoneOffset) * 1000).getDate();
	return `7-day minimum: ${round(coldestDay.temp.min, 1)}°C on the ${toOrdinal(
		coldestDate,
	)}, 7-day maximum: ${round(hottestDay.temp.max, 1)}°C on the ${toOrdinal(hottestDate)}.`;
};

/**
 * Like "5m/s NNE wind with gusts of up to 10m/s"
 */
const describeWind = (speed: number, angle: number, gust?: number): string => {
	const gusts = gust !== undefined ? ` with gusts of up to ${gust}m/s` : "";
	if (speed === 0) {
		return `no wind${gusts}`;
	} else {
		return `${speed}m/s ${degreesToCardinal(angle)} wind${gusts}`;
	}
};

/**
 * Empty or like "5mm 🌧 in the past hour. "
 *
 * Includes trailing space if not empty
 */
const describePrecipitation = (rainfall: number, snowfall: number): string => {
	if (rainfall !== 0 && snowfall !== 0) {
		return `${rainfall}mm 🌧 and ${snowfall}mm 🌨${VS16} in the past hour. `;
	} else if (rainfall !== 0) {
		return `${rainfall}mm 🌧${VS16} in the past hour. `;
	} else if (snowfall !== 0) {
		return `${snowfall}mm 🌨${VS16} in the past hour. `;
	} else {
		return ``;
	}
};

/**
 * Like "500m visibility 🌫"
 */
const describeVisibility = (visibility: number): string => {
	if (visibility === 10000) {
		return "clear visibility";
	}
	return `${visibility}m visibility 🌫${VS16}`;
};

/**
 * Empty, or like "\n⚠🥶 alert: cold weather warning x2\n⚠ alert: sandstorm alert"
 *
 * Includes leading line break if not empty
 */
export const describeAlerts = (alerts: Alert[]): string => {
	if (alerts.length === 0) {
		return "";
	}
	const dedupedAlerts = new Map<string, number>();
	for (const alert of alerts) {
		const count = dedupedAlerts.get(alert.event) ?? 0;
		dedupedAlerts.set(alert.event, count + 1);
	}
	return [...dedupedAlerts.entries()]
		.map(([event, count]) => {
			const emoji = findAlertEmoji(event) ?? "";
			const countText = count > 1 ? ` x${count}` : "";
			return `\n⚠${VS16}${emoji} alert: ${event}${countText}`;
		})
		.join("");
};

const getWeather = async (locationArg: string, userId: string, domain: string): Promise<string> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getCurrentAndDailyWeather(location.latitude, location.longitude);
	const { current, daily } = data;
	const day = current.dt > current.sunrise && current.dt < current.sunset;
	const date = new Date((current.dt + data.timezone_offset) * 1000);
	const time = `${date.getUTCHours().toString().padStart(2, "0")}:${date
		.getUTCMinutes()
		.toString()
		.padStart(2, "0")}`;
	const [hottestDay, coldestDay] = findExtremes(daily);

	return (
		`${capitalizeFirstChar(location.toString())} at ${time}:` +
		`${describeWeatherConditions(current.weather, day)} ` +
		`${round(current.temp, 1)}°C (feels-like: ${round(current.feels_like, 1)}°C), ` +
		`${current.humidity}% humidity, ` +
		`${current.pressure}hPa, ` +
		`${describeWind(round(current.wind_speed, 1), current.wind_deg, current.wind_gust)}, ` +
		`${current.clouds}% ☁${VS16}, UV index ${round(current.uvi, 1)}, ` +
		`${describeVisibility(current.visibility)}. ` +
		describePrecipitation(current.rain?.["1h"] ?? 0, current.snow?.["1h"] ?? 0) +
		describeExtremes(hottestDay, coldestDay, data.timezone_offset) +
		(data.alerts !== undefined ? describeAlerts(data.alerts) : "")
	);
};

const cWeather: CommandFunction = async (cCall) => {
	cCall.reply(await getWeather(cCall.argstring, cCall.user.id, cCall.domain));
};

const slashWeather: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const location = interaction.options.getString("location") ?? "";
	return getWeather(location, interaction.user.id, interaction.guildId);
};

addCommand({
	name: "weather_old",
	run: cWeather,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description:
		'Gets the weather in the specified city or at the specified coordinates. Cities can be specified with countries, like "London, US", if needed.',
	slash: {
		run: slashWeather,
		options: [LOCATION_OPTION],
		description: "Gets the weather in the specified city or at the specified coordinates.",
	},
});
