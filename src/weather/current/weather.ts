import { addCommand, CommandFunction, SlashCommandFunction } from "../../commands.js";
import { Location } from "../geocoding.js";
import { getAirPollution, getCurrentMinutelyAndDailyWeather } from "../API_calls.js";
import { LOCATION_OPTION, LOCATION_USAGE_WITH_USER_LOCATION } from "../constants.js";
import { capitalizeFirstChar, degreesToCardinal, degreesToEmoji, round } from "../util.js";
import { CommandError } from "../../Errors.js";
import { VS16 } from "../../characters.js";
import { describeAlerts, describeWeatherConditions } from "./weather_old.js";
import { canSendAttachment } from "../../permission checks.js";
import { AttachmentBuilder, TextChannel, ThreadChannel } from "discord.js";
import { MinutelyWeather } from "../API_interfaces.js";
import { minutelyPrecipitationGraph } from "../minutely/minutely.js";
import { aqiThresholds, selectAirQualityEmoji } from "./air_quality.js";
import { getUviEmoji } from "../hourly/uv_index.js";

const getWeather = async (
	locationArg: string,
	userId: string,
	domain: string,
	isDiscord: boolean,
	canAttach: boolean,
): Promise<string | { content: string; files: [AttachmentBuilder] }> => {
	const location = await Location.fromInputOrUser(locationArg, {
		user: userId,
		domain,
	});
	const data = await getCurrentMinutelyAndDailyWeather(location.latitude, location.longitude);
	const pollution = await getAirPollution(location.latitude, location.longitude);

	const escape = isDiscord ? "\\" : "";
	const bold = isDiscord ? "**" : "";

	const { current, daily } = data;
	const day = current.dt > current.sunrise && current.dt < current.sunset;
	const date = new Date((current.dt + data.timezone_offset) * 1000);
	const time = `${date.getUTCHours().toString().padStart(2, "0")}:${date
		.getUTCMinutes()
		.toString()
		.padStart(2, "0")}`;

	const minutely = data.minutely?.slice(1, 61);
	const minutelyOutput = await describePrecipitation(minutely, data.timezone_offset, canAttach);
	const precipitationText = typeof minutelyOutput === "string" ? minutelyOutput : "";
	const aqi = pollution.list[0]!.main.aqi;

	let temperatureEmoji = getTemperatureEmoji(current.feels_like);
	if (temperatureEmoji !== "") {
		temperatureEmoji = ` ${escape}${temperatureEmoji}`;
	}

	const output =
		`${capitalizeFirstChar(location.toString())} at ${time}:` +
		`${describeWeatherConditions(current.weather, day)} ` +
		`${bold}${round(current.temp, 1)}${bold}°C (feels-like: ${bold}${round(
			current.feels_like,
			1,
		)}${bold}°C${temperatureEmoji}), ` +
		`${describeWind(
			current.wind_speed,
			current.wind_deg,
			current.wind_gust ?? 0,
			escape,
			bold,
		)}` +
		`UVI ${bold}${round(current.uvi, 1)}${bold}${getUviEmoji(current.uvi, escape)}, ` +
		`AQI ${bold}${round(aqi, 1)}${bold} ${escape}${selectAirQualityEmoji(
			aqi,
			aqiThresholds,
		)}, ` +
		`${describeVisibility(current.visibility)}` +
		`${describeSunriseSunset(
			current.dt,
			current.sunrise,
			current.sunset,
			daily[1]!.sunrise,
			daily[1]!.sunset,
			escape,
			bold,
		)}.` +
		`${precipitationText}` +
		(data.alerts !== undefined ? describeAlerts(data.alerts) : "");

	if (canAttach && typeof minutelyOutput !== "string") {
		return { content: output, files: [minutelyOutput] };
	} else {
		return output;
	}
};

const getTemperatureEmoji = (temperature: number): string => {
	if (temperature >= 30) {
		return "🥵";
	} else if (temperature <= 0) {
		return "🥶";
	} else {
		return "";
	}
};

const describePrecipitation = async (
	minutely: MinutelyWeather[] | undefined,
	timezone_offset: number,
	canAttach: boolean,
): Promise<string | AttachmentBuilder> => {
	if (minutely === undefined || minutely.length === 0) {
		return "";
	}
	const sum = minutely.reduce((acc, curr) => acc + curr.precipitation, 0);
	if (sum === 0) {
		return ` No precipitation over the next hour.`;
	} else if (!canAttach) {
		return ` ${round(sum / 60, 1)}mm precipitation over the next hour.`;
	} else {
		return minutelyPrecipitationGraph(minutely, timezone_offset);
	}
};

/** Empty if wind speeds low, otherwise like "10.3m/s NE↗️ wind with gusts of up to 12.9m/s, ". */
const describeWind = (
	wind_speed: number,
	angle: number,
	gust: number,
	escape: string,
	bold: string,
): string => {
	const WIND_THRESHOLD = 7;
	const GUST_THRESHOLD = 11;
	const GUST_EXCESS_THRESHOLD = 2.5;
	if (wind_speed < WIND_THRESHOLD && gust < GUST_THRESHOLD) {
		return "";
	}
	const wind = `${bold}${round(wind_speed, 1)}${bold}m/s ${degreesToCardinal(
		angle,
	)}${escape}${degreesToEmoji(angle)} wind`;
	if (gust - wind_speed > GUST_EXCESS_THRESHOLD) {
		return `${wind} with gusts of up to ${bold}${round(gust, 1)}${bold}m/s, `;
	} else {
		return `${wind}, `;
	}
};

/**
 * Empty if clear, otherwise like "500m visibility 🌫, ".
 */
const describeVisibility = (visibility: number): string => {
	if (visibility === 10000) {
		return "";
	}
	return `${visibility}m visibility 🌫${VS16}, `;
};

/**
 * Like "sunrise in 1h30m".
 */
const describeSunriseSunset = (
	time: number,
	sunrise: number,
	sunset: number,
	nextSunrise: number,
	nextSunset: number,
	escape: string,
	bold: string,
): string => {
	const events = [sunrise, sunset, nextSunrise, nextSunset];
	const index = events.findIndex((eventTime) => eventTime > time);
	const eventTime = events[index]!;
	const difference = eventTime - time;
	const text = index % 2 == 0 ? "sunrise" : "sunset";
	const emoji = difference < 60 * 60 ? (index % 2 == 0 ? `${escape}🌅` : `${escape}🌃`) : "";
	return `${text} in ${bold}${formatInterval(difference)}${bold}${emoji}`;
};

const formatInterval = (interval: number): string => {
	let allMinutes = Math.round(interval / 60);
	const hours = Math.floor(allMinutes / 60);
	const minutes = allMinutes % 60;
	if (hours > 0) {
		return `${hours}h${minutes}m`;
	} else {
		return `${minutes}m`;
	}
};

const cWeather: CommandFunction = async (cCall) => {
	const canAttach = cCall.network === "discord" ? cCall.canReplyAttachment : false;
	const result = await getWeather(
		cCall.argstring,
		cCall.user.id,
		cCall.domain,
		cCall.network === "discord",
		canAttach,
	);
	if (typeof result === "string") {
		cCall.reply(result);
	} else if (cCall.network === "discord") {
		cCall.channel.send(result);
	}
};

const slashWeather: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const location = interaction.options.getString("location") ?? "";
	const channel = interaction.channel;
	if (!channel || !(channel instanceof TextChannel || channel instanceof ThreadChannel))
		throw new CommandError("Could not get channel");
	const canAttach = canSendAttachment(channel);
	return getWeather(location, interaction.user.id, interaction.guildId, true, canAttach);
};

addCommand({
	name: "weather",
	run: cWeather,
	category: "weather",
	usage: LOCATION_USAGE_WITH_USER_LOCATION,
	description:
		'Gets the weather in the specified city or at the specified coordinates. Cities can be specified with countries, like "London, US", if needed.',
	slash: {
		run: slashWeather,
		options: [LOCATION_OPTION],
		description: "Gets the weather in the specified city or at the specified coordinates.",
	},
});
