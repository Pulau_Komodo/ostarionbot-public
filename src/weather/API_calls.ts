import got from "got";
import { SimpleCache } from "../cache.js";
import { weatherApiKey } from "../config.js";
import { CommandError } from "../Errors.js";
import { isNonEmptyArray } from "../type_guards.js";
import { AirPollutionResponse, GeocodingResponse, WeatherResponse } from "./API_interfaces.js";
import { countryCodes } from "./country_codes.js";

/** one-minute cache */
const currentAndDailyCache = new SimpleCache<string, Omit<WeatherResponse, "minutely" | "hourly">>(
	60 * 1000,
);
/** one-minute cache */
const currentMinutelyAndDailyCache = new SimpleCache<string, Omit<WeatherResponse, "hourly">>(
	60 * 1000,
);
/** one-minute cache */
const minutelyCache = new SimpleCache<
	string,
	Omit<WeatherResponse, "current" | "hourly" | "daily" | "alerts">
>(60 * 1000);
/** one-minute cache */
const hourlyCache = new SimpleCache<
	string,
	Omit<WeatherResponse, "current" | "minutely" | "daily" | "alerts">
>(60 * 1000);
/** one-minute cache */
const dailyCache = new SimpleCache<
	string,
	Omit<WeatherResponse, "current" | "minutely" | "hourly" | "alerts">
>(60 * 1000);
/** one-minute cache */
const alertCache = new SimpleCache<
	string,
	Omit<WeatherResponse, "current" | "minutely" | "hourly" | "daily">
>(60 * 1000);
/** one-minute cache */
const airPollutionCache = new SimpleCache<string, AirPollutionResponse>(60 * 1000);

/** 1-week cache */
const settlementCache = new SimpleCache<string, LocationApiData>(7 * 24 * 60 * 60 * 1000);
/** 1-week cache */
const coordinatesCache = new SimpleCache<string, LocationApiData>(7 * 24 * 60 * 60 * 1000);

export interface LocationApiData {
	name: string;
	country: string;
	latitude: number;
	longitude: number;
}

export const lookUpSettlement = async (
	settlement: string,
): Promise<LocationApiData | undefined> => {
	if (/[^\p{L} ,.'-]/u.test(settlement))
		throw new CommandError("Invalid characters in settlement name"); // Ugly measure because somehow API treats encoded &s as normal &s and who knows what else it does

	const key = settlement.toLowerCase();
	const cachedData = settlementCache.get(key);
	if (cachedData !== undefined) return cachedData;

	const encodedSettlement = encodeURIComponent(settlement);
	const data = await got(
		`http://api.openweathermap.org/geo/1.0/direct?q=${encodedSettlement}&limit=1&appid=${weatherApiKey}`,
	);
	const parsedData = JSON.parse(data.body) as GeocodingResponse;

	if (!isNonEmptyArray(parsedData)) return;

	const location = {
		name: parsedData[0].name,
		country: countryCodes.get(parsedData[0].country) ?? parsedData[0].country,
		latitude: parsedData[0].lat,
		longitude: parsedData[0].lon,
	};

	settlementCache.set(key, location);
	return location;
};

export const lookUpCoords = async (
	latitude: number,
	longitude: number,
): Promise<LocationApiData | undefined> => {
	const key = `${latitude},${longitude}`;
	const cachedData = coordinatesCache.get(key);
	if (cachedData !== undefined) return cachedData;

	const data = await got(
		`http://api.openweathermap.org/geo/1.0/reverse?lat=${latitude}&lon=${longitude}&limit=1&appid=${weatherApiKey}`,
	);
	const parsedData = JSON.parse(data.body) as GeocodingResponse;
	if (!isNonEmptyArray(parsedData)) return;

	const location = {
		name: parsedData[0].name,
		country: countryCodes.get(parsedData[0].country) ?? parsedData[0].country,
		longitude: parsedData[0].lon,
		latitude: parsedData[0].lat,
	};

	coordinatesCache.set(key, location);
	return location;
};

export const getCurrentAndDailyWeather = async (
	latitude: number,
	longitude: number,
): Promise<Omit<WeatherResponse, "minutely" | "hourly">> => {
	const key = `${latitude},${longitude}`;
	const cachedData = currentAndDailyCache.get(key);
	if (cachedData !== undefined) return cachedData;

	const data = await got(
		`http://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=minutely,hourly&units=metric&appid=${weatherApiKey}`,
	);
	const parsedData = JSON.parse(data.body) as Omit<WeatherResponse, "minutely" | "hourly">;

	currentAndDailyCache.set(key, parsedData);
	dailyCache.set(key, parsedData);
	alertCache.set(key, parsedData);
	return parsedData;
};

export const getCurrentMinutelyAndDailyWeather = async (
	latitude: number,
	longitude: number,
): Promise<Omit<WeatherResponse, "hourly">> => {
	const key = `${latitude},${longitude}`;
	const cachedData = currentMinutelyAndDailyCache.get(key);
	if (cachedData !== undefined) return cachedData;

	const data = await got(
		`http://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=hourly&units=metric&appid=${weatherApiKey}`,
	);
	const parsedData = JSON.parse(data.body) as Omit<WeatherResponse, "hourly">;

	currentMinutelyAndDailyCache.set(key, parsedData);
	minutelyCache.set(key, parsedData);
	dailyCache.set(key, parsedData);
	alertCache.set(key, parsedData);
	return parsedData;
};

export const getMinutelyWeather = async (
	latitude: number,
	longitude: number,
): Promise<Omit<WeatherResponse, "current" | "hourly" | "daily" | "alerts">> => {
	const key = `${latitude},${longitude}`;
	const cachedData = minutelyCache.get(key);
	if (cachedData !== undefined) return cachedData;

	const data = await got(
		`http://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=current,hourly,daily,alerts&units=metric&appid=${weatherApiKey}`,
	);
	const parsedData = JSON.parse(data.body) as Omit<
		WeatherResponse,
		"current" | "hourly" | "daily" | "alerts"
	>;

	minutelyCache.set(key, parsedData);
	return parsedData;
};

export const getHourlyWeather = async (
	latitude: number,
	longitude: number,
): Promise<Omit<WeatherResponse, "current" | "minutely" | "daily" | "alerts">> => {
	const key = `${latitude},${longitude}`;
	const cachedData = hourlyCache.get(key);
	if (cachedData !== undefined) return cachedData;

	const data = await got(
		`http://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=current,minutely,daily,alerts&units=metric&appid=${weatherApiKey}`,
	);
	const parsedData = JSON.parse(data.body) as Omit<
		WeatherResponse,
		"current" | "minutely" | "daily" | "alerts"
	>;

	hourlyCache.set(key, parsedData);
	return parsedData;
};

export const getDailyWeather = async (
	latitude: number,
	longitude: number,
): Promise<Omit<WeatherResponse, "current" | "minutely" | "hourly" | "alerts">> => {
	const key = `${latitude},${longitude}`;
	const cachedData = dailyCache.get(key);
	if (cachedData !== undefined) return cachedData;

	const data = await got(
		`http://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=current,minutely,hourly,alerts&units=metric&appid=${weatherApiKey}`,
	);
	const parsedData = JSON.parse(data.body) as Omit<
		WeatherResponse,
		"current" | "minutely" | "hourly" | "alerts"
	>;

	dailyCache.set(key, parsedData);
	return parsedData;
};

export const getWeatherAlerts = async (
	latitude: number,
	longitude: number,
): Promise<Omit<WeatherResponse, "current" | "minutely" | "hourly" | "daily">> => {
	const key = `${latitude},${longitude}`;
	const cachedData = alertCache.get(key);
	if (cachedData !== undefined) return cachedData;

	const data = await got(
		`http://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=current,minutely,hourly,daily&units=metric&appid=${weatherApiKey}`,
	);
	const parsedData = JSON.parse(data.body) as Omit<
		WeatherResponse,
		"current" | "minutely" | "hourly" | "daily"
	>;

	alertCache.set(key, parsedData);
	return parsedData;
};

const getCurrentWeather = async (
	latitude: number,
	longitude: number,
): Promise<Omit<WeatherResponse, "minutely" | "hourly" | "daily">> => {
	const data = await got(
		`http://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=minutely,hourly,daily&units=metric&appid=${weatherApiKey}`,
	);
	const parsedData = JSON.parse(data.body) as Omit<
		WeatherResponse,
		"minutely" | "hourly" | "daily"
	>;
	return parsedData;
};

export const getAirPollution = async (
	latitude: number,
	longitude: number,
): Promise<AirPollutionResponse> => {
	const key = `${latitude},${longitude}`;
	const cachedData = airPollutionCache.get(key);
	if (cachedData !== undefined) return cachedData;

	const data = await got(
		`http://api.openweathermap.org/data/2.5/air_pollution?lat=${latitude}&lon=${longitude}&appid=${weatherApiKey}`,
	);
	const parsedData = JSON.parse(data.body) as AirPollutionResponse;

	airPollutionCache.set(key, parsedData);
	return parsedData;
};
