import { AttachmentBuilder, EmbedBuilder, escapeMarkdown, TextChannel } from "discord.js";
import { Command } from "./commands.js";
import { DiscordMessage, GuildMessage } from "./discordclient.js";
import { queueMessage, queueMessageWithAddition } from "./message.js";
import { canSendAttachment, canSendEmbed } from "./permission checks.js";
import { TwitchMessage } from "./twitch/twitch_chat.js";

class BaseCommandCall {
	/** The prefix that was used to call the command */
	readonly prefix: string;
	/** The standard name of the command that was called */
	readonly command: string;
	/** Usage explanation for the command */
	readonly usage: string;
	/** The part of the message after the whitespace after the command. Mind that on Discord, mentions are broken with a \u200b (ZWS). */
	readonly argstring: string;
	/** Domain of the command (guild for Discord, channel for Twitch) */
	readonly domain: string;
	constructor(prefix: string, command: Command, argstring: string, domain: string) {
		this.prefix = prefix;
		this.argstring = argstring;
		this.command = command.name;
		this.usage = command.usage;
		this.domain = domain;
	}
	#args: this["args"] | undefined;
	/** Arguments split by whitespace. Mind that on Discord, mentions are broken with a \u200b (ZWS) right after the @. */
	get args(): string[] {
		return (this.#args ??= this.argstring.split(/\s+/));
	}
	#commaArgs: this["commaArgs"] | undefined;
	/** Arguments split by commas (may need trimming). Mind that on Discord, mentions are broken with a \u200b (ZWS) right after the @. */
	get commaArgs(): string[] {
		return (this.#commaArgs ??= this.argstring.split(","));
	}
}

class DiscordCommandUser {
	readonly id: string;
	#rawDisplayName: string;
	constructor(id: string, rawDisplayName: string) {
		this.id = id;
		this.#rawDisplayName = rawDisplayName;
	}
	#displayName: string | undefined;
	/** The markdown-escaped display name in the guild */
	get displayName(): string {
		return (this.#displayName ??= escapeMarkdown(this.#rawDisplayName));
	}
}

export class DiscordCommandCall extends BaseCommandCall {
	readonly network = "discord";
	readonly channel: DiscordGuildTextChannel;
	readonly guildId: string;
	readonly user: DiscordCommandUser;
	readonly message: GuildMessage;
	readonly whisper = false;
	readonly reply = (text: string): void => {
		queueMessage(this.network, this.channel, text, this.whisper);
	};
	readonly replyWithAddition = (
		text: string,
		addition: AttachmentBuilder | EmbedBuilder,
	): void => {
		queueMessageWithAddition(this.channel, text, addition);
	};
	readonly replyUsage = (): void => {
		this.reply(`Usage: ${this.prefix}${this.command}${this.usage}`);
	};
	constructor(message: DiscordMessage, prefix: string, command: Command, argstring: string) {
		super(prefix, command, argstring, message.message.guild.id);
		this.channel = message.message.channel;
		this.guildId = message.message.guild.id;
		this.message = message.message;
		this.user = new DiscordCommandUser(
			message.message.author.id,
			message.message.member.displayName,
		);
	}
	#canReplyAttachment: boolean | undefined;
	get canReplyAttachment(): boolean {
		return (this.#canReplyAttachment ??= canSendAttachment(this.channel));
	}
	#canReplyEmbed: boolean | undefined;
	get canReplyEmbed(): boolean {
		return (this.#canReplyEmbed ??= canSendEmbed(this.channel));
	}
}

export class TwitchCommandCall extends BaseCommandCall {
	readonly network = "twitch";
	readonly channel: { id: string };
	readonly guildId = "0";
	readonly user: {
		readonly id: string;
		readonly displayName: string;
	};
	readonly whisper: boolean;
	readonly reply = (text: string): void => {
		queueMessage(this.network, this.channel.id, text, this.whisper);
	};
	readonly replyUsage = (): void => {
		this.reply(`Usage: ${this.prefix}${this.command}${this.usage}`);
	};
	constructor(message: TwitchMessage, prefix: string, command: Command, argstring: string) {
		super(prefix, command, argstring, message.channel);
		this.channel = { id: message.channel };
		this.user = {
			id: message.userId,
			displayName: message.userId,
		};
		this.whisper = message.whisper;
		this.channel = { id: message.whisper ? message.userId : message.channel };
	}
}

export type CommandCall = DiscordCommandCall | TwitchCommandCall;
