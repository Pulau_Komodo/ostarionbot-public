import { AttachmentBuilder } from "discord.js";
import { execFile } from "child_process";
import { promisify } from "util";

const promiseExecFile = promisify(execFile);

/**
 * Generates a preview of one or two colours in ABCDEF format, and returns it in a MessageAttachment
 */
export const colourPreview = async (
	colour1: string,
	colour2?: string,
): Promise<AttachmentBuilder> => {
	const args = colour2 ? [colour1, colour2] : [colour1];
	try {
		const result = await promiseExecFile("./colour_sample.exe", args, {
			encoding: "buffer",
		});
		const name = `${colour1}${colour2 ? `-${colour2}` : ``}.png`;
		return new AttachmentBuilder(result.stdout, { name });
	} catch (error) {
		console.error(error);
		return new AttachmentBuilder("./data/static/placeholder.png", {
			name: "error.png",
		});
	}
};
