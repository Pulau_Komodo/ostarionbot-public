import { namedColours as coloursNTCRaw } from "./colours_NTC.js";
import { namedColours as coloursCommonRaw } from "./colours_common.js";
import { namedColours as coloursEmojisRaw } from "./colours_emotes.js";

export type ColourSet = "ntc" | "common" | "emoji";

export type NamedColourRaw = {
	name: string;
	aliases?: string[];
	hex: string;
};

export type NamedColour = {
	name: string;
	aliases: string[];
	hex: string;
	rgb: [number, number, number];
};

const colourDistance = (
	[r, g, b]: [number, number, number],
	[r2, g2, b2]: [number, number, number],
): number => {
	const r_a = (r + r2) / 2;
	const r_delta = Math.abs(r - r2);
	const g_delta = Math.abs(g - g2);
	const b_delta = Math.abs(b - b2);
	return (
		(2 + r_a / 256) * Math.pow(r_delta, 2) +
		4 * Math.pow(g_delta, 2) +
		(2 + (255 - r_a) / 256) * Math.pow(b_delta, 2)
	);
};

export const getColourDistance = (colour1: string, colour2: string): number => {
	const distance = Math.pow(colourDistance(getRgb(colour1), getRgb(colour2)), 0.5);
	return distance;
};

const getRgb = (color: string): [number, number, number] => {
	return [
		parseInt(color.slice(0, 2), 16),
		parseInt(color.slice(2, 4), 16),
		parseInt(color.slice(4, 6), 16),
	];
};

class ColourList {
	nameMap: Map<string, NamedColour> = new Map();
	list: Array<NamedColour> = [];
	constructor() {}
	populate(rawColours: Array<NamedColourRaw>) {
		for (const rawColour of rawColours) {
			const colour = {
				name: rawColour.name,
				aliases: rawColour.aliases ?? [],
				hex: rawColour.hex,
				rgb: getRgb(rawColour.hex),
			};
			this.nameMap.set(rawColour.name.toLowerCase(), colour);
			this.list.push(colour);
			for (const alias of colour.aliases) {
				this.nameMap.set(alias.toLowerCase(), colour);
			}
		}
	}
	nameToRgb(colourName: string): NamedColour | undefined {
		return this.nameMap.get(colourName.toLowerCase());
	}
	rgbToName(colour: string):
		| {
				hex: string;
				name: string;
				exact: boolean;
				distance: number;
				equidistantColours: string[];
		  }
		| undefined {
		const rgb = getRgb(colour);
		let smallestDistance = Infinity;
		let equidistantColours = [];

		let closestColour: NamedColour | undefined;
		for (const namedColour of this.list) {
			if (colour === namedColour.hex)
				return {
					hex: namedColour.hex,
					name: namedColour.name,
					exact: true,
					distance: 0,
					equidistantColours: [],
				};

			const distance = colourDistance(rgb, namedColour.rgb);

			if (distance < smallestDistance) {
				smallestDistance = distance;
				closestColour = namedColour;
				equidistantColours = [];
			} else if (distance === smallestDistance) {
				equidistantColours.push(namedColour.name);
			}
		}
		if (!closestColour) {
			console.log(`Colour error: ${colour}`);
			return;
		}
		return {
			hex: closestColour.hex,
			name: closestColour.name,
			exact: false,
			distance: Math.pow(smallestDistance, 0.5),
			equidistantColours,
		};
	}
}

const colourListsRaw = {
	ntc: coloursNTCRaw,
	common: coloursCommonRaw,
	emoji: coloursEmojisRaw,
} as const;

export const colourLists = {
	ntc: new ColourList(),
	common: new ColourList(),
	emoji: new ColourList(),
} as const;

for (const [listName, list] of Object.entries(colourListsRaw) as Entries<typeof colourListsRaw>) {
	colourLists[listName].populate(list);
}

export const defaultColours = colourLists.ntc;
