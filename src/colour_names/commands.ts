import {
	Role,
	GuildMember,
	Guild,
	AttachmentBuilder,
	TextChannel,
	ThreadChannel,
	ChatInputCommandInteraction,
	escapeMarkdown,
	ApplicationCommandOptionType,
} from "discord.js";

import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";
import { getColourDistance, NamedColour, ColourSet, colourLists } from "./colour_names.js";
import { colourPreview } from "./colourPreview.js";

import { namedColours as coloursNTC } from "./colours_NTC.js";
import { namedColours as coloursCommon } from "./colours_common.js";
import { namedColours as coloursEmojis } from "./colours_emotes.js";

import { log } from "../log.js";
import { CommandError } from "../Errors.js";
import { canSendAttachment } from "../permission checks.js";
import { CommandCall } from "../CommandCall.js";

const zws = "\u200b";
const mentionRegex = new RegExp(`^<@${zws}?[!&]?\\d+>$`);
const mentionRegexWithCaptures = new RegExp(`^<@${zws}?([!&]?)(\\d+)>$`);

interface FoundColour {
	hex: string;
	role?: Role;
	member?: GuildMember;
	fromRgb?: boolean;
	random?: boolean;
	fromName?: NamedColour;
}

export const outputPossiblyWithAttachment = async (
	output: string,
	colour1: string,
	colour2: string | undefined,
	canAttach: boolean,
): Promise<string | { content: string; files: [AttachmentBuilder] }> => {
	if (canAttach) {
		const attachment = await colourPreview(colour1, colour2);
		return { content: output, files: [attachment] };
	} else {
		return output;
	}
};

/** Throws a CommandError with message if it looks like a colour name but can't find a colour by that name, or if it doesn't match any expected format */
const processColourArg = async (
	arg: string,
	set: ColourSet,
	guild: Guild | undefined,
): Promise<FoundColour> => {
	arg = arg.trim().toUpperCase();

	// Is on Discord and looks like an escaped or non-escaped user or role mention, like <@​0123456789>, with optionally ZWS (​) right after @
	if (guild && mentionRegex.test(arg)) {
		const match = arg.match(mentionRegexWithCaptures) as null | [string, string, string];
		if (match) {
			// Role mention
			if (match[1] === "&") {
				const role = await guild.roles.fetch(match[2]).catch(log);
				if (!role) throw new CommandError("Error fetching role");
				const hex = role.hexColor.slice(1).toUpperCase().padStart(6, "0");
				return { hex, role };
				// User mention
			} else {
				const member = await guild.members.fetch(match[2]).catch(log);
				if (!member) throw new CommandError("Error fetching member");
				const hex = member.displayHexColor.slice(1).toUpperCase().padStart(6, "0");
				return { hex, member };
			}
		}
		// Looks like hex colour code, "FF0000"
	} else if (/^#?[0-9A-F]{6}$/.test(arg)) {
		const hex = arg.startsWith("#") ? arg.slice(1) : arg;
		return { hex, fromRgb: true };
		// "RANDOM"
	} else if (arg === "RANDOM") {
		// Generate a random RGB
		const hex = Math.floor(Math.random() * 16777216)
			.toString(16)
			.toUpperCase()
			.padStart(6, "0");
		return { hex, random: true };
		// May be a colour name, like "BURNT UMBER"
	} else if (/^[A-Z0-9# ]+$/.test(arg)) {
		const namedColour = colourLists[set].nameToRgb(arg);
		if (!namedColour) throw new CommandError(`Could not find ${arg.toLowerCase()} as a colour`);
		return { hex: namedColour.hex, fromName: namedColour };
	}
	throw new CommandError("Invalid argument");
};

const colourInternal = async (
	set: ColourSet,
	colourArgs: string[],
	guild: Guild | undefined,
	canSendAttachment: boolean,
): Promise<
	| string
	| {
			content: string;
			files: [AttachmentBuilder];
	  }
	| undefined
> => {
	const foundColours: FoundColour[] = await Promise.all(
		colourArgs.map((arg) => processColourArg(arg, set, guild)),
	);

	// Found one colour through the above methods
	if (foundColours.length === 1) {
		const colour = foundColours[0]!;
		// If colour was supplied by name
		if (colour.fromName) {
			const namedColour = colour.fromName;
			let output = `${namedColour.name[0]?.toUpperCase() + namedColour.name.slice(1)} is ${
				namedColour.hex
			}.`;
			if (namedColour.aliases.length > 0) {
				output += ` Alias${
					namedColour.aliases.length !== 1 ? "es" : ""
				}: ${namedColour.aliases.join(", ")}.`;
			}
			return outputPossiblyWithAttachment(
				output,
				namedColour.hex,
				undefined,
				canSendAttachment,
			);
		}
		const foundColour = colourLists[set].rgbToName(colour.hex);
		if (!foundColour) throw new CommandError("It somehow did not find a colour"); // Should not be possible
		const { hex, name, exact, distance, equidistantColours } = foundColour;

		let output = ``;
		if (colour.role) {
			output += `Role ${escapeMarkdown(colour.role.name)} has colour ${colour.hex}, which`;
		} else if (colour.member) {
			output += `${escapeMarkdown(colour.member.displayName)} has colour ${
				colour.hex
			}, which`;
		} else if (colour.random) {
			output += `Randomly selected colour: ${colour.hex}. It`;
		} else {
			output += `${colour.hex}`;
		}

		if (exact) {
			output += ` is an exact match for ${name}.`;
		} else {
			output += ` is closest to ${name} (${hex}). Distance: ${Math.round(distance)}.`;
			if (equidistantColours.length > 0) {
				output += ` Equidistant colour${
					equidistantColours.length !== 1 ? "s" : ""
				}: ${equidistantColours.join(", ")}.`;
			}
		}

		return outputPossiblyWithAttachment(
			output,
			hex,
			exact ? undefined : colour.hex,
			canSendAttachment,
		);
	}
	// Found two colours through the above methods
	if (foundColours.length === 2) {
		const colours = foundColours as [FoundColour, FoundColour];
		const distance = getColourDistance(colours[0].hex, colours[1].hex);
		let output = `The distance between `;
		output += foundColours
			.map((colour) => {
				if (colour.role) {
					return `${colour.hex} (from ${escapeMarkdown(colour.role.name)})`;
				} else if (colour.member) {
					return `${colour.hex} (from ${escapeMarkdown(colour.member.displayName)})`;
				} else if (colour.random) {
					return `randomly selected colour ${colour.hex}`;
				} else {
					return colour.hex;
				}
			})
			.join(" and ");
		output += ` is ${Math.round(distance)}.`;

		return outputPossiblyWithAttachment(
			output,
			colours[0].hex,
			colours[1].hex,
			canSendAttachment,
		);
	}

	// Didn't resemble any colour format
	throw new CommandError();
};

const commandAnyColourSet = async (cCall: CommandCall, set: ColourSet): Promise<void> => {
	const guild = cCall.network === "discord" ? cCall.message.guild : undefined;
	const canSendAttachment = cCall.network === "discord" ? cCall.canReplyAttachment : false;
	if (!cCall.commaArgs[0]) throw new CommandError();
	const result = await colourInternal(set, cCall.commaArgs.slice(0, 2), guild, canSendAttachment);
	if (!result) throw new CommandError();
	if (typeof result === "string") {
		cCall.reply(result);
	} else if (cCall.network === "discord") {
		cCall.replyWithAddition(result.content, result.files[0]);
	}
};
const commandColour: CommandFunction = async (cCall) => {
	await commandAnyColourSet(cCall, "ntc");
};
const commandCommonColour: CommandFunction = async (cCall) => {
	await commandAnyColourSet(cCall, "common");
};
const commandEmojiColour: CommandFunction = async (cCall) => {
	await commandAnyColourSet(cCall, "emoji");
};

const slashAnyColourSet = async (interaction: ChatInputCommandInteraction, set: ColourSet) => {
	const guild = interaction.guild;
	if (!guild) throw new CommandError("Could not get guild");
	const channel = interaction.channel;
	if (!channel || !(channel instanceof TextChannel || channel instanceof ThreadChannel))
		throw new CommandError("Could not get channel");
	const args = [interaction.options.getString("first", true)];
	const second = interaction.options.getString("second");
	if (second) args.push(second);
	const canAttach = canSendAttachment(channel);
	const output = await colourInternal(set, args, guild, canAttach);
	if (!output) throw new CommandError("Invalid input");
	else return output;
};
const slashColour: SlashCommandFunction = (interaction) => {
	return slashAnyColourSet(interaction, "ntc");
};
const slashCommonColour: SlashCommandFunction = (interaction) => {
	return slashAnyColourSet(interaction, "common");
};
const slashEmojiColour: SlashCommandFunction = (interaction) => {
	return slashAnyColourSet(interaction, "emoji");
};

const category = "colour";
const options = [
	{
		name: "first",
		type: ApplicationCommandOptionType.String,
		description: 'A colour to look up or compare, like "8A3324" or "burnt umber"',
		required: true,
	} as const,
	{
		name: "second",
		type: ApplicationCommandOptionType.String,
		description: 'A colour to compare the first colour to, like "901E1E" or "old brick"',
	} as const,
];
addCommand({
	name: "colour",
	run: commandColour,
	category,
	usage: "<hexadecimal RGB, role mention, user mention, or colour name>,[hexadecimal RGB, role mention, user mention, or colour name]",
	description: `Gets the closest name from a list of ${coloursNTC.length} named colours, for a given colour, or for the colour of the given user or role. Alternatively, gives the RGB value for a given colour name from the list. With two arguments, it compares the colours from the two.`,
	aliases: ["color"],
	slash: {
		run: slashColour,
		description:
			"Looks up names for colour codes, codes for colour names, or compares colours.",
		options,
	},
});
addCommand({
	name: "commoncolour",
	run: commandCommonColour,
	category,
	usage: "<hexadecimal RGB, role mention, user mention, or colour name>,[hexadecimal RGB, role mention, user mention, or colour name]",
	description: `Gets the closest name from a list of ${coloursCommon.length} common named colours, for a given colour, or for the colour of the given user or role. Alternatively, gives the RGB value for a given colour name from the list. With two arguments, it compares the colours from the two.`,
	aliases: ["commoncolor"],
	slash: {
		run: slashCommonColour,
		description:
			"Looks up names for colour codes in the common list, codes for colour names, or compares colours.",
		options,
	},
});
addCommand({
	name: "emojicolour",
	run: commandEmojiColour,
	category,
	usage: "<hexadecimal RGB, role mention, user mention, or colour name>,[hexadecimal RGB, role mention, user mention, or colour name]",
	description: `Gets the closest name from a list of ${coloursEmojis.length} colours used by Tweemoji, named for their most salient usage, for a given colour, or for the colour of the given user or role. Alternatively, gives the RGB value for a given colour name from the list. With two arguments, it compares the colours from the two.`,
	aliases: ["emojicolor"],
	slash: {
		run: slashEmojiColour,
		description:
			"Looks up names for colour codes in the emoji list, codes for colour names, or compares colours.",
		options,
	},
});
