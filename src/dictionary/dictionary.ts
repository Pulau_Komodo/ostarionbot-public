/* eslint-disable @typescript-eslint/no-unnecessary-condition */
/* eslint-disable @typescript-eslint/restrict-template-expressions */
import Database from "better-sqlite3";
import { dictionaryApiKey } from "../config.js";
import got from "got";
import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";
import { ApiResponse } from "./APIResponse.js";
import { CommandError } from "../Errors.js";
import { ApplicationCommandOptionType } from "discord.js";

const dbPath = `./data/dynamic/dictionary.db`;
let dictionaryDatabase;
try {
	dictionaryDatabase = new Database(dbPath, { fileMustExist: true });
} catch (error) {
	console.error(`Failed to open database at path "${dbPath}"`);
	throw error;
}

const dictionaryCache = dictionaryDatabase;
dictionaryCache.pragma("foreign_keys = ON");

interface WordInfo {
	headword: string;
	grammaticalFunction: string;
	shortDefinitions: string[];
}

/** Don't use cached entries older than this, roughly half a year, in seconds */
const maxCacheAge = 6 * 4 * 7 * 24 * 60 * 60;

const queryGetApiResponse = (query: string): ApiResponse[] | undefined => {
	const qGetApiResponse = `
		SELECT full_json
		FROM queries_cache
		WHERE query = $query
	`;
	const result = dictionaryCache.prepare(qGetApiResponse).get({ query }) as { full_json: string };
	if (!result) return;
	const parsed = JSON.parse(result.full_json) as ApiResponse[] | string[];
	if (isStringArray(parsed)) {
		return [];
	}
	return parsed;
};

const queryAddQuery = (query: string, results: string): void => {
	const qAddQuery = `
		INSERT INTO queries_cache (query, full_json)
		VALUES ($query, $results)
	`;
	dictionaryCache.prepare(qAddQuery).run({ query, results });
};

const searchWord = async (word: string): Promise<ApiResponse[]> => {
	const result = queryGetApiResponse(word);
	if (result) return result;
	const apiResult = await queryApi(word);
	return apiResult;
};

const baseUrl = "https://www.dictionaryapi.com/api/v3/references/collegiate/json/";

const formattingTokenRegex = /{(?:\\\/)?(?:b|(?:inf)|(?:it)|(?:sc)|(?:sup))}/g;

const isStringArray = (variable: string[] | unknown[]): variable is string[] => {
	if (variable.some((item) => typeof item !== "string")) return false;
	return true;
};

const queryApi = async (query: string): Promise<ApiResponse[]> => {
	const { body } = await got(`${baseUrl + encodeURIComponent(query)}?key=${dictionaryApiKey}`);
	//console.log(body);
	const results = JSON.parse(body) as ApiResponse[] | string[];
	queryAddQuery(query, body);
	if (isStringArray(results)) {
		return [];
	}
	return results;
};

const processWords = (results: ApiResponse[], query: string, _body: string): WordInfo[] => {
	try {
		results.forEach((word) => (word.hwi.hw = word.hwi.hw.replace(/\*/g, "")));
		const words = results
			.filter(
				(word) =>
					word.hwi.hw.toLowerCase() === query.toLowerCase() &&
					(word.fl || word.shortdef.length > 0),
			)
			.map((word) => {
				const headword = word.hwi.hw;
				const grammaticalFunction = word.fl ?? "n/a";
				const shortDefinitions = word.shortdef;
				return { headword, grammaticalFunction, shortDefinitions };
			});
		return words;
	} catch (error) {
		console.log(error);
		console.log(results);
		throw new CommandError("Error processing result");
	}
};

const getAbridgedDefinitions = async (input: string, joiner = " "): Promise<string> => {
	if (input === "") throw new CommandError();
	const wordInfo = processWords(await searchWord(input), input, "test");
	if (wordInfo.length === 0) {
		return `No dictionary result for "${input}"`;
	}
	let output = "";
	for (const word of wordInfo) {
		output += `**${word.headword}** - *${word.grammaticalFunction}*${joiner}`;
		if (word.shortDefinitions.length > 0)
			output += `- ${word.shortDefinitions.join(`${joiner}- `)}${joiner}`;
	}
	return output.trim();
};

const commandDictionary: CommandFunction = async (cCall) => {
	const joiner = cCall.network === "discord" ? "\n" : " ";
	cCall.reply(await getAbridgedDefinitions(cCall.argstring, joiner));
};

const slashDictionary: SlashCommandFunction = (interaction) => {
	return getAbridgedDefinitions(interaction.options.getString("word", true), "\n");
};

const commandFullDefinitions: CommandFunction = async (cCall) => {
	if (cCall.user.id !== "86575941819588608") throw new CommandError("Admin only");
	if (cCall.argstring === "") {
		cCall.replyUsage();
		return;
	}
	const joiner = cCall.network === "discord" ? "\n" : " ";
	const apiResponse = await searchWord(cCall.argstring);
	const words = apiResponse
		.filter((word) => word.hwi.hw.toLowerCase() === cCall.argstring.toLowerCase())
		.map((word) => {
			const headword = word.hwi.hw;
			const homonymNumber = word.hom;
			const grammaticalFunction = word.fl ?? "n/a";
			const transitive = word.sgram;
			const definitions = word.def.map((definition) => {
				const transitive = definition.vd?.startsWith("transitive") ? true : false;
				const senseSequence = definition.sseq ?? [];
				const texts = senseSequence.map((sequence) => {
					let text = "";
					sequence.forEach((sequenceElement) => {
						console.log(sequenceElement);
						if (sequenceElement[0] === "sense") {
							console.log(sequenceElement[1]?.dt);
							text += `**${sequenceElement[1].sn}**`;
							text += sequenceElement[1]?.dt?.join(" - ");
						} else if (sequenceElement[0] === "sen") {
							text += `**${sequenceElement[1].sn}**`;
							text += `<truncated sense>`;
						} else if (sequenceElement[0] === "bs") {
							text += `**${sequenceElement[1].sn}**`;
							text += `<binding sense>`;
							text += sequenceElement[1]?.dt?.join(" - ");
						} else if (sequenceElement[0] === "pseq") {
							sequenceElement[1].forEach((parenthesizedSequenceElement) => {
								console.log(parenthesizedSequenceElement);
								text += "(";
								if (parenthesizedSequenceElement[0] === "sense") {
									text += `**${parenthesizedSequenceElement[1].sn}**`;
									text += parenthesizedSequenceElement[1]?.dt?.join(" - ");
								} else if (parenthesizedSequenceElement[0] === "sen") {
									text += `**${parenthesizedSequenceElement[1].sn}**`;
									text += `<truncated sense>`;
								} else if (parenthesizedSequenceElement[0] === "bs") {
									text += `**${parenthesizedSequenceElement[1].sn}**`;
									text += `<binding sense>`;
									text += parenthesizedSequenceElement[1]?.dt?.join(" - ");
								} else {
									console.log(
										`Query for "${cCall.argstring}" gave the following parenthesizedSequenceElement:`,
									);
									console.log(parenthesizedSequenceElement);
								}
								text += ")";
							});
						} else {
							console.log(
								`Query for "${cCall.argstring}" gave the following sequenceElement:`,
							);
							console.log(sequenceElement);
						}
					});
					return text;
				});
				const text = texts.join(joiner);
				return text;
			});
			return {
				headword,
				homonymNumber,
				grammaticalFunction,
				transitive,
				definitions,
			} as const;
		});
	let output = "";
	for (const word of words) {
		output += `**${word.headword}** ${word.homonymNumber ? `${word.homonymNumber} ` : ""}- *${
			word.grammaticalFunction
		}*${joiner}- ${word.definitions.join(`${joiner}- `)}${joiner}`;
	}
	cCall.reply(output.trim());
};

addCommand({
	name: "dictionary",
	run: commandDictionary,
	category: "miscellaneous",
	usage: "<word>",
	description:
		"Looks up a word in the Merriam Webster dictionary and gives the abridged definitions",
	aliases: ["dict", "define"],
	slash: {
		run: slashDictionary,
		options: [
			{
				name: "word",
				type: ApplicationCommandOptionType.String,
				description: "The word to look up",
				required: true,
			},
		],
	},
});

addCommand({
	name: "dictionarytest",
	run: commandFullDefinitions,
	category: "miscellaneous",
	usage: "<word>",
	description: "Looks up a word in the Merriam Webster dictionary and gives the full definitions",
});
