export interface ApiResponse {
	meta: {
		/** headword or headword:homonym number */
		id: string;
		/** universally unique identifier */
		uuid: string;
		/** "a 9-digit code which may be used to sort entries in the proper dictionary order if alphabetical display is needed" */
		sort: string;
		/** "source data set for entry—ignore" */
		src: string;
		/** "indicates the section the entry belongs to in print, where "alpha" indicates the main alphabetical section, "biog" biographical, "geog" geographical, and "fw&p" the foreign words & phrases section." */
		section: string;
		/** "lists all of the entry's headwords, variants, inflections, undefined entry words, and defined run-on phrases. Each stem string is a valid search term that should match this entry." */
		stems: string[];
		/** "true if there is a label containing "offensive" in the entry; otherwise, false" */
		offensive: boolean;
	};
	/** homonym number if there are homonyms */
	hom?: number;
	/** headword information */
	hwi: {
		/** headword */
		hw: string;
		/** pronunciations */
		prs?: Pronunciation[];
	};
	/** alternate headwords */
	ahws?: AlternateHeadword[];
	/** variants */
	vrs?: Variant[];
	/** functional label (grammatical function) */
	fl?: string;
	/** general labels */
	lbs?: string[];
	/** subject/status labels (subject area or regional/usage status) */
	sls?: string[];
	/** parenthesized subect/status label (subject area or regional/usage status typically shown in parentheses)*/
	psl?: string;
	/** sense-specific inflection plural label (notes on singular/plural) */
	spl?: string;
	/** sense-specific grammatical label (transitive/intransitive) */
	sgram?: string;
	/** inflections */
	ins?: Inflection[];
	/** cognate cross-references */
	cxs?: CognateCrossReference[];
	/** definitions */
	def: Definition[];
	/** "An undefined entry word is derived from or related to the headword, carries a functional label and possibly other information, but does not have any definitions." */
	uros?: UndefinedRunOn[];
	/** "A defined run-on phrase is an expression or phrasal verb formed from the headword. This phrase, its definition section, and other information such as pronunciations, together make up a defined run-on." */
	dros?: DefinedRunOn[];
	/** "Directional cross-references to other entries may be presented after the main definition section" */
	dxnls?: string[];
	/** more extensive usage discussion */
	usages?: Usage[];
	/** synonym discussion */
	syns?: SynonymDiscussion[];
	/** quotes */
	quotes?: Quote[];
	/** artwork */
	art?: Art;
	/** "A reference from an entry to a table" */
	table?: Table;
	/** etymology */
	et?: Etymology;
	/** first known use */
	date: string;
	/** abridged definitions, only first three senses */
	shortdef: string[];
}
interface AlternateHeadword {
	/** headword */
	hw: string;
	/** pronunciations */
	prs?: Pronunciation[];
	/** "parenthesized subject/status label" */
	psl?: string;
}
interface Variant {
	/** variant */
	va: string;
	/** "variant label, such as 'or'"" */
	vl?: string;
	/** pronunciations */
	prs?: Pronunciation[];
	/** "sense-specific inflection plural label" */
	spl?: string;
}
interface Pronunciation {
	/** Merriam-Webster format */
	mw?: string;
	/** "pronunciation label before pronunciation" */
	l?: string;
	/** "pronunciation label after pronunciation" */
	l2?: string;
	/** punctuation to separate pronunciation objects */
	pun?: string;
	/** audio playback information */
	audio?: {
		/** base filename for audio playback */
		audio: string;
		/** ignore */
		ref: string;
		/** ignore */
		stat: string;
	};
}
interface Inflection {
	/** inflection */
	if?: string;
	/** inflection cutback (ending, like "-ing") */
	ifc?: string;
	/** inflection label (like "also, "plural", "or") */
	il?: string;
	/** pronunciations */
	prs?: Pronunciation[];
	/** "sense-specific inflection plural label" */
	spl?: string;
}
interface CognateCrossReference {
	/** cognate cross-reference label */
	cxl: string;
	/** cognate cross-reference targets */
	cxtis: {
		//** cognate cross-reference label */
		cxl: string;
		//** "when present, use as cross-reference target ID for immediately preceding cxt" */
		cxr?: string;
		/** "provides hyperlink text in all cases, and cross-reference target ID when no immediately following cxr" */
		cxt: string;
		/** "sense number of cross-reference target" */
		cxn: string;
	}[];
}
interface Definition {
	/** verb divider ("transitive verb", "intransitive verb") */
	vd?: string;
	/** "sense sequence", truncated "sen"se is a sense without definition, "b"inding"s"ubstitute is a broad, general sense introducing other senses */
	sseq?: (
		| ["sense", Sense]
		| ["sen", TruncatedSense]
		| ["bs", Sense]
		| ["pseq", (["sense", Sense] | ["sen", TruncatedSense] | ["bs", Sense])[]]
		| (Sense | ["bs", Sense])[]
	)[][];
}
interface Sense {
	/** sense number, like "1 a" or "b", identifying which sense or subsense in a series */
	sn?: string;
	/** defining text (the actual text of the definition), likely to contain formatting {tokens} */
	dt: DefiningText[];
	/** etymology */
	et?: Etymology;
	/** inflections */
	ins?: Inflection[];
	/** general labels */
	lbs?: string[];
	/** pronunciations */
	prs?: Pronunciation[];
	/** divided sense, a sense closely relating to the parent sense */
	sdsense?: DividedSense[];
	/** sense-specific grammatical label (transitive/intransitive) */
	sgram?: string;
	/** subject/status labels (subject area or regional/usage status) */
	sls?: string[];
	/** variants */
	vrs?: Variant[];
}
/** a sense closely relating to the parent sense */
interface DividedSense {
	/** sense divider */
	sd: string;
	/** defining text (the actual text of the definition), likely to contain formatting {tokens} */
	dt: DefiningText[];
	/** etymology */
	et?: Etymology;
	/** inflections */
	ins?: Inflection[];
	/** general labels */
	lbs?: string[];
	/** pronunciations */
	prs?: Pronunciation[];
	/** sense-specific grammatical label (transitive/intransitive) */
	sgram?: string;
	/** subject/status labels (subject area or regional/usage status) */
	sls?: string[];
	/** variants */
	vrs?: Variant[];
}

type DefiningText = (
	| ["text", string]
	| ["vis", VerbalIllustration[]]
	| ["ri", RunIn[]]
	| ["bnw", BiographicalNameWrap]
	| ["ca", CalledAlsoNote]
	| ["snote", SupplementalInformationNote[]]
	| ["uns", UsageNotes[]]
)[];

interface TruncatedSense {
	/** sense number, like "1 a" or "b", identifying which sense or subsense in a series */
	sn?: string;
	/** etymology */
	et?: Etymology;
	/** inflections */
	ins?: Inflection[];
	/** general labels */
	lbs?: string[];
	/** pronunciations */
	prs?: Pronunciation[];
	/** sense-specific grammatical label (transitive/intransitive) */
	sgram?: string;
	/** subject/status labels (subject area or regional/usage status) */
	sls?: string[];
	/** variants */
	vrs?: Variant[];
}
/** verbal illustration (example usage) */
interface VerbalIllustration {
	/** the text */
	t: string;
	/** "author quotation" */
	aq?: AttributionOfQuote;
}
interface AttributionOfQuote {
	/** name of author */
	auth?: string;
	/** source work */
	source?: string;
	/** publication date */
	aqdate?: string;
	/** further details, such as larger work in which an essay is found */
	subsource?: {
		/** source work */
		source?: string;
		/** publication date */
		aqdate?: string;
	};
}
/** "A run-in entry word is a defined word that occurs in the running text of an entry. The run-in ri groups together one or more run-in entry words rie and any accompanying pronunciations or variants. Run-ins occur most frequently in geographical entries." */
type RunIn = ["riw", RunInWrap][];
type RunInWrap = RunInEntryWord | Pronunciation | ["text", string] | Variant;
interface RunInEntryWord {
	/** "run-in entry word" */
	rie: string;
}
/** "A biographical name wrap groups together personal name, surname, and alternate name information within a biographical entry." */
interface BiographicalNameWrap {
	/** personal/first name */
	pname?: string;
	/** surname */
	sname?: string;
	/** alternate name, such as pen name, nickname, title etc. */
	altname?: string;
}
/** "A called-also note lists other names a headword is called in a given sense." */
interface CalledAlsoNote {
	/** contains introductory text "called also" */
	intro?: string;
	/** "called-also targets" */
	cats?: {
		/** "called-also target text" */
		cat?: string;
		/** "called-also reference containing target ID" */
		catref?: string;
		/** parenthesized number */
		pn?: string;
		/** pronunciations */
		prs?: Pronunciation[];
		/** parenthesized subect/status label (subject area or regional/usage status typically shown in parentheses)*/
		psl?: string;
	}[];
}
/** "This note provides explanatory or historical information that supplements the definition text." */
type SupplementalInformationNote = (
	| ["t", string]
	| ["ri", RunIn[] | ["vis", VerbalIllustration[]]]
)[];
/** "Provide usage information on a headword, defined run-on phrase, or undefined entry word" */
type UsageNotes = (["text", string] | ["ri", RunIn[] | ["vis", VerbalIllustration[]]])[];
interface UndefinedRunOn {
	/** undefined entry word */
	ure?: string;
	/** functional label (grammatical function) */
	fl?: string;
	utxt?: (["vis", VerbalIllustration[]] | ["uns", UsageNotes[]])[];
	/** inflections */
	ins?: Inflection[];
	/** general labels */
	lbs?: string[];
	/** pronunciations */
	prs?: Pronunciation[];
	/** parenthesized subect/status label (subject area or regional/usage status typically shown in parentheses)*/
	psl?: string;
	/** subject/status labels (subject area or regional/usage status) */
	sls?: string[];
	/** variants */
	vrs?: Variant[];
}
interface DefinedRunOn {
	/** "run-on objects" */
	dros: {
		/** "defined run-on phrase" */
		drp?: string;
		/** definitions */
		def: Definition[];
		/** etymology */
		et?: Etymology;
		/** general labels */
		lbs?: string[];
		/** pronunciations */
		prs?: Pronunciation[];
		/** parenthesized subect/status label (subject area or regional/usage status typically shown in parentheses)*/
		psl?: string;
		/** subject/status labels (subject area or regional/usage status) */
		sls?: string[];
		/** variants */
		vrs?: Variant[];
	}[];
}
interface Usage {
	/** paragraph label, to show at top of section */
	pl: string;
	pt: (["text", string] | ["vis", VerbalIllustration[]])[];
	uarefs: ["uaref", string][];
}
interface SynonymDiscussion {
	/** paragraph label, to show at top of section */
	pl: string;
	pt: (["text", string] | ["vis", VerbalIllustration[]])[];
	sarefs: string[];
}
interface Quote {
	/** quotation text */
	t: string;
	/** "author quotation" */
	aq: AttributionOfQuote;
}
interface Art {
	/** filename of target image, https://www.merriam-webster.com/assets/mw/static/art/dict/{artid}.gif */
	artid: string;
	/** image caption text */
	capt: string;
}
interface Table {
	/** "ID of the target table", https://www.merriam-webster.com/table/collegiate/[base filename].htm */
	tableid: string;
	/** "text to display in table link" */
	displayname: string;
}
type Etymology = (["text", string] | ["et_snote", ["t", string][]])[];
