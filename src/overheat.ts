import { log } from "./log.js";

import { db } from "./db.js";
import { Network } from "./common_types.js";

/** How much heat a channel loses per ms. 0.005 amounts to 5 heat/s, 0.2 s/heat */
const channelHeatDecay = 0.005;
const channelHeat = {
	twitch: new Map(),
	discord: new Map(),
};

/**
 * Adds heat to a channel, and returns the new amount
 */
export const addChannelHeat = (network: Network, channel: string, amount: number): number => {
	const now = Date.now();
	const { heat, timestamp } = channelHeat[network].get(channel) || {
		heat: 0,
		timestamp: 0,
	};
	const time_passed = heat ? now - timestamp : 0;
	const new_heat = heat + amount - channelHeatDecay * time_passed;
	channelHeat[network].set(channel, { heat: new_heat, timestamp: now });
	return new_heat;
};

/** How much heat a user loses per ms. 0.00004 amounts to 0.04 heat/s, 25 s/heat */
const userHeatDecay = 0.00004;
const userHeat: {
	[key in Network]: Map<string, { heat: number; timestamp: number }>;
} = {
	twitch: new Map(),
	discord: new Map(),
};
const userMarks: { [key in Network]: Map<string, number[]> } = {
	twitch: new Map(),
	discord: new Map(),
};
const userBans: { [key in Network]: Map<string, number | "indefinite"> } = {
	twitch: new Map(),
	discord: new Map(),
};

/**
 * Check whether a user is banned, updating and saving to db if a ban expired
 */
export const checkBan = (network: Network, user: string): boolean => {
	if (!userBans[network].has(user)) return false;

	const ban = userBans[network].get(user);
	if (ban && ban !== "indefinite" && ban < Date.now()) {
		userBans[network].delete(user);
		log(`Ban on ${user} expired.`);
		saveBansAndMarks(network, user);
		return false;
	}
	return true;
};

/**
 * Adds heat to a user and marks if necessary
 */
export const addUserHeat = (network: Network, user: string, amount: number): void => {
	const now = Date.now();
	const { heat, timestamp } = userHeat[network].get(user) || {
		heat: 0,
		timestamp: 0,
	};
	const time_passed = heat ? now - timestamp : 0;
	const new_heat = heat + amount - userHeatDecay * time_passed;
	userHeat[network].set(user, { heat: new_heat, timestamp: now });
	if (new_heat > 12) {
		markUser(network, user);
	} else if (new_heat > 9) {
		log(`${user} is getting close to using too many commands. (heat: ${new_heat}/12)`);
	}
};

/** Adds the base level heat that every command usage adds */
export const addBaseUserHeat = (network: Network, user: string): void => {
	addUserHeat(network, user, 1);
};

/** Adds the heat that scales with the message's length */
export const addScalingUserHeat = (network: Network, user: string, length: number): void => {
	const heat = Math.max(0, Math.min((length - 80) / 80, 2));
	if (heat > 0) {
		addUserHeat(network, user, heat);
	}
};

/**
 * Adds a mark to a user, bans the appropriate duration, and saves to db
 */
const markUser = (network: Network, user: string): void => {
	const marks = userMarks[network].get(user) || [];
	const now = Date.now();
	marks.push(now);

	const marksTwoWeeks = marks.filter((x) => now - x < 14 * 24 * 60 * 60 * 1000); // 2 weeks
	userMarks[network].set(user, marksTwoWeeks);
	if (marksTwoWeeks.length > 3) {
		userBans[network].set(user, "indefinite");
		log(`${user} will be ignored indefinitely.`);
		saveBansAndMarks(network, user);
		return;
	}
	const marksDay = marksTwoWeeks.filter((x) => now - x < 24 * 60 * 60 * 1000); // 1 day
	if (marksDay.length > 1) {
		userBans[network].set(user, now + 2 * 24 * 60 * 60 * 1000); // 2 days
		log(`${user} will be ignored for 2 days.`);
		saveBansAndMarks(network, user);
		return;
	}
	userBans[network].set(user, now + 60 * 60 * 1000); // 1 hour
	log(`${user} will be ignored for 1 hour.`);
	saveBansAndMarks(network, user);
	return;
};

/**
 * Saves a user's bans and marks to db
 */
const saveBansAndMarks = (network: Network, user: string): void => {
	const ban = userBans[network].get(user);
	const [mark1, mark2, mark3] = userMarks[network].get(user) || [];

	const qSave = `
		INSERT INTO
			marks_and_bans(network, user, ban, mark1, mark2, mark3)
		VALUES
			($network, $user, $ban, $mark1, $mark2, $mark3)
	`;
	db.prepare(qSave).run({ network, user, ban, mark1, mark2, mark3 });
};

/**
 * Loads all users' bans and marks from db
 */
const loadBansAndMarks = (): void => {
	const now = Date.now();

	const qLoad = `
		SELECT
			network, user, ban, mark1, mark2, mark3
		FROM
			marks_and_bans
	`;
	const results = db.prepare(qLoad).all();
	for (const { network, user, ban, mark1, mark2, mark3 } of results as {
		network: Network;
		user: string;
		ban: number | "indefinite";
		mark1?: number;
		mark2?: number;
		mark3?: number;
	}[]) {
		if ((typeof ban === "number" && ban > now) || ban === "indefinite") {
			userBans[network].set(user, ban);
		}
		const marks = [mark1, mark2, mark3];
		const markArray = [];
		for (const mark of marks) {
			if (mark) {
				markArray.push(mark);
			}
		}
		if (markArray.length > 0) {
			userMarks[network].set(user, markArray);
		}
	}
};

loadBansAndMarks();
