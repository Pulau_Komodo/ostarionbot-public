import { PermissionFlagsBits } from "discord.js";

export const canSend = (channel: DiscordGuildTextChannel): boolean => {
	const me = channel.guild.members.me;
	if (!me) return false;
	return channel.permissionsFor(me).has(PermissionFlagsBits.SendMessages);
};

export const canSendEmbed = (channel: DiscordGuildTextChannel): boolean => {
	const me = channel.guild.members.me;
	if (!me) return false;
	if (!canSend(channel)) return false;
	return channel.permissionsFor(me).has(PermissionFlagsBits.EmbedLinks);
};

export const canSendAttachment = (channel: DiscordGuildTextChannel): boolean => {
	const me = channel.guild.members.me;
	if (!me) return false;
	if (!canSend(channel)) return false;
	return channel.permissionsFor(me).has(PermissionFlagsBits.AttachFiles);
};
