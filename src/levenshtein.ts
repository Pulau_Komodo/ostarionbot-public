import { addCommand, CommandFunction } from "./commands.js"; // Map of commands

const commandLevenshtein: CommandFunction = function (cCall) {
	const trimmedArgs = cCall.commaArgs.map((arg) => arg.trim());
	if (trimmedArgs[0] !== undefined && trimmedArgs[1] !== undefined) {
		cCall.reply(`Levenshtein distance is ${levenshtein(trimmedArgs[0], trimmedArgs[1])}`);
	} else {
		cCall.replyUsage();
	}
};

/**
 * Gets the Levenshtein distance between two strings
 */
const levenshtein = (a: string, b: string): number => {
	// degenerate cases
	if (a === b) return 0;
	if (a.length === 0) return b.length;
	if (b.length === 0) return a.length;

	let distance = 0;

	const cache = Array<number>(a.length);
	for (let i = 0; i < a.length; i++) {
		cache[i] = i + 1;
	}
	let distanceA: number;
	let distanceB: number;

	for (let indexB = 0; indexB < b.length; indexB++) {
		const charB = b[indexB]!;
		distanceA = indexB;

		for (let indexA = 0; indexA < a.length; indexA++) {
			const charA = a[indexA]!;
			distanceB = charA === charB ? distanceA : distanceA + 1;

			distanceA = cache[indexA]!;

			if (distanceA > distance) {
				if (distanceB > distance) {
					distance++;
				} else {
					distance = distanceB;
				}
			} else if (distanceB > distanceA) {
				distance = distanceA + 1;
			} else {
				distance = distanceB;
			}

			cache[indexA] = distance;
		}
	}

	return distance;
};

addCommand({
	name: "levenshtein",
	run: commandLevenshtein,
	category: "miscellaneous",
	usage: "<text 1>,<text 2>",
	description: "Calculates the Levenshtein distance between two strings.",
});
