import { db } from "../db.js";
import { addCommand, DiscordCommandFunction, SlashCommandFunction } from "../commands.js";
import { discordclient } from "../discordclient.js";
import { log } from "../log.js";

import { defaultColours } from "../colour_names/colour_names.js";

import {
	ApplicationCommandOptionType,
	escapeMarkdown,
	Guild,
	GuildMember,
	PartialGuildMember,
	PermissionFlagsBits,
	Role,
	TextChannel,
	ThreadChannel,
} from "discord.js";
import { CommandError } from "../Errors.js";
import { stringifyInterval } from "interval-conversions";
import { canSendAttachment } from "../permission checks.js";
import { outputPossiblyWithAttachment } from "../colour_names/commands.js";

/** Returns input if it's only digits, returns ID if it looks like a user mention, and undefined if it fails both */
const parseUserMentionOrId = (text: string): string | undefined => {
	if (/^[0-9]+$/.test(text)) return text;
	const result = /^<@\u200b!?(\d+)>$/.exec(text);
	if (result) return result[1];
	return;
};

/** Returns input if it's only digits, returns ID if it looks like a user mention, and undefined if it fails both */
const parseRoleMentionOrId = (text: string): string | undefined => {
	if (/^[0-9]+$/.test(text)) return text;
	const result = /^<@\u200b&(\d+)>$/.exec(text);
	if (result) return result[1];
	return;
};

/** Apply the personal role a user is supposed to have according to the database, to be run on any user join */
const addPersonalRoles = async (member: GuildMember | PartialGuildMember) => {
	if (!(member instanceof GuildMember)) return;
	if (!member.guild.members.me?.permissions.has(PermissionFlagsBits.ManageRoles)) return;
	const { roleid } = queryCheckRole(member.guild.id, member.id);
	if (!roleid) return;
	const role = await member.guild.roles.fetch(roleid).catch(console.log);
	if (!role) return console.log(`Missing personal role ${roleid} for ${member.id}`);
	if (member.guild.members.me.roles.highest.comparePositionTo(role) <= 0)
		return console.log(`Role ${roleid} position too high to assign`);
	member.roles.add(role).catch(console.log);
};

discordclient.on("guildMemberAdd", addPersonalRoles);

/** Assigns an existing role to a member as their personal role, removing their previous role if applicable
 * @returns Output string
 */
const assignRole = async (member: GuildMember, role: Role): Promise<string> => {
	const guildId = member.guild.id;
	const userId = member.id;

	if (!(await member.guild.roles.fetch(role.id).catch(console.log)))
		return "That role does not exist in this guild";

	const { roleid: oldRoleId } = queryCheckRole(guildId, userId);
	const oldRole = oldRoleId ? member.roles.cache.get(oldRoleId) : undefined;
	if (oldRoleId && oldRoleId !== role.id) {
		queryRemoveRole(guildId, userId);
		if (oldRole) {
			const comparePosition = member.guild.members.me?.roles.highest.comparePositionTo(role);
			if (comparePosition && comparePosition > 0) {
				member.roles.remove(oldRoleId).catch((error) => {
					log(error);
					throw new CommandError("Error with removing previous role");
				});
			} else {
				console.log(`Lacked permission to remove previous role`);
			}
		}
	}

	const alreadyRole = member.roles.cache.get(role.id);
	let addedRole;
	if (!alreadyRole) {
		addedRole = await member.roles.add(role).catch((error) => {
			log(error);
			throw new CommandError("Error with adding role");
		});
	}

	if (oldRoleId === role.id) {
		if (alreadyRole) {
			return `User already had that role assigned`;
		}
		if (addedRole) {
			return `User role of ${
				member.displayName
			} was already ${role.toString()}, but now it is also assigned on Discord.`;
		}
		throw new Error();
	}

	querySetRole(guildId, userId, role.id);

	if (oldRoleId) {
		if (oldRole) {
			return `Changed user role of ${
				member.displayName
			} from ${oldRole.toString()} to ${role.toString()}.`;
		}
		return `Changed user role of ${
			member.displayName
		} from an invalid role to ${role.toString()}.`;
	}
	return `Assigned ${role.toString()} to ${member.displayName}.`;
};

/** Creates a role for the specified member, then assigns it, unassigning their previous one if applicable
 * @returns created role
 */
const makeRole = async (member: GuildMember): Promise<Role> => {
	const { roleid: oldRoleId } = queryCheckRole(member.guild.id, member.id);
	const oldRole = oldRoleId ? member.roles.cache.get(oldRoleId) : undefined;
	if (oldRoleId) {
		queryRemoveRole(member.guild.id, member.id);
		if (oldRole) {
			const comparePosition =
				member.guild.members.me?.roles.highest.comparePositionTo(oldRole);
			if (comparePosition && comparePosition > 0) {
				await member.roles.remove(oldRoleId).catch((error) => {
					log(error);
					throw new CommandError("Error with removing previous role");
				});
			} else {
				console.log(`Lacked permission to remove previous role`);
			}
		}
	}
	const newRole = await member.guild.roles
		.create({
			name: member.displayName,
			position: member.guild.roles.everyone.position,
			permissions: [],
			hoist: false,
			mentionable: false,
			reason: `Personal role created via makerole command`,
		})
		.catch((error) => {
			console.log(error);
			throw new Error();
		});
	await member.roles.add(newRole).catch((error) => {
		log(error);
		throw new CommandError("Error with adding role");
	});

	querySetRole(member.guild.id, member.id, newRole.id);

	return newRole;
};

type HexColour = `#${string}`;

/** @returns [role mention, escaped member display name, new colour hex, old colour hex] */
const setRoleColour = async (
	member: GuildMember,
	colour: HexColour,
): Promise<[string, string, string, string]> => {
	const { roleid, lastchanged: lastChanged = 0 } = queryCheckRole(member.guild.id, member.id);
	if (!roleid) throw new CommandError(`User has no role assigned`);
	const role = await member.guild.roles.fetch(roleid).catch(console.log);
	if (!role) throw new CommandError(`User's assigned role does not seem to exist`);

	const comparePosition = member.guild.members.me?.roles.highest.comparePositionTo(role);
	if (comparePosition && comparePosition <= 0)
		throw new CommandError(`I lack permission to change that role`);

	const whenAllowed = lastChanged + 24 * 60 * 60 * 1000 - Date.now();
	if (whenAllowed > 0)
		throw new CommandError(
			`Colour change on cooldown for another ${stringifyInterval(whenAllowed)}`,
		);

	const oldColour = role.hexColor.slice(1).toUpperCase();

	await role.setColor(colour, `Colour changed via setcolour command`).catch((error) => {
		log(error);
		throw new CommandError("Error with setting colour");
	});

	const qSetLastChanged = `
		UPDATE user_roles
		SET lastchanged = $lastchanged
		WHERE guild = $guildid AND user = $userid
	`;
	db.prepare(qSetLastChanged).run({
		guildid: member.guild.id,
		userid: member.id,
		lastchanged: Date.now(),
	});

	const setColour = colour.slice(1).toUpperCase();
	return [role.toString(), escapeMarkdown(member.displayName), setColour, oldColour];
};

/**
 * Finds the current role assigned to a user in a guild in the database
 * @returns Role ID and ms timestamp of the most recent user change, both undefined if it couldn't find a role
 */
const queryCheckRole = (
	guildid: string,
	userid: string,
): { roleid: string | undefined; lastchanged: number | undefined } => {
	const qCheckRole = `
		SELECT role, lastchanged
		FROM user_roles
		WHERE guild = $guildid AND user = $userid
	`;
	const result = db.prepare(qCheckRole).get({ guildid, userid }) as
		| { role: string; lastchanged: number }
		| undefined;
	if (result) return { roleid: result.role, lastchanged: result.lastchanged };
	return { roleid: undefined, lastchanged: undefined };
};

/** Assigns the role to the user for that guild in the database */
const querySetRole = (guildid: string, userid: string, roleid: string) => {
	const qSetRole = `
		INSERT INTO user_roles(guild, user, role)
		VALUES ($guildid, $userid, $roleid)
	`;
	db.prepare(qSetRole).run({ guildid, userid, roleid });
};

/**
 * Removes any current roles assigned to a user in a guild from the database
 * @returns The number of roles deleted (typically 1)
 */
const queryRemoveRole = (guildid: string, userid: string): number => {
	const qDeleteRole = `
		DELETE FROM user_roles
		WHERE guild = $guildid AND user = $userid
	`;
	const deleteresult = db.prepare(qDeleteRole).run({ guildid, userid });
	return deleteresult.changes;
};

const commandAssignRole: DiscordCommandFunction = async (cCall) => {
	if (!cCall.message.member.permissions.has(PermissionFlagsBits.ManageRoles))
		throw new CommandError(`User lacks permission to change roles`);
	if (!cCall.message.guild.members.me?.permissions.has(PermissionFlagsBits.ManageRoles))
		throw new CommandError(`I lack permission to change roles`);

	const userId = parseUserMentionOrId(cCall.args[0] ?? "");
	const roleId = parseRoleMentionOrId(cCall.args[1] ?? "");
	if (!userId || !roleId) throw new CommandError();

	const member = await cCall.message.guild.members.fetch(userId).catch(console.log);
	if (!member) throw new CommandError(`No such user in this guild`);
	const role = await cCall.message.guild.roles.fetch(roleId).catch(console.log);
	if (!role) throw new CommandError(`No such role in this guild`);

	if (cCall.message.guild.members.me.roles.highest.comparePositionTo(role) <= 0)
		throw new CommandError(`I lack the permission to assign that role`);

	const output = await assignRole(member, role).catch(console.log);
	if (!output) throw new CommandError("Something went wrong assigning the role");
	cCall.reply(output);
};

const commandMakeRole: DiscordCommandFunction = async (cCall) => {
	if (!cCall.message.member.permissions.has(PermissionFlagsBits.ManageRoles))
		throw new CommandError(`User lacks permission to change roles`);
	if (!cCall.message.guild.members.me?.permissions.has(PermissionFlagsBits.ManageRoles))
		throw new CommandError(`I lack permission to change roles`);
	const userId = parseUserMentionOrId(cCall.args[0] ?? "");
	if (!userId) throw new CommandError();
	const member = await cCall.message.guild.members.fetch(userId).catch(console.log);
	if (!member) throw new CommandError(`No such user in this guild`);

	const role = await makeRole(member);
	cCall.reply(`Created ${role.toString()} for ${escapeMarkdown(member.displayName)}.`);
};

const setColour = async (
	argument: string,
	guild: Guild,
	member: GuildMember,
	canAttach: boolean,
) => {
	const arg = argument.toUpperCase();

	if (!guild.members.me?.permissions.has(PermissionFlagsBits.ManageRoles))
		throw new CommandError("I lack permission to change roles");

	// Looks like hex colour code, like "FF0000", or is "RANDOM"
	if (/^(?:#?[0-9A-F]{6}|RANDOM)$/.test(arg)) {
		const [output, oldColour, newColour] = await handleHexOrRandom(arg, member);
		return outputPossiblyWithAttachment(output, oldColour, newColour, canAttach);
	}

	// May be a colour name, like "BURNT UMBER"
	if (/^[A-Z ]+$/i.test(arg)) {
		const [output, oldColour, newColour] = await handleColourName(arg, member);
		return outputPossiblyWithAttachment(output, oldColour, newColour, canAttach);
	}
};

const handleHexOrRandom = async (
	arg: string,
	member: GuildMember,
): Promise<[string, string, string]> => {
	const getHexColour = (text: string): HexColour => {
		if (arg === "RANDOM") {
			return `#${Math.floor(Math.random() * 0x1000000)
				.toString(16)
				.padStart(6, "0")}`;
		} else if (text.startsWith("#")) {
			return text as HexColour;
		} else {
			return `#${text}`;
		}
	};
	const colour = getHexColour(arg);

	const [roleMention, displayName, newColour, oldColour] = await setRoleColour(member, colour);

	let output = `Colour for ${roleMention} of ${displayName} changed from ${oldColour} to ${newColour}, which `;

	const foundColour = defaultColours.rgbToName(newColour);
	if (!foundColour) {
		// Should not be possible
		output += "somehow failed to match any colour.";
		return [output, oldColour, newColour];
	}

	const { hex, name, exact, distance, equidistantColours } = foundColour;

	if (exact) {
		output += `is an exact match for ${name}.`;
	} else {
		output += `is closest to ${name} (${hex}). Distance: ${Math.round(distance)}.`;
		if (equidistantColours.length > 0) {
			output += ` Equidistant colour${
				equidistantColours.length !== 1 ? "s" : ""
			}: ${equidistantColours.join(", ")}.`;
		}
	}
	return [output, oldColour, newColour];
};

const handleColourName = async (
	arg: string,
	member: GuildMember,
): Promise<[string, string, string]> => {
	const namedColour = defaultColours.nameToRgb(arg);
	if (!namedColour) throw new CommandError(`No colour found by that name`);

	const [roleMention, displayName, newColour, oldColour] = await setRoleColour(
		member,
		`#${namedColour.hex}`,
	);

	const output = `Colour for ${roleMention} of ${displayName} changed from ${oldColour} to ${namedColour.name} (${newColour}).`;

	return [output, oldColour, newColour];
};

const commandSetColour: DiscordCommandFunction = async (cCall) => {
	const result = await setColour(
		cCall.argstring,
		cCall.message.guild,
		cCall.message.member,
		canSendAttachment(cCall.channel),
	);
	if (!result) throw new CommandError();
	if (typeof result === "string") {
		cCall.reply(result);
	} else {
		cCall.replyWithAddition(result.content, result.files[0]);
	}
};

const slashSetColour: SlashCommandFunction = async (interaction) => {
	if (!interaction.guild) throw new CommandError("Could not retrieve guild");
	const canAttach =
		interaction.channel &&
		(interaction.channel instanceof TextChannel || interaction.channel instanceof ThreadChannel)
			? canSendAttachment(interaction.channel)
			: false;
	if (!(interaction.member instanceof GuildMember))
		throw new CommandError("Could not retrieve humans");
	const result = await setColour(
		interaction.options.getString("colour", true),
		interaction.guild,
		interaction.member,
		canAttach,
	);
	if (!result) throw new CommandError("Invalid input");
	return result;
};

addCommand({
	name: "assignrole",
	run: commandAssignRole,
	category: "role",
	usage: "<user> <role>",
	description:
		"Assigns the chosen user the chosen role as a personal role. Manage roles permission required.",
	discordOnly: true,
});
addCommand({
	name: "makerole",
	run: commandMakeRole,
	category: "role",
	usage: "<user>",
	description:
		"Creates a new personal role for the chosen user. Manage roles permission required.",
	discordOnly: true,
});
addCommand({
	name: "setcolour",
	run: commandSetColour,
	category: "role",
	usage: '<colour or "random">',
	description:
		"Changes the colour of the personal role of the user. 24 hour cooldown. Example colour format: 1E90FF.",
	aliases: ["setcolor", "changecolour", "changecolor"],
	slash: {
		run: slashSetColour,
		description: "Changes the colour of the personal role of the user. 24 hour cooldown.",
		options: [
			{
				name: "colour",
				type: ApplicationCommandOptionType.String,
				description:
					'A specific colour, like "deep blue", or a colour code, like "#220878", or "random"',
				required: true,
			},
		],
	},
	discordOnly: true,
});
