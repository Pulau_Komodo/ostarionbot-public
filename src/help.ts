import {
	addCommand,
	getCommand,
	listCommands,
	listCategories,
	CommandFunction,
} from "./commands.js"; // Map of commands
import { edgyDomains, prefixes } from "./settings.js";

const help: CommandFunction = (cCall) => {
	const prefix = cCall.prefix;

	const argument = cCall.args[0]?.toLowerCase();
	if (!argument) {
		cCall.reply(
			`Usage: ${prefix}${cCall.command}${cCall.usage} - Use ${prefix}commands to explore available commands.`,
		);
		return;
	}

	const command = getCommand(argument);
	if (!command) {
		cCall.reply(
			`No command ${prefix}${argument}. Use ${prefix}commands to explore available commands.`,
		);
		return;
	}
	let output = `Usage: ${prefix}${command.name}${command.usage}`;
	if (command.description) {
		output += ` - ${command.description}`;
	}
	if (command.discordOnly) {
		if (cCall.network === "discord") output += ` Discord only.`;
		else output = `Discord only! ${output}`;
	}
	if (command.edgy) output += ` (edgy)`;
	if (command.aliases.length > 0) {
		output += command.aliases.length === 1 ? " Alias: " : " Aliases: ";
		output += command.aliases.join(", ");
	}
	cCall.reply(output);
};

const commands: CommandFunction = (cCall) => {
	//	if (cCall.network === "twitch") {cCall.reply("); return;}

	const twitch = cCall.network === "twitch";
	const discord = cCall.network === "discord";
	const edgy = edgyDomains.has(cCall.domain);

	const argument = cCall.args[0]?.toLowerCase();
	if (!argument) {
		const categoryList = listCategories({ discord, edgy });
		const prefix = prefixes.get(cCall.domain);
		if (!prefix) throw new Error(`Prefix for ${cCall.domain} is somehow unset`);
		let output = `Command categories: ${categoryList}. Use ${prefix}${cCall.command} <category> to get a list of commands from that category.`;
		if (twitch) output += ` See also: https://www.twitch.tv/ostarionbot`;
		cCall.reply(output);
		return;
	}

	const commandList = listCommands(argument, { discord, edgy });

	if (commandList === undefined) {
		cCall.reply("No such category");
		return;
	}
	if (commandList === "") {
		cCall.reply("No commands in that category left after applying filters");
		return;
	}
	cCall.reply(`Commands in category ${argument}: ${commandList}.`);
};

addCommand({
	name: "help",
	run: help,
	category: "miscellaneous",
	usage: "<command>",
	description:
		"Gives usage and description of a command. Use commands command to get a list of commands.",
	heat: { fixed: 1 },
});
addCommand({
	name: "commands",
	run: commands,
	category: "miscellaneous",
	usage: "[category]",
	description:
		"On Discord, gives a list of command categories, or all the commands of a chosen category. On Twitch, links the command list.",
	heat: { fixed: 1 },
	aliases: ["commandlist"],
});
