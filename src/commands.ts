import {
	ApplicationCommandOptionData,
	ChatInputApplicationCommandData,
	ChatInputCommandInteraction,
	AttachmentBuilder,
	EmbedBuilder,
	MessageMentionOptions,
} from "discord.js";
import { CommandCall, DiscordCommandCall } from "./CommandCall.js";

const categories = [
	"miscellaneous",
	"event",
	"weather",
	"colour",
	"role",
	"emote",
	"fancytext",
	"twitch",
	"wiki",
	"spotify",
	"config",
	"debug",
] as const;

type CommandCategory = (typeof categories)[number];

export interface SlashReplyOptions {
	content?: string;
	embeds?: EmbedBuilder[];
	files?: AttachmentBuilder[];
	allowedMentions?: MessageMentionOptions;
	ephemeral?: boolean;
}

export type CommandFunction = (cCall: CommandCall) => void | Promise<void>;
export type DiscordCommandFunction = (cCall: DiscordCommandCall) => void | Promise<void>;
export type SlashCommandFunction = (
	interaction: ChatInputCommandInteraction,
) => SlashReplyOptions | string | undefined | Promise<SlashReplyOptions | string | undefined>;

interface BaseCommandOptions {
	/** The standard name of the command */
	readonly name: string;
	/** The function to call */
	readonly run: CommandFunction | DiscordCommandFunction;
	readonly category: CommandCategory;
	/** Usage notes like "<text>" */
	readonly usage?: string;
	/** Description for help command */
	readonly description: string;
	/** Alternative names by which this command can be invoked */
	readonly aliases?: readonly string[];
	/** The data needed to also add this as a slash command */
	readonly slash?: SlashCommandOptions;
	/** How much user heat this command generates */
	readonly heat?: {
		/** The fixed portion (default 1) */
		readonly fixed?: number;
		/** The heat per character from the argstring (default 0) */
		readonly perChar?: number;
	};
	/** Whether this command can only be used on Discord */
	readonly discordOnly?: boolean;
	/** Whether this command is edgy (requires edgy flag on on domain) */
	readonly edgy?: boolean;
}

interface GenericCommandOptions extends BaseCommandOptions {
	readonly run: CommandFunction;
}

interface DiscordCommandOptions extends BaseCommandOptions {
	readonly run: DiscordCommandFunction;
	readonly discordOnly: true;
}

type CommandOptions = GenericCommandOptions | DiscordCommandOptions;

interface SlashCommandOptions {
	readonly run: SlashCommandFunction;
	/** A description override for slash commands. Normal description is used if this is not set. */
	readonly description?: string;
	/** Arguments the command takes, in Discord's format */
	readonly options?: ApplicationCommandOptionData[];
}

interface BaseCommand {
	/** The standard name of the command */
	readonly name: string;
	/** The function to call */
	readonly run: CommandFunction | DiscordCommandFunction;
	readonly category: CommandCategory;
	/** Usage notes like "<text>" */
	readonly usage: string;
	/** Description for help command */
	readonly description: string;
	/** Alternative names by which this command can be invoked */
	readonly aliases: readonly string[];
	/** How much user heat this command generates */
	readonly heat: {
		/** The fixed portion (default 1) */
		readonly fixed: number;
		/** The heat per character from the argstring (default 0) */
		readonly perChar: number;
	};
	/** Whether this command can only be used on Discord */
	readonly discordOnly: boolean;
	/** Whether this command is edgy (requires edgy flag on on domain) */
	readonly edgy: boolean;
}

interface GenericCommand extends BaseCommand {
	readonly run: CommandFunction;
	readonly discordOnly: false;
}

interface DiscordCommand extends BaseCommand {
	readonly run: DiscordCommandFunction;
	readonly discordOnly: true;
}

export type Command = GenericCommand | DiscordCommand;

const commandMap: Map<string, Command> = new Map();
const aliasMap: Map<string, Command> = new Map();
const categoryMap: Map<string, Map<string, Command>> = new Map();

type SlashCommand = {
	run: SlashCommandFunction;
	edgy: boolean;
} & ChatInputApplicationCommandData;

export const slashCommandMap = new Map<string, SlashCommand>();

/**
 * Adds a command to the list
 */
export const addCommand = ({
	name,
	run,
	category,
	usage = "",
	description,
	aliases = [],
	slash,
	heat = { fixed: 1 },
	discordOnly = false,
	edgy = false,
}: CommandOptions): void => {
	if (usage !== "") usage = ` ${usage}`;
	const { fixed = 0, perChar = 0 } = heat;

	const commandObject = {
		name,
		run,
		category,
		usage,
		description,
		aliases,
		heat: {
			fixed,
			perChar,
		},
		discordOnly,
		edgy,
	} as Command;

	commandMap.set(name, commandObject);

	// Put each alias, Command in alias map
	for (const alias of aliases) {
		aliasMap.set(alias, commandObject);
	}

	// Put each command, Command in the specific category
	if (!categoryMap.has(category)) {
		categoryMap.set(category, new Map());
	}
	const commandCategory = categoryMap.get(category);
	if (commandCategory) {
		commandCategory.set(name, commandObject);
	}

	if (slash !== undefined) {
		if (!slash.description && description.length > 100)
			console.warn(
				`Command ${name} is having its description truncated for the slash command. Write a separate description for the slash command.`,
			);
		if (slash.options?.some((option) => option.description.length > 100))
			console.error(
				`Command ${name} has an option with a description over 100 characters long.`,
			);
		slashCommandMap.set(name, {
			name,
			run: slash.run,
			description: slash.description ?? description.slice(0, 100),
			options: slash.options,
			edgy,
		});
	}
};

export const addSlashCommand = (
	name: string,
	run: SlashCommandFunction,
	description: string,
	edgy: boolean,
	options?: ApplicationCommandOptionData[],
) => {
	if (options?.some((option) => option.description.length > 100))
		console.error(`Command ${name} has an option with a description over 100 characters long.`);
	slashCommandMap.set(name, {
		name,
		run,
		description,
		options,
		edgy,
	});
};

export const getCommand = (command: string): Command | undefined => {
	return commandMap.get(command) || aliasMap.get(command);
};

interface CommandFilters {
	/** Whether to include Discord-only commands */
	readonly discord: boolean;
	/** Whether to include edgy commands */
	readonly edgy: boolean;
}
export const listCommands = (
	category: string,
	{ discord, edgy }: CommandFilters,
): string | undefined => {
	const cat = categoryMap.get(category);
	if (!cat) return;
	const commands = [...cat]
		.filter(([, command]) => (!command.discordOnly || discord) && (!command.edgy || edgy))
		.map(([key]) => key);
	return commands.join(", ");
};

export const listCategories = ({ discord, edgy }: CommandFilters): string => {
	const categories = [...categoryMap]
		.map(
			([categoryName, map]) =>
				[
					categoryName,
					[...map].filter(
						([, command]) =>
							(!command.discordOnly || discord) && (!command.edgy || edgy),
					).length,
				] as const,
		)
		.filter(([, length]) => length > 0)
		.map(([categoryName, length]) => `${categoryName} (${length})`);
	return categories.join(", ");
};
