import { db } from "../db.js";

// Gets the Gamepedia and Liquipedia article link segments for a given topic, if they exist
export const queryTopic = (
	topic: string,
):
	| {
			gamepedia: string | null;
			liquipedia: string | null;
	  }
	| undefined => {
	const qTopic = `
		SELECT gamepedia, liquipedia
		FROM dota_2_wiki_articles
		WHERE topic = $topic
	`;
	const result = db.prepare(qTopic).get({ topic }) as {
		gamepedia: string | null;
		liquipedia: string | null;
	} | null;
	return result ?? undefined;
};

/** Adds the given topic with the given Gamepedia and Liquipedia article link segments */
export const queryAddTopic = (
	topic: string,
	gamepedia: string | null,
	liquipedia: string | null,
) => {
	const qAddTopic = `
		INSERT INTO dota_2_wiki_articles(topic, gamepedia, liquipedia)
		VALUES ($topic, $gamepedia, $liquipedia)
	`;
	db.prepare(qAddTopic).run({ topic, gamepedia, liquipedia });
};

/** Sets the given topic's Gamepedia and Liquipedia article link segments as given. Will overwrite with null if passed. */
export const queryUpdateArticles = (
	topic: string,
	gamepedia: string | null,
	liquipedia: string | null,
) => {
	const qUpdateArticles = `
		UPDATE dota_2_wiki_articles
		SET gamepedia = $gamepedia, liquipedia = $liquipedia
		WHERE topic = $topic
	`;
	db.prepare(qUpdateArticles).run({ topic, gamepedia, liquipedia });
};

/** Adds the given alias for the given topic */
export const queryAddAlias = (topic: string, alias: string) => {
	const qAddAlias = `
		INSERT INTO dota_2_wiki_aliases(topic, alias)
		VALUES ($topic, $alias)
	`;
	db.prepare(qAddAlias).run({ topic, alias });
};

/** Gets the topic for the given alias */
export const queryAlias = (alias: string): string | undefined => {
	const qAlias = `
		SELECT topic
		FROM dota_2_wiki_aliases
		WHERE alias = $alias
	`;
	return (db.prepare(qAlias).get({ alias }) as { topic: string } | undefined)?.topic ?? undefined;
};
