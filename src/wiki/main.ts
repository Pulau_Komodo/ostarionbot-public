import got from "got";
import { addCommand, CommandFunction } from "../commands.js";
import { Network } from "../common_types.js";
import { CommandError } from "../Errors.js";
import { isNonEmptyArray } from "../type_guards.js";
import { MediaWikiApiResponse } from "./api_interface.js";
import { queryAddTopic, queryAlias, queryTopic, queryUpdateArticles } from "./queries.js";

interface WikiInfo {
	baseUrl: string;
	searchUrl: string;
}

const wikis = {
	dota2gamepedia: {
		baseUrl: "http://dota2.gamepedia.com/",
		searchUrl:
			"http://dota2.gamepedia.com/api.php?format=json&action=query&list=search&srlimit=1&srsearch=",
	},
	dota2liquipedia: {
		baseUrl: "https://liquipedia.net/dota2/",
		searchUrl:
			"https://liquipedia.net/dota2/api.php?format=json&action=query&list=search&srlimit=1&srsearch=",
	},
	guildwars2: {
		baseUrl: "https://wiki.guildwars2.com/wiki/",
		searchUrl:
			"https://wiki.guildwars2.com/api.php?format=json&action=query&list=search&srlimit=1&srsearch=",
	},
} as const;

type WikiId = keyof typeof wikis;

/**
 * Searches the given wiki with a given query and outputs a user-friendly return string. Surrounds the link with <> if network is "discord".
 *
 * Throws an error if the query failed. Getting 0 results is not a failure.
 */
const wikiSearch = async (
	searchString: string,
	which: WikiId,
	network?: Network,
): Promise<string> => {
	const searchUrl = `${wikis[which].searchUrl}${encodeURIComponent(searchString).replace(
		/ /g,
		"+",
	)}`;
	//console.log(`About to search ${searchUrl}`);
	const { body } = await got(searchUrl);
	const response = JSON.parse(body) as RecursivePartial<MediaWikiApiResponse>;
	const result = response.query?.search;
	if (!Array.isArray(result)) throw new Error("Something went wrong with the wiki search");
	if (isNonEmptyArray(result)) {
		if (!result[0].title) throw new Error("Wiki search result had no title");
		const title = result[0].title.replace(/ /g, "_");
		let link = `${wikis[which].baseUrl}${title}`;
		if (network === "discord") {
			link = `<${link}>`;
		}
		const totalHits = response.query?.searchinfo?.totalhits;
		if (totalHits !== undefined) {
			if (totalHits === 1) {
				return `1 result: ${link}`;
			} else {
				return `Top result out of ${totalHits} is ${link}`;
			}
		} else {
			return `Top result is ${link}`;
		}
	} else {
		return "No search results.";
	}
};

interface WikiSelector {
	gamepedia?: boolean;
	liquipedia?: boolean;
}

/** Add or update a given topic with the given article link segment, on the given wikis. Will not change the link segments of the wiki not selected. */
const addTopic = (topic: string, article: string, which: WikiSelector) => {
	const currentTopic = queryTopic(topic);
	const gamepedia = which.gamepedia ? article : null;
	const liquipedia = which.liquipedia ? article : null;
	if (!currentTopic) {
		queryAddTopic(topic, gamepedia, liquipedia);
		return;
	}
	queryUpdateArticles(
		topic,
		gamepedia ?? currentTopic.gamepedia,
		liquipedia ?? currentTopic.liquipedia,
	);
};

/** Look up a given alias's article link segment for a given wiki. Returns undefined if none found. */
const wikiLookUp = (
	searchString: string,
	which: "dota2liquipedia" | "dota2gamepedia",
	network: string,
): string | undefined => {
	const topic = queryAlias(searchString);
	if (topic === undefined) return undefined;
	const result = queryTopic(topic);
	if (result === undefined) return undefined;
	const article = which === "dota2gamepedia" ? result.gamepedia : result.liquipedia;
	if (article === null) return undefined;
	let link = `${wikis[which].baseUrl}${article.replace(/ /g, "+")}`;
	if (network === "discord") {
		link = `<${link}>`;
	}
	return link;
};

export const cWiki: CommandFunction = async (cCall) => {
	if (!cCall.argstring) throw new CommandError();
	const which = cCall.command === "gamepedia" ? "dota2gamepedia" : "dota2liquipedia";
	cCall.reply(
		wikiLookUp(cCall.argstring, which, cCall.network) ??
			(await wikiSearch(cCall.argstring, which, cCall.network)),
	);
};

export const cWikiSearch: CommandFunction = async (cCall) => {
	if (!cCall.argstring) throw new CommandError();
	const which =
		cCall.command === "gamepediasearch"
			? "dota2gamepedia"
			: cCall.command === "liquipediasearch"
			? "dota2liquipedia"
			: "guildwars2";
	cCall.reply(await wikiSearch(cCall.argstring, which, cCall.network));
};

addCommand({
	name: "gamepedia",
	run: cWiki,
	category: "wiki",
	usage: "<topic>",
	description: "Looks up a topic on the Gamepedia Dota 2 wiki.",
});
addCommand({
	name: "liquipedia",
	run: cWiki,
	category: "wiki",
	usage: "<topic>",
	description: "Looks up a topic on the Liquipedia Dota 2 wiki.",
});
addCommand({
	name: "gamepediasearch",
	run: cWikiSearch,
	category: "wiki",
	usage: "<topic>",
	description:
		"Searches the Gamepedia Dota 2 wiki, bypassing the manually maintained alias table.",
});
addCommand({
	name: "liquipediasearch",
	run: cWikiSearch,
	category: "wiki",
	usage: "<topic>",
	description:
		"Searches the Liquipedia Dota 2 wiki, bypassing the manually maintained alias table.",
});
/*
addCommand({
	name: "gw2wiki",
	run: cWikiSearch,
	category: "wiki",
	usage: "<topic>",
	description: "Searches the Guild Wars 2 wiki.",
});
*/
