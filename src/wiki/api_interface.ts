export interface MediaWikiApiResponse {
	batchcomplete: string;
	continue: {
		sroffset: number;
		continue: string;
	};
	query: {
		searchinfo: {
			totalhits: number;
		};
		search: SearchResult[];
	};
}

interface SearchResult {
	ns: number;
	title: string;
	pageid: number;
	size: number;
	wordcount: number;
	snippet: string;
	/** Timestamp formatted like "2022-01-25T06:28:12Z" */
	timestamp: string;
}
