import { CommandError } from "./Errors.js";
import { addCommand, CommandFunction } from "./commands.js"; // Map of commands

// Dodge Twitch's word censor
//	chars: [],
//	chars.normal: ["aeopcyxisABEMHOPCTXIS"],
//	chars.alt: ["аеорсухіѕАВЕМНОРСТХІЅ"],

/** The words Twitch will censor (quite outdated) */
const words = [
	"bitch",
	"pussy",
	"cunt",
	"cunts",
	"cock",
	"nigger",
	"nigga",
	"tranny",
	"trannies",
	"fag",
	"faggot",
	"faggots",
	"gook",
	"gooks",
	"motherfucker",
	"motherfucking",
	"shithead",
	"shit-head",
	"kanker",
	"nigguh",
	"shemale",
	"shitter",
];
// old words: "shit", "fuck", "fucks", "fucking", "fucked", "ass", "asshole", "handjob", "turd", "bullshit", "bullshits", "bullshitting", "bullshitted", "bullshitter", "bullshitters"
/** Words that have a small chance of appearing when decensoring */
const memeWords = ["theist", "normie"];
/** Regex of everything that will be censored (as far as I know, information very outdated) */
const wordsRegex = new RegExp(`(^|\\s)(${words.join("|")})(?=$|\\s)`, "ig"); // "nigger" is a special case, censored even when not isolated}

/** Insert an invisible character (discretionary hyphen) in front of (or inside) any censored word to prevent the censorship */
export const uncensor = (text: string): string => {
	/** Discretionary hyphen */
	const invisChar = "­";
	return text
		.replace(wordsRegex, (_, space: string, word: string) => {
			return `${space}${invisChar}${word}`;
		})
		.replace(/nigger/gi, `n${invisChar}igger`); // "nigger" is a special case, needs the character inside of it
};

/** Replaces *** with the best guess at what word used to be there */
const decensor: CommandFunction = (cCall) => {
	if (cCall.argstring === "") throw new CommandError();
	const regexCensored = /(^|\s)\*\*\*(?=$|\s)/g;

	let censorshipFound = false as boolean;
	const result = cCall.argstring.replace(regexCensored, (_, space: string) => {
		censorshipFound = true;
		const word =
			Math.random() > 0.995 // 1 in 200 chance
				? memeWords[Math.floor(Math.random() * memeWords.length)]
				: words[Math.floor(Math.random() * words.length)];
		return space + word;
	});

	if (!censorshipFound)
		throw new CommandError(`Nothing in that message appears to be censored by Twitch`);

	cCall.reply(result);
};

addCommand({
	name: "decensor",
	run: decensor,
	category: "miscellaneous",
	usage: "<text>",
	description: "Algorithmically determines what a message censored by Twitch might have said.",
	edgy: true,
});

export default uncensor;
