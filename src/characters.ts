/** Non-breaking space */
export const NBSP = "\u00A0";
/** Narrow non-breaking space */
export const NNBSP = "\u202F";
/** Figure space (non-breaking space for use in numbers) */
export const FSP = "\u2007";
/** Zero-width non-breaking space */
export const ZWNBSP = "\uFEFF";
/** Non-breaking hyphen */
export const NBH = "\u2011";
/** Word joiner (used to combine characters that would otherwise not be seen as part of the same word into a word, preventing line break) */
export const WJ = "\u2060";
/** Zero-width joiner (used to join emojis and other characters) */
export const ZWJ = "\u200D";
/** Zero-width non-joiner (used to avoid joining characters) */
export const ZWNJ = "\u200C";
/** Variant selector-15 (makes the preceding character be rendered as a non-emoji, if possible, if otherwise it uses an emoji variant) */
export const VS15 = "\uFE0E";
/** Variant selector-16 (makes the preceding character be rendered as an emoji, if possible, if otherwise it uses a non-emoji variant) */
export const VS16 = "\uFE0F";
