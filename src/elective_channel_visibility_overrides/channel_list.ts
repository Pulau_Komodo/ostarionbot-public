import {
	ChatInputCommandInteraction,
	NewsChannel,
	StageChannel,
	TextChannel,
	VoiceChannel,
} from "discord.js";
import { SlashCommandFunction, SlashReplyOptions, addSlashCommand } from "../commands.js";
import { CommandError } from "../Errors.js";

const listChannels: SlashCommandFunction = async (
	interaction: ChatInputCommandInteraction,
): Promise<SlashReplyOptions> => {
	if (!interaction.guild) {
		throw new CommandError("Could not get guild.");
	}
	const content = interaction.guild.channels.cache
		.filter(
			(channel): channel is TextChannel | NewsChannel | StageChannel | VoiceChannel =>
				channel.isTextBased() && !channel.isThread(),
		)
		.sort((a, b) =>
			a.parent === b.parent
				? a.position - b.position
				: (a.parent?.position ?? -1) - (b.parent?.position ?? -1),
		)
		.map((channel) => `<#${channel.id}> ${channel.name.slice(0, 5)}`)
		.join("\n")
		.slice(0, 2000);
	if (content.length === 0) {
		throw new CommandError("Could not get any channels.");
	}

	return { content, ephemeral: true };
};

addSlashCommand("listchannels", listChannels, "List all channels with links and names.", false);
