import { ChannelType, GuildMember, PermissionFlagsBits, TextChannel } from "discord.js";
import { DiscordCommandCall } from "../CommandCall.js";
import { CommandError } from "../Errors.js";
import { addCommand, DiscordCommandFunction } from "../commands.js";
import { formatList } from "../formatList.js";

/** Check whether a channel already has a channel override to hide it from a user */
const isHiddenFromUser = (user: GuildMember, channel: TextChannel): boolean => {
	const overrides = channel.permissionOverwrites.cache.get(user.id);
	if (!overrides) return false;
	return overrides.deny.has(PermissionFlagsBits.ViewChannel);
};

/** Whether the bot has permission to apply an override */
const canOverride = (channel: TextChannel): boolean => {
	const me = channel.guild.members.me;
	if (!me) return false;
	const permissions = me.permissionsIn(channel);
	return permissions.has(PermissionFlagsBits.ManageRoles);
};

/** Sets an override for a user to hide a channel, and returns whether it succeeded */
const setInvisible = async (user: GuildMember, channel: TextChannel): Promise<boolean> => {
	try {
		// eslint-disable-next-line @typescript-eslint/naming-convention
		await channel.permissionOverwrites.edit(
			user,
			{ ViewChannel: false },
			{ reason: "Requested by user", type: 1 },
		);
		return true;
	} catch {
		return false;
	}
};

/** Unsets an override that hides a channel, and returns whether it succeeded. This doesn't necessarily make the channel visible, if it is hidden for other reasons. */
const unsetInvisible = async (user: GuildMember, channel: TextChannel): Promise<boolean> => {
	try {
		// eslint-disable-next-line @typescript-eslint/naming-convention
		await channel.permissionOverwrites.edit(
			user,
			{ ViewChannel: null },
			{ reason: "Requested by user", type: 1 },
		);
		return true;
	} catch {
		return false;
	}
};

const parseChannelIdOrMention = (text: string): string | undefined => {
	const match = /^(?:(\d+)|<#(\d+)>)$/.exec(text);
	const id = match?.[1] ?? match?.[2];
	if (!id) return undefined;
	return id;
};

/** All the checks */
const getOverwriteableChannel = (cCall: DiscordCommandCall): TextChannel => {
	if (!cCall.args[0]) throw new CommandError();
	const id = parseChannelIdOrMention(cCall.args[0]);
	if (!id) throw new CommandError();
	const channel = cCall.channel.guild.channels.resolve(id);
	if (!channel) throw new CommandError("No such channel");
	if (!(channel instanceof TextChannel))
		throw new CommandError("This command only works on text channels");
	if (!canOverride(channel))
		throw new CommandError("I lack permissions to apply overrides on that channel");
	return channel;
};

const cHideChannel: DiscordCommandFunction = async (cCall) => {
	const channel = getOverwriteableChannel(cCall);
	if (isHiddenFromUser(cCall.message.member, channel))
		throw new CommandError("That channel is already hidden");
	const success = await setInvisible(cCall.message.member, channel);
	if (!success) throw new CommandError("Something went wrong in hiding the channel");
	cCall.reply(`Channel <#${channel.id}> hidden from user.`);
};

const cUnhideChannel: DiscordCommandFunction = async (cCall) => {
	const channel = getOverwriteableChannel(cCall);
	if (!isHiddenFromUser(cCall.message.member, channel))
		throw new CommandError("That channel is not hidden");
	const success = await unsetInvisible(cCall.message.member, channel);
	if (!success) throw new CommandError("Something went wrong in unhiding the channel");
	cCall.reply(`Channel <#${channel.id}> unhidden from user.`);
};

const cFindChannelIds: DiscordCommandFunction = async (cCall) => {
	const searchText = cCall.args[0];
	if (!searchText) throw new CommandError();
	let channels = cCall.message.guild.channels.cache
		.filter(({ name, type }) => name.includes(searchText) && type === ChannelType.GuildText)
		.map((channel) => `${channel.toString()}: ${channel.id}`);
	if (channels.length === 0)
		throw new CommandError(`No text channel names include "${searchText}"`);
	const count = channels.length;
	channels = channels.slice(0, 4);
	let output = formatList(channels, {
		opening: [
			`The only text channel including "${searchText}" is `,
			`The channels including "${searchText}" are `,
		],
		closing: ["."],
	});
	const omittedCount = count - channels.length;
	if (omittedCount > 0) {
		const s = omittedCount === 1 ? "" : "s";
		output += ` ${omittedCount} channel${s} omitted because there were too many results.`;
	}
	cCall.reply(output);
};

addCommand({
	name: "hidechannel",
	run: cHideChannel,
	category: "miscellaneous",
	usage: "<channel ID or mention>",
	description: "Hide a channel for yourself.",
	discordOnly: true,
});
addCommand({
	name: "unhidechannel",
	run: cUnhideChannel,
	category: "miscellaneous",
	usage: "<channel ID or mention>",
	description: "Unhide a channel previously hidden for yourself.",
	discordOnly: true,
});
addCommand({
	name: "channelid",
	run: cFindChannelIds,
	category: "miscellaneous",
	usage: "<search text>",
	description: "Find all channels.",
	aliases: ["channelids"],
	heat: { perChar: 0.02 },
	discordOnly: true,
});
