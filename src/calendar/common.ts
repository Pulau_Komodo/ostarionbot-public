export const CATEGORY = "event";
/** The smallest interval, in ms, that a recurring event is allowed to have */
export const MINIMUM_INTERVAL = 4 * 60 * 60 * 1000;
