import {
	queryCheckSubscription,
	queryDeleteEvent,
	queryEvent,
	querySubscribe,
	querySubscriberCount,
	queryUnsubscribe,
} from "./queries.js";
import { CommandError } from "../Errors.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";
import { getDisplayName } from "../discord_util.js";
import { CATEGORY } from "./common.js";
import { Network } from "../common_types.js";
import { ApplicationCommandOptionType } from "discord.js";

/** Subscribe to an event, returns event name */
const subscribe = (
	userId: string,
	network: Network,
	channelId: string,
	eventName: string,
): string => {
	const event = queryEvent(network, channelId, eventName);
	if (!event) throw new CommandError("Error: no event by that name");
	if (queryCheckSubscription(userId, network, channelId, event.name))
		throw new CommandError("Error: already subscribed");
	querySubscribe(userId, network, channelId, event.name);
	return event.name;
};

const commandSubscribe: CommandFunction = (cCall) => {
	const {
		network,
		channel: { id: channelId },
		user: { id: userId, displayName },
	} = cCall;
	const inputEventName = cCall.commaArgs[0]?.trim();
	if (!inputEventName) throw new CommandError();
	const eventName = subscribe(userId, network, channelId, inputEventName);
	cCall.reply(`${displayName} subscribed to event "${eventName}".`);
};

const slashSubscribe: SlashCommandFunction = (interaction) => {
	const userId = interaction.user.id;
	const channelId = interaction.channelId;
	const inputEventName = interaction.options.getString("event", true);
	const displayName = getDisplayName(interaction);
	const eventName = subscribe(userId, "discord", channelId, inputEventName);
	return `${displayName} subscribed to event "${eventName}"`;
};

/** Unsubscribes from an event and deletes it if there are no other subscribers, returns the event name and whether it deleted it */
const unsubscribe = (
	userId: string,
	network: Network,
	channelId: string,
	eventName: string,
): [string, boolean] => {
	const event = queryEvent(network, channelId, eventName);
	if (!event) throw new CommandError("Error: no event by that name");
	const wasSubscribed = queryUnsubscribe(userId, network, channelId, event.name);
	if (!wasSubscribed) throw new CommandError("Error: not subscribed");

	const subscriberCount = querySubscriberCount(network, channelId, event.name);
	if (subscriberCount === 0 && !event.persist) {
		queryDeleteEvent(network, channelId, event.name);
		return [event.name, true];
	} else {
		return [event.name, false];
	}
};

const commandUnsubscribe: CommandFunction = (cCall) => {
	const {
		network,
		channel: { id: channelId },
		user: { id: userId, displayName },
	} = cCall;
	const inputEventName = cCall.commaArgs[0]?.trim();
	if (!inputEventName) throw new CommandError();
	const [eventName, deleted] = unsubscribe(userId, network, channelId, inputEventName);
	if (deleted) {
		cCall.reply(`Event "${eventName}" deleted because the only subscriber unsubscribed.`);
	} else {
		cCall.reply(`${displayName} unsubscribed from event "${eventName}".`);
	}
};

const slashUnsubscribe: SlashCommandFunction = (interaction) => {
	const userId = interaction.user.id;
	const channelId = interaction.channelId;
	const inputEventName = interaction.options.getString("event", true);
	const displayName = getDisplayName(interaction);
	const [eventName, deleted] = unsubscribe(userId, "discord", channelId, inputEventName);
	if (deleted) {
		return `Event "${eventName}" deleted because the only subscriber unsubscribed.`;
	} else {
		return `${displayName} unsubscribed from event "${eventName}"`;
	}
};

addCommand({
	name: "subscribe",
	run: commandSubscribe,
	category: CATEGORY,
	usage: "<event>",
	description: "Subscribe to the chosen event.",
	slash: {
		run: slashSubscribe,
		options: [
			{
				name: "event",
				type: ApplicationCommandOptionType.String,
				description: "The event to subscribe to",
				required: true,
			},
		],
	},
});
addCommand({
	name: "unsubscribe",
	run: commandUnsubscribe,
	category: CATEGORY,
	usage: "<event>",
	description: "Unsubscribes from the chosen event.",
	slash: {
		run: slashUnsubscribe,
		options: [
			{
				name: "event",
				type: ApplicationCommandOptionType.String,
				description: "The event to unsubscribe from",
				required: true,
			},
		],
	},
});
