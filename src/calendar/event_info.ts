import { stringifyDateTime } from "../datetimeformat.js";
import {
	queryEvent,
	queryEventCount,
	queryEventList,
	queryNextEvent,
	querySubscriberCount,
} from "./queries.js";
import { getDateFromEventOrTime, parseAsTimezone, ParsedDate } from "./time_parsing.js";
import { checkEvent } from "./event_checks.js";
import { CommandCall } from "../CommandCall.js";
import { CommandError } from "../Errors.js";
import { stringifyInterval, stringifyIntervalShort } from "interval-conversions";
import { dateTime, getDisplayName } from "../discord_util.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";
import { CATEGORY } from "./common.js";
import { Network } from "../common_types.js";
import { ApplicationCommandOptionType } from "discord.js";

/** Gets detailed info about a specific event. Returns a string if there should be output. */
const getInfo = (
	network: Network,
	channelId: string,
	eventName: string,
	timezone: string | undefined,
): string | undefined => {
	const event = queryEvent(network, channelId, eventName);
	if (!event) throw new CommandError(`Error: no event by that name`);
	const subscriberCount = querySubscriberCount(network, channelId, event.name);
	const intervalText = stringifyInterval(event.time - Date.now());
	const date = new Date(event.time);
	const description = event.description
		? `description: "${event.description}"`
		: `no description`;
	const subscribers = subscriberCount === 1 ? `1 subscriber` : `${subscriberCount} subscribers`;
	if (event.time < Date.now()) {
		// If the event is in the past, just run a CheckEvent, should take the necessary steps
		checkEvent(network, channelId, event.name);
		return;
	}
	// Timezone handling
	if (timezone !== undefined) {
		const offset = parseAsTimezone(timezone);
		if (offset === undefined) {
			throw new CommandError(`Timezone argument did not look like a timezone`);
		}
		date.setUTCHours(date.getUTCHours() + offset);
	}

	const timeText =
		network === "discord" && timezone === undefined
			? dateTime(date.getTime() / 1000)
			: `${stringifyDateTime(date)} ${timezone ?? "UTC"}`;
	let output = event.persist ? `Persistent event` : `Event`;
	output += ` "${event.name}", at ${timeText} (in ${intervalText})`;
	if (event.interval) {
		if (typeof event.interval === "number" && Number.isInteger(event.interval)) {
			output += `, recurring every ${stringifyInterval(event.interval)}`;
		} else {
			output += `, recurring every ${event.interval}`;
		}
	}
	output += `, ${description}, ${subscribers}`;
	return output;
};

const commandGetInfo: CommandFunction = (cCall) => {
	const {
		network,
		channel: { id: channelId },
	} = cCall;
	const eventName = cCall.commaArgs[0]?.trim();
	const timezone = cCall.commaArgs[1]?.trim()?.toUpperCase() as string | undefined;
	if (eventName === undefined || eventName === "") throw new CommandError();
	const output = getInfo(network, channelId, eventName, timezone);
	if (!output) return;
	cCall.reply(output);
};

const slashGetInfo: SlashCommandFunction = (interaction) => {
	const channelId = interaction.channelId;
	const eventName = interaction.options.getString("event", true);
	const timezone = interaction.options.getString("timezone")?.toUpperCase();
	return getInfo("discord", channelId, eventName, timezone);
};

addCommand({
	name: "event",
	run: commandGetInfo,
	category: CATEGORY,
	usage: "<event>,[timezone]",
	description: "Gives information about the chosen event.",
	slash: {
		run: slashGetInfo,
		options: [
			{
				name: "event",
				type: ApplicationCommandOptionType.String,
				description: "The event to get information about",
				required: true,
			},
			{
				name: "timezone",
				type: ApplicationCommandOptionType.String,
				description: "The timezone for the event time output",
			},
		],
	},
});

/** Gets the next event for a channel, optionally specific to the user */
const nextEvent = (
	network: Network,
	channelId: string,
	offset: number,
	user: { id: string; displayName: string } | undefined,
): string => {
	offset = Math.max(0, offset);
	const eventCount = queryEventCount(network, channelId, user?.id);
	const userText = user === undefined ? "" : ` for ${user.displayName}`;
	if (eventCount === 0) return `There are no events${userText} in this channel.`;
	if (offset + 1 > eventCount && eventCount === 1)
		return `There is only 1 event${userText} in this channel.`;
	if (offset + 1 > eventCount)
		return `There are only ${eventCount} events${userText} in this channel.`;
	const event = queryNextEvent(network, channelId, offset, user?.id);
	const time = stringifyInterval(event.time - Date.now());
	let output = offset === 0 ? `Next event` : `Upcoming event #${offset + 1}`;
	output += ` out of ${eventCount}${userText} in this channel is "${event.name}", in ${time}.`;
	return output;
};

const commandNextEvent: CommandFunction = (cCall) => {
	const {
		network,
		channel: { id: channelId },
		user,
	} = cCall;
	const all = cCall.args[0]?.toLowerCase() === "all";
	let offset = all ? parseInt(cCall.args[1] ?? "") : parseInt(cCall.args[0] ?? "");
	if (isNaN(offset)) offset = 0;
	else if (offset > 0) --offset;
	const filterUser = all ? undefined : user;
	const output = nextEvent(network, channelId, offset, filterUser);
	cCall.reply(output);
};

const slashNextEvent: SlashCommandFunction = (interaction) => {
	const channelId = interaction.channelId;
	const offset = (interaction.options.getInteger("offset") ?? 1) - 1;
	const all = interaction.options.getString("all") === "all";
	const user = all
		? undefined
		: { id: interaction.user.id, displayName: getDisplayName(interaction) };
	return nextEvent("discord", channelId, offset, user);
};

addCommand({
	name: "nextevent",
	run: commandNextEvent,
	category: CATEGORY,
	usage: '["all"] [number]',
	description:
		'Gives the next event the user is subscribed to. With "all" specified, it gives the next event in the channel. With number specified, it can give a later event.',
	aliases: ["ne"],
	slash: {
		run: slashNextEvent,
		description: "Gives the next event in the channel",
		options: [
			{
				name: "all",
				type: ApplicationCommandOptionType.String,
				description:
					"If supplied, gives the next event for anyone, not filtered by what the user is subscribed to",
				choices: [
					{
						name: "all",
						value: "all",
					},
				],
			},
			{
				name: "offset",
				type: ApplicationCommandOptionType.Integer,
				description: "The how maniest event to get",
			},
		],
	},
});

/** Gives the names of the next 20 events for a channel, optionally specific to the user */
const eventList = (
	network: Network,
	channelId: string,
	offset: number,
	user: { id: string; displayName: string } | undefined,
): string => {
	offset = Math.max(0, offset) * 20;
	const eventCount = queryEventCount(network, channelId, user?.id);
	const userText = user === undefined ? "" : ` for ${user.displayName}`;
	if (eventCount === 0) return `There are no events${userText} in this channel.`;
	if (offset + 1 > eventCount && eventCount === 1)
		return `There is only 1 event${userText} in this channel.`;
	if (offset + 1 > eventCount)
		return `There are only ${eventCount} events${userText} in this channel.`;
	const events = queryEventList(network, channelId, offset, user?.id);
	const descriptionIcon = network === "discord" ? " \\📝" : " 📝";
	const descriptionIcons = events.map((event) => (event.description ? descriptionIcon : ""));
	let output = ``;
	const firstEvent = events[0]!;
	const firstIcon = descriptionIcons[0]!;
	const firstEventTime = stringifyIntervalShort(firstEvent.time - Date.now());
	if (events.length === 1) {
		if (offset === 0)
			output = `The only event${userText} in this channel is "${firstEvent.name}"${firstIcon} (within ${firstEventTime}).`;
		else
			output = `Event #${offset + 1} out of ${eventCount}${userText} in this channel is "${
				firstEvent.name
			}"${firstIcon} (${firstEventTime}).`;
	} else {
		output = `Events #${offset + 1}-#${
			offset + events.length
		} out of ${eventCount}${userText} in this channel are "${
			firstEvent.name
		}"${firstIcon} (within ${firstEventTime})`;
		for (let i = 1; i < events.length - 1; i++)
			output += `, "${events[i]!.name}"${descriptionIcons[i]!}`;
		const lastEvent = events[events.length - 1]!;
		const lastIcon = descriptionIcons[events.length - 1]!;
		const lastEventTime = stringifyIntervalShort(lastEvent.time - Date.now());
		output += ` and "${lastEvent.name}"${lastIcon} (within ${lastEventTime}).`;
	}
	return output;
};

const commandEventList: CommandFunction = (cCall) => {
	const {
		network,
		channel: { id: channelId },
		user,
	} = cCall;
	const all = cCall.args[0]?.toLowerCase() === "all";
	let offset = all ? parseInt(cCall.args[1] ?? "") : parseInt(cCall.args[0] ?? "");
	if (isNaN(offset)) offset = 0;
	else if (offset > 0) --offset;
	const filterUser = all ? undefined : user;
	cCall.reply(eventList(network, channelId, offset, filterUser));
};

const slashEventList: SlashCommandFunction = (interaction) => {
	const channelId = interaction.channelId;
	const offset = (interaction.options.getInteger("offset") ?? 1) - 1;
	const all = interaction.options.getString("all") === "all";
	const user = all
		? undefined
		: { id: interaction.user.id, displayName: getDisplayName(interaction) };
	return eventList("discord", channelId, offset, user);
};

addCommand({
	name: "eventlist",
	run: commandEventList,
	category: CATEGORY,
	usage: '["all"] [page]',
	description:
		'Gives the next 20 events the user is subscribed to. With "all" specified, it gives the next 20 events in the channel. With page specified, it can give a later pages of events.',
	heat: { fixed: 2 },
	aliases: ["el"],
	slash: {
		run: slashEventList,
		description: "Gives the next 20 events in the channel",
		options: [
			{
				name: "all",
				type: ApplicationCommandOptionType.String,
				description:
					"If supplied, gives the next 20 events for anyone, not filtered by what the user is subscribed to",
				choices: [
					{
						name: "all",
						value: "all",
					},
				],
			},
			{
				name: "offset",
				type: ApplicationCommandOptionType.Integer,
				description: "The how maniest page of events to get",
			},
		],
	},
});

const time = (
	times: [ParsedDate?, ParsedDate?],
	timezone: { name: string; offset: number } | undefined,
): string => {
	if (!times[0]) {
		if (!times[1]) {
			const date = new Date();
			if (timezone) {
				date.setUTCHours(date.getUTCHours() + timezone.offset);
				return `It is currently ${stringifyDateTime(date)} ${timezone.name}.`;
			} else {
				return `It is currently ${dateTime(date.getTime() / 1000)}`;
			}
		} else {
			// If there is only one time, ensure it's the first
			times[0] = times[1];
			times[1] = undefined;
		}
	}

	let secondTimeIsNow = false;
	if (!times[1]) {
		times[1] = {
			date: new Date(),
			duration: false,
		};
		secondTimeIsNow = true;
	}

	const timesFull = times as [ParsedDate, ParsedDate];
	const timestamps = timesFull.map(({ date }) => date.getTime()) as [number, number];

	// Both events are at the time
	if (timestamps[0] === timestamps[1]) {
		if (timesFull[0].event) {
			if (timesFull[1].event) {
				return `Events "${timesFull[0].event}" and "${timesFull[1].event}" are at exactly the same time.`;
			} else if (secondTimeIsNow) {
				return `Event "${timesFull[0].event}" is right now.`;
			} else {
				return `Event "${timesFull[0].event}" is at exactly that time.`;
			}
		} else if (timesFull[1].event) {
			return `Event "${timesFull[1].event}" is at exactly that time.`;
		} else if (secondTimeIsNow) {
			return `That time is right now.`;
		} else {
			return `Those times are exactly the same.`;
		}
	}

	const timeBetween = stringifyInterval(timestamps[0] - timestamps[1]);

	const stringify = (parsedDate: ParsedDate) => {
		if (parsedDate.event) {
			return `event "${parsedDate.event}"`;
		} else if (timezone) {
			// This is a bad way of doing timezones
			const adjustedDate = new Date(
				parsedDate.date.getTime() + timezone.offset * 60 * 60 * 1000,
			);
			return `${stringifyDateTime(adjustedDate)} ${timezone.name}`;
		} else {
			return dateTime(parsedDate.date.getTime() / 1000);
		}
	};

	const secondTimeComesFirst = timestamps[0] > timestamps[1];

	if (secondTimeIsNow) {
		return `The time ${secondTimeComesFirst ? "until" : "since"} ${stringify(
			timesFull[0],
		)} is ${timeBetween}.`;
	}

	const firstText = stringify(timesFull[0]);
	return `${firstText.slice(0, 1).toUpperCase()}${firstText.slice(1)} is ${timeBetween} ${
		secondTimeComesFirst ? "after" : "before"
	} ${stringify(timesFull[1])}.`;
};

/** Throws a CommandError if any date was supplied that could not be parsed */
const validateParsedDates = (parsedDates: (ParsedDate | string)[]): void => {
	if (typeof parsedDates[0] === "string") {
		if (typeof parsedDates[1] === "string") {
			throw new CommandError(
				`Neither "${parsedDates[0]}" nor "${parsedDates[1]}" could be found as events or parsed as times.`,
			);
		}
		throw new CommandError(
			`"${parsedDates[0]}" could not be found as an event or parsed as a time`,
		);
	}
	if (typeof parsedDates[1] === "string") {
		throw new CommandError(
			`"${parsedDates[1]}" could not be found as an event or parsed as a time`,
		);
	}
};

export const commandTime: CommandFunction = (cCall) => {
	const {
		network,
		channel: { id: channel },
		commaArgs,
	} = cCall;
	const args = commaArgs.slice(0, 3).map((element) => element.trim());

	let timezoneOffset;
	let timezone;

	if (args.length > 0) {
		try {
			const lastArg = args[args.length - 1]!.toUpperCase();
			const offset = parseAsTimezone(lastArg);
			if (offset !== undefined) {
				timezoneOffset = offset;
				timezone = lastArg;
				args.pop(); // Remove timezone from array
			} else if (args.length === 3)
				throw new CommandError(`Third argument does not look like a timezone`);
		} catch (error) {
			if (error instanceof CommandError && error.message.startsWith("Timezone")) {
				if (args.length === 3)
					throw new CommandError(`Third argument is not a timezone in the database`);
			} else {
				throw error;
			}
		}
	}

	const parsedDates = args
		.filter((arg) => arg !== "")
		.map((arg) => {
			const parsedDate = getDateFromEventOrTime(network, channel, arg);
			if (parsedDate) return parsedDate;
			else return arg;
		});
	validateParsedDates(parsedDates);

	const timezoneObject =
		timezone && timezoneOffset !== undefined
			? { name: timezone, offset: timezoneOffset }
			: undefined;
	const output = time(
		parsedDates as [ParsedDate | undefined, ParsedDate | undefined],
		timezoneObject,
	);
	cCall.reply(output);
};

export const slashTime: SlashCommandFunction = (interaction) => {
	const channel = interaction.channelId;

	const first = interaction.options.getString("first");
	const second = interaction.options.getString("second");
	let timezone = interaction.options.getString("timezone");

	const parsedDates: (string | ParsedDate)[] = [];
	for (const arg of [first, second]) {
		if (arg) {
			parsedDates.push(getDateFromEventOrTime("discord", channel, arg) ?? arg);
		}
	}
	validateParsedDates(parsedDates);

	let offset;
	if (timezone) {
		timezone = timezone.toUpperCase();
		try {
			offset = parseAsTimezone(timezone);
		} catch (error) {
			if (error instanceof CommandError && error.message.startsWith("Timezone")) {
				throw new CommandError(`Timezone argument is not a timezone in the database`);
			} else {
				throw error;
			}
		}
		if (offset === undefined) {
			throw new CommandError(`Timezone argument does not look like a timezone`);
		}
	}

	const timezoneObject =
		timezone && offset !== undefined ? { name: timezone, offset } : undefined;
	return time(parsedDates as [ParsedDate | undefined, ParsedDate | undefined], timezoneObject);
};

addCommand({
	name: "time",
	run: commandTime,
	category: CATEGORY,
	usage: "[event or time],[event or time],[timezone]",
	description:
		"Gives interval between two events or times, or an event or time and now, or the current time.",
	aliases: ["t", "timebetween", "timeuntil"],
	slash: {
		run: slashTime,
		options: [
			{
				name: "first",
				type: ApplicationCommandOptionType.String,
				description: "A time or event",
			},
			{
				name: "second",
				type: ApplicationCommandOptionType.String,
				description: "A time or event",
			},
			{
				name: "timezone",
				type: ApplicationCommandOptionType.String,
				description: "The timezone of the output, optionally with an offset (like PST+5)",
			},
		],
	},
});
