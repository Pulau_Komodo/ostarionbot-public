import { queueMessage } from "../message.js";
import { log } from "../log.js";
import { db } from "../db.js";
import { queryEvent } from "./queries.js";
import { parseDuration } from "./time_parsing.js";
import { stringifyInterval } from "interval-conversions";
import { Network } from "../common_types.js";
import { MINIMUM_INTERVAL } from "./common.js";

/** How much error in setInterval and setTimeout to account for, as a factor.
 * This seems to increase precision but I'm not entirely sure why. */
export const margin = 0.1;
/** The interval, in ms, at which to check all non-soon events, and the threshold below which an event is considered soon*/
export const interval = 60 * 60 * 1000;

/** Events being tracked via setTimeout because they are soon */
const soonEvents: Record<
	"twitch" | "discord",
	Map<string, Map<string, ReturnType<typeof setTimeout>>>
> = {
	twitch: new Map<string, Map<string, ReturnType<typeof setTimeout>>>(),
	discord: new Map<string, Map<string, ReturnType<typeof setTimeout>>>(),
};

/** Get all events before the given time, optionally just those not already marked soon (default) */
const querySoonEvents = (
	time: number,
	network?: string,
	includeAlreadySoon = false,
): { network: Network; channel: string; name: string; time: number }[] => {
	if (!network) {
		const qSoonEvents = includeAlreadySoon
			? `
			SELECT network, channel, name, time
			FROM events
			WHERE time < $time
		`
			: `
			SELECT network, channel, name, time
			FROM events
			WHERE time < $time AND soon = 0
		`;
		return db.prepare(qSoonEvents).all({ time }) as any;
	}
	const qSoonEvents = includeAlreadySoon
		? `
		SELECT network, channel, name, time
		FROM events
		WHERE time < $time AND network = $network
	`
		: `
		SELECT network, channel, name, time
		FROM events
		WHERE time < $time AND network = $network AND soon = 0
	`;
	return db.prepare(qSoonEvents).all({ time, network }) as any;
};

/** Calls a check on each event
 * @param {boolean} ignore_already_soon (optional) If true, will check all events. If omitted or false, will check only events that aren't happening within the next interval.
 */
export const checkSoonEvents = (network?: string, includeAlreadySoon = false): void => {
	log(
		`Checking all events${includeAlreadySoon ? `` : ` not marked 'soon'`} ` +
			`with times under ${stringifyInterval(interval * (1 + margin))}` +
			`${network ? ` on ${network}` : ""}`,
	);
	const now = Date.now();
	//	console.log("now: " + now);
	const oneIntervalIntoTheFuture = now + interval * (1 + margin);
	console.log(`interval_into_future: ${oneIntervalIntoTheFuture}`);

	const eventResults = querySoonEvents(oneIntervalIntoTheFuture, network, includeAlreadySoon);

	for (const event of eventResults) {
		checkEvent(event.network, event.channel, event.name, now);
	}
};

/** Get all subscribers for an event */
const queryGetSubs = (network: string, channel: string, event: string): { user: string }[] => {
	const qGetSubs = `
		SELECT user
		FROM subscriptions
		WHERE network = $network AND channel = $channel AND event = $event
	`;
	return db.prepare(qGetSubs).all({ network, channel, event }) as any;
};

/**
 * Checks a specific event to see if it's soon, now, or in the past.
 * Starts the tracking with setTimeout of soon events, and triggers now or past events.
 * @param now The current time, in milliseconds since Jan 1 1970
 */
export const checkEvent = function (
	network: Network,
	channel: string,
	eventName: string,
	now: number = Date.now(),
): void {
	log(`checking event ${eventName}`);
	// Mark an event as soon
	const querysetsoon = `
		UPDATE events
		SET soon = $soon
		WHERE network = $network AND channel = $channel AND name = $name
	`;
	// Set the event's next occurrence
	const querysetnext = `
		UPDATE events
		SET time = $time, soon = 0
		WHERE network = $network AND channel = $channel AND name = $name
	`;
	// Remove the event
	const querydeleteevent = `
		DELETE FROM events
		WHERE network = $network AND channel = $channel AND name = $name
	`;

	const event = queryEvent(network, channel, eventName);
	if (!event) {
		log("Error: CheckEvent query did not find an event");
		console.log([network, channel, eventName]);
		return;
	}
	// If now is within a quarter of a second before the event and a second after, or past the event, trigger the event
	if (now > event.time || (now - 1000 < event.time && now + 250 > event.time)) {
		log(`Triggering event ${event.name}`);
		const subs = queryGetSubs(network, channel, event.name);
		let newTime,
			difference = 0;
		if (event.interval) {
			if (typeof event.interval === "number") {
				newTime = event.time + event.interval;
			} else {
				const parsedNewTime = parseDuration(event.interval, new Date(event.time));
				if (!parsedNewTime) {
					log(`Error: somehow failed to parse stored interval "${event.interval}"`);
					return;
				}
				difference = parsedNewTime.difference || 0;
				if (difference >= MINIMUM_INTERVAL) {
					newTime = event.time + difference;
				} else {
					newTime = event.time + MINIMUM_INTERVAL;
					log(
						`Recurring event ${event.name}'s automatic ${stringifyInterval(
							difference,
						)} interval was too small, so it was set to ${stringifyInterval(
							MINIMUM_INTERVAL,
						)} instead.`,
					);
				}
			}
		}

		let message = event.interval ? `Recurring event ` : `Event `;
		message += `"${event.name}" `;
		message +=
			now - 1000 > event.time ? `was ${stringifyInterval(now - event.time)} ago.` : `is now!`;

		if (network === "discord") {
			for (const sub of subs) message += ` <@${sub.user}>`;
		} else {
			for (const sub of subs) message += ` ${sub.user}`;
		}

		if (event.description !== "" && event.description !== null)
			message += ` ${event.description}`;

		if (event.interval) {
			if (newTime) {
				message +=
					newTime > now ? ` It will recur in ${stringifyInterval(newTime - now)}.` : ``;
			} else {
				` The next recurrence has been cancelled because ${event.interval} `;
				if (difference < 0) {
					message += `would have been in the past somehow.`;
				} else {
					message += `would have been too close somehow.`;
				}
			}
		}

		queueMessage(network, channel, message, false, true); // Send the message that the event completed

		if (event.interval && newTime) {
			// If repeating event
			db.prepare(querysetnext).run({
				network,
				channel,
				name: event.name,
				time: newTime,
			}); // Set the next occurrence
			checkEvent(network, channel, event.name, now);
		} else {
			db.prepare(querydeleteevent).run({ network, channel, name: event.name }); // Delete the event
		}
		// If event is within the next interval (and margin)
	} else if (event.time - now < interval * (1 + margin)) {
		db.prepare(querysetsoon).run({
			network,
			channel,
			name: event.name,
			soon: 1,
		});
		const effective_margin = margin * (event.time - now) > 1000 ? margin : 0; // Only bother with a margin if the margin is over a second
		const delay = Math.round((event.time - now) * (1 - effective_margin));

		const timeout = setTimeout(
			checkEvent,
			delay,
			network,
			channel,
			eventName,
		) as unknown as ReturnType<typeof setTimeout>;
		let channelEventMap = soonEvents[network].get(channel);
		if (!channelEventMap) {
			channelEventMap = new Map<string, ReturnType<typeof setTimeout>>();
			soonEvents[network].set(channel, channelEventMap);
		}
		channelEventMap.set(eventName, timeout);
		// It's not soon but it's marked soon for whatever reason
	} else if (event.soon) {
		db.prepare(querysetsoon).run({
			network,
			channel,
			name: event.name,
			soon: 0,
		});
		log(`Cleaned up wrongful 'soon' on ${event.name}`);

		// If event was being tracked via timeout, stop that
		const cancelled = cancelTimeout(network, channel, eventName);
		if (cancelled) log(`Error: event was erroneously being tracked`);
	}
};

/** Cancels an event setTimeout if it exists, and returns whether it did exist. eventName is case sensitive. */
export const cancelTimeout = (
	network: "twitch" | "discord",
	channel: string,
	eventName: string,
): boolean => {
	const channelEventMap = soonEvents[network].get(channel);
	if (!channelEventMap) return false;
	const timeout = channelEventMap.get(eventName);
	if (!timeout) return false;
	channelEventMap.delete(eventName);
	clearTimeout(timeout);
	return true;
};

setInterval(checkSoonEvents, interval); // Check which events are near every interval
