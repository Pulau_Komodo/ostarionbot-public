import { Network } from "../common_types.js";
import { db } from "../db.js";

type EventQueryResult = {
	name: string;
	time: number;
	interval: number | string | null;
	description: string | null;
	persist: 0 | 1;
	soon: 0 | 1;
};

export const queryEvent = function (
	network: Network,
	channel: string,
	name: string,
): EventQueryResult | undefined {
	const qEvent = `
		SELECT name, time, interval, description, persist, soon
		FROM events
		WHERE network = $network AND channel = $channel AND name = $name
	`;
	return db.prepare(qEvent).get({ network, channel, name }) as any;
};

/** Subscribe a user to an event */
export const querySubscribe = (
	user: string,
	network: string,
	channel: string,
	event: string,
): void => {
	const qSubscribe = `
		INSERT INTO subscriptions(user, network, channel, event)
		VALUES ($user, $network, $channel, $event)
	`;
	db.prepare(qSubscribe).run({ user, network, channel, event });
};

/** Unsubscribe a user from an event. Returns whether a subscription was actually deleted. If not, it probably didn't exist. */
export const queryUnsubscribe = (
	user: string,
	network: string,
	channel: string,
	event: string,
): boolean => {
	const qUnsubscribe = `
		DELETE FROM subscriptions
		WHERE user = $user AND network = $network AND channel = $channel AND event = $event
	`;
	const result = db.prepare(qUnsubscribe).run({ user, network, channel, event });
	return result.changes !== 0;
};

/** Check whether a given user is subscribed to a given event */
export const queryCheckSubscription = (
	user: string,
	network: string,
	channel: string,
	event: string,
): boolean => {
	const qCheckSubscription = `
		SELECT count(*) count
		FROM subscriptions
		WHERE user = $user AND network = $network AND channel = $channel AND event = $event
	`;
	const result = db.prepare(qCheckSubscription).get({ user, network, channel, event }) as {count: number};
	return result.count !== 0;
};

export const querySubscriberCount = (network: string, channel: string, event: string): number => {
	const qSubscriberCount = `
		SELECT count(*) count
		FROM subscriptions
		WHERE network = $network AND channel = $channel AND event = $event
	`;
	return (db.prepare(qSubscriberCount).get({ network, channel, event }) as any).count;
};

export const queryAddEvent = (
	network: string,
	guild: string,
	channel: string,
	event: string,
	description: string,
	time: number,
	interval?: number | string,
): void => {
	const qAddEvent = `
		INSERT INTO events(network, guild, channel, name, description, time, interval)
		VALUES ($network, $guild, $channel, $event, $description, $time, $interval)
	`;
	db.prepare(qAddEvent).run({
		network,
		guild,
		channel,
		event,
		description,
		time,
		interval,
	});
};

export const queryDeleteEvent = (network: string, channel: string, event: string): void => {
	const qDeleteEvent = `
		DELETE FROM events
		WHERE network = $network AND channel = $channel AND name = $event
	`;
	db.prepare(qDeleteEvent).run({ network, channel, event });
};

export const queryRenameEvent = (
	network: string,
	channel: string,
	event: string,
	newName: string,
): void => {
	const qRenameEvent = `
		UPDATE events
		SET name = $newName
		WHERE network = $network AND channel = $channel AND name = $event
	`;
	db.prepare(qRenameEvent).run({ network, channel, event, newName });
};

export const queryChangeEventTime = (
	network: string,
	channel: string,
	event: string,
	time: number,
): void => {
	const qChangeEventTime = `
		UPDATE events
		SET time = $time
		WHERE network = $network AND channel = $channel AND name = $event
	`;
	db.prepare(qChangeEventTime).run({ network, channel, event, time });
};

export const queryChangeEventInterval = (
	network: string,
	channel: string,
	event: string,
	interval?: number | string,
): void => {
	const qChangeEventInterval = `
		UPDATE events
		SET interval = $interval
		WHERE network = $network AND channel = $channel AND name = $event
	`;
	db.prepare(qChangeEventInterval).run({ network, channel, event, interval });
};

export const queryChangeEventDescription = (
	network: string,
	channel: string,
	event: string,
	description?: string,
): void => {
	const qChangeEventDescription = `
		UPDATE events
		SET description = $description
		WHERE network = $network AND channel = $channel AND name = $event
	`;
	db.prepare(qChangeEventDescription).run({
		network,
		channel,
		event,
		description,
	});
};

export const querySetEventPersistence = (
	network: string,
	channel: string,
	event: string,
	persist: boolean,
): void => {
	const qSetEventPersistence = `
		UPDATE events
		SET persist = $persist
		WHERE network = $network AND channel = $channel AND name = $event
	`;
	db.prepare(qSetEventPersistence).run({
		network,
		channel,
		event,
		persist: persist ? 1 : 0,
	});
};

/** Check whether a given user is an event admin */
export const queryAdminStatus = (network: string, adminDomain: string, user: string): boolean => {
	const qAdminStatus = `
		SELECT count(*) count
		FROM moderators
		WHERE network = $network AND admin_domain = $adminDomain AND user = $user AND admin = 1
	`;
	const result = db.prepare(qAdminStatus).get({ network, adminDomain, user }) as {count: number};
	return result.count !== 0;
};

/** Set a user as event moderator */
export const queryAddModerator = (network: string, adminDomain: string, user: string): void => {
	const qAddModerator = `
		INSERT INTO moderators(network, admin_domain, user)
		VALUES ($network, $adminDomain, $user)
	`;
	db.prepare(qAddModerator).run({ network, adminDomain, user });
};

/** Remove a user as event moderator */
export const queryRemoveModerator = (network: string, adminDomain: string, user: string): void => {
	const qRemoveModerator = `
		DELETE FROM moderators
		WHERE network = $network AND admin_domain = $adminDomain AND user = $user
	`;
	db.prepare(qRemoveModerator).run({ network, adminDomain, user });
};

/** Checks whether a given user is an event moderator on a given network and guild or channel */
export const queryModeratorStatus = (
	network: string,
	adminDomain: string,
	user: string,
): boolean => {
	// Check if user is a moderator
	const qModerator = `
		SELECT count(*) count
		FROM moderators
		WHERE network = $network AND admin_domain = $adminDomain AND user = $user
	`;
	const moderator = db.prepare(qModerator).get({ network, adminDomain, user }) as {count: number};
	return moderator.count !== 0;
};

/** Get the number of events for a channel, optionally filtered to just ones a user is subscribed to */
export const queryEventCount = (network: string, channel: string, user?: string): number => {
	const qEventCount = `
		SELECT count(*) count
		FROM events
		WHERE network = $network AND channel = $channel
	`;
	const qUserEventCount = `
		SELECT count(*) count
		FROM
			events INNER JOIN subscriptions ON
			events.network = subscriptions.network AND
			events.channel = subscriptions.channel AND
			events.name = subscriptions.event
		WHERE events.network = $network AND events.channel = $channel AND subscriptions.user = $user
	`;
	if (user) return (db.prepare(qUserEventCount).get({ network, channel, user }) as any).count;
	return (db.prepare(qEventCount).get({ network, channel }) as any ).count;
};

/** Get the next event for a channel, optionally filtered to just the ones a user is subscribed to */
export const queryNextEvent = (
	network: string,
	channel: string,
	offset = 0,
	user?: string,
): { name: string; description: string; time: number } => {
	const qNextEvent = `
		SELECT name, description, time
		FROM events
		WHERE network = $network AND channel = $channel
		ORDER BY time ASC
		LIMIT 1 OFFSET $offset
	`;
	// Get next event for a user in a channel
	const qNextUserEvent = `
		SELECT events.name, events.description, events.time
		FROM
			events INNER JOIN subscriptions ON
				events.network = subscriptions.network AND
				events.channel = subscriptions.channel AND
				events.name = subscriptions.event
		WHERE events.network = $network AND events.channel = $channel AND subscriptions.user = $user
		ORDER BY time ASC
		LIMIT 1 OFFSET $offset
	`;
	if (user) return db.prepare(qNextUserEvent).get({ network, channel, offset, user }) as any;
	return db.prepare(qNextEvent).get({ network, channel, offset }) as any;
};

/** Get next 20 events in a channel, optionally filtered to those a user is subscribed to */
export const queryEventList = (
	network: string,
	channel: string,
	offset = 0,
	user?: string,
): { name: string; description: string; time: number }[] => {
	// Get next 10 event names for a channel
	const qEventList = `
		SELECT name, description, time
		FROM events
		WHERE network = $network AND channel = $channel
		ORDER BY time ASC
		LIMIT 20 OFFSET $offset
	`;
	// Get next 10 event names for a user in a channel
	const qUserEventList = `
		SELECT events.name, events.description, time
		FROM
			events INNER JOIN subscriptions ON
				events.network = subscriptions.network AND
				events.channel = subscriptions.channel AND
				events.name = subscriptions.event
		WHERE events.network = $network AND events.channel = $channel AND subscriptions.user = $user
		ORDER BY time ASC
		LIMIT 20 OFFSET $offset
	`;
	if (user) return db.prepare(qUserEventList).all({ network, channel, user, offset }) as any;
	return db.prepare(qEventList).all({ network, channel, offset }) as any;
};
