import {
	queryChangeEventDescription,
	queryChangeEventInterval,
	queryChangeEventTime,
	queryDeleteEvent,
	queryEvent,
	queryModeratorStatus,
	queryRenameEvent,
	querySetEventPersistence,
	querySubscriberCount,
} from "./queries.js";
import { cancelTimeout, checkEvent } from "./event_checks.js";
import { getDateFromEventOrTime, parseDuration } from "./time_parsing.js";
import { CommandError } from "../Errors.js";
import { stringifyInterval } from "interval-conversions";
import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";
import { CATEGORY, MINIMUM_INTERVAL } from "./common.js";
import { Network } from "../common_types.js";
import { log } from "../log.js";
import { ApplicationCommandOptionType } from "discord.js";

/** Deletes an event */
const deleteEvent = (
	network: Network,
	adminDomain: string,
	channelId: string,
	eventName: string,
	userId: string,
): string => {
	if (!queryModeratorStatus(network, adminDomain, userId))
		throw new CommandError("Error: you are not an event moderator");
	if (!eventName) throw new CommandError();
	const event = queryEvent(network, channelId, eventName);
	if (!event) throw new CommandError("Error: no event by that name");
	queryDeleteEvent(network, channelId, event.name);
	// If event was being tracked via timeout, stop that
	cancelTimeout(network, channelId, event.name);
	log(`User ${userId} deleted event "${event.name}"`);
	return `Event "${event.name}" was deleted.`;
};

const commandDeleteEvent: CommandFunction = (cCall) => {
	const { network, channel, user, domain } = cCall;
	const eventName = cCall.commaArgs[0]?.trim();
	if (!eventName) throw new CommandError();
	cCall.reply(deleteEvent(network, domain, channel.id, eventName, user.id));
};

const slashDeleteEvent: SlashCommandFunction = (interaction) => {
	const adminDomain = interaction.guildId;
	if (!adminDomain) throw new CommandError("Could not get guild ID");
	const event = interaction.options.getString("event", true);
	return deleteEvent("discord", adminDomain, interaction.channelId, event, interaction.user.id);
};

addCommand({
	name: "deleteevent",
	run: commandDeleteEvent,
	category: CATEGORY,
	usage: "<event>",
	description: "Deletes the chosen event. (event moderators only)",
	slash: {
		run: slashDeleteEvent,
		options: [
			{
				name: "event",
				type: ApplicationCommandOptionType.String,
				description: "The event to delete",
				required: true,
			},
		],
	},
});

/** Changes the event name */
const renameEvent = (
	network: Network,
	adminDomain: string,
	channelId: string,
	eventName: string,
	newEventName: string | undefined,
	userId: string,
): string => {
	if (!queryModeratorStatus(network, adminDomain, userId))
		throw new CommandError("Error: you are not an event moderator");
	if (!eventName || !newEventName) throw new CommandError();
	if (newEventName.length > 40) throw new CommandError("Error: new name is over 40 characters");
	if (/ [+-] |<@[!&]?\d+>/.test(newEventName))
		throw new CommandError(
			`Error: " + ", " - " (with the spaces) and mentions are not allowed in event names`,
		);
	if (eventName === newEventName) throw new CommandError("Error: it already has that name");

	const event = queryEvent(network, channelId, eventName);
	if (!event) throw new CommandError("Error: no event by that name");
	const newEvent = queryEvent(network, channelId, newEventName);
	if (newEvent)
		throw new CommandError(`Error: an event by the name "${newEvent.name}" already exists`);

	queryRenameEvent(network, channelId, event.name, newEventName);
	// If event was being tracked via timeout under old name, stop that, and check it again
	const cancelled = cancelTimeout(network, channelId, event.name);
	if (cancelled) checkEvent(network, channelId, newEventName);

	return `Event "${event.name}" has been renamed to "${newEventName}".`;
};

const commandRenameEvent: CommandFunction = (cCall) => {
	const { network, channel, user, domain } = cCall;
	const eventName = cCall.commaArgs[0]?.trim();
	if (!eventName) throw new CommandError();
	const newEventName = cCall.commaArgs[1]?.trim();
	cCall.reply(renameEvent(network, domain, channel.id, eventName, newEventName, user.id));
};

const slashRenameEvent: SlashCommandFunction = (interaction) => {
	const adminDomain = interaction.guildId;
	if (!adminDomain) throw new CommandError("Could not get guild ID");
	const event = interaction.options.getString("event", true);
	const newEventName = interaction.options.getString("new_name", true);
	return renameEvent(
		"discord",
		adminDomain,
		interaction.channelId,
		event,
		newEventName,
		interaction.user.id,
	);
};

addCommand({
	name: "renameevent",
	run: commandRenameEvent,
	category: CATEGORY,
	usage: "<event>,<new name>",
	description: "Changes the name of the chosen event. (event moderators only)",
	slash: {
		run: slashRenameEvent,
		options: [
			{
				name: "event",
				type: ApplicationCommandOptionType.String,
				description: "The event to rename",
				required: true,
			},
			{
				name: "new_name",
				type: ApplicationCommandOptionType.String,
				description: "The new name for the event",
				required: true,
			},
		],
	},
});

/** Changes when the event will be */
const changeEventTime = (
	network: Network,
	adminDomain: string,
	channelId: string,
	eventName: string,
	newEventTime: string | undefined,
	userId: string,
): string => {
	if (!queryModeratorStatus(network, adminDomain, userId))
		throw new CommandError("Error: you are not an event moderator");
	if (!eventName) throw new CommandError();
	if (!newEventTime) throw new CommandError();

	const event = queryEvent(network, channelId, eventName);
	if (!event) throw new CommandError("Error: no event by that name");
	const parsedDate = getDateFromEventOrTime(
		network,
		channelId,
		newEventTime,
		new Date(event.time),
	);
	if (!parsedDate) throw new CommandError("Error: could not parse time format");

	const { date } = parsedDate;
	if (date.getTime() < Date.now()) throw new CommandError("Error: specified time is in the past");
	if (date.getTime() === event.time)
		throw new CommandError("Error: event is already at that time");

	queryChangeEventTime(network, channelId, event.name, date.getTime());
	// If event was being tracked via timeout, stop that
	cancelTimeout(network, channelId, event.name);
	checkEvent(network, channelId, event.name);

	const change = stringifyInterval(date.getTime() - event.time);
	const upback = date.getTime() > event.time ? "back" : "up";
	return `Event "${event.name}" time moved ${change} ${upback}. It is now ${stringifyInterval(
		date.getTime() - Date.now(),
	)} from now.`;
};

const commandChangeEventTime: CommandFunction = (cCall) => {
	const { network, channel, user, domain } = cCall;
	const eventName = cCall.commaArgs[0]?.trim();
	if (!eventName) throw new CommandError();
	const newEventTime = cCall.commaArgs[1]?.trim();
	cCall.reply(changeEventTime(network, domain, channel.id, eventName, newEventTime, user.id));
};

const slashChangeEventTime: SlashCommandFunction = (interaction) => {
	const adminDomain = interaction.guildId;
	if (!adminDomain) throw new CommandError("Could not get guild ID");
	const event = interaction.options.getString("event", true);
	const newEventTime = interaction.options.getString("newtime", true);
	return changeEventTime(
		"discord",
		adminDomain,
		interaction.channelId,
		event,
		newEventTime,
		interaction.user.id,
	);
};

addCommand({
	name: "moveevent",
	run: commandChangeEventTime,
	category: CATEGORY,
	usage: "<event>,<time>",
	description: "Changes the time of the chosen event. (event moderators only)",
	slash: {
		run: slashChangeEventTime,
		options: [
			{
				name: "event",
				type: ApplicationCommandOptionType.String,
				description: "The event to move",
				required: true,
			},
			{
				name: "newtime",
				type: ApplicationCommandOptionType.String,
				description:
					"The new time for the event. This can be a specific time, or an offset from the current time.",
				required: true,
			},
		],
	},
});

/** Changes the event interval */
const changeEventInterval = (
	network: Network,
	adminDomain: string,
	channelId: string,
	eventName: string,
	newEventInterval: string | undefined,
	userId: string,
): string => {
	if (!queryModeratorStatus(network, adminDomain, userId))
		throw new CommandError("Error: you are not an event moderator");
	if (!eventName) throw new CommandError();

	const event = queryEvent(network, channelId, eventName);
	if (!event) throw new CommandError("Error: no event by that name");

	// Remove interval
	if (!newEventInterval) {
		if (!event.interval) throw new CommandError("Error: event already lacked an interval");
		queryChangeEventInterval(network, channelId, event.name);
		const intervalText =
			typeof event.interval === "number" ? stringifyInterval(event.interval) : event.interval;
		return `Interval of ${intervalText} for event "${event.name}" removed. It is no longer a recurring event.`;
	}

	const parsedInterval = parseDuration(newEventInterval);
	if (!parsedInterval) throw new CommandError("Error: could not parse interval");
	const { invariable, difference = 0 } = parsedInterval;
	/** Number in ms */
	const intervalTextOrNumber = invariable
		? (parsedInterval.difference as number)
		: (parsedInterval.text as string);
	// To do: replace above assertions with interfaces on the parseDuration object

	if (intervalTextOrNumber === event.interval)
		throw new CommandError("Error: event is already at that interval");
	if (difference < MINIMUM_INTERVAL) throw new CommandError("Error: interval is under 4 hours");

	queryChangeEventInterval(network, channelId, event.name, intervalTextOrNumber);

	let output = ``;
	if (event.interval) {
		output += `Interval for event "${event.name}" `;
		if (invariable && typeof event.interval === "number") {
			const intervalChange = stringifyInterval(event.interval - difference);
			output += `made ${intervalChange} ${
				event.interval > difference ? `shorter` : `longer`
			}.`;
		} else {
			output += `changed.`;
		}
		output += ` It is now ${invariable ? stringifyInterval(difference) : intervalTextOrNumber}`;
	} else {
		output += `Added an interval of ${
			invariable ? stringifyInterval(difference) : intervalTextOrNumber
		} to event "${event.name}". It is now a recurring event.`;
	}
	return output;
};

/** Changes the event interval */
const commandChangeEventInterval: CommandFunction = (cCall) => {
	const { network, channel, user, domain } = cCall;
	const eventName = cCall.commaArgs[0]?.trim();
	if (!eventName) throw new CommandError();
	const newEventInterval = cCall.commaArgs[1]?.trim();
	cCall.reply(
		changeEventInterval(network, domain, channel.id, eventName, newEventInterval, user.id),
	);
};

const slashChangeEventInterval: SlashCommandFunction = (interaction) => {
	const adminDomain = interaction.guildId;
	if (!adminDomain) throw new CommandError("Could not get guild ID");
	const event = interaction.options.getString("event", true);
	const newEventInterval = interaction.options.getString("interval") ?? undefined;
	return changeEventInterval(
		"discord",
		adminDomain,
		interaction.channelId,
		event,
		newEventInterval,
		interaction.user.id,
	);
};

addCommand({
	name: "changeeventinterval",
	run: commandChangeEventInterval,
	category: CATEGORY,
	usage: "<event>,[interval]",
	description:
		"Adds an interval to the chosen event, or changes or removes an existing one. (event moderators only)",
	slash: {
		run: slashChangeEventInterval,
		options: [
			{
				name: "event",
				type: ApplicationCommandOptionType.String,
				description: "The event to change the interval of",
				required: true,
			},
			{
				name: "interval",
				type: ApplicationCommandOptionType.String,
				description: "The new interval for the event",
			},
		],
	},
});

/** Changes the event description */
const changeEventDescription = (
	network: Network,
	adminDomain: string,
	channelId: string,
	eventName: string,
	newDescription: string | undefined,
	userId: string,
): string => {
	const event = queryEvent(network, channelId, eventName);
	if (!event) throw new CommandError("Error: no event by that name");
	if (newDescription === "") newDescription = undefined;
	if (!newDescription && !event.description)
		throw new CommandError("Error: event already lacks a description");
	if (newDescription === event.description)
		throw new CommandError("Error: event already has that description");

	if (event.description && !queryModeratorStatus(network, adminDomain, userId))
		throw new CommandError(`Error: only event admins can change existing descriptions`);

	queryChangeEventDescription(network, channelId, eventName, newDescription);

	if (!newDescription) {
		return `The description for event "${event.name}" has been removed.`;
	} else if (!event.description) {
		return `A description has been added to event "${event.name}".`;
	} else {
		return `The description for event "${event.name}" has been changed.`;
	}
};

const commandChangeEventDescription: CommandFunction = (cCall) => {
	const { network, channel, user, domain } = cCall;
	const eventName = cCall.commaArgs[0]?.trim();
	const newDescription = cCall.commaArgs.slice(1).join(",").trim();
	if (!eventName) throw new CommandError();
	cCall.reply(
		changeEventDescription(network, domain, channel.id, eventName, newDescription, user.id),
	);
};

const slashChangeEventDescription: SlashCommandFunction = (interaction) => {
	const adminDomain = interaction.guildId;
	if (!adminDomain) throw new CommandError("Could not get guild ID");
	const event = interaction.options.getString("event", true);
	const newDescription = interaction.options.getString("description") ?? undefined;
	return changeEventDescription(
		"discord",
		adminDomain,
		interaction.channelId,
		event,
		newDescription,
		interaction.user.id,
	);
};

addCommand({
	name: "describeevent",
	run: commandChangeEventDescription,
	category: CATEGORY,
	usage: "<event>,[description]",
	description:
		"Add a description to the chosen event, or (event admin only) change an existing description.",
	slash: {
		run: slashChangeEventDescription,
		options: [
			{
				name: "event",
				type: ApplicationCommandOptionType.String,
				description: "The event to change the description of",
				required: true,
			},
			{
				name: "description",
				type: ApplicationCommandOptionType.String,
				description: "The new description for the event",
			},
		],
	},
});

/** Sets whether an event persists even when there are no subscribers */
const persistEvent = (
	network: Network,
	adminDomain: string,
	channelId: string,
	eventName: string | undefined,
	persist: boolean,
	userId: string,
): string => {
	if (!queryModeratorStatus(network, adminDomain, userId))
		throw new CommandError("Error: you are not an event moderator");
	if (!eventName) throw new CommandError();
	const event = queryEvent(network, channelId, eventName);
	if (!event) throw new CommandError("Error: no event by that name");

	querySetEventPersistence(network, channelId, event.name, persist);

	if (persist) {
		return `Event "${event.name}" will now persist even if it loses all subscribers.`;
	} else if (querySubscriberCount(network, channelId, event.name) === 0) {
		queryDeleteEvent(network, channelId, event.name);
		return `Event "${event.name}" was deleted because it had no subscribers and it was no longer persistent.`;
	} else {
		return `Event "${event.name}" will no longer persist if it loses all subscribers.`;
	}
};

const commandPersistEvent: CommandFunction = (cCall) => {
	const { network, channel, user, domain } = cCall;
	const eventName = cCall.commaArgs[0]?.trim();
	const persistArgument = cCall.commaArgs[1]?.trim()?.toLowerCase();
	if (
		persistArgument !== undefined &&
		persistArgument !== "" &&
		persistArgument !== "on" &&
		persistArgument !== "off"
	)
		throw new CommandError();
	const persist = persistArgument !== "off";
	cCall.reply(persistEvent(network, domain, channel.id, eventName, persist, user.id));
};

const slashPersistEvent: SlashCommandFunction = (interaction) => {
	const adminDomain = interaction.guildId;
	if (!adminDomain) throw new CommandError("Could not get guild ID");
	const event = interaction.options.getString("event", true);
	const persist = interaction.options.getString("persist") !== "off";
	return persistEvent(
		"discord",
		adminDomain,
		interaction.channelId,
		event,
		persist,
		interaction.user.id,
	);
};

addCommand({
	name: "persistevent",
	run: commandPersistEvent,
	category: CATEGORY,
	usage: `<event>,["on" (default) or "off"]`,
	description:
		"Sets whether the event will persist even when it has no subscribers. (event moderators only)",
	slash: {
		run: slashPersistEvent,
		options: [
			{
				name: "event",
				type: ApplicationCommandOptionType.String,
				description: "The event to change the persistence of",
				required: true,
			},
			{
				name: "persist",
				type: ApplicationCommandOptionType.String,
				description: "Whether to turn persistence on or off (default: on)",
				choices: [
					{
						name: "on",
						value: "on",
					},
					{
						name: "off",
						value: "off",
					},
				],
			},
		],
	},
});
