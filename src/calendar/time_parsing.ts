import { CommandError } from "../Errors.js";
import { db } from "../db.js";
import { queryEvent } from "./queries.js";
import { parseInterval } from "interval-conversions";
import { Network } from "../common_types.js";

export type ParsedDate = {
	date: Date;
	/** The input interval, if it ended up parsed as an interval. */
	text?: string;
	/** Whether the input ended up parsed as an interval, like 3h. */
	duration: boolean;
	/** The number of ms between a specified date (or now) and the target time. */
	difference?: number;
	/** Whether the actual amount of time varies by when the input is parsed, because there were months and/or years in the input. */
	invariable?: boolean;
	/** The name of the event the input was interpreted as, and the date was retrieved from, if any. */
	event?: string;
};

/**
 * Sees if text is the name of an event, or event + offset, otherwise tries to parse as time.
 * @param date If it ends up parsed as an offset, it will be as an offset off this date (or now, if not supplied)
 * @returns date is a Date object, event is the event's name, if an event was matched
 */
export const getDateFromEventOrTime = (
	network: Network,
	channel: string,
	eventOrTime: string,
	datearg?: Date,
): ParsedDate | undefined => {
	// Try to split it up into event and duration. If this succeeds but parsing fails, throw.
	const match = /^(.+?)(?: ([+-]) (.+))$/i.exec(eventOrTime) as
		| null
		| [string, string, "+" | "-", string];
	if (match) {
		const eventName = match[1];
		const eventresult = queryEvent(network, channel, eventName);
		if (!eventresult)
			throw new CommandError(`There is no event by the name "${eventName}" in the database.`);
		let durationstring = match[3];
		if (match[2] === "-") {
			if (durationstring[0] === "-") {
				durationstring = durationstring.slice(1);
			} else {
				durationstring = `-${durationstring}`;
			}
		}
		const parsedDuration = parseDuration(durationstring, new Date(eventresult.time));
		if (!parsedDuration) throw new CommandError(`Could not parse offset.`);
		parsedDuration.event = eventresult.name;
		return parsedDuration;
	}

	// Try to get it as an event
	const eventresult = queryEvent(network, channel, eventOrTime);
	if (eventresult) {
		const date = new Date(eventresult.time);
		return {
			event: eventresult.name,
			date,
			duration: false,
			text: undefined,
			difference: undefined,
			invariable: undefined,
		};
	}
	// Try to parse it as time
	return parseTime(eventOrTime, datearg);
};

/** Parses a user string like 1y2mo3w4d5h6m7s */
export const parseDuration = (text: string, startDate?: Date): ParsedDate | undefined => {
	text = text.trim();
	const difference = parseInterval(text, startDate);
	if (difference === undefined) return;
	const invariable = !/\d*\.?\d+ ?(?:y(?:ears?)?|mo(?:nths?)?)/i.test(text);
	const date = new Date((startDate?.getTime() ?? Date.now()) + difference);
	return {
		date,
		text,
		duration: true,
		difference,
		invariable,
		event: undefined,
	};
};
/**
 * Tries to parse a string as if it contains a timestamp in format 20:30[:00] [CET[+2]]
 * @param string The string presumed to hold a timestamp
 * @returns A Date object, or undefined if it failed
 */
const parseTimeOfDay = (string: string): Date | undefined => {
	const match = /^(\d\d):(\d\d)(?::(\d\d))?(?: ([a-z]{1,4})([+-]\d{1,4})?)?$/i.exec(string) as
		| null
		| [string, string, string, string | undefined]
		| [string, string, string, string | undefined, string, string | undefined];
	const format = `format: 20:30[:00] [CET[+2]]`;
	if (!match) return; // No regex match, fail

	let hours = Number(match[1]);
	const minutes = Number(match[2]);
	const seconds = match[3] ? Number(match[3]) : 0; // If no seconds, set to 0

	if (isNaN(hours) || hours < 0 || hours >= 24)
		// hours invalid or missing
		throw new CommandError(`Invalid hours, ${format}`);
	if (isNaN(minutes) || minutes < 0 || minutes >= 60)
		// minutes invalid or missing
		throw new CommandError(`Invalid minutes, ${format}`);
	if (isNaN(seconds) || seconds < 0 || seconds >= 60)
		// seconds invalid
		throw new CommandError(`Invalid seconds, ${format}`);

	const offset = match[4] === undefined ? 0 : lookUpTimezone(match[4], match[5]);

	hours -= offset; // adjust for offset
	const date = new Date();
	date.setUTCHours(hours, minutes, seconds, 0);
	const days_behind = Math.ceil((Date.now() - date.getTime()) / 1000 / 60 / 60 / 24); // Calculate how many days behind the present, rounded up,
	date.setUTCDate(date.getUTCDate() + days_behind); // then as many days as needed.
	return date;
};
/**
 * Tries replacing the timezone at the end of a string with a different one, to see if new Date() will take it then
 * @returns Date object or undefined if it didn't work
 */
const parseConvertedTimezone = (string: string): Date | undefined => {
	const match = / (([a-z]{1,4})([+-]\d{1,4})?)$/i.exec(string) as
		| null
		| [string, string, string, string | undefined];
	if (!match) return undefined; // Could not find anything that looks like a time zone

	let offset;
	try {
		offset = lookUpTimezone(match[2], match[3]);
	} catch (error) {
		if (error instanceof CommandError && error.message.startsWith("Timezone")) return;
		else throw error;
	}
	if (offset >= 100 || offset <= -100) throw new CommandError(`Offset too large`);

	const plus = offset >= 0 ? "+" : "";
	string = string.slice(-0, -match[1].length); // Remove the timezone string
	const offset_string = Math.round(offset * 100)
		.toString(10)
		.padStart(4, "0"); // Pad to 0200 format
	const date = new Date(`${string}UTC${plus}${offset_string}`);
	if (isNaN(date.getTime()))
		// Invalid date
		return;
	return date;
};

/**
 * Takes timezone input like "MDT" or "UTC+1"
 *
 * Returns an offset (in hours) if it succeeded
 *
 * Returns undefined if it did not look like a timezone
 *
 * Throws a CommandError if the offset was too large or if the timezone was not found in the database
 */
export const parseAsTimezone = (string: string): number | undefined => {
	const match = /^([a-z]{1,4})([+-]\d{1,4})?$/i.exec(string) as
		| null
		| [string, string, string | undefined];
	if (!match) return; // Could not find anything that looks like a timezone
	const offset = lookUpTimezone(match[1], match[2]);
	if (offset >= 100 || offset <= -100) throw new CommandError(`Offset too large`);
	return offset;
};
/**
 * Looks up a timezone's offset and optionally combines it with a numerical offset, or throws an error
 * @param timezone Timezone abbreviation (UTC, CEST etc.)
 * @param offset Offset (+5, +0500, -3 etc.)
 * @returns Total offset in hours
 */
const lookUpTimezone = (timezone: string, offset?: string): number => {
	// Searches the timezone database
	const querytimezone = `
		SELECT offset
		FROM timezones
		WHERE abbreviation = $timezone
	`;

	timezone = timezone.toUpperCase();

	const utcOffset = db.prepare(querytimezone).get({ timezone }) as { offset: number };
	if (!utcOffset) throw new CommandError(`Timezone ${timezone} not in database`);

	let offsetNumber: number;
	if (offset !== undefined) {
		offsetNumber = Number(offset);
		if (isNaN(offsetNumber))
			throw new CommandError(`Invalid offset, format: 20:30[:00] [CET[+10]]`);
		if (offsetNumber >= 100 || offsetNumber <= -100) {
			// If it's 100, 1000, convert to 1 or 10, for consistency with Date
			offsetNumber = offsetNumber / 100;
		}
	} else {
		offsetNumber = 0;
	}
	return utcOffset.offset + offsetNumber;
};

/**
 * Tries four different ways of interpreting a string that is meant to contain time information:
 * --- HH:mm[:ss][ timezone[+offset]] timestamp for next time it's this time of day
 * --- [1d][2h][3m][4s] duration
 * --- new Date() (who knows what it will accept) timestamp
 * --- Replacing timestamp with a UTC+0000 one, then new Date() again
 * @param {string} string The string presumed to hold a timestamp or duration
 * @param datearg A Date object, to add durations to. If omitted, acts as a new Date representing now.
 */
const parseTime = (string: string, datearg?: Date): ParsedDate | undefined => {
	// Try my HH:mm[:ss][ timezone[+offset]] parser
	let date = parseTimeOfDay(string);
	if (date) {
		return parsedDateFromDate(date);
	}
	// Try my [1y][2mo][3w][4d][5h][6m][7s] parser
	const duration = parseDuration(string, datearg);
	if (duration) {
		return duration;
	}
	// try Date()
	date = new Date(string);
	if (!isNaN(date.getTime())) {
		return parsedDateFromDate(date);
	}
	// Try converting timezone, then Date() again
	date = parseConvertedTimezone(string);
	if (date && !isNaN(date.getTime())) {
		return parsedDateFromDate(date);
	}
	// Give up
	return;
};

/**
 * Makes a `ParsedDate` from a `Date`, which just holds `Date` and has all other fields default or blank.
 */
const parsedDateFromDate = (date: Date): ParsedDate => {
	return {
		date,
		duration: false,
		event: "",
		difference: 0,
		invariable: false,
		text: "",
	};
};
