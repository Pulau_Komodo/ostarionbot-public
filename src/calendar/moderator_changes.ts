import {
	queryAddModerator,
	queryAdminStatus,
	queryModeratorStatus,
	queryRemoveModerator,
} from "./queries.js";
import { CommandCall } from "../CommandCall.js";
import { CommandError } from "../Errors.js";
import { addCommand, CommandFunction } from "../commands.js";
import { CATEGORY } from "./common.js";

/** Adds a user as moderator */
export const addModerator: CommandFunction = (cCall) => {
	const {
		network,
		user: { id: user },
		domain: adminDomain,
	} = cCall;
	const targerUser = cCall.args[0];

	if (!queryAdminStatus(network, adminDomain, user))
		throw new CommandError(`You are not an administrator here.`);
	if (!targerUser) throw new CommandError();
	if (queryModeratorStatus(network, adminDomain, targerUser))
		throw new CommandError(`Error: ${targerUser} is already an event moderator here.`);

	queryAddModerator(network, adminDomain, targerUser);

	cCall.reply(`${targerUser} is now an event admin here.`);
};

/** Removes a user as moderator */
export const removeModerator: CommandFunction = (cCall: CommandCall): void => {
	const {
		network,
		user: { id: user },
		domain: adminDomain,
	} = cCall;
	const targetUser = cCall.args[0];

	if (!queryAdminStatus(network, adminDomain, user))
		throw new CommandError(`You are not an administrator here.`);
	if (!targetUser) throw new CommandError();
	if (queryAdminStatus(network, adminDomain, targetUser))
		throw new CommandError(`Error: ${targetUser} is an administrator here.`);
	if (!queryModeratorStatus(network, adminDomain, targetUser))
		throw new CommandError(`Error: ${targetUser} is not an event moderator here.`);

	queryRemoveModerator(network, adminDomain, targetUser);

	cCall.reply(`${targetUser} is no longer an event moderator here.`);
};
addCommand({
	name: "addeventmoderator",
	run: addModerator,
	category: CATEGORY,
	usage: "<user ID>",
	description: "Gives event moderator status to the chosen user. (event admins only)",
	aliases: ["addeventmod"],
});
addCommand({
	name: "removeeventmoderator",
	run: removeModerator,
	category: CATEGORY,
	usage: "<user ID>",
	description: "Removes event moderator status from the chosen user. (event admins only)",
	aliases: ["removeeventmod"],
});
