import { stringifyDateTime } from "../datetimeformat.js";
import { queryAddEvent, queryEvent, querySubscribe } from "./queries.js";
import { parseDuration, getDateFromEventOrTime } from "./time_parsing.js";
import { checkEvent } from "./event_checks.js";
import { CommandError } from "../Errors.js";
import { stringifyInterval } from "interval-conversions";
import {
	addCommand,
	CommandFunction,
	DiscordCommandFunction,
	SlashCommandFunction,
} from "../commands.js";
import { CATEGORY, MINIMUM_INTERVAL } from "./common.js";
import { Network } from "../common_types.js";
import { ApplicationCommandOptionType } from "discord.js";

/**
 * Performs various checks, and adds an event if all pass.
 *
 * Throws CommandError on check failures.
 */
const createEvent = (
	network: Network,
	guildId: string,
	channelId: string,
	eventName: string,
	eventTime: string,
	description: string,
	isRecurring: boolean,
	interval: string | undefined,
	userId: string,
): string => {
	if (!eventName) throw new CommandError("Error: the event needs a name");
	if (eventName.length > 40) throw new CommandError("Error: name is over 40 characters");
	if (/ [+-] |<@[!&]?\d+>/.test(eventName))
		throw new CommandError(
			`Error: " + ", " - " (with the spaces) and mentions are not allowed in event names`,
		);
	if (!eventTime) throw new CommandError("Error: the event needs a time");

	const parsedDate = getDateFromEventOrTime(network, channelId, eventTime);
	if (!parsedDate)
		throw new CommandError(
			"Error: could not parse time format. Example format: 2020-12-31 11:30",
		);

	const { date, duration, event: parsedEvent } = parsedDate;
	if (date.getTime() < Date.now()) throw new CommandError("Error: specified time is in the past");

	let parsedInterval,
		intervalTextOrNumber,
		difference = 0;
	if (isRecurring) {
		if (!interval) throw new CommandError("Error: no interval specified");
		parsedInterval = parseDuration(interval);
		if (!parsedInterval) throw new CommandError("Error: could not parse interval");
		intervalTextOrNumber = parsedInterval.invariable
			? parsedInterval.difference
			: parsedInterval.text;
		difference = parsedInterval.difference || 0;
	}

	if (isRecurring && difference < MINIMUM_INTERVAL)
		throw new CommandError("Error: interval is too short (under 4 hours)");
	if (description.length > 200)
		throw new CommandError("Error: description is over 200 characters");

	const event = queryEvent(network, channelId, eventName);
	if (event) throw new CommandError(`An event by the name "${event.name}" already exists`);

	queryAddEvent(
		network,
		guildId,
		channelId,
		eventName,
		description,
		date.getTime(),
		intervalTextOrNumber,
	);
	querySubscribe(userId, network, channelId, eventName);

	checkEvent(network, channelId, eventName);

	let output = parsedInterval ? `Recurring event` : `Event`;
	output += ` "${eventName}" added`;
	if (isRecurring) {
		output += ` with an interval of `;
		output += parsedInterval?.invariable
			? `${stringifyInterval(difference)}`
			: `${intervalTextOrNumber ?? "error"}`;
	}
	output += `. It is `;
	output += duration || parsedEvent ? `at ${stringifyDateTime(date)} UTC` : ``;
	output += parsedEvent ? `, ` : ``;
	output +=
		!duration || parsedEvent
			? `${stringifyInterval(date.getTime() - Date.now())} from now`
			: ``;
	output += `.`;

	return output;
};

const commandAddEvent: CommandFunction = (cCall) => {
	const {
		network,
		channel: { id: channelId },
		guildId,
		user: { id: userId },
	} = cCall;
	const eventName = cCall.commaArgs[0]?.trim() ?? "";
	const eventTime = cCall.commaArgs[1]?.trim() ?? "";
	const description = cCall.commaArgs.slice(2).join(",").trim();

	const output = createEvent(
		network,
		guildId,
		channelId,
		eventName,
		eventTime,
		description,
		false,
		undefined,
		userId,
	);
	cCall.reply(output);
};

const commandAddRecurringEvent: CommandFunction = (cCall) => {
	const {
		network,
		channel: { id: channelId },
		guildId,
		user: { id: userId },
	} = cCall;
	const eventName = cCall.commaArgs[0]?.trim() ?? "";
	const eventTime = cCall.commaArgs[1]?.trim() ?? "";
	const interval = cCall.commaArgs[2]?.trim() ?? "";
	const description = cCall.commaArgs.slice(3).join(",").trim();

	const output = createEvent(
		network,
		guildId,
		channelId,
		eventName,
		eventTime,
		description,
		true,
		interval,
		userId,
	);
	cCall.reply(output);
};

const commandRemindMe: DiscordCommandFunction = (cCall) => {
	const {
		network,
		channel: { id: channelId },
		guildId,
		user: { id: userId },
	} = cCall;
	const eventName = Date.now().toString(36);
	const eventTime = cCall.commaArgs[0]?.trim() ?? "";
	const description = cCall.message.url;

	const output = createEvent(
		network,
		guildId,
		channelId,
		eventName,
		eventTime,
		description,
		false,
		undefined,
		userId,
	);
	cCall.reply(output);
};

const slashAddEvent: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const eventName = interaction.options.getString("name", true);
	const eventTime = interaction.options.getString("time", true);
	const description = interaction.options.getString("description");
	return createEvent(
		"discord",
		interaction.guildId,
		interaction.channelId,
		eventName,
		eventTime,
		description ?? "",
		false,
		undefined,
		interaction.user.id,
	);
};

const slashAddRecurringEvent: SlashCommandFunction = (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const eventName = interaction.options.getString("name", true);
	const eventTime = interaction.options.getString("time", true);
	const interval = interaction.options.getString("interval", true);
	const description = interaction.options.getString("description");
	const output = createEvent(
		"discord",
		interaction.guildId,
		interaction.channelId,
		eventName,
		eventTime,
		description ?? "",
		true,
		interval,
		interaction.user.id,
	);
	checkEvent("discord", interaction.channelId, eventName);
	return output;
};

addCommand({
	name: "addevent",
	run: commandAddEvent,
	category: CATEGORY,
	usage: "<name>,<time>,[description]",
	description:
		"Adds a new event at the specified time. Time format: 2050-01-25 06:05:00 UTC-4, or 06:05 CET, or 1d2h3m4s.",
	slash: {
		run: slashAddEvent,
		description: "Adds a new event at the specified time.",
		options: [
			{
				name: "name",
				type: ApplicationCommandOptionType.String,
				description: "The name the event will have",
				required: true,
			},
			{
				name: "time",
				type: ApplicationCommandOptionType.String,
				description: "The time at which the event will trigger",
				required: true,
			},
			{
				name: "description",
				type: ApplicationCommandOptionType.String,
				description: "A description of the event",
			},
		],
	},
	heat: { fixed: 2 },
});
addCommand({
	name: "addrecurringevent",
	run: commandAddRecurringEvent,
	category: CATEGORY,
	usage: "<name>,<time>,<interval>,[description]",
	description:
		"Adds a new recurring event at the specified time and interval. Interval must be at least 4 hours. Interval format: 1d2h3m4s or 1y.",
	slash: {
		run: slashAddRecurringEvent,
		description: "Adds a new recurring event at the specified time.",
		options: [
			{
				name: "name",
				type: ApplicationCommandOptionType.String,
				description: "The name the event will have",
				required: true,
			},
			{
				name: "time",
				type: ApplicationCommandOptionType.String,
				description: "The time at which the event will trigger",
				required: true,
			},
			{
				name: "interval",
				type: ApplicationCommandOptionType.String,
				description:
					'The interval between event triggers, with a format like "1d" or "1d12h"',
				required: true,
			},
			{
				name: "description",
				type: ApplicationCommandOptionType.String,
				description: "A description of the event",
			},
		],
	},
	heat: { fixed: 2 },
});
addCommand({
	name: "remindme",
	run: commandRemindMe,
	category: CATEGORY,
	usage: "<time>",
	description:
		"Adds a new event at the specified time, with a generated name and a link to the comment with the command as the description. Time format: 2050-01-25 06:05:00 UTC-4, or 06:05:00 -04:00, or 1d2h3m4s.",
	heat: { fixed: 2 },
	discordOnly: true,
});
