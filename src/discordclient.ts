import {
	Client,
	Message,
	TextChannel,
	GuildMember,
	Guild,
	GatewayIntentBits,
	Partials,
	ThreadChannel,
	InteractionReplyOptions,
	CommandInteraction,
} from "discord.js"; // Interacts with Discord
import { checkSoonEvents } from "./calendar/event_checks.js";
import { slashCommandMap } from "./commands.js";

import { discordtoken } from "./config.js"; // bot configuration file
import { CommandError } from "./Errors.js";
import { log } from "./log.js"; // make timestamps of chosen format
import onMessage from "./onMessage.js";
import { addBaseUserHeat, addScalingUserHeat, checkBan } from "./overheat.js";
import { edgyDomains } from "./settings.js";

export const discordClient = new Client({
	intents: [
		GatewayIntentBits.Guilds,
		GatewayIntentBits.GuildMembers,
		GatewayIntentBits.GuildEmojisAndStickers,
		GatewayIntentBits.GuildMessages,
		GatewayIntentBits.GuildMessageReactions,
		GatewayIntentBits.MessageContent,
	],
});

export interface GuildMessage extends Message {
	guild: Guild;
	channel: TextChannel;
	member: GuildMember;
}

const zws = "\u200b";

/** Inserts zero-width spaces after each @ that is part of a mention */
const escapeMentions = (text: string): string => {
	text = text.replace(/<@([&!]?\d+)>/g, `<@${zws}$1>`); // Escape user and role mentions
	text = text.replace(/@(everyone|here)/gi, `@${zws}$1`); // Escape everyone and here mentions
	return text;
};

export class DiscordMessage {
	readonly network = "discord";
	readonly message: GuildMessage;
	readonly content: string;
	readonly userId: string;
	readonly channel: TextChannel;
	readonly whisper = false;
	constructor(message: GuildMessage) {
		this.message = message;
		this.content = escapeMentions(message.content);
		this.userId = message.author.id;
		this.channel = message.channel;
	}
}

const isGuildMessage = (object: Message): object is GuildMessage => {
	return object.channel instanceof TextChannel || object.channel instanceof ThreadChannel;
};

discordClient.login(discordtoken).catch(console.log);

discordClient.on("ready", () => {
	log("Discord ready");
	checkSoonEvents("discord", true);
});

discordClient.on("messageCreate", (message: Message) => {
	if (!isGuildMessage(message)) return; // Not a message in a guild text channel
	if (message.author.id === discordClient.user?.id) return; // Own message

	void onMessage(new DiscordMessage(message));
});

const replyOrFollowup = (
	interaction: CommandInteraction,
	replyOptions: InteractionReplyOptions,
) => {
	if (interaction.replied) interaction.followUp(replyOptions).catch(log);
	else interaction.reply(replyOptions).catch(log);
};

discordClient.on("interactionCreate", async (interaction) => {
	if (!interaction.isChatInputCommand()) return;
	if (checkBan("discord", interaction.user.id)) return;
	const slashCommand = slashCommandMap.get(interaction.commandName);
	if (slashCommand !== undefined) {
		const domain = interaction.guildId;
		if (slashCommand.edgy && domain && !edgyDomains.has(domain)) return;
		log(`slash command: ${slashCommand.name}`);
		try {
			addBaseUserHeat("discord", interaction.user.id);
			const output = await slashCommand.run(interaction);
			if (output === undefined) return;
			const text = typeof output === "string" ? output : output.content ?? "";
			log(`slash command output: ${text}`);
			addScalingUserHeat("discord", interaction.user.id, text.length);
			await interaction.reply(output).catch(console.error);
		} catch (error) {
			if (error instanceof CommandError) {
				if (error.message) {
					replyOrFollowup(interaction, {
						content: error.message,
						ephemeral: true,
					});
				} else {
					log("Tried to reply usage to a slash command");
				}
			} else {
				replyOrFollowup(interaction, {
					content: "Unknown error executing command",
					ephemeral: true,
				});
				log(error);
			}
		}
	}
});

export const discordclient = discordClient;
export default discordClient;
