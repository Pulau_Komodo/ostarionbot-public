import { PermissionFlagsBits } from "discord.js";
import { addCommand, DiscordCommandFunction, slashCommandMap } from "./commands.js";
import discordClient from "./discordclient.js";
import { edgyDomains } from "./settings.js";

const cRegisterGuildCommands: DiscordCommandFunction = async (cCall) => {
	if (!cCall.message.member.permissions.has(PermissionFlagsBits.Administrator)) return;

	const data = [...slashCommandMap.values()]
		.filter(({ edgy }) => !edgy || edgyDomains.has(cCall.domain))
		.map(({ name, description, options }) => {
			return { name, description, options };
		});

	const commands = await cCall.message.guild.commands.set(data).catch(console.error);
	if (commands) {
		const commandInfo = commands.map((command) => {
			return {
				id: command.id,
				name: command.name,
				description: command.description,
				optionCount: command.options.length,
				defaultPermissions: command.defaultMemberPermissions,
			};
		});
		console.log(commandInfo);
		cCall.reply(`Set ${commands.size} slash command${commands.size === 1 ? "" : "s"}.`);
	} else {
		cCall.reply("Failed somehow.");
	}
};

addCommand({
	name: "registerguildcommands",
	run: cRegisterGuildCommands,
	category: "debug",
	description: "Developer-only. Registers the current list of slash commands with the guild.",
	discordOnly: true,
});

const cClearGlobalCommands: DiscordCommandFunction = async (cCall) => {
	if (!cCall.message.member.permissions.has(PermissionFlagsBits.Administrator)) return;

	const commands = await discordClient.application?.commands.set([]);
	if (commands) {
		cCall.reply("Success.");
	} else {
		cCall.reply("Failed somehow.");
	}
};

addCommand({
	name: "clearglobalcommands",
	run: cClearGlobalCommands,
	category: "debug",
	description: "Developer-only. Clears all global slash commands.",
	discordOnly: true,
});
