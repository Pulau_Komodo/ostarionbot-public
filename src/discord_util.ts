import { escapeMarkdown, GuildMember, Interaction } from "discord.js";

/**
 * Outputs in format "08/01/2023 12:00:00".
 * Time is Unix timestamp in seconds.
 */
export const dateTime = (time: number): string => {
	time = Math.round(time);
	return `<t:${time}:d> <t:${time}:T>`;
};

/*
	T: 12:00:00
	t: 12:00
	F: Sunday, 8 January 2023 12:00
	f: 8 January 2023 12:00
	D: 8 January 2023
	d: 08/01/2023
	R: in 2 years
*/

// eslint-disable-next-line @typescript-eslint/ban-types
const hasOwnProperty = <X extends Object, Y extends PropertyKey>(
	obj: X,
	prop: Y,
): obj is X & Record<Y, unknown> => {
	return obj.hasOwnProperty(prop);
};

/** Gets the markdown-escaped display name from an interaction, if possible, or falls back to username */
export const getDisplayName = (interaction: Interaction): string => {
	const { member, user } = interaction;
	if (member instanceof GuildMember) {
		return escapeMarkdown(member.displayName);
	} else if (
		member instanceof Object &&
		hasOwnProperty(member, "nick") &&
		typeof member.nick === "string"
	) {
		return escapeMarkdown(member.nick);
	} else {
		return user.username;
	}
};
