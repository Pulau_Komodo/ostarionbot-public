/** The bot's account name on Twitch. Also for use in user-friendly output. */
export const botName = "CoolBot";
/** oauth for Twitch chat, including the "oath:" */
export const oauth = "oauth:abcdefghijklmnopqrstuvwxyz0123";
/** Token for Discord API */
export const discordtoken = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
/** Key for dictionaryapi.com API */
export const dictionaryApiKey = "a-b-c-d";
/** Key for openweathermap.org API */
export const weatherApiKey = "d-c-b-a";
export const twitch = {
	clientId: "123",
	clientSecret: "456",
	userId: "789",
} as const;
export const spotify = {
	clientId: "123",
	clientSecret: "456",
} as const;
/** Default command prefix */
export const defaultPrefix = "!";
