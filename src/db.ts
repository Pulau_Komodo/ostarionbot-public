import Database from "better-sqlite3";
const dbPath = `./data/dynamic/db.db`;
let database;
try {
	database = new Database(dbPath, { fileMustExist: true });
} catch (error) {
	console.error(`Failed to open database at path "${dbPath}"`);
	throw error;
}
export const db = database;
db.pragma("foreign_keys = ON");
