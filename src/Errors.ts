/** Errors thrown by user commands going wrong, with a custom message for the end user. Without a message, the output will be the command's usage note. */
export class CommandError extends Error {}
/** Errors thrown by web apis giving unexpected results. */
export class WebApiError extends Error {}
