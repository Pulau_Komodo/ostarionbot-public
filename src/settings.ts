import { PermissionFlagsBits } from "discord.js";
import { CommandError } from "./Errors.js";
import { addCommand, CommandFunction } from "./commands.js";
import { db } from "./db.js";

/** Creates a domain config entry if one doesn't already exist */
const ensureDomainConfigExists = (domain: string, network: string): undefined => {
	const qCheckDomain = `
		SELECT domain
		FROM domain_config
		WHERE domain = $domain
	`;
	const result = db.prepare(qCheckDomain).get({ domain });
	if (result) return; // Yes, it exists
	const qAddDomain = `
		INSERT INTO domain_config(domain, network)
		VALUES ($domain, $network)
	`;
	db.prepare(qAddDomain).run({ domain, network });
};

const qPrefixes = `
	SELECT domain, prefix
	FROM domain_config
`;
/** Map with domains as keys and prefixes as values */
export const prefixes: Map<string, string> = new Map(
	(db
		.prepare(qPrefixes)
		.all() as {domain: string, prefix: string}[])
		.map((config) => {
			return [config.domain, config.prefix];
		}),
);

const cSetPrefix: CommandFunction = (cCall) => {
	if (
		!(cCall.network === "twitch" && cCall.user.id === cCall.domain.slice(1)) &&
		!(
			cCall.network === "discord" &&
			cCall.message.member.permissions.has(PermissionFlagsBits.Administrator)
		)
	)
		throw new CommandError(`Only channel owner or administrator can set prefix`);

	const oldPrefix = prefixes.get(cCall.domain);
	if (!oldPrefix) throw new Error(`Prefix for ${cCall.domain} is somehow unset`);
	const newPrefix = cCall.args[0] ?? "";
	if (oldPrefix === newPrefix) throw new CommandError(`Prefix is already ${newPrefix}`);
	if (["?", "l?"].includes(newPrefix.toLowerCase()))
		throw new CommandError(`Cannot use ${newPrefix} as prefix`);

	ensureDomainConfigExists(cCall.domain, cCall.network);
	const qSetPrefix = `
		UPDATE domain_config
		SET prefix = $newPrefix
		WHERE domain = $domain
	`;
	db.prepare(qSetPrefix).run({ newPrefix, domain: cCall.domain });
	prefixes.set(cCall.domain, newPrefix);
	cCall.reply(`Prefix changed to ${newPrefix} from ${oldPrefix}.`);
};

addCommand({
	name: "setprefix",
	run: cSetPrefix,
	category: "config",
	usage: "<prefix>",
	description: "Sets the prefix for commands for this domain. (Owner or admin only)",
});

const qEdgy = `
	SELECT domain
	FROM domain_config
	WHERE edgy = true
`;
/** Set of domains where edgy is enabled */
export const edgyDomains = new Set(
	(db
		.prepare(qEdgy)
		.all() as {domain: string}[])
		.map((item) => item.domain),
);

const cEdgy: CommandFunction = (cCall) => {
	if (!cCall.args[0]) {
		if (edgyDomains.has(cCall.domain)) cCall.reply(`Edgy is on for this domain.`);
		else cCall.reply(`Edgy is off for this domain.`);
		return;
	}
	if (
		!(cCall.network === "twitch" && cCall.user.id === cCall.domain.slice(1)) &&
		!(
			cCall.network === "discord" &&
			cCall.message.member.permissions.has(PermissionFlagsBits.Administrator)
		)
	)
		throw new CommandError(`Only channel owner or administrator can turn edgy on or off`);
	cCall.args[0]?.toLowerCase();
	if (!["on", "off"].includes(cCall.args[0])) throw new CommandError();
	const edgy = cCall.args[0] === "on";
	if (edgy === edgyDomains.has(cCall.domain))
		throw new CommandError(`Edgy is already ${cCall.args[0]} for this domain`);

	ensureDomainConfigExists(cCall.domain, cCall.network);

	const edgyNumber = edgy ? 1 : 0;
	const qSetEdgy = `
		UPDATE domain_config
		SET edgy = $edgy
		WHERE domain = $domain
	`;
	db.prepare(qSetEdgy).run({ edgy: edgyNumber, domain: cCall.domain });
	if (edgy) {
		edgyDomains.add(cCall.domain);
		cCall.reply(`Edgy has been turned on for this domain. Edgy commands can now be used.`);
	} else {
		edgyDomains.delete(cCall.domain);
		cCall.reply(
			`Edgy has been turned off for this domain. Edgy commands can no longer be used.`,
		);
	}
};

addCommand({
	name: "edgy",
	run: cEdgy,
	category: "config",
	usage: '["on" or "off"]',
	description:
		"Turns edgy on or off for this domain. Without arguments, says the current setting. Edgy commands can only be used if edgy is on. (Only owner or admin can change this)",
});
