interface FormatListOptions {
	/** ", " */
	readonly connector?: string;
	/** " and " */
	readonly finalConnector?: string;
	/** ["game ", "games "] */
	readonly opening?: [] | [string, string?];
	/** [" is", " are"] */
	readonly closing?: [] | [string, string?];
	/** ["<", ">"] */
	readonly enclosing?: [] | [string, string?];
}

export const formatList = (items: string[], options: FormatListOptions = {}): string => {
	if (items.length === 0) return ``;

	const {
		connector = ", ",
		finalConnector = " and ",
		opening: [openingSingular = "", openingPlural = openingSingular] = [],
		closing: [closingSingular = "", closingPlural = closingSingular] = [],
		enclosing: [enclosingBefore = "", enclosingAfter = enclosingBefore] = [],
	} = options;

	if (items.length === 1)
		return `${openingSingular}${enclosingBefore}${items[0]!}${enclosingAfter}${closingSingular}`;

	let output = `${openingPlural}${enclosingBefore}${items[0]!}${enclosingAfter}`;
	for (let i = 1; i < items.length - 1; i++) {
		output += `${connector}${enclosingBefore}${items[i]!}${enclosingAfter}`;
	}
	output += `${finalConnector}${enclosingBefore}${items[
		items.length - 1
	]!}${enclosingAfter}${closingPlural}`;
	return output;
};
