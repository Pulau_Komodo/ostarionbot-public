﻿process.env.TZ = "UTC";

import "./watch_news.js";
import "./help.js";
import "./emote_voting/main.js"; // System for ranking emotes via vote
import "./levenshtein.js"; // Levenshtein string difference comparison
import "./calendar/main.js";
import "./colour_names/commands.js";
import "./misc/button_grid.js";
import "./misc/math.js";
import "./misc/fancytext.js"; // Adds chat commands to return text with some fancy Unicode characters
import "./misc/hentai.js"; // Adds a chat command to post a random hentai quote
import "./misc/inspirobot.js";
import "./misc/wikipedia.js";
import "./misc/misc_commands.js";
import "./misc/optional_roles.js";
import "./misc/estimate_spam.js";
import "./misc/emoji_regex_tests.js";
import "./misc/on_this_date.js";
import "./misc/ouija_board.js";
import "./personal_roles/personal_roles.js";
import "./elective_channel_visibility_overrides/channel_overrides.js";
import "./elective_channel_visibility_overrides/channel_list.js";
import "./twitch/streaminfo.js";
import "./twitch/topstreams.js";
import "./twitch/listeners.js";
import "./twitch/twitch_chat.js";
import "./dictionary/dictionary.js";
import "./weather/main.js";
import "./uncensor.js";
import "./wiki/main.js";
import "./spotify/commands.js";

import "./settings.js";

import "./slash_commands.js";

import "./discordclient.js";
import { timestamp } from "./timestamp.js";

process.on("beforeExit", (number) => {
	console.log(`beforeExit event at ${timestamp("{h}:{m}:{s}.{ms}")} with code ${number}`);
});

process.on("exit", (number) => {
	console.log(`exit event at ${timestamp("{h}:{m}:{s}.{ms}")} with code ${number}`);
});
