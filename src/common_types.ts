import { TextChannel, ThreadChannel } from "discord.js";
import { TwitchMessage } from "./twitch/twitch_chat.js";
import { DiscordMessage } from "./discordclient.js";

type Entry<O, K extends keyof O> = [K, O[K]];

declare global {
	type GenericMessage = DiscordMessage | TwitchMessage;
	type GenericChannel = TextChannel | ThreadChannel | string;
	type DiscordGuildTextChannel = TextChannel | ThreadChannel;

	type Entries<O> = Entry<O, keyof O>[];

	type RecursivePartial<T> = {
		[P in keyof T]?: T[P] extends (infer U)[]
			? RecursivePartial<U>[]
			: T[P] extends object
			? RecursivePartial<T[P]>
			: T[P];
	};
}

export type Network = "discord" | "twitch";
