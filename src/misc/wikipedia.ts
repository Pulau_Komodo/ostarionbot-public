import got from "got";
import { addCommand, CommandFunction } from "../commands.js";

interface ResponseObject {
	query: {
		random: {
			id: number;
			ns: number;
			title: string;
		}[];
	};
}

const commandRandomWikipediaArticle: CommandFunction = async (cCall) => {
	const response = await got(
		"https://en.wikipedia.org/w/api.php?action=query&list=random&rnnamespace=0&rnlimit=1&format=json",
	);
	const responseObject: ResponseObject = JSON.parse(response.body);
	const article = responseObject.query.random[0];
	if (!article) throw new Error();
	cCall.reply(`https://en.wikipedia.org/wiki/${article.title.replace(/ /g, "_")}`);
};

addCommand({
	name: "randomwikipediaarticle",
	run: commandRandomWikipediaArticle,
	category: "wiki",
	description: "Links to a random article in the main namespace of Wikipedia.",
	aliases: ["randomwikipedia", "randomwp", "rwp"],
});
