import { PermissionFlagsBits } from "discord.js";
import { addCommand, DiscordCommandFunction } from "../commands.js"; // Map of commands

const optionalRoles = [
	"428930650410713092", // dotapatchalert
	"428934154756030495", // dotablogalert
	"472387712372965388", // free games alert
	"586243507375833098", // free games snobs
];

const assignOrUnassignRole: DiscordCommandFunction = (cCall) => {
	if (cCall.message.guild.id !== "240523866026278913") return;
	if (!cCall.argstring.match(/^[\w\s]+$/)) {
		cCall.replyUsage();
		return;
	}

	const rolename = cCall.argstring.toLowerCase();

	const role = cCall.message.guild.roles.cache.find(
		(x) => x.name.toLowerCase() === rolename || x.id === cCall.argstring,
	);
	if (!role) {
		cCall.reply(`No role here by that name or ID`);
		return;
	}
	if (!optionalRoles.includes(role.id)) {
		cCall.reply(`That is not an optional role`);
		return;
	}
	if (!cCall.message.guild.members.me?.permissions.has(PermissionFlagsBits.ManageRoles)) {
		cCall.reply(`I lack the permission to change roles`);
		return;
	}
	if (cCall.message.guild.members.me.roles.highest.comparePositionTo(role) <= 0) {
		cCall.reply(`I lack the permission to change that role`);
		return;
	}

	const hadRole = cCall.message.member.roles.cache.has(role.id);
	if (cCall.command === "getrole") {
		if (hadRole) {
			cCall.reply(`User already has that role`);
			return;
		}

		cCall.message.member.roles
			.add(role)
			.then(() => {
				cCall.reply(`Successfully gave role ${role.name} to user.`);
			})
			.catch((error) => {
				console.log(error.stack);
				cCall.reply(`Failed to give role somehow.`);
			});
	} else if (cCall.command === "removerole") {
		if (!hadRole) {
			cCall.reply(`User doesn't have that role`);
			return;
		}

		cCall.message.member.roles
			.remove(role)
			.then((_member) => {
				cCall.reply(`Successfully removed role ${role.name} from user.`);
			})
			.catch((error) => {
				console.log(error.stack);
				cCall.reply(`Failed to remove role somehow.`);
			});
	}
};

addCommand({
	name: "getrole",
	run: assignOrUnassignRole,
	category: "role",
	usage: "<role name or ID>",
	description: "Gives the user the chosen optional role.",
	discordOnly: true,
});
addCommand({
	name: "removerole",
	run: assignOrUnassignRole,
	category: "role",
	usage: "<role name or ID>",
	description: "Removes the chosen optional role from the user.",
	discordOnly: true,
});
