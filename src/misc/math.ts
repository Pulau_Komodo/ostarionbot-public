import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js"; // Map of commands

import { create, all } from "mathjs";
import { log } from "../log.js";
import { CommandError } from "../Errors.js";
import { ApplicationCommandOptionType } from "discord.js";

try {
	if (!all) {
		throw new Error("Somehow failed to load math.js properly.");
	}
	const mathjs = create(all, {});
	const limitedEvaluate = mathjs.evaluate;

	if (!limitedEvaluate || !mathjs.import)
		throw new Error("Somehow failed to load mathjs properly");

	// "The precise equivalence between calories and joules has varied over the years, but in thermochemistry and nutrition it is now generally assumed that one (small) calorie (thermochemical calorie) is equal to exactly 4.184 J, and therefore one kilocalorie (one large calorie) is 4184 J, or 4.184 kJ." https://en.wikipedia.org/wiki/Calorie
	mathjs.createUnit({
		calorie: {
			definition: "4.184 joule",
			prefixes: "long",
		},
		cal: {
			definition: "4.184 joule",
			prefixes: "short",
		},
	});

	mathjs.import(
		{
			print: function () {
				throw new CommandError("Function print is disabled");
			}, // Won't work anyway I think
			map: function () {
				throw new CommandError("Function map is disabled");
			}, // Won't work anyway I think
			import: function () {
				throw new CommandError("Function import is disabled");
			}, // Supposedly security
			createUnit: function () {
				throw new CommandError("Function createUnit is disabled");
			}, // Supposedly security
			evaluate: function () {
				throw new CommandError("Function evaluate is disabled");
			}, // Supposedly security
			parse: function () {
				throw new CommandError("Function parse is disabled");
			}, // Supposedly security
			simplify: function () {
				throw new CommandError("Function simplify is disabled");
			}, // Supposedly security
			derivative: function () {
				throw new CommandError("Function derivative is disabled");
			}, // Supposedly security
		},
		{ override: true },
	);

	const calculate = (expression: string): string => {
		try {
			if (/\[\]/.test(expression)) throw new Error();
			return limitedEvaluate(expression).toString();
		} catch (error) {
			if (error instanceof Error) {
				const message = error.message;
				if (message.startsWith("Units do not match"))
					throw new CommandError("Cannot convert between those units");
				const undefinedSymbol = /Undefined symbol (\S+)/.exec(message) as
					| null
					| [string, string];
				if (undefinedSymbol)
					throw new CommandError(`Undefined symbol ${undefinedSymbol[1]}`);
				const unexpectedPart = /Unexpected part "(\S+)"/.exec(message) as
					| null
					| [string, string];
				if (unexpectedPart)
					throw new CommandError(`Unexpected part "${unexpectedPart[1]}"`);
			}
			console.log(error);
			throw new CommandError("Invalid math expression");
		}
	};

	const cMath: CommandFunction = (cCall) => {
		//if (!(cCall.network === "discord" && cCall.message.guild.id === "409416288440549397")) {cCall.reply(`Command disabled`); return;}
		cCall.reply(calculate(cCall.argstring));
	};

	const slashMath: SlashCommandFunction = (interaction) => {
		const expression = interaction.options.getString("expression", true);
		return `${expression} = ${calculate(expression)}`;
	};

	addCommand({
		name: "math",
		run: cMath,
		category: "miscellaneous",
		usage: "<expression>",
		description: "Evaluates the math expression.",
		slash: {
			run: slashMath,
			options: [
				{
					name: "expression",
					type: ApplicationCommandOptionType.String,
					description: "The expression to evaluate",
					required: true,
				},
			],
		},
	});
} catch (error) {
	log(error);
}
