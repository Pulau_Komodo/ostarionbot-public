import { addCommand, DiscordCommandFunction } from "../commands.js";

const characterWidthMap = new Map([
	["a", 7.8],
	["A", 11.05],
	["b", 8.45],
	["B", 8.75],
	["c", 7.45],
	["C", 10.2],
	["d", 8.45],
	["D", 11.2],
	["e", 7.85],
	["E", 8.1],
	["f", 4.8],
	["F", 7.65],
	["g", 8.05],
	["G", 11.1],
	["h", 8.35],
	["H", 11.25],
	["i", 3.7],
	["I", 4.25],
	["j", 3.7],
	["J", 5.8],
	["k", 2.05],
	["K", 9.7],
	["l", 3.7],
	["L", 7.45],
	["m", 13.1],
	["M", 14.5],
	["n", 8.35],
	["N", 11.25],
	["o", 8.35],
	["O", 11.9],
	["p", 8.5],
	["P", 8.5],
	["q", 8.4],
	["Q", 11.95],
	["r", 5.5],
	["R", 9.1],
	["s", 7],
	["S", 8.2],
	["t", 5.3],
	["T", 9.3],
	["u", 8.35],
	["U", 10.85],
	["v", 7.5],
	["V", 10.9],
	["w", 11.65],
	["W", 16.4],
	["x", 7.5],
	["X", 10.15],
	["y", 7.7],
	["Y", 10.05],
	["z", 7.2],
	["Z", 9.55],
	["0", 9.85],
	["1", 5.65],
	["2", 8.55],
	["3", 8.35],
	["4", 9.5],
	["5", 8.55],
	["6", 9.1],
	["7", 8.5],
	["8", 9.1],
	["9", 9.1],
	["!", 4.25],
	[",", 3.5],
	[".", 3.5],
	[" ", 3.6],
	["?", 7.95],
	["🤔", 22],
]);

const commandEstimateSpam: DiscordCommandFunction = async (cCall) => {
	const messageId = cCall.message.reference?.messageId;
	if (!messageId) {
		cCall.reply(`Reply to a message when using ${cCall.prefix}${cCall.command}`);
		return;
	}
	const message = await cCall.channel.messages.fetch(messageId);
	const content = message.content;
	const lines = content.split(/\r\n?|\n/) as [string, ...string[]];
	let spamScore = lines.length;
	lines[0] = `${message.author.bot ? "BOT " : ""}${message.member?.displayName ?? ""} ${
		lines[0]
	}`;
	for (let line of lines) {
		line = line.replace(/<:(\w+):\d+>/g, "🤔");
		let lineWidth = 0;
		for (const character of line) {
			lineWidth += characterWidthMap.get(character) ?? 8;
		}
		spamScore += Math.floor(lineWidth / 496);
	}
	cCall.reply(`That message has an estimated spam score of ${spamScore}.`);
};

addCommand({
	name: "estimatespam",
	run: commandEstimateSpam,
	category: "miscellaneous",
	usage: "while replying to a message",
	description: "Estimates the spamminess (how much vertical screen space it uses) of a message",
	discordOnly: true,
});
