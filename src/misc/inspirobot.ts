import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";
import got from "got";
import { log } from "../log.js";
import { sleep } from "../promise_util.js";
import { CommandError } from "../Errors.js";

const cooldownDuration = 10 * 1000;

type Resolve = (message: string) => void;
type QueueItem = { count: 0 } | { count: 1; resolve: Resolve };

/** A map of channels and whether an action is queued. Inclusion in the map implies being on cooldown. */
const queue = new Map<string, QueueItem>();

/** A user attempts to get an inspirobot image link */
const inspirobot = (channelId: string): Promise<string> => {
	return new Promise((resolve, reject) => {
		const queueItem = queue.get(channelId);
		if (queueItem && queueItem.count === 1) reject("Message already queued");
		else {
			queue.set(channelId, { count: 1, resolve });
			if (!queueItem) void queueLoop(channelId);
		}
	});
};

const queueLoop = async (channelId: string) => {
	let queueItem;
	while (((queueItem = queue.get(channelId)), queueItem?.count === 1)) {
		queue.set(channelId, { count: 0 });
		const link = await getImageLink();
		queueItem.resolve(link);
		await sleep(cooldownDuration);
	}
	queue.delete(channelId);
};

const getImageLink = async (): Promise<string> => {
	const link = await got("https://inspirobot.me/api?generate=true");
	if (/^https:\/\/generated\.inspirobot\.me\/a\/\w+\.jpg$/.test(link.body)) {
		return link.body;
	} else {
		log("Error retrieving Inspirobot link");
		log(link.headers);
		log(link.body);
		throw new Error();
	}
};

const commandInspirobot: CommandFunction = async (cCall) => {
	cCall.reply(await inspirobot(cCall.channel.id));
};

const slashInspirobot: SlashCommandFunction = async (interaction) => {
	await interaction.deferReply();
	try {
		const link = await inspirobot(interaction.channelId);
		interaction.editReply(link).catch(log);
		return undefined;
	} catch (error) {
		if (typeof error === "string") {
			interaction.followUp({ content: error, ephemeral: true }).catch(log);
			interaction.deleteReply().catch(log);
			return;
		}
		interaction.deleteReply().catch(log);
		throw new Error();
	}
};

addCommand({
	name: "inspirobot",
	run: commandInspirobot,
	category: "miscellaneous",
	description: "Links to a random inspirational image from Inspirobot.",
	aliases: ["inspireme"],
	slash: { run: slashInspirobot },
	heat: { fixed: 2 },
});
