import { readFile } from "fs/promises";
import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js"; // Map of commands

import { escapeMarkdown } from "discord.js";
import { log } from "../log.js";
import { CommandError } from "../Errors.js";

/** Array of quotes */
let lines: string[] = [];
/** Name of file with a quote on each line */
const quotesFile = "hentai.txt";
/** Duration of cooldown between quotes, in ms */
const cooldown = 20 * 1000;
/** Set of channels where the command is on cooldown */
const onCooldown: Set<string> = new Set();
/** Set of channels where the slash command is on cooldown */
const onCooldownSlash: Set<string> = new Set();
/** Set of channels where another message is queued */
const queuedLines: Set<string> = new Set();

const loadLines = async () => {
	const content = await readFile(`./data/static/${quotesFile}`, "utf-8");
	lines = content.split(/\r?\n/);
	log(`File "${quotesFile}" read successfully`);

	addCommand({
		name: "hentai",
		run: commandRandomLine,
		category: "miscellaneous",
		description: "Posts a random element of rich Japanese culture.",
		slash: {
			run: slashRandomLine,
		},
		heat: { fixed: 2 },
		edgy: true,
	});
};

loadLines().catch(console.error);

/** Returns a random item from the quotes array */
const randomLine = (): string => {
	return lines[Math.floor(Math.random() * lines.length)] ?? "";
};

/** Sends a random quote to the channel, or queues a quote if it was already on cooldown (or does nothing if one was already queued) */
const commandRandomLine: CommandFunction = (cCall) => {
	if (onCooldown.has(cCall.channel.id)) {
		queuedLines.add(cCall.channel.id);
		return;
	}

	onCooldown.add(cCall.channel.id);
	let line = randomLine();
	if (cCall.network === "discord") line = escapeMarkdown(line);
	cCall.reply(line);

	setTimeout(sendQueuedLine, cooldown, cCall.channel.id, cCall.network, cCall.reply);
};

/**
 * Sends the queued message or cools down if there is none
 * @param channel Channel ID
 * @param reply Callback to call with message
 */
const sendQueuedLine = function (
	channel: string,
	network: string,
	reply: (arg0: string) => void,
): void {
	if (queuedLines.delete(channel)) {
		let line = randomLine();
		if (network === "discord") line = escapeMarkdown(line);
		reply(line);
		setTimeout(sendQueuedLine, cooldown, channel, network, reply);
	} else {
		onCooldown.delete(channel);
	}
};

const slashRandomLine: SlashCommandFunction = (interaction) => {
	if (onCooldownSlash.has(interaction.channelId)) throw new CommandError("On cooldown");
	onCooldownSlash.add(interaction.channelId);
	setTimeout(() => onCooldownSlash.delete(interaction.channelId), cooldown);
	return escapeMarkdown(randomLine());
};
