import { Message, OmitPartialGroupDMChannel, PermissionFlagsBits } from "discord.js";
import { addCommand, DiscordCommandFunction } from "../commands.js";
import { CommandError } from "../Errors.js";
import { sleep } from "../promise_util.js";

const LETTERS = [
	"🇦",
	"🇧",
	"🇨",
	"🇩",
	"🇪",
	"🇫",
	"🇬",
	"🇭",
	"🇮",
	"🇯",
	"🇰",
	"🇱",
	"🇲",
	"🇳",
	"🇴",
	"🇵",
	"🇶",
	"🇷",
	"🇸",
	"🇹",
	"🇺",
	"🇻",
	"🇼",
	"🇽",
	"🇾",
	"🇿",
	"✅",
] as const;

const commandOuija: DiscordCommandFunction = async (cCall) => {
	if (!cCall.message.member.permissions.has(PermissionFlagsBits.Administrator)) {
		throw new CommandError("Admin-only.");
	}
	const message_am = await cCall.channel.send("A-M");
	const message_nz = await cCall.channel.send("N-Z");
	let message = message_am;
	for (let i = 0; i < LETTERS.length; i++) {
		if (i === 13) {
			message = message_nz;
		}
		try {
			await Promise.all([message.react(LETTERS[i]!), sleep(100)]);
		} catch (error) {
			throw new CommandError("Something went wrong.");
		}
	}
};

addCommand({
	name: "ouija",
	run: commandOuija,
	category: "miscellaneous",
	description: "Makes two messages with the 26 letter reactions split between them.",
	discordOnly: true,
});

const commandReactionReport: DiscordCommandFunction = async (cCall) => {
	let message_partial: OmitPartialGroupDMChannel<Message<boolean>>;
	try {
		message_partial = await cCall.message.fetchReference();
	} catch (error) {
		throw new CommandError("No referenced message.");
	}
	const message = await message_partial.fetch();
	const reactions = message.reactions.cache;
	if (reactions.size === 0) {
		throw new CommandError("That message has no reactions.");
	}
	const processed = reactions
		.mapValues((reaction) => {
			const count = reaction.me ? reaction.count - 1 : reaction.count;
			return { reaction, count };
		})
		.filter(({ reaction: _reaction, count }, _name) => count > 0)
		.sort((a, b) => b.count - a.count);
	if (processed.size === 0) {
		throw new CommandError("That message has no reactions other than my own.");
	}
	for (const { reaction, count: _count } of processed.values()) {
		await reaction.users.fetch();
	}
	const output = processed
		.map(({ reaction, count }, _name) => {
			const users = reaction.users.cache
				.filter((_user, id) => id !== cCall.message.guild.members.me?.id)
				.map((user, _id) => user.toString())
				.join(", ");
			return `${reaction.emoji}x${count}: ${users}`;
		})
		.join("\n");
	await cCall.message.reply({ content: output, allowedMentions: { parse: [] } });
};

addCommand({
	name: "reactionreport",
	run: commandReactionReport,
	category: "miscellaneous",
	description: "Lists all the reactions to a given message, with their counts and reactors.",
	discordOnly: true,
});
