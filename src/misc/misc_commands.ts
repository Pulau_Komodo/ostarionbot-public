import {
	addCommand,
	CommandFunction,
	DiscordCommandFunction,
	SlashCommandFunction,
} from "../commands.js"; // Map of commands

import { botName } from "../config.js";
import { log } from "../log.js";

import { ApplicationCommandOptionType, SnowflakeUtil } from "discord.js";
import { CommandError } from "../Errors.js";

const code: DiscordCommandFunction = (cCall) => {
	if (!cCall.argstring) throw new CommandError();
	cCall.reply(`\`${cCall.argstring}\``);
};

addCommand({
	name: "code",
	run: code,
	category: "miscellaneous",
	usage: "<text>",
	description: "Posts the message with `s around it. This is mainly to test some things.",
	heat: { perChar: 0.02 },
	discordOnly: true,
});

const commandDivide: CommandFunction = (cCall) => {
	if (cCall.args[0] === undefined || cCall.args[1] === undefined) throw new CommandError();
	const a = Number(cCall.args[0]);
	const b = Number(cCall.args[1]);
	if (a < 0) throw new CommandError(`First argument needs to be non-negative`);
	if (b <= 0) throw new CommandError(`Second argument needs to be above zero`);
	if (isNaN(Number(a)) || isNaN(Number(b))) throw new CommandError();

	cCall.reply(`${a}/${b} = ${Math.floor(a / b)} + ${a % b}/${b}`);
};

addCommand({
	name: "divide",
	run: commandDivide,
	category: "miscellaneous",
	usage: "<dividend> <divisor>",
	description:
		"Divides the dividend by the divisor and gives the remainder as a fraction of the divisor.",
});

const roll = (arg1?: number, arg2?: number): [number, number, number] => {
	let num1 = 100;
	let num2 = 1;
	let c: number;

	if (arg1 !== undefined) {
		num1 = Math.floor(arg1);
	}
	if (arg2 !== undefined) {
		num2 = Math.floor(arg2);
	}

	if (num2 > num1) {
		c = num1;
		num1 = num2;
		num2 = c;
	} // Make sure num1 is highest

	return [num1, num2, num2 + Math.floor(Math.random() * (num1 - num2 + 1))];
};

const commandRoll: CommandFunction = (cCall) => {
	const first = cCall.args[0] ? Number(cCall.args[0]) : undefined;
	if (first !== undefined && isNaN(first))
		throw new CommandError("The first argument could not be parsed as a number");
	const second = cCall.args[1] ? Number(cCall.args[1]) : undefined;
	if (second !== undefined && isNaN(second))
		throw new CommandError("The second argument could not be parsed as a number");
	const [num1, num2, outcome] = roll(first, second);
	const output = `${cCall.user.displayName} rolls a number between ${num2} and ${num1}: ${outcome}`;
	cCall.reply(output);
};

const slashRoll: SlashCommandFunction = (interaction) => {
	const first = interaction.options.getInteger("first") ?? 100;
	const second = interaction.options.getInteger("second") ?? 1;
	const [num1, num2, outcome] = roll(first, second);
	return `You roll a number between ${num2} and ${num1}: ${outcome}`;
};

addCommand({
	name: "roll",
	run: commandRoll,
	category: "miscellaneous",
	usage: "[number 1] [number 2]",
	description:
		"Gives a random integer between 1 and 100, or between 1 and number 1, or between number 1 and number 2.",
	slash: {
		run: slashRoll,
		description: "Gives a random number between two integers.",
		options: [
			{
				name: "first",
				type: ApplicationCommandOptionType.Integer,
				description: "One of the two numbers to random between, defaulting to 100",
			},
			{
				name: "second",
				type: ApplicationCommandOptionType.Integer,
				description: "The other of the two numbers to random between, defaulting to 1",
			},
		],
	},
});

const commandPenisVolume: CommandFunction = (cCall) => {
	if (!cCall.args[0] || !cCall.args[1]) throw new CommandError();
	const output = penisVolume(cCall.args[0], cCall.args[1]);
	if (!output) throw new CommandError();
	cCall.reply(output);
};

const penisVolume = (length: string, circumference: string) => {
	// Circumference and length need to be supplied, a number and above 0
	if (circumference !== "" && length !== "") {
		const c = Number(circumference);
		const l = Number(length);
		if (!isNaN(c) && !isNaN(l) && c > 0 && l > 0) {
			const radius = c / (2 * Math.PI);
			if (l - radius < 0) {
				return `Such a penis would be weird.`;
			} else {
				const volume =
					Math.pow(radius, 2) * Math.PI * (l - radius) +
					(2 / 3) * Math.pow(radius, 3) * Math.PI;
				return `A penis with ${l} cm length and ${c} cm circumference would be approximately ${Math.round(
					volume,
				)} cm^3.`;
			}
		}
	}
	return;
};

addCommand({
	name: "penisvolume",
	run: commandPenisVolume,
	category: "miscellaneous",
	usage: "<length in cm> <circumference in cm>",
	description:
		"Calculates the volume of a penis, given length and circumference, simplified to a cylinder with a domed end.",
	edgy: true,
});

// Picks a random option from an array based on an array of weights for those options, and a total weight
// arguments: options array, weights array, total weight number
// returns chosen option
const randomOption = <T>(options: T[], weights: number[], total_weight: number): T => {
	let cumulative = 0;
	const randomnumber = Math.random() * total_weight;

	const zipped = options.map((option, index) => [option, weights[index] ?? 0] as const);
	for (const [option, weight] of zipped) {
		cumulative += weight;
		if (randomnumber < cumulative) return option;
	}

	throw new Error("Somehow never selected a weighted option");
};

const commandRandomWord: CommandFunction = (message) => {
	message.reply(`${randomWord()} ${randomWord()} ${randomWord()}`);
};

//prettier-ignore
type Letter = " " | "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" | "j" | "k" | "l" | "m" | "n" | "o" | "p" | "q" | "r" | "s" | "t" | "u" | "v" | "w" | "x" | "y" | "z";

// Generates a random word
const randomWord = (): string => {
	let output = "";
	let letter: Letter = " ";
	let newletter: Letter;
	const randomnumber = Math.random();

	for (let i = 0; i < 100; ++i) {
		newletter = randomOption(
			letterChances[letter].options,
			letterChances[letter].weights,
			letterChances[letter].total_weight,
		) as Letter;
		if (newletter !== " ") {
			// If it's not a space,
			letter = newletter;
			output += letter; // add to the word.
		} else if (randomnumber > Math.pow(0.75, i) - 0.25)
			// Otherwise, if you pass a check with an increasing chance (0.25, 0.5, 0.69, 0.83, ...)  of succeeding,
			return output; // finish the word.
	}
	return output;
};

let letterChances: Record<Letter, { options: string[]; weights: number[]; total_weight: number }>;

import { readFile } from "fs/promises";

const loadRandomWordData = async () => {
	const data = await readFile("./data/static/randomwords.txt", "utf-8");
	letterChances = JSON.parse(data);
	log(`File "randomwords.txt" read successfully`);

	addCommand({
		name: "randomword",
		run: commandRandomWord,
		category: "miscellaneous",
		description: "Generates three random words based on letter sequence frequencies.",
	});
};

loadRandomWordData().catch(console.error);

const commandTeamMmr: CommandFunction = (cCall) => {
	if (
		cCall.args[0] === undefined ||
		cCall.args[0] === "" ||
		cCall.args[1] === undefined ||
		cCall.args[1] === ""
	)
		throw new CommandError();

	const a = Number(cCall.args[0]);
	const b = Number(cCall.args[1]);
	if (isNaN(a) || isNaN(b)) throw new CommandError();

	// Make sure mmr1 is highest
	const mmr1 = a > b ? a : b;
	const mmr2 = a > b ? b : a;

	let result = 1.25 * mmr2 - 0.25 * mmr1;
	result = Math.round(result * 100) / 100;

	let output = `Top MMR ${mmr1} with average team MMR ${mmr2} means the other four players average ${result} MMR.`;
	if (mmr1 - result > 3000) output += ` Good job, Valve.`;
	cCall.reply(output);
};

addCommand({
	name: "teammmr",
	run: commandTeamMmr,
	category: "miscellaneous",
	usage: "<top or average mmr> <average or top mmr>",
	description:
		"For a known average and top MMR on a five-player team, calculates the average MMR of the other four players.",
});

import * as emoji from "node-emoji"; // does emoji things

const commandEmojify: CommandFunction = (cCall) => {
	if (!cCall.argstring) throw new CommandError();

	const output =
		cCall.network === "twitch"
			? emoji.emojify(cCall.argstring)
			: `\`${emoji.emojify(cCall.argstring)}\``;
	cCall.reply(output);
};

const commandDemojify: CommandFunction = (cCall) => {
	if (!cCall.argstring) throw new CommandError();

	const output =
		cCall.network === "twitch"
			? emoji.unemojify(cCall.argstring)
			: `\`${emoji.unemojify(cCall.argstring)}\``;
	cCall.reply(output);
};

addCommand({
	name: "emojify",
	run: commandEmojify,
	category: "miscellaneous",
	usage: "<text>",
	description: "Replaces :emotes: in the text with emojis.",
	heat: { perChar: 0.02 },
});
addCommand({
	name: "demojify",
	run: commandDemojify,
	category: "miscellaneous",
	usage: "<text>",
	description: "Replaces emojis in the text with :emotes:.",
	heat: { perChar: 0.02 },
});

import discordclient from "../discordclient.js";

const commandGuilds: CommandFunction = (message) => {
	message.reply(`${botName} is in ${discordclient.guilds.cache.size} guilds.`);
};

addCommand({
	name: "guilds",
	run: commandGuilds,
	category: "miscellaneous",
	description: `Says how many guilds ${botName} is in.`,
});

const deconstructSnowflake = (snowflake: string): string => {
	const decon = SnowflakeUtil.deconstruct(snowflake);
	return (
		`${snowflake} deconstructed: ` +
		`timestamp: ${decon.timestamp} (${new Date(Number(decon.timestamp)).toISOString()}), ` +
		`workerID: ${decon.workerId}, ` +
		`processID: ${decon.processId}, ` +
		`increment: ${decon.increment}`
	);
};

const commandSnowflake: CommandFunction = (cCall) => {
	if (!cCall.argstring) throw new CommandError();
	if (!/^\d+$/.test(cCall.argstring)) throw new CommandError(`String was not a snowflake`);
	cCall.reply(deconstructSnowflake(cCall.argstring));
};

const slashSnowflake: SlashCommandFunction = (interaction) => {
	return deconstructSnowflake(interaction.options.getInteger("snowflake", true).toString());
};

addCommand({
	name: "snowflake",
	run: commandSnowflake,
	category: "miscellaneous",
	usage: "<snowflake>",
	description: "Deconstructs the specified Discord snowflake, used for various IDs.",
	slash: {
		run: slashSnowflake,
		options: [
			{
				name: "snowflake",
				type: ApplicationCommandOptionType.Integer,
				description: "The snowflake (Discord ID) to deconstruct",
				required: true,
			},
		],
	},
});

const digits = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+/";
const digitMap = new Map<string, bigint>();
let value = 0n;
// Build a map from digits to values
for (const digit of digits) {
	digitMap.set(digit, value);
	value++;
}

/** Converts numbers between arbitrary bases, from 2 to 64. Input string has to match \\^(?:\\[0-9A-Z]*\\.)?\\[0-9A-Z]+$\\i */
const convertBase = (text: string, fromBase: number, toBase: number): string | void => {
	if (fromBase < 2 || fromBase > digits.length || toBase < 2 || toBase > digits.length)
		throw new Error(); // Unsupported base

	const [integral, fractional = ""] = text.split(".") as [string, ...string[]];

	const fromBaseBig = BigInt(fromBase);
	const toBaseBig = BigInt(toBase);
	let value = 0n;
	for (const digit of integral) {
		const digitValue = digitMap.get(digit);
		if (digitValue === undefined || digitValue >= fromBase) return; // Digit was not legal in the source base
		value *= fromBaseBig;
		value += digitValue;
	}
	const fractionalArray: bigint[] = [];
	for (const digit of fractional) {
		const digitValue = digitMap.get(digit);
		if (digitValue === undefined || digitValue >= fromBase) return; // Digit was not legal in the source base
		fractionalArray.push(digitValue);
	}

	let lastvalue = 0n;
	for (const digit of fractionalArray) {
		lastvalue = (lastvalue + digit) / fromBaseBig;
	}

	let output = "";
	while (value > 0) {
		const digitValue = value % toBaseBig;
		output = digits[Number(digitValue)] + output;
		value -= digitValue;
		value /= toBaseBig;
	}

	return output;
};

const convertBaseShared = (number: string, fromBase: number, toBase: number): string => {
	if (
		!(fromBase >= 2 && fromBase <= 64) || // Base out of range
		!(toBase >= 2 && toBase <= 64) // Base out of range
	)
		throw new CommandError("Error: base outside of range 2-64");
	if (fromBase === toBase) throw new CommandError("Error: to and from bases are the same");

	let changedCase = false;
	if (fromBase <= 36 && /[A-Z]/.test(number)) {
		number = number.toLowerCase();
		changedCase = true;
	}

	const result = convertBase(number, fromBase, toBase);
	if (!result) throw new CommandError("Error: not all the digits exist in the specified base");

	let output = `${number} in base ${fromBase} is ${result} in base ${toBase.toString()}.`;
	if (changedCase) {
		output += " (The input was converted to lower-case)";
	}
	return output;
};

const commandConvertBase: CommandFunction = (cCall) => {
	if (
		!cCall.args[0] ||
		!cCall.args[1] ||
		!cCall.args[2] || // Too few arguments
		!/^\d+$/.test(cCall.args[1]) || // First argument didn't consist only of digits (and at least 1)
		!/^\d+$/.test(cCall.args[2]) // Second argument didn't consist only of digits (and at least 1)
	)
		throw new CommandError();
	const fromBase = parseInt(cCall.args[1]);
	const toBase = parseInt(cCall.args[2]);
	cCall.reply(convertBaseShared(cCall.args[0], fromBase, toBase));
};

const slashConvertBase: SlashCommandFunction = (interaction) => {
	const number = interaction.options.getString("number", true);
	const fromBase = interaction.options.getInteger("from", true);
	const toBase = interaction.options.getInteger("to", true);
	return convertBaseShared(number, fromBase, toBase);
};

addCommand({
	name: "convertbase",
	run: commandConvertBase,
	category: "miscellaneous",
	usage: "<number> <from base> <to base>",
	description:
		"Converts the given number from the given base, to the given base. Base can be from 2 to 64.",
	slash: {
		run: slashConvertBase,
		options: [
			{
				name: "number",
				type: ApplicationCommandOptionType.String,
				description: "The number to convert",
				required: true,
			},
			{
				name: "from",
				type: ApplicationCommandOptionType.Integer,
				description: "The base to interpret the number as",
				required: true,
			},
			{
				name: "to",
				type: ApplicationCommandOptionType.Integer,
				description: "The base to output the number in",
				required: true,
			},
		],
	},
});
