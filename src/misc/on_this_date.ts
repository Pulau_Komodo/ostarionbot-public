import got from "got";
import { CommandError } from "../Errors.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";

interface ResponseBody {
	wikipedia: string;
	date: string;
	events: WikipediaEvent[];
}

interface WikipediaEvent {
	year: string;
	description: string;
	wikipedia: {
		title: string;
		wikipedia: string;
	}[];
}

type Month = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;
type Day =
	| 1
	| 2
	| 3
	| 4
	| 5
	| 6
	| 7
	| 8
	| 9
	| 10
	| 11
	| 12
	| 13
	| 14
	| 15
	| 16
	| 17
	| 18
	| 19
	| 20
	| 21
	| 22
	| 23
	| 24
	| 25
	| 26
	| 27
	| 28
	| 29
	| 30
	| 31;

const monthNames: Record<Month, string> = {
	1: "January",
	2: "February",
	3: "March",
	4: "April",
	5: "May",
	6: "June",
	7: "July",
	8: "August",
	9: "September",
	10: "October",
	11: "November",
	12: "December",
} as const;

type MonthEventCache = Map<Day, WikipediaEvent[]>;
const eventCache = new Map<Month, MonthEventCache>();

const getEventsFromApi = async (month: Month, day: Day): Promise<WikipediaEvent[] | undefined> => {
	const monthEncoded = encodeURIComponent(month.toString());
	const dayEncoded = encodeURIComponent(day.toString());
	const body = await got
		.get(`https://byabbe.se/on-this-day/${monthEncoded}/${dayEncoded}/events.json`, {
			headers: {
				accept: "application/json",
			},
		})
		.json() as ResponseBody | undefined;
	if (!body) return undefined;
	console.log(`Historical events API came back with ${body.events.length} events`);
	return body.events;
};

const getEventsFromCacheOrApi = async (
	month: Month,
	day: Day,
): Promise<WikipediaEvent[] | undefined> => {
	const events = eventCache.get(month)?.get(day);
	if (events) return events;
	const apiEvents = await getEventsFromApi(month, day);
	if (!apiEvents) return undefined;
	const monthCache = eventCache.get(month) ?? new Map<Day, WikipediaEvent[]>();
	monthCache.set(day, apiEvents);
	eventCache.set(month, monthCache);
	return apiEvents;
};

const getEvent = async (): Promise<string> => {
	const now = new Date();
	const month = (now.getMonth() + 1) as Month;
	const day = now.getDate() as Day;
	const events = await getEventsFromCacheOrApi(month, day);
	if (!events || events[0] === undefined) throw new CommandError("Somehow no events found");
	const event = events[Math.floor(Math.random() * events.length)]!;
	return `${monthNames[month]} ${day} of ${event.year}: ${event.description}`;
};

const cGetEvent: CommandFunction = async (cCall) => {
	cCall.reply(await getEvent());
};

const slashGetEvent: SlashCommandFunction = (_) => {
	return getEvent();
};

addCommand({
	name: "onthisdate",
	run: cGetEvent,
	category: "miscellaneous",
	description:
		"Gets a random historical event that took place on the current month and day of the month.",
	aliases: ["otd"],
	slash: {
		run: slashGetEvent,
	},
});
