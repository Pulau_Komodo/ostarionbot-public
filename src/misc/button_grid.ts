import {
	ActionRowBuilder,
	ButtonBuilder,
	ButtonStyle,
	ComponentType,
	GuildMember,
} from "discord.js";
import { addCommand, DiscordCommandFunction } from "../commands.js";
import { log } from "../log.js";
import { ZWJ } from "../characters.js";

const GRID_DURATION = 5 * 60 * 1000;
/** Chosen to be blank, but not cause the API to complain about the label being empty. */
const BUTTON_LABEL = ZWJ;

const chosenStyles = new Map<GuildMember, 0 | 1 | 2 | 3>();

function makeButtons(styles: StyleRows, disabled: boolean): ActionRowBuilder<ButtonBuilder>[] {
	return styles.map((row, y) => {
		return new ActionRowBuilder<ButtonBuilder>().addComponents(
			row.map((style, x) =>
				new ButtonBuilder()
					.setLabel(BUTTON_LABEL)
					.setStyle(buttonStyle(style))
					.setCustomId(`${x},${y}`)
					.setDisabled(disabled),
			),
		);
	});
}

function makeStyles(): StyleRows {
	const rows = new Array(5).fill(null).map(() => new Array(5).fill(0));
	return rows as StyleRows;
}

type StyleRow = [number, number, number, number, number];
type StyleRows = [StyleRow, StyleRow, StyleRow, StyleRow, StyleRow];

function pickerButtons(): ActionRowBuilder<ButtonBuilder> {
	const components = [0, 1, 2, 3].map((n) => {
		return new ButtonBuilder().setLabel(BUTTON_LABEL).setCustomId(`${n}`).setStyle(buttonStyle(n));
	});
	return new ActionRowBuilder<ButtonBuilder>().addComponents(components);
}

/**
 *  Can throw an error
 */
async function makeGrid(channel: DiscordGuildTextChannel): Promise<void> {
	const styles = makeStyles();
	const components = makeButtons(styles, false);
	const message = await channel.send({ components });
	const collector = message.createMessageComponentCollector<ComponentType.Button>({
		time: GRID_DURATION,
	});
	collector.on("collect", (interaction) => {
		interaction.deferUpdate().catch(log);
		const [x_str, y_str] = interaction.customId.split(",");
		const x = Number(x_str);
		const y = Number(y_str);
		styles[y]![x] = chosenStyles.get(interaction.member) ?? 1;
		const components = makeButtons(styles, false);
		message.edit({ components }).catch(log);
	});
	const pickerMessage = await channel.send({
		content: "**picker**",
		components: [pickerButtons()],
	});
	collector.on("end", (_) => {
		const components = makeButtons(styles, true);
		message.edit({ components }).catch(log);
		pickerMessage.delete().catch(log);
	});
	const pickerCollector = pickerMessage.createMessageComponentCollector<ComponentType.Button>({
		time: GRID_DURATION,
	});
	pickerCollector.on("collect", (interaction) => {
		interaction.deferUpdate().catch(log);
		const style = Number(interaction.customId) as 0 | 1 | 2 | 3;
		chosenStyles.set(interaction.member, style);
	});
}

function buttonStyle(number: number): ButtonStyle {
	if (number == 1) {
		return ButtonStyle.Primary;
	} else if (number == 2) {
		return ButtonStyle.Success;
	} else if (number == 3) {
		return ButtonStyle.Danger;
	} else {
		return ButtonStyle.Secondary;
	}
}

const cMakeGrid: DiscordCommandFunction = (command_call) => {
	try {
		makeGrid(command_call.channel);
	} catch (error) {
		log(error);
	}
};

addCommand({
	name: "grid",
	run: cMakeGrid,
	category: "miscellaneous",
	description: "Make an interactible grid of buttons.",
	discordOnly: true,
});
