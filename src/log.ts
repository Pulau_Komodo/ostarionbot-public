import { timestamp } from "./timestamp.js";

/**
 * console.log, with a timestamp prepended if the argument was a string
 */
export const log = (message: unknown): void => {
	if (typeof message === "string") console.log(`${timestamp()} ${message}`);
	else {
		console.log(timestamp());
		console.log(message);
	}
};
