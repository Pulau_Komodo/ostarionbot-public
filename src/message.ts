import { discordClient } from "./discordclient.js"; // Discord client object
import { addChannelHeat } from "./overheat.js";
import { TextChannel, EmbedBuilder, AttachmentBuilder, ThreadChannel } from "discord.js";

import { log } from "./log.js";
import { canSend, canSendAttachment } from "./permission checks.js";
import { uncensor } from "./uncensor.js"; // Dodges the Twitch censor
import { edgyDomains } from "./settings.js";
import { twitchChat } from "./twitch/twitch_chat.js";
import { Network } from "./common_types.js";
import { twitchApi } from "./twitch/twitchApi.js";
import { twitch } from "./config.js";

const sleep = (ms: number) => {
	return new Promise((resolve) => setTimeout(resolve, ms));
};

/** The cooldown time on sending a message, in milliseconds */
//const cooldown = 6*1000;
/** The maximum number of characters that can be sent in 1 message */
const maxMessageLength = {
	twitch: 500,
	discord: 1500,
};
/** For each channel, an array of queued messages */
const queuedTwitchMessages: Map<string, string[]> = new Map();
type QueuedDiscordMessage = {
	message: string;
	addition?: AttachmentBuilder | EmbedBuilder;
};
/** For each channel, an array of queued messages and embeds */
const queuedDiscordMessages: Map<string, QueuedDiscordMessage[]> = new Map();
/** Channels message sending is on cooldown for */
const onCooldown: { [key in "twitch" | "discord"]: Set<string> } = {
	twitch: new Set(),
	discord: new Set(),
};
/** The previous message that was sent for each Twitch channel */
const previousTwitchMessage = new Map();

export const queueMessage = (
	network: Network,
	channel: string | DiscordGuildTextChannel,
	message: string,
	whisper = false,
	tinyDelay = false,
): void => {
	// If an excessively long message is generated somewhere, cut it off here
	if (message.length > maxMessageLength[network]) {
		message = `${message.slice(0, maxMessageLength[network] - 5)}...`;
	}

	if (network === "twitch" && typeof channel === "string") {
		void queueTwitchMessage(channel, message, whisper, tinyDelay);
	} else if (network === "discord") {
		// If it's a channel ID rather than a channel object
		if (typeof channel === "string") {
			const channelResult = discordClient.channels.cache.get(channel);
			if (!(channelResult instanceof TextChannel || channelResult instanceof ThreadChannel)) {
				log("Error converting Discord channel ID");
				return;
			}
			channel = channelResult;
		}
		void queueDiscordMessage(channel, message, tinyDelay);
	} else {
		log(`Error`);
	}
};

export const queueMessageWithAddition = (
	channel: string | DiscordGuildTextChannel,
	message: string,
	addition?: AttachmentBuilder | EmbedBuilder,
	tinyDelay = false,
): void => {
	// If it's a channel ID rather than a channel object
	if (typeof channel === "string") {
		const channelresult = discordClient.channels.cache.get(channel);
		if (!(channelresult instanceof TextChannel)) {
			log("Error converting Discord channel ID");
			return;
		}
		channel = channelresult;
	}
	void queueDiscordMessage(channel, message, tinyDelay, addition);
};

const queueTwitchMessage = async (
	channel: string,
	message: string,
	whisper: boolean,
	tinyDelay: boolean,
) => {
	//	if (edgyDomains.has(channel))
	//		message = uncensor(message);

	let queue = queuedTwitchMessages.get(channel);
	if (!queue) {
		queue = [];
		queuedTwitchMessages.set(channel, queue);
	}
	queue.push(message);

	if (tinyDelay) await sleep(2);

	if (!onCooldown.twitch.has(channel)) {
		onCooldown.twitch.add(channel);
		void processTwitchQueue(channel, whisper);
	}
};

const queueDiscordMessage = async (
	channel: DiscordGuildTextChannel,
	message: string,
	tinyDelay: boolean,
	addition?: AttachmentBuilder | EmbedBuilder,
) => {
	if (!canSend(channel)) {
		log(`No permission to send to "${channel.name}", dropping message "${message}"`);
		return;
	}
	let queue = queuedDiscordMessages.get(channel.id);
	if (!queue) {
		queue = [];
		queuedDiscordMessages.set(channel.id, queue);
	}
	if (canSendAttachment(channel)) queue.push({ message, addition });
	else queue.push({ message });

	if (tinyDelay) await sleep(2);

	if (!onCooldown.discord.has(channel.id)) {
		onCooldown.discord.add(channel.id);
		void processDiscordQueue(channel);
	}
};

const processTwitchQueue = async (channel: string, whisper: boolean) => {
	const queue = queuedTwitchMessages.get(channel);
	if (!queue) {
		log("Error getting channel queue");
		return;
	}
	while (queue.length > 0) {
		// Add messages to string until full
		let message = `➜ ${queue.shift() ?? ""}`;
		while (queue[0] !== undefined) {
			if (message.length + queue[0].length + 4 > maxMessageLength.twitch) break;
			message += ` |➜ ${queue.shift() ?? ""}`;
		}

		// If not connected
		if (!twitchChat.isConnected) {
			// Put the message back into the queue
			queue.unshift(message);
			// Try again in 10s
			await sleep(10 * 1000);
			continue;
		}

		// If message happens to be identical to previous
		if (previousTwitchMessage.get(channel) === message) {
			// Add a discretionary hyphen character so Twitch doesn't drop it
			message += "­";
		} else {
			previousTwitchMessage.set(channel, message);
		}

		log(`Twitch:${channel}: "${message}"`);
		// Send the message to Twitch
		if (whisper) {
			void twitchApi.whispers.sendWhisper(twitch.userId, channel, message).catch(console.error);
		} else {
			void twitchChat.say(channel, message).catch(console.error);
		}

		// If the bot has been sending too many messages, increase the cooldown
		const heat = whisper ? 0 : addChannelHeat("twitch", channel, 50 + message.length);
		let cooldown = 0;
		if (heat > 100) {
			cooldown = ((heat - 100) / 25) * 1000;
			log(
				`Increased Twitch:${channel} message cooldown to ${
					Math.round(cooldown / 10) / 100
				}s because too many messages were being sent`,
			);
		}
		await sleep(cooldown);
	}
	// When out of queued text, go off cooldown
	onCooldown.twitch.delete(channel);
};

const processDiscordQueue = async (channel: DiscordGuildTextChannel) => {
	const queue = queuedDiscordMessages.get(channel.id);
	if (!queue) {
		log("Error getting channel queue");
		return;
	}
	// Loop that sends messages
	while (queue.length > 0) {
		// Add messages to string until full
		const nextitem = queue.shift();
		if (!nextitem) {
			log("Error getting next item");
			return; // Should not actually be possible, but Typescript complained
		}
		let { message, addition } = nextitem;
		message = message !== "" ? `➜ ${message}` : ""; // Add arrow if there was any message content

		let embed_count = 0;
		const additionsArray: (AttachmentBuilder | EmbedBuilder)[] = addition ? [addition] : [];

		// Loop that builds a single message
		for (let i = 0; i < queue.length; ) {
			const item = queue[i]!;
			if (addition) {
				if (addition instanceof EmbedBuilder) {
					// Go to next message if it has an attachment but we already had four
					if (embed_count >= 4) {
						i++;
						continue;
					}
					embed_count++;
					// Go to next message if it has an attachment but we already had one
				} else if (item.addition instanceof AttachmentBuilder) {
					i++;
					continue;
				}
				additionsArray.push(addition);
			}
			if (message.length + item.message.length + 4 > maxMessageLength.discord) break;
			message += item.message !== "" ? ` |➜ ${item.message}` : ""; // Add continuation arrow if there was any content in next message
			if (item.addition) {
				addition = item.addition;
			}
			queue.splice(i, 1);
		}

		const withAddition =
			additionsArray.length > 0 ? " with at least one embed or attachment" : "";
		log(`Discord:${channel.id}: "${message}"${withAddition}`);
		try {
			if (additionsArray.length > 0 && canSendAttachment(channel)) {
				const files = additionsArray.filter(
					(addition) => addition instanceof AttachmentBuilder,
				) as AttachmentBuilder[];
				const embeds = additionsArray.filter(
					(addition) => addition instanceof EmbedBuilder,
				) as EmbedBuilder[];
				await channel.send({ content: message, embeds, files });
			} else {
				if (!message) continue; // Skip empty messages
				await channel.send(message);
			}
		} catch (error) {
			log(`Discord message failed:`);
			log(error);
			if (error instanceof Error) {
				console.log(error.stack);
				if (error.message === "Missing Permissions") {
					continue; // Not allowed to post, so skip entirely
				}
			}
			// Put the message back into the queue
			queue.unshift({ message, addition });
			// Try again in 10s
			await sleep(10 * 1000);
			continue;
		}

		// If the bot has been sending too many messages, increase the cooldown
		const heat = addChannelHeat("discord", channel.id, 25 + message.length / 2);
		let cooldown = 0;
		if (heat > 100) {
			cooldown = ((heat - 100) / 25) * 1000;
			log(
				`Increased Discord:${channel.id} message cooldown to ${
					Math.round(cooldown / 10) / 100
				}s because too many messages were being sent`,
			);
		}
		await sleep(cooldown);
	}
	// When out of queued text, go off cooldown
	onCooldown.discord.delete(channel.id);
};
