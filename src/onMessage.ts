import { Command, getCommand } from "./commands.js";
import { DiscordMessage } from "./discordclient.js";
import { defaultPrefix } from "./config.js";
import { addUserHeat, checkBan } from "./overheat.js";
import { log } from "./log.js";
import { DiscordCommandCall, TwitchCommandCall } from "./CommandCall.js";
import { TwitchMessage } from "./twitch/twitch_chat.js";
import { queueMessage } from "./message.js";
import { edgyDomains, prefixes } from "./settings.js";
import { CommandError } from "./Errors.js";

// Processes every message read on IRC or Discord
export const onMessage = async (message: GenericMessage): Promise<void> => {
	//	console.log(Timestamp() + " " + ch + " " + username + ": " + message);

	const { network, content, userId } = message;
	if (checkBan(network, userId)) return; // Ignored for spamming too much

	const reply = (text: string): void => {
		queueMessage(network, message.channel, text, message.whisper);
	};

	const domain = message.network === "discord" ? message.message.guild.id : message.channel;
	const prefix = prefixes.get(domain) ?? defaultPrefix;

	const executeCommand = async (
		message: GenericMessage,
		prefix: string,
		commandObject: Command,
		argstring: string,
	) => {
		if (commandObject.edgy && !edgyDomains.has(domain) && !message.whisper) return;
		try {
			let outcome;
			if (message instanceof DiscordMessage) {
				const cCall = new DiscordCommandCall(message, prefix, commandObject, argstring);
				outcome = commandObject.run(cCall);
			} else {
				if (commandObject.discordOnly) return;
				const cCall = new TwitchCommandCall(message, prefix, commandObject, argstring);
				outcome = commandObject.run(cCall);
			}
			let heat = commandObject.heat.fixed + commandObject.heat.perChar * argstring.length;
			heat = Math.min(3, Math.max(1, heat)); // Floor heat at 1 and cap it at 3
			addUserHeat(network, userId, heat); // and generate its user heat.
			await outcome;
		} catch (error) {
			// If a custom error
			if (error instanceof CommandError) {
				if (error.message) reply(error.message);
				else reply(`Usage: ${prefix}${commandObject.name}${commandObject.usage}`);
			} else {
				reply(`Unexpected error with command`);
				log(error);
			}
		}
	};

	if (content.startsWith(prefix)) {
		// Message starts with prefix
		const match = content.slice(prefix.length).match(/^(\w+)(?:\s+(\S.*))?/) as
			| null
			| [string, string, string | undefined];
		if (!match) return;
		const command = match[1].toLowerCase();
		const argstring = match[2]?.trim() ?? "";
		const commandObject = getCommand(command);
		if (!commandObject) return;
		log(`command: ${command}, arguments: ${argstring}`);
		void executeCommand(message, prefix, commandObject, argstring);
		return;
	}

	const match = content.match(/^(l?\?)(\w.*)/) as null | [string, "?" | "l?", string];
	if (match) {
		// Looks like ?query or l?query
		addUserHeat(network, userId, 1);

		const command = match[1] === "?" ? "gamepedia" : "liquipedia";
		const commandObject = getCommand(command);
		if (!commandObject) {
			console.error("Wiki command somehow did not exist");
			return;
		}
		void executeCommand(message, match[1], commandObject, match[2]);
		return;
	}

	if (
		message instanceof TwitchMessage &&
		content === "hello" && // Debug message
		["asplosions", "mal1t1a", "keeposa"].includes(userId.toLowerCase()) // Debug whitelist
	) {
		log('Debug message "hello"');
		reply(`Hi there, ${userId}!`);
	}
};

export default onMessage;
