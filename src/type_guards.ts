export const isNonEmptyArray = <T>(array: T[]): array is [T, ...T[]] => {
	return array.length > 0;
};

export const isArrayWithAtLeastTwoMembers = <T>(array: T[]): array is [T, T, ...T[]] => {
	return array.length > 1;
};
