const msPerMinute = 60 * 1000;

export const msToMinutesSeconds = (ms: number, round = false): string => {
	const minutes = Math.floor(ms / msPerMinute);
	let seconds = (ms % msPerMinute) / 1000;
	if (round) {
		seconds = Math.round(seconds);
	}
	return `${minutes}m ${seconds}s`;
};
