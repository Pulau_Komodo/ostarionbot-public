import { ApplicationCommandOptionType, EmbedBuilder, TextChannel, ThreadChannel } from "discord.js";
import { CommandError } from "../Errors.js";
import { CommandFunction, SlashCommandFunction, addCommand } from "../commands.js";
import { formatList } from "../formatList.js";
import { isMarket, searchArtist, searchTrack, spotifyApi } from "./SpotifyApi.js";
import { groupDigits } from "../util.js";
import { msToMinutesSeconds } from "./util.js";

const artistTopTracks = async (
	artistQuery: string,
	countryCode: string = "US",
): Promise<string> => {
	countryCode = countryCode.toUpperCase();
	if (!isMarket(countryCode)) {
		throw new CommandError("Invalid country code");
	}
	const artist = await searchArtist(artistQuery);
	if (artist === undefined) {
		return "No artist found by that name";
	}
	const result = await spotifyApi.artists.topTracks(artist.id, countryCode);
	if (result.tracks.length === 0) {
		return `${artist.name} has no top tracks in ${countryCode}.`;
	}
	if (result.tracks.length === 1) {
		return `The only top track by ${artist.name} in ${countryCode} is **${
			result.tracks[0]!.name
		}**.`;
	} else {
		const list = formatList(result.tracks.map((track) => `**${track.name}**`));
		return `The top tracks by ${artist.name} in ${countryCode} are ${list}.`;
	}
};

const cArtistTopTracks: CommandFunction = async (cCall) => {
	cCall.reply(await artistTopTracks(cCall.argstring));
};

const slashArtistTopTracks: SlashCommandFunction = async (interaction) => {
	if (!interaction.guildId) throw new CommandError("Could not get guild ID");
	const channel = interaction.channel;
	if (!channel || !(channel instanceof TextChannel || channel instanceof ThreadChannel))
		throw new CommandError("Could not get channel");
	const artist = interaction.options.getString("artist", true);
	const countryCode = interaction.options.getString("country_code");
	return artistTopTracks(artist, countryCode ?? undefined);
};

addCommand({
	name: "toptracks",
	run: cArtistTopTracks,
	category: "spotify",
	usage: "<artist>",
	description:
		"Looks up an artist's top tracks on Spotify. Only shows tracks available on Spotify in the US.",
	slash: {
		run: slashArtistTopTracks,
		description: "Looks up an artist's top tracks on Spotify.",
		options: [
			{
				name: "artist",
				type: ApplicationCommandOptionType.String,
				description: "The artist to look up the top tracks of",
				required: true,
			},
			{
				name: "country_code",
				type: ApplicationCommandOptionType.String,
				description:
					"The region the tracks need to be available in, to be listed. Default: US",
			},
		],
	},
});

const artistInfo: CommandFunction = async (cCall) => {
	const artist = await searchArtist(cCall.argstring);
	if (artist === undefined) {
		return cCall.reply("No artist found by that name");
	}
	const image = artist.images[1] ?? artist.images[0];
	const followerS = artist.followers.total === 1 ? "" : "s";
	const genreS = artist.genres.length === 1 ? "" : "s";
	const genreText =
		artist.genres.length === 0 ? "" : ` Genre${genreS}: ${formatList(artist.genres)}.`;
	const text = `${artist.name} has ${groupDigits(
		artist.followers.total,
	)} follower${followerS} and a popularity rating of ${artist.popularity}.${genreText}`;
	if (cCall.network === "discord" && image && cCall.canReplyEmbed) {
		const embed = new EmbedBuilder();
		embed.setImage(image.url);
		cCall.replyWithAddition(text, embed);
	} else {
		cCall.reply(text);
	}
};

addCommand({
	name: "spotifyartist",
	run: artistInfo,
	category: "spotify",
	usage: "<artist>",
	description: "Looks up an artist on Spotify",
});

const relatedArtists = async (artistQuery: string): Promise<string> => {
	const artist = await searchArtist(artistQuery);
	if (artist === undefined) {
		return "No artist found by that name";
	}
	const result = await spotifyApi.artists.relatedArtists(artist.id);
	if (result.artists.length === 0) {
		return `${artist.name} has no related artists.`;
	} else {
		return `People who listen to ${artist.name} also listen to ${formatList(
			result.artists.map((relatedArtist) => `**${relatedArtist.name}**`),
		)}.`;
	}
};

const cRelatedArtists: CommandFunction = async (cCall) => {
	cCall.reply(await relatedArtists(cCall.argstring));
};

addCommand({
	name: "relatedartists",
	run: cRelatedArtists,
	category: "spotify",
	usage: "<artist>",
	description: "Looks up what other artists listeners of this artist listen to.",
});

const trackInfo = async (trackQuery: string): Promise<string> => {
	const track = await searchTrack(trackQuery);
	if (track === undefined) {
		return "No track found by that name";
	}
	const discText = track.disc_number === 1 ? "" : `disc #${track.disc_number} of `;
	return `**${track.name}** (${msToMinutesSeconds(track.duration_ms, true)}) by ${formatList(
		track.artists.map((artist) => `**${artist.name}**`),
	)} is track #${track.track_number} on${discText} **${track.album.name}**, which came out ${
		track.album.release_date
	}. It has a popularity rating of ${track.popularity}.`;
};

const cTrackInfo: CommandFunction = async (cCall) => {
	cCall.reply(await trackInfo(cCall.argstring));
};

addCommand({
	name: "spotifytrack",
	run: cTrackInfo,
	category: "spotify",
	usage: "<track>",
	description: "Looks up a track on Spotify.",
});

const audioFeatures = async (trackQuery: string): Promise<string> => {
	const track = await searchTrack(trackQuery);
	if (track === undefined) {
		return "No track found by that name";
	}
	const features = await spotifyApi.tracks.audioFeatures(track.id);
	const mode = features.mode === 1 ? "major" : "minor";
	return `**${track.name}** (${msToMinutesSeconds(
		features.duration_ms,
	)}) has the following features: acousticness: ${features.acousticness}, danceability: ${
		features.danceability
	}, energy: ${features.energy}, instrumentalness: ${
		features.instrumentalness
	} (values over 0.5 mean it is probably instrumental), key: ${describeKey(
		features.key,
	)}, liveness: ${features.liveness} (values over 0.8 mean very probably live), loudness: ${
		features.loudness
	}dB, mode: ${mode}, speechiness: ${
		features.speechiness
	} (values below 0.33 are probably no speech, values above 0.66 are probably all speech), tempo: ${
		features.tempo
	} BPM, time signature: ${features.time_signature}/4, valence: ${features.valence}.`;
};

const cAudioFeatures: CommandFunction = async (cCall) => {
	cCall.reply(await audioFeatures(cCall.argstring));
};

addCommand({
	name: "audiofeatures",
	run: cAudioFeatures,
	category: "spotify",
	usage: "<track>",
	description: "Looks up a track's audio features on Spotify.",
});

const describeKey = (key: number): string => {
	const keys = [
		"C",
		"C# or D♭",
		"D",
		"D♯ or E♭",
		"E",
		"F",
		"F♯ or G♭",
		"G",
		"G♯ or A♭",
		"A",
		"A♯ or B♭",
		"B",
	] as const;
	if (key === -1) {
		return "none detected";
	} else if (key > 11) {
		console.error(`Invalid key ${key}`);
		return "invalid";
	}
	return keys[key]!;
};
