import { queueMessage } from "./message.js";
import { addCommand, CommandFunction } from "./commands.js";

import { db } from "./db.js";
import { log } from "./log.js";
import { stringifyInterval } from "interval-conversions";
import got from "got";

interface NewsItem {
	gid: string;
	title: string;
	url: string;
	// eslint-disable-next-line @typescript-eslint/naming-convention
	is_external_url: boolean;
	author: string;
	contents: string;
	feedlabel: string;
	date: number;
	feedname: string;
	feed_type: number;
	appid: number;
	tags: string[];
}

interface AppNews {
	appid: number;
	newsitems: NewsItem[];
	count: number;
}

// Got this from https://stackoverflow.com/questions/295566/sanitize-rewrite-html-on-the-client-side/430240#430240
// Should thoroughly remove all non-displayed HTML page bits
const tagBody = "(?:[^\"'>]|\"[^\"]*\"|'[^']*')*";
const tagOrComment = new RegExp(
	`${
		"<(?:" +
		// Comment body.
		"!--(?:(?:-*[^->])*--+|-?)" +
		// Special "raw text" elements whose content should be elided.
		"|script\\b"
	}${tagBody}>[\\s\\S]*?</script\\s*` +
		`|style\\b${tagBody}>[\\s\\S]*?</style\\s*` +
		// Regular name
		`|/?[a-z]${tagBody})>`,
	"gi",
);
function removeTags(html: string) {
	let oldhtml;
	do {
		oldhtml = html;
		html = html.replace(tagOrComment, "");
	} while (html !== oldhtml);
	return html;
}
/** Regex pattern to find the most recent blog post */
const blogRegex =
	/<div class="widget valve_widget_recent_entries">\s*<div class="recent_entry">\s*<a href="(https?:\/\/blog.dota2.com\/.*?\/)"/;
/** Regex pattern to find the most recent update news post */
const newsRegex = /\[{&quot;clanid&quot;:3703047,&quot;unique_id&quot;:&quot;(\d*)&quot;/;
/** Regex pattern to find the contents of a news post */
const newsContentsRegex = /,&quot;body&quot;:&quot;(.*?)&quot;,&quot;commentcount&quot;:/;
const blogAddress = "http://blog.dota2.com/"; // Blog moved and broke my stuff :(
const newsAddress = "https://store.steampowered.com/newshub/app/570";
const newsApi =
	"http://api.steampowered.com/ISteamNews/GetNewsForApp/v2/?appid=570&count=1&maxlength=0&format=json"; // Not using this cause it sucked
const interval = 60 * 1000; // Time between checking the blog and news in milliseconds
let lastNews = "";
let lastNewsUrl = "";
let lastNewsLength = 0;
let lastNewsTime = 0;

const loadLastNews = () => {
	const qLoadLastNews = `
		SELECT value as lastNews,
		(SELECT value FROM global_variables WHERE key = 'lastNewsUrl') as lastNewsUrl,
		(SELECT value FROM global_variables WHERE key = 'lastNewsLength') as lastNewsLength,
		(SELECT value FROM global_variables WHERE key = 'lastNewsTime') as lastNewsTime
		FROM global_variables WHERE key = 'lastNews';
	`;
	const result = db.prepare(qLoadLastNews).get() as { lastNews: string, lastNewsUrl: string, lastNewsLength: number, lastNewsTime: number};
	({ lastNews, lastNewsUrl, lastNewsLength, lastNewsTime } = result);
};

const saveLastNews = (
	lastNews: string,
	lastNewsUrl: string,
	lastNewsLength: number,
	lastNewsTime: number,
) => {
	const qSaveLastNews = `
		UPDATE global_variables
		SET value = $lastNews
		WHERE key = 'lastNews'
	`;
	const qSaveLastNewsUrl = `
		UPDATE global_variables
		SET value = $lastNewsUrl
		WHERE key = 'lastNewsUrl'
	`;
	const qSaveLastNewsLength = `
		UPDATE global_variables
		SET value = $lastNewsLength
		WHERE key = 'lastNewsLength'
	`;
	const qSaveLastNewsTime = `
		UPDATE global_variables
		SET value = $lastNewsTime
		WHERE key = 'lastNewsTime'
	`;
	const transaction = db.transaction(() => {
		db.prepare(qSaveLastNews).run({ lastNews });
		db.prepare(qSaveLastNewsUrl).run({ lastNewsUrl });
		db.prepare(qSaveLastNewsLength).run({ lastNewsLength });
		db.prepare(qSaveLastNewsTime).run({ lastNewsTime });
	});
	transaction();
};

loadLastNews();

const checkPost = async () => {
	try {
		const response = await got(newsApi).catch(console.error);
		if (!response) {
			log(`News checker error: no response`);
			return;
		}
		const data = JSON.parse(response.body) as RecursivePartial<{ appnews: AppNews }>;
		// eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
		const item = data?.appnews?.newsitems?.[0];
		if (
			item === undefined ||
			item.gid === undefined ||
			item.date === undefined ||
			item.contents === undefined ||
			item.url === undefined
		) {
			log("Invalid news response");
			log(data);
			return;
		}
		if (item.gid === lastNews) return;
		if (item.date < lastNewsTime) {
			console.warn("News was older than last news");
			return;
		}

		const contentLength = item.contents.length; // the number of characters in the news post

		const url = item.url.replace(/ /g, "%20");
		const message = `NEW NEWS POST:  ${url} (${contentLength} characters)`;
		queueMessage("twitch", "#asplosions", message);
		queueMessage("twitch", "#nasabot", message);
		queueMessage("twitch", "#kaz1", message);
		queueMessage("discord", "240523866026278913", `${message} <@&428930650410713092>`); // #general on offline chat
		//log(`NEW NEWS POST:  ${baseUri + match[1]} (${contentLength} characters) (not sent to channels)`);

		lastNews = item.gid;
		lastNewsUrl = url;
		lastNewsLength = contentLength;
		lastNewsTime = item.date;

		saveLastNews(lastNews, lastNewsUrl, lastNewsLength, lastNewsTime);
	} catch (error) {
		console.error(error);
	}
};

setInterval(checkPost, interval);

const cLastNews: CommandFunction = (cCall) => {
	cCall.reply(
		`${lastNewsUrl} (${lastNewsLength} characters) ${stringifyInterval(
			lastNewsTime * 1000 - Date.now(),
		)} ago`,
	);
};

const cLastBlog: CommandFunction = (cCall) => {
	//cCall.reply(`${lastpost} ${intervalToFriendlyString(lastposttime)} ago`);
	cCall.reply(
		`Automatic blog checking does no longer work. since the website changed. If someone knows of an API or something, let Asplosions know.`,
	);
};

addCommand({
	name: "lastnews",
	run: cLastNews,
	category: "miscellaneous",
	usage: "",
	description:
		"Posts the most recently found Dota 2 news article from https://store.steampowered.com/newshub/app/570/",
	aliases: ["latestnews"],
});
addCommand({
	name: "lastblog",
	run: cLastBlog,
	category: "miscellaneous",
	usage: "",
	description: "Posts the most recently found Dota 2 blog post from https://blog.dota2.com/",
	aliases: ["latestblog"],
});
