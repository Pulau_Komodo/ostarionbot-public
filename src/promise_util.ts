/** Rejects if the Promise hasn't settled after the chosen number of ms */
/*export const promiseWithTimeout = async <T>(promise: Promise<T>, waitTime: number): Promise<T> => {
	let timer: ReturnType<typeof setTimeout>;
	const outcome = await Promise.race([promise, new Promise<T>(
		(_, reject) => {
			timer = setTimeout(reject, waitTime, `Promise timed out after ${waitTime}ms`);
		}
	)]);
	if (outcome) clearTimeout(timer);
	return outcome;
};*/

/** Resolves after the chosen number of ms */
export const sleep = (duration: number): Promise<unknown> => {
	return new Promise((resolve) => setTimeout(resolve, duration));
};

const testFunction = async (time: number) => {
	await sleep(time);
	console.log("Hello");
};

//const timer: ReturnType<typeof setTimeout> = setTimeout(Math.random, 5);
