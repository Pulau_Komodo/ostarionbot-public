import { log } from "./log.js";

export const fetchJson = async (input: RequestInfo | URL, init?: RequestInit): Promise<unknown> => {
	const response = await fetch(input, init);
	if (response.status !== 200) {
		log(await response.text());
		throw Error(`${response.url} gave status code ${response.status} (${response.statusText})`);
	}
	return response.json();
};
