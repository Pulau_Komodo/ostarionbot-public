import { db } from "../db.js";
import { log } from "../log.js";
import { downvotePower } from "./common.js";

type EmoteQueryResult = {
	emote: string;
	id: string;
	image: string;
	upvotes: number;
	downvotes: number;
};

/**
 * Queries the database for all information about an emote
 * @param guild Guild ID
 * @param emote Emote ID
 */
export const queryEmote = (guild: string, emote: string): EmoteQueryResult | undefined => {
	const qEmote = `
		SELECT emote, id, image, upvotes, downvotes
		FROM emotes
		WHERE guild = $guild AND emote = $emote
	`;
	const qpEmote = db.prepare(qEmote);
	return qpEmote.get({ guild, emote }) as any;
};

/** Get image link for emote, optionally for a specific variant */
export const queryEmoteImage = (
	guild: string,
	emote: string,
	variant?: string,
): string | undefined => {
	if (variant !== undefined) {
		const qEmoteImage = `
			SELECT image
			FROM emote_variants
			WHERE guild = $guild AND emote = $emote AND variant = $variant
		`;
		return (
			db.prepare(qEmoteImage).get({ guild, emote, variant }) as { image: string } | undefined
		)?.image;
	} else {
		const qEmoteImage = `
			SELECT image
			FROM emotes
			WHERE guild = $guild AND emote = $emote
		`;
		return (db.prepare(qEmoteImage).get({ guild, emote }) as { image: string } | undefined)
			?.image;
	}
};

/** Check current vote of user */
export const queryVote = (guild: string, emote: string, user: string): 0 | 1 | undefined => {
	const qVote = `
		SELECT up
		FROM emote_votes
		WHERE guild = $guild AND emote = $emote AND user = $user
	`;
	const result = db.prepare(qVote).get({ guild, emote, user }) as { up: 0 | 1 } | undefined;
	if (!result) return;
	return result.up;
};

/** Add a vote */
export const queryAddVote = (guild: string, emote: string, user: string, up: 0 | 1): void => {
	const qAddVote = `
		INSERT INTO emote_votes(guild, user, emote, up)
		VALUES ($guild, $user, $emote, $up)
	`;
	db.prepare(qAddVote).run({ guild, user, emote, up });
};

/** Change a vote */
export const queryChangeVote = (guild: string, emote: string, user: string, up: 0 | 1): void => {
	const qChangeVote = `
		UPDATE emote_votes
		SET up = $up
		WHERE guild = $guild AND emote = $emote AND user = $user
	`;
	db.prepare(qChangeVote).run({ guild, user, emote, up });
};

/** Get all emotes for a guild, sorted by score */
export const queryAllEmotes = (guild: string): { emote: string; score: number }[] => {
	const qAllEmotes = `
		SELECT emote, (upvotes-$downvote_power*downvotes) score
		FROM emotes
		WHERE guild = $guild
		ORDER BY score DESC, upvotes DESC
	`;
	return db.prepare(qAllEmotes).all({ guild, downvote_power: downvotePower }) as any;
};

/** Get all emotes for a guild and their images, sorted by score */
export const queryAllEmoteImages = (guild: string): { emote: string; image: string }[] => {
	const qAllEmotes = `
		SELECT emote, image
		FROM emotes
		WHERE guild = $guild
		ORDER BY (upvotes-$downvote_power*downvotes) DESC, upvotes DESC
	`;
	return db.prepare(qAllEmotes).all({ guild, downvote_power: downvotePower }) as any;
};

/** Get top 20 emotes with optional offset */
export const queryTopEmotes = (guild: string, offset = 0): { emote: string; score: number }[] => {
	const qTopEmotes = `
		SELECT emote, (upvotes-$downvote_power*downvotes) score
		FROM emotes
		WHERE guild = $guild
		ORDER BY score DESC, upvotes DESC
		LIMIT 20 OFFSET $offset
	`;
	return db.prepare(qTopEmotes).all({ downvote_power: downvotePower, guild, offset }) as any;
};

/** Get the upvote and downvote counts on a specific emote */
export const queryCountVotes = (
	guild: string,
	emote: string,
): { upvotes: number; downvotes: number } | undefined => {
	const qCountVotes = `
		SELECT
			(SELECT COUNT()
			FROM emote_votes
			WHERE guild = $guild AND emote = $emote AND up = 1)
		AS upvotes,
			(SELECT COUNT()
			FROM emote_votes
			WHERE guild = $guild AND emote = $emote AND up = 0)
		AS downvotes
	`;
	return db.prepare(qCountVotes).get({ guild, emote }) as any;
};

/** Sets the image link for an emote */
export const querySetImage = (guild: string, emote: string, image: string): void => {
	const qSetImage = `
		UPDATE emotes
		SET image = $image
		WHERE guild = $guild AND emote = $emote
	`;
	db.prepare(qSetImage).run({ guild, emote, image });
};

/** Adds a new emote, with its default 1 upvote */
export const queryAddEmote = (guild: string, emote: string, image: string): void => {
	const qAddEmote = `
		INSERT INTO emotes(emote, guild, image, upvotes)
		VALUES ($emote, $guild, $image, 1)
	`;
	db.prepare(qAddEmote).run({ emote, guild, image });
};

/** Deletes an emote, should delete its votes through database relationships */
export const queryDeleteEmote = (guild: string, emote: string): void => {
	const qDeleteEmote = `
		DELETE FROM emotes
		WHERE guild = $guild AND emote = $emote
	`;
	db.prepare(qDeleteEmote).run({ guild, emote });
};

/** Changes the name of an existing emote */
export const queryRenameEmote = (guild: string, emote: string, newName: string): void => {
	const qRenameEmote = `
		UPDATE emotes
		SET emote = $newName
		WHERE guild = $guild AND emote = $emote
	`;
	db.prepare(qRenameEmote).run({ newName, guild, emote });
};

/** Sets an emote variant */
export const querySetVariant = (
	guild: String,
	emote: String,
	variant: String,
	image: String,
): void => {
	const qSetVariant = `
		INSERT INTO emote_variants (emote, guild, variant, image)
		VALUES ($emote, $guild, $variant, $image)
	`;
	db.prepare(qSetVariant).run({ guild, emote, variant, image });
};

/** Removes an emote variant */
export const queryRemoveVariant = (guild: String, emote: String, variant: String): void => {
	const qSetVariant = `
		DELETE FROM emote_variants
		WHERE emote = $emote AND guild = $guild AND variant = $variant
	`;
	db.prepare(qSetVariant).run({ guild, emote, variant });
};

/** Get all of a user's votes on one guild */
export const queryUserVotes = (guild: string, user: string): { emote: string; up: 0 | 1 }[] => {
	const qUserVotes = `
		SELECT emote, up
		FROM emote_votes
		WHERE guild = $guild AND user = $user
		ORDER BY emote ASC
	`;
	return db.prepare(qUserVotes).all({ guild, user }) as any;
};

/** Check multiple emotes for whether they exist and get their names */
export const queryMultipleEmotes = (
	guild: string,
	emotes: string[],
): { emote: string; existent: boolean }[] => {
	if (emotes.length === 0) return [];
	const qEmote = `
		SELECT emote
		FROM emotes
		WHERE guild = $guild AND emote = $emote
	`;
	const qpEmote = db.prepare(qEmote);
	return emotes.map((emote) => {
		const result = qpEmote.get({ guild, emote }) as { emote: string } | undefined;
		if (result) return { emote: result.emote, existent: true };
		return { emote, existent: false };
	});
};

/** Check multiple votes for whether they are up, down or non-existent */
export const queryMultipleVotes = (
	guild: string,
	user: string,
	emotes: string[],
): { emote: string; vote: -1 | 0 | 1 }[] => {
	if (emotes.length === 0) return [];
	const qVote = `
		SELECT up
		FROM emote_votes
		WHERE guild = $guild AND user = $user AND emote = $emote
	`;
	const qpVote = db.prepare(qVote);
	return emotes.map((emote) => {
		const result = qpVote.get({ guild, user, emote }) as { up: 0 | 1 } | undefined;
		if (result === undefined) return { emote, vote: 0 };
		if (result.up === 1) return { emote, vote: 1 };
		return { emote, vote: -1 };
	});
};

/** Add votes with the same value for multiple emotes for one user */
export const queryAddMultipleVotes = (
	guild: string,
	user: string,
	emotes: string[],
	up: 0 | 1,
): void => {
	if (emotes.length === 0) return;
	const qAddVote = `
		INSERT INTO emote_votes(guild, user, emote, up)
		VALUES ($guild, $user, $emote, $up)
	`;
	const qpAddVote = db.prepare(qAddVote);
	emotes.forEach((emote) => qpAddVote.run({ guild, user, emote, up }));
};

/** Changes the votes to the same value for multiple emotes for one user */
export const queryChangeMultipleVotes = (
	guild: string,
	user: string,
	emotes: string[],
	up: 0 | 1,
): void => {
	if (emotes.length === 0) return;
	const qChangeVote = `
		UPDATE emote_votes
		SET up = $up
		WHERE guild = $guild AND user = $user AND emote = $emote
	`;
	const qpChangeVote = db.prepare(qChangeVote);
	emotes.forEach((emote) => qpChangeVote.run({ guild, user, emote, up }));
};

/** Removes the votes for multiple emotes for one user */
export const queryRemoveMultipleVotes = (guild: string, user: string, emotes: string[]): void => {
	if (emotes.length === 0) return;
	const qRemoveVote = `
		DELETE FROM emote_votes
		WHERE guild = $guild AND user = $user AND emote = $emote
	`;
	const qpRemoveVote = db.prepare(qRemoveVote);
	emotes.forEach((emote) => qpRemoveVote.run({ guild, user, emote }));
};

/** Recount totals and update tallies for multiple emotes. To do: This could probably be done with one query, or even automatically on all votes. */
export const queryCountAndUpdateMultipleTallies = (guild: string, emotes: string[]): void => {
	if (emotes.length === 0) return;
	// Count current votes
	const qCountVotes = `
		SELECT
			(SELECT COUNT()
			FROM emote_votes
			WHERE guild = $guild AND emote = $emote AND up = 1)
		AS upvotes,
			(SELECT COUNT()
			FROM emote_votes
			WHERE guild = $guild AND emote = $emote AND up = 0)
		AS downvotes
	`;
	// Update vote tallies in the database
	const qUpdateTallies = `
		UPDATE emotes
		SET upvotes = $upvotes, downvotes = $downvotes
		WHERE guild = $guild AND emote = $emote
	`;
	const qpCountVotes = db.prepare(qCountVotes);
	const qpUpdateTallies = db.prepare(qUpdateTallies);
	for (const emote of emotes) {
		const voteResults = qpCountVotes.get({ guild, emote }) as
			| { upvotes: number; downvotes: number }
			| undefined;
		if (!voteResults) {
			log("Error: emote that was just voted on has no score found");
			continue;
		}
		const { upvotes, downvotes } = voteResults;
		qpUpdateTallies.run({ guild, emote, upvotes, downvotes });
	}
};
