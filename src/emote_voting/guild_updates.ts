import { ApplicationCommandOptionType, Guild, GuildMember, Permissions } from "discord.js";
import { DiscordCommandFunction, SlashCommandFunction, addCommand } from "../commands.js";
import { CommandError } from "../Errors.js";
import {
	parseEmoteWithId,
	getGuildAndMember,
	category,
	parseEmote,
	getImage,
	validateEmotePermissions,
	validateVariantName,
} from "./common.js";
import { queryEmote, queryEmoteImage } from "./queries.js";
import { sleep } from "../promise_util.js";

/** A user attempts to remove an emote from the guild's list */
const removeEmoteFromDiscord = async (
	guild: Guild,
	emoteArg: string | undefined,
	member: GuildMember,
	reason: string,
): Promise<string> => {
	validateEmotePermissions(member);
	if (!emoteArg) throw new CommandError();

	const { emote, id: emoteId } = parseEmoteWithId(emoteArg);
	const emoteCache = guild.emojis.cache;
	const emoteObject = emoteId
		? emoteCache.get(emoteId)
		: emoteCache.find((x) => !x.animated && x.name?.toLowerCase() === emote.toLowerCase());
	if (!emoteObject)
		throw new CommandError(`There is no emote named ${emote} in this Discord guild`);

	const removedEmote = await emoteObject.delete(reason).catch(console.log);
	if (!removedEmote)
		throw new CommandError(`There was no emote named ${emote} in this Discord guild`);
	return `${removedEmote.name ?? "unknown"} was removed from the guild's emote list.`;
};

const commandRemoveEmoteFromDiscord: DiscordCommandFunction = async (cCall) => {
	cCall.reply(
		await removeEmoteFromDiscord(
			cCall.message.guild,
			cCall.args[0],
			cCall.message.member,
			`Removal requested by ${cCall.message.member.displayName} (${cCall.user.id}) using ${cCall.prefix}${cCall.command}`,
		),
	);
};

const slashRemoveEmoteFromDiscord: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emoteArg = interaction.options.getString("emote", true);
	return removeEmoteFromDiscord(
		guild,
		emoteArg,
		member,
		`Removal requested by ${member.displayName} (${member.id}) using /${
			interaction.command?.name ?? "unknown"
		}`,
	);
};

addCommand({
	name: "deactivateemote",
	run: commandRemoveEmoteFromDiscord,
	category,
	usage: "<emote>",
	description:
		"Removes the chosen emote from the Discord guild. (emote manage permission needed)",
	slash: {
		run: slashRemoveEmoteFromDiscord,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "The emote to remove from the guild",
				required: true,
			},
		],
	},
	discordOnly: true,
});

/** A user attempts to add an emote to the guild's list */
const addEmoteToDiscord = async (
	guild: Guild,
	emoteArg: string | undefined,
	variant: string | undefined,
	member: GuildMember,
	reason: string,
): Promise<string> => {
	validateEmotePermissions(member);
	if (!emoteArg) throw new CommandError();

	let emote = parseEmote(emoteArg);
	const emoteCache = guild.emojis.cache;
	const emoteObject = emoteCache.find(
		(x) => !x.animated && x.name?.toLowerCase() === emote.toLowerCase(),
	);
	if (emoteObject)
		throw new CommandError(
			`There is already an emote <:${emoteObject.name ?? "unknown"}:${
				emoteObject.id
			}> in this Discord guild`,
		);
	const dbEmote = queryEmote(guild.id, emote);
	if (dbEmote) {
		emote = dbEmote.emote;
	}
	const image = await getImage(guild.id, emote, variant);
	if (!image) throw new CommandError(`Could not get the emote's ${variant ?? "default"} image`);
	const newEmote = await guild.emojis.create({
		attachment: image,
		name: emote,
		reason,
	});
	return `<:${newEmote.name ?? "unknown"}:${newEmote.id}> was added to the guild's emote list.`;
};

const commandAddEmoteToDiscord: DiscordCommandFunction = async (cCall) => {
	cCall.reply(
		await addEmoteToDiscord(
			cCall.message.guild,
			cCall.args[0],
			undefined,
			cCall.message.member,
			`Addition requested by ${cCall.message.member.displayName} (${cCall.user.id}) using ${cCall.prefix}${cCall.command}`,
		),
	);
};

const slashAddEmoteToDiscord: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emoteArg = interaction.options.getString("emote", true);
	return addEmoteToDiscord(
		guild,
		emoteArg,
		undefined,
		member,
		`Addition requested by ${member.displayName} (${member.id}) using /${
			interaction.command?.name ?? "unknown"
		}`,
	);
};

addCommand({
	name: "activateemote",
	run: commandAddEmoteToDiscord,
	category,
	usage: "<emote>",
	description: "Adds the chosen emote to the Discord guild. (emote manage permission needed)",
	slash: {
		run: slashAddEmoteToDiscord,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "The name of the new emote",
				required: true,
			},
		],
	},
	discordOnly: true,
});

/** A user attempts to add an emote to the guild's list */
const setEmoteVariant = async (
	guild: Guild,
	emoteArg: string | undefined,
	variantArg: string | undefined,
	member: GuildMember,
	reason: string,
): Promise<string> => {
	validateEmotePermissions(member);
	if (!emoteArg) throw new CommandError();
	let emote = parseEmote(emoteArg);

	if (variantArg) {
		validateVariantName(variantArg);
		let image = queryEmoteImage(guild.id, emote, variantArg);
		if (!image) {
			throw new CommandError(`No ${variantArg} variant set for ${emote}.`);
		}
	}

	await removeEmoteFromDiscord(guild, emoteArg, member, reason);
	await sleep(500); // To do: solve this race condition better
	await addEmoteToDiscord(guild, emote, variantArg, member, reason);

	return `Succesfully set ${emote} to ${variantArg ?? "default"} variant.`;
};

const commandActivateVariant: DiscordCommandFunction = async (cCall) => {
	cCall.reply(
		await setEmoteVariant(
			cCall.message.guild,
			cCall.args[0],
			cCall.args[1],
			cCall.message.member,
			`Replacement with ${cCall.args[1] ?? "the default"} variant requested by ${
				cCall.message.member.displayName
			} (${cCall.user.id}) using ${cCall.prefix}${cCall.command}`,
		),
	);
};

const slashSetVariant: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emoteArg = interaction.options.getString("emote", true);
	const variantArg = interaction.options.getString("variant", false);
	return setEmoteVariant(
		guild,
		emoteArg,
		variantArg ?? undefined,
		member,
		`Replacement with ${variantArg ?? "the default"} variant requested by ${
			member.displayName
		} (${member.id}) using /${interaction.command?.name ?? "unknown"}`,
	);
};

addCommand({
	name: "activatevariant",
	run: commandActivateVariant,
	category,
	usage: "<emote> [variant]",
	description:
		"Sets the chosen variant for the chosen emote on the Discord guild. (emote manage permission needed)",
	slash: {
		run: slashSetVariant,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "The name of the emote to change to a different variant",
				required: true,
			},
			{
				name: "variant",
				type: ApplicationCommandOptionType.String,
				description: "The name of the variant. Omit to reset to default.",
				required: false,
			},
		],
	},
	discordOnly: true,
});
