import { addCommand, DiscordCommandFunction } from "../commands.js";
import { formatList } from "../formatList.js";
import { queryAllEmoteImages } from "./queries.js";

const whitelist = [
	"https://cdn.discordapp.com/",
	"https://media.discordapp.net/",
	"https://i.imgur.com/",
	"https://i.redd.it/",
];

export const hasWhitelistedImageDomain = (link: string): boolean => {
	return whitelist.some((domain) => link.startsWith(domain));
};

const listUnwhitelisted = (guildId: string): string => {
	const emotes = queryAllEmoteImages(guildId)
		.filter(({ image }) => !hasWhitelistedImageDomain(image))
		.map(({ emote }) => emote);
	if (emotes.length === 0) {
		return "There are no emotes without whitelisted images. 👍";
	} else if (emotes.length === 1 && emotes[0] !== undefined) {
		return `The only emote without a whitelisted image is ${emotes[0]}.`;
	} else {
		return `The ${emotes.length} emotes without whitelisted images are ${formatList(emotes)}.`;
	}
};

const commandListUnwhitelisted: DiscordCommandFunction = (cCall) => {
	cCall.reply(listUnwhitelisted(cCall.guildId));
};

addCommand({
	name: "emotelistunwhitelisted",
	run: commandListUnwhitelisted,
	category: "emote",
	description: "Lists all emote proposals that have images on non-whitelisted domains.",
	discordOnly: true,
});
