import { db } from "../db.js";
import { rankChannels, votingRoles, adminRoles, rankMessageIds } from "./common.js";

// Loads per-guild configuration things
// Sets each guild's rank channel and sets up hourly updates on the rank posts

// Get the emote rank channel for all guilds that have them
const qGetGuildConfig = `
	SELECT guild, emote_rank_channel channel, emote_voting_role voting_role, emote_admin_role admin_role
	FROM discord_config
`;
// Get the IDs of the previous rank messages
const qGetRankMessages = `
	SELECT guild, message_id
	FROM emote_rank_messages
	ORDER BY message_number ASC
`;

const guildConfigResults = db.prepare(qGetGuildConfig).all() as { guild: string, channel: string, voting_role: string, admin_role: string}[];
for (const guildConfig of guildConfigResults) {
	rankChannels.set(guildConfig.guild, guildConfig.channel);
	votingRoles.set(guildConfig.guild, guildConfig.voting_role);
	adminRoles.set(guildConfig.guild, guildConfig.admin_role);
	rankMessageIds.set(guildConfig.guild, []);
}

const rankMessageResults = db.prepare(qGetRankMessages).all() as { guild: string, message_id: string }[];
for (const rankMessage of rankMessageResults) {
	rankMessageIds.get(rankMessage.guild)?.push(rankMessage.message_id);
}

import { updateAllRankPosts } from "./rank_list.js";

setTimeout(updateAllRankPosts, 10 * 60 * 1000); // Wait 10 minutes to do the first update
setInterval(updateAllRankPosts, 60 * 60 * 1000); // Update the rank posts hourly

import "./changes.js";
import "./guild_updates.js";
import "./info.js";
import "./voting.js";
import "./id.js";
import "./variants.js";
