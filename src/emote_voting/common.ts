import {
	Collection,
	GuildEmoji,
	Guild,
	CommandInteraction,
	GuildMember,
	AttachmentBuilder,
	PermissionFlagsBits,
} from "discord.js";
import fsPromises from "fs/promises";
import got from "got";
import sharp from "sharp";
import { CommandError } from "../Errors.js";
import { emotePreview } from "./emotePreview.js";
import { hasWhitelistedImageDomain } from "./hasWhitelistedImageDomain.js";
import { queryEmoteImage } from "./queries.js";

/** How much power a downvote has compared to an upvote */
export const downvotePower = 0.5;
/** A map with guild ids as keys and ids of channels where their full emote list should be posted as values */
export const rankChannels: Map<string, string> = new Map();
/** A map with guild ids as keys and arrays of ids of messages containing the most recent full emote list as values */
export const rankMessageIds: Map<string, string[]> = new Map();
/** A map with guild ids as keys and ids of roles that can vote and propose emotes as values */
export const votingRoles: Map<string, string> = new Map();
/** A map with guild ids as keys and ids of roles that can perform administrative commands as values */
export const adminRoles: Map<string, string> = new Map();
/** The name of the category for the command handler */
export const category = "emote";

/** Returns a map of the guild's available, non-animated emotes with names as keys and IDs as values */
export const makeEmoteMap = (guild: Guild): Map<string, string> => {
	const emoteMap: Map<string, string> = new Map();
	guild.emojis.cache
		.filter((emote) => (emote.available ?? false) && !emote.animated)
		.each((emote) => emoteMap.set(emote.name as string, emote.id)); // Supposedly name can only be null in reaction emoji objects
	return emoteMap;
};
/**
 * Turn an emote's name into an emote, if it exists in the map
 * @param emotesMap Map with emote names as keys and emote IDs as properties, or the Discord guild's emotes cache
 * @returns A string formatted to display the emote, or just the emote's name
 */
export const emotify = (
	emoteName: string,
	emotesMap: Collection<string, GuildEmoji> | Map<string, string>,
): string => {
	const regexEscape = /(\*|_|`|~|\\)/g;
	if (emotesMap instanceof Collection) {
		const emote = emotesMap.find((x) => x.name === emoteName);
		return emote && emote.available && !emote.animated
			? emote.toString()
			: emoteName.replace(regexEscape, "\\$1");
	} else {
		const id = emotesMap.get(emoteName);
		return id ? `<:${emoteName}:${id}>` : emoteName.replace(regexEscape, "\\$1");
	}
};
/**
 * Finds the name of an emote in <:emote:id> or :emote: format
 * @returns If it couldn't match a format, returns input string
 */
export const parseEmote = (emote: string): string => {
	const match = /^<:(\w+):\d+>$/.exec(emote) as null | [string, string];
	if (match)
		// <:emote:id>
		return match[1];
	const match2 = /^:(\w+):$/.exec(emote) as null | [string, string];
	if (match2)
		// :emote:
		return match2[1];
	return emote;
};
/**
 * Finds the name and ID of an emote in <:emote:id> or :emote: format
 * @returns Emote will be the input string if it couldn't match a format. ID will be undefined if it couldn't find an ID.
 */
export const parseEmoteWithId = (emote: string): { emote: string; id: string | undefined } => {
	const match = /^<:(\w+):(\d+)>$/.exec(emote) as null | [string, string, string];
	if (match)
		// <:emote:id>
		return { emote: match[1], id: match[2] };
	const match2 = /^:(\w+):$/.exec(emote) as null | [string, string];
	if (match2)
		// :emote:
		return { emote: match2[1], id: undefined };
	return { emote, id: undefined };
};

/** Returns the directory the emote image should be in, if one is cached. */
const cacheDirectory = (guildId: string, variant?: string): string => {
	const variantPath = variant ? `/${variant}` : "";
	return `./data/cache/emotes/${guildId}${variantPath}`;
};

/** Gets the guild and member from an interaction, or throws a CommandError */
export const getGuildAndMember = (interaction: CommandInteraction): [Guild, GuildMember] => {
	const guild = getGuild(interaction);
	const member = interaction.member;
	if (!(member instanceof GuildMember)) throw new CommandError("Could not get member");
	return [guild, member];
};

/** Gets the guild from an interaction, or throws a CommandError */
export const getGuild = (interaction: CommandInteraction): Guild => {
	const guild = interaction.guild;
	if (!guild) throw new CommandError("Could not get guild");
	return guild;
};

/** Returns plain text or text with attachment depending on whether canAttach is true and imageLink is whitelisted */
export const outputPossiblyWithPreview = async (
	text: string,
	guildId: string,
	emote: string,
	canAttach: boolean,
): Promise<string | { content: string; files: [AttachmentBuilder] }> => {
	if (canAttach) {
		const image = await getImage(guildId, emote);
		if (image !== undefined) {
			const preview = await emotePreview(image);
			if (preview !== undefined) {
				return { content: text, files: [preview] };
			}
		}
	}
	return text;
};

/** The maximum file size Discord will accept, in bytes */
const MAX_EMOJI_SIZE = 256_000;
/** The maximum file size getImage will bother trying to convert, in bytes */
const MAX_IMAGE_SIZE = 3_000_000;

/**
 * Gets an emote image from the cache, or tries to get it from the link and cache it.
 *
 * Returns undefined if there was no matching image in cache or matching URL in database. Or if the link was not whitelisted.
 */
export const getImage = async (
	guildId: string,
	emote: string,
	variant?: string,
): Promise<Buffer | undefined> => {
	const dir = cacheDirectory(guildId, variant);
	const path = `${dir}/${emote}.png`;
	try {
		await fsPromises.mkdir(dir, { recursive: true });
		try {
			const image = await fsPromises.readFile(path);
			if (image.byteLength > 0) {
				return image;
			}
		} catch (_) {
			const imageLink = queryEmoteImage(guildId, emote, variant);
			if (!imageLink) {
				console.log(`Emote ${emote} had no ${variant ?? "default"} image link`);
				return undefined;
			} else if (!hasWhitelistedImageDomain(imageLink)) {
				console.log(
					`Emote ${emote} ${
						variant ?? "default"
					} image link (${imageLink}) was not whitelisted`,
				);
				return undefined;
			}
			const image = await got(imageLink).buffer();
			if (image.byteLength > MAX_IMAGE_SIZE) {
				console.log(
					`Emote ${emote}'s image was too large (${image.byteLength}) before compression (max: ${MAX_IMAGE_SIZE})`,
				);
				return undefined;
			}
			const sharpImage = await sharp(image).png().toBuffer();
			if (sharpImage.byteLength > MAX_EMOJI_SIZE) {
				console.log(
					`Emote ${emote}'s image was too large (${sharpImage.byteLength}) after compression (max: ${MAX_EMOJI_SIZE})`,
				);
				return undefined;
			}
			await fsPromises.writeFile(path, sharpImage);
			return sharpImage;
		}
	} catch (error) {
		console.log(error);
		return undefined;
	}
};

export const clearCachedImage = (guildId: string, emote: string, variant?: string): void => {
	const dir = cacheDirectory(guildId, variant);
	const path = `${dir}/${emote}.png`;
	fsPromises.unlink(path).catch(console.error);
};

export const renameCachedImage = (
	guildId: string,
	emoteOld: string,
	emoteNew: string,
	variant?: string,
): void => {
	const dir = cacheDirectory(guildId, variant);
	const oldPath = `${dir}/${emoteOld}.png`;
	const newPath = `${dir}/${emoteNew}.png`;
	try {
		void fsPromises.rename(oldPath, newPath);
	} catch (error) {
		console.error(error);
	}
};

/** Throws an appropriate CommandError if either the member or the bot client lacks permission to change emotes */
export const validateEmotePermissions = (member: GuildMember) => {
	const userPermission = member.permissions.has(PermissionFlagsBits.ManageEmojisAndStickers);
	const botPermission = member.guild.members.me?.permissions.has(
		PermissionFlagsBits.ManageEmojisAndStickers,
	);
	if (!userPermission) {
		if (!botPermission)
			throw new CommandError(`User and I *both* lack permission to manage emotes`);
		else throw new CommandError(`User lacks permission to manage emotes`);
	}
	if (!botPermission) throw new CommandError(`I lack permission to manage emotes`);
};

type ValidVariantName = string & { readonly __tag: unique symbol };

/** Throws an appropriate `CommandError`if  the variant name is not valid. A valid variant name contains only 1-20 characters [a-z]. */
export function validateVariantName(variant: string): asserts variant is ValidVariantName {
	if (!/^[a-z]{1,20}$/.test(variant)) {
		throw new CommandError(
			"Variant names have to be between 1 and 20 plain a-z letters, and nothing else.",
		);
	}
}
