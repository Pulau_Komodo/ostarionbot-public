import { AttachmentBuilder } from "discord.js";
import sharp from "sharp";
import { log } from "../log.js";

const smallEmoteSize = 22; // 22x22
const largeEmoteSize = 48; // 48x48
const padding = 5;

export const emotePreview = async function (image: Buffer): Promise<AttachmentBuilder | undefined> {
	try {
		const largeImage = sharp(image).png().ensureAlpha();
		const smallImage = largeImage.clone();

		largeImage.resize({
			width: largeEmoteSize,
			height: largeEmoteSize,
			fit: "inside",
		});
		smallImage.resize({
			width: smallEmoteSize,
			height: smallEmoteSize,
			fit: "inside",
		});

		largeImage.extend({
			right: smallEmoteSize + padding,
			background: { r: 0, g: 0, b: 0, alpha: 0 },
		});
		const smallImageBuffer = await smallImage.toBuffer();
		largeImage.composite([{ input: smallImageBuffer, gravity: "east" }]);
		const outputBuffer = await largeImage.toBuffer();

		const attachment = new AttachmentBuilder(outputBuffer, {
			name: "emote-preview.png",
			description: `A preview of an emote`,
		});
		return attachment;
	} catch (error) {
		log(`Error generating emote preview`);
		if (error instanceof Error) log(error.stack);
		return;
	}
};
