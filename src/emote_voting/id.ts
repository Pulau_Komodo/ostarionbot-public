import { CommandError } from "../Errors.js";
import { addCommand, CommandFunction, SlashCommandFunction } from "../commands.js";
import { formatList } from "../formatList.js";
import { ApplicationCommandOptionType } from "discord.js";

/** Get the IDs of all used emotes (just because there is no good way to get them on mobile) */
const emoteId = (text: string): string => {
	const matches = [...text.matchAll(/<:(\w+):(\d+)>/g)] as unknown as [string, string, string][];
	const ids = matches.map((match) => `${match[1]} ${match[2]}`);
	if (ids.length === 0) throw new CommandError("No emotes were found");
	return `Emote ${formatList(ids, { opening: ["ID: ", "IDs: "] })}`;
};

const cEmoteId: CommandFunction = (cCall) => {
	if (!cCall.argstring) throw new CommandError();
	cCall.reply(emoteId(cCall.argstring));
};

const slashEmoteId: SlashCommandFunction = (interaction) => {
	const text = interaction.options.getString("emotes", true);
	return emoteId(text);
};

addCommand({
	name: "emoteid",
	run: cEmoteId,
	category: "emote",
	usage: "<emotes>",
	description: "Gets the IDs for the given custom emotes.",
	slash: {
		run: slashEmoteId,
		options: [
			{
				name: "emotes",
				type: ApplicationCommandOptionType.String,
				description: "Text containing at least one custom emote",
				required: true,
			},
		],
	},
	discordOnly: true,
});
