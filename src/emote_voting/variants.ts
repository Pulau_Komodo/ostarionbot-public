import { ApplicationCommandOptionType, Guild, GuildMember } from "discord.js";
import { addCommand, DiscordCommandFunction, SlashCommandFunction } from "../commands.js";
import { CommandError } from "../Errors.js";
import {
	emotify,
	getGuildAndMember,
	getImage,
	parseEmoteWithId,
	validateEmotePermissions,
} from "./common.js";
import { queryEmote } from "./queries.js";

/** A user attempts to set an active emote to a specific variant */
const selectEmoteVariant = async (
	member: GuildMember,
	guild: Guild,
	emoteArg: string | undefined,
	variant?: string,
): Promise<string> => {
	validateEmotePermissions(member);
	if (!emoteArg) throw new CommandError();
	if (variant === undefined) {
		const { emote, id: emoteId } = parseEmoteWithId(emoteArg);

		const dbEmote = queryEmote(guild.id, emote);

		const emoteCache = guild.emojis.cache;
		const discordEmote = emoteId
			? emoteCache.get(emoteId)
			: emoteCache.find((x) => !x.animated && x.name?.toLowerCase() === emote.toLowerCase());

		if (!dbEmote) {
			if (!discordEmote)
				throw new CommandError(
					`There is no emote named ${emote} in the voting database *or* in the Discord guild`,
				);
			throw new CommandError(
				`The emote ${emotify(
					discordEmote.name ?? "",
					emoteCache,
				)} is not in the voting database`,
			);
		}
		if (!discordEmote)
			throw new CommandError(`The emote ${dbEmote.emote} is not currently active`);

		const image = await getImage(guild.id, dbEmote.emote);
		if (!image) throw new CommandError(`Somehow could not get an image for the emote`);

		const removedEmote = await discordEmote
			.delete(`Removed emote to set its image back to default`)
			.catch(console.log);
		if (!removedEmote)
			throw new CommandError(
				`Somehow could not delete emote named ${
					discordEmote.name ?? "?"
				} in this Discord guild`,
			);

		const newEmote = await guild.emojis.create({
			attachment: image,
			name: dbEmote.emote,
			reason: `Re-added emote to set its image back to default`,
		});
		return `<:${newEmote.name ?? "unknown"}:${newEmote.id}> was set to its default image.`;
	} else {
		throw new CommandError("Variants are not yet implemented");
	}
};

const cSelectEmoteVariant: DiscordCommandFunction = async (cCall) => {
	cCall.reply(
		await selectEmoteVariant(
			cCall.message.member,
			cCall.message.guild,
			cCall.args[0],
			cCall.args[1],
		),
	);
};

const slashSelectEmoteVariant: SlashCommandFunction = (interaction): Promise<string> => {
	const [guild, member] = getGuildAndMember(interaction);
	const emoteArg = interaction.options.getString("emote", true);
	const variantArg = interaction.options.getString("variant") ?? undefined;
	return selectEmoteVariant(member, guild, emoteArg, variantArg);
};

addCommand({
	name: "selectemotevariant",
	run: cSelectEmoteVariant,
	category: "emote",
	usage: "[variant]",
	description:
		"Sets an emote image to a specific variant, or to the default if no variant was specified.",
	slash: {
		run: slashSelectEmoteVariant,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "The emote to change the image of",
				required: true,
			},
			{
				name: "variant",
				type: ApplicationCommandOptionType.String,
				description: "The variant to set the emote image to",
			},
		],
	},
	discordOnly: true,
});
