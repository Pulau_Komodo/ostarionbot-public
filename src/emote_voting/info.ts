import { addCommand, DiscordCommandFunction, SlashCommandFunction } from "../commands.js";
import { log } from "../log.js";
import { emotePreview } from "./emotePreview.js";
import { queryEmote, queryTopEmotes, queryUserVotes } from "./queries.js";
import { hasWhitelistedImageDomain } from "./hasWhitelistedImageDomain.js";
import {
	Guild,
	AttachmentBuilder,
	TextChannel,
	ThreadChannel,
	User,
	ApplicationCommandOptionType,
} from "discord.js";
import { CommandError } from "../Errors.js";
import { formatList } from "../formatList.js";
import {
	makeEmoteMap,
	emotify,
	parseEmote,
	downvotePower,
	category,
	outputPossiblyWithPreview,
	getGuildAndMember,
	getGuild,
} from "./common.js";
import { canSendAttachment } from "../permission checks.js";
import { isNonEmptyArray } from "../type_guards.js";
import got from "got";

/** A user attempts to get all the info on a specific emote, with preview */
const getEmoteInfo = async (
	guild: Guild,
	emoteArg: string | undefined,
	canAttach: boolean,
): Promise<string | { content: string; files: [AttachmentBuilder] }> => {
	if (!emoteArg) throw new CommandError();
	const emote = parseEmote(emoteArg);
	const emoteResult = queryEmote(guild.id, emote);
	if (!emoteResult) throw new CommandError(`There is no emote named ${emote} in the database`);

	const score = emoteResult.upvotes - downvotePower * emoteResult.downvotes;
	let outputText = `${emotify(emoteResult.emote, guild.emojis.cache)} `;
	outputText += `**${emoteResult.upvotes}**👍 **${emoteResult.downvotes}**👎 **${score}**📈`;

	let output = await outputPossiblyWithPreview(
		outputText,
		guild.id,
		emoteResult.emote,
		canAttach,
	);
	if (typeof output === "string") {
		// Kinda ugly way to still add link if preview adding failed
		output += ` <${emoteResult.image}>`;
	}
	return output;
};

const commandEmoteInfo: DiscordCommandFunction = async (cCall) => {
	const result = await getEmoteInfo(cCall.message.guild, cCall.args[0], cCall.canReplyAttachment);
	if (typeof result === "string") {
		cCall.reply(result);
	} else {
		cCall.replyWithAddition(result.content, result.files[0]);
	}
};

const slashEmoteInfo: SlashCommandFunction = (interaction) => {
	const guild = getGuild(interaction);
	const emote = interaction.options.getString("emote", true);
	const channel = interaction.channel;
	const canAttach =
		channel instanceof TextChannel || channel instanceof ThreadChannel
			? canSendAttachment(channel)
			: false;
	return getEmoteInfo(guild, emote, canAttach);
};

addCommand({
	name: "emote",
	run: commandEmoteInfo,
	category,
	usage: "<emote>",
	description: "Gets basic information about the chosen emote.",
	slash: {
		run: slashEmoteInfo,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "The emote to get information about",
				required: true,
			},
		],
	},
	discordOnly: true,
});

/** A user attempts to get an emote preview from a link */
const preview = async (
	imageLink: string | undefined,
	canAttach: boolean,
): Promise<AttachmentBuilder> => {
	if (!imageLink) throw new CommandError();
	if (!canAttach) throw new CommandError(`I cannot post attachments in this channel`);
	if (!hasWhitelistedImageDomain(imageLink))
		throw new CommandError(
			`Only direct links to images hosted on Discord, Imgur or Reddit work`,
		);
	try {
		const image = await got(imageLink).buffer();
		const attachment = await emotePreview(image);
		if (!attachment) throw new Error(`Failed to generate preview from ${imageLink}`);
		return attachment;
	} catch (error) {
		throw new Error(`Failed to load image from ${imageLink}`);
	}
};

const commandPreview: DiscordCommandFunction = async (cCall) => {
	const attachment = await preview(cCall.args[0], cCall.canReplyAttachment);
	cCall.replyWithAddition("Preview", attachment);
};

const slashCommandPreview: SlashCommandFunction = async (interaction) => {
	const imageLink = interaction.options.getString("link", true);
	const channel = interaction.channel;
	const canAttach =
		channel instanceof TextChannel || channel instanceof ThreadChannel
			? canSendAttachment(channel)
			: false;
	const attachment = await preview(imageLink, canAttach);
	return {
		content: `Preview for <${imageLink}>`,
		files: [attachment],
	};
};

addCommand({
	name: "emotepreview",
	run: commandPreview,
	category,
	usage: "<image link>",
	description:
		"Generates an image that shows approximately what the linked image will look like as a Discord emote.",
	slash: {
		run: slashCommandPreview,
		options: [
			{
				name: "link",
				type: ApplicationCommandOptionType.String,
				description: "The link to the emote image",
				required: true,
			},
		],
	},
	discordOnly: true,
});

/** Gets the top 20 emotes by score, or the next 20, or the next etc. Pages start at 1. */
const topEmotes = (guild: Guild, page = 1): string => {
	if (isNaN(page) || page < 1) throw new CommandError(`That is not a valid page number`);
	const offset = (page - 1) * 20;
	const emoteResults = queryTopEmotes(guild.id, offset);
	if (emoteResults.length === 0)
		throw new CommandError(
			`There are not enough emotes in the database to have a page ${page}`,
		);

	/** A map of emotes with names as keys and IDs as values */
	const emoteMap = makeEmoteMap(guild);
	const emotes = emoteResults.map((emote) => emotify(emote.emote, emoteMap));
	const singularOpening = offset === 0 ? "The only emote is " : `Emote #${offset + 1} is `;
	const pluralOpening = `Emotes #${offset + 1}-#${offset + emoteResults.length} are `;
	return formatList(emotes, { opening: [singularOpening, pluralOpening] });
};

const commandTopEmotes: DiscordCommandFunction = (cCall) => {
	const page = cCall.args[0] ? parseInt(cCall.args[0]) : undefined;
	cCall.reply(topEmotes(cCall.message.guild, page));
};

const slashTopEmotes: SlashCommandFunction = (interaction) => {
	const guild = getGuild(interaction);
	const page = interaction.options.getInteger("page") ?? undefined;
	return topEmotes(guild, page);
};

addCommand({
	name: "topemotes",
	run: commandTopEmotes,
	category,
	usage: "[page]",
	description: "Gets the top 20 emotes by score, or a lower 20, if page is specified.",
	heat: { fixed: 2 },
	slash: {
		run: slashTopEmotes,
		options: [
			{
				name: "page",
				type: ApplicationCommandOptionType.Integer,
				description: "Which page of 20 emotes to get (default 1)",
			},
		],
	},
	discordOnly: true,
});

/** Get a user's emote votes for one guild and DM them to the user */
const userVotes = async (guild: Guild, user: User): Promise<string> => {
	const voteResults = queryUserVotes(guild.id, user.id);
	const upvoted = [];
	const downvoted = [];
	if (voteResults.length === 0) return "User has no votes.";
	for (const { up, emote } of voteResults) {
		if (up === 1) {
			upvoted.push(emote);
		} else {
			downvoted.push(emote);
		}
	}
	/** A map of emotes with names as keys and IDs as values */
	const emoteMap = makeEmoteMap(guild);

	const stringBits = [];
	if (isNonEmptyArray(upvoted)) {
		stringBits.push(`👍 ${emotify(upvoted[0], emoteMap)} `);
		stringBits.push(...upvoted.slice(1).map((emote) => `${emotify(emote, emoteMap)} `));
	}
	if (isNonEmptyArray(downvoted)) {
		stringBits.push(`${upvoted.length > 0 ? `\n` : ``}👎 ${emotify(downvoted[0], emoteMap)} `);
		stringBits.push(...downvoted.slice(1).map((emote) => `${emotify(emote, emoteMap)} `));
	}

	const messages: [string, ...string[]] = [""];
	for (const stringBit of stringBits) {
		if (messages[messages.length - 1]!.length + stringBit.length > 2000)
			messages.push(stringBit);
		else messages[messages.length - 1] += stringBit;
	}
	await sendDms(user, messages);
	const dms = messages.length > 1 ? `${messages.length} DMs` : "a DM";
	const votes = voteResults.length > 1 ? "votes" : "vote";
	return `Sent ${dms} with user's ${voteResults.length} ${votes}.`;
};

const sendDms = async (user: User, messages: string[]) => {
	try {
		for (const message of messages) {
			await user.send(message);
		}
	} catch (error) {
		log(`Could not send DM:`);
		log(error);
		throw new CommandError(`Could not send user DMs`);
	}
};

const commandUserVotes: DiscordCommandFunction = async (cCall) => {
	cCall.reply(await userVotes(cCall.message.guild, cCall.message.author));
};

const slashUserVotes: SlashCommandFunction = async (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const message = await userVotes(guild, member.user);
	return {
		content: message,
		ephemeral: true,
	};
};

addCommand({
	name: "myvotes",
	run: commandUserVotes,
	category,
	description: "Sends the user a DM with all the user's current votes.",
	heat: { fixed: 2 },
	slash: { run: slashUserVotes },
	discordOnly: true,
});
