import { addCommand, DiscordCommandFunction, SlashCommandFunction } from "../commands.js";
import {
	adminRoles,
	parseEmote,
	emotify,
	votingRoles,
	parseEmoteWithId,
	category,
	getGuildAndMember,
	clearCachedImage,
	renameCachedImage,
	validateVariantName,
} from "./common.js";
import {
	queryAddEmote,
	queryAddVote,
	queryDeleteEmote,
	queryEmote,
	queryRemoveVariant,
	queryRenameEmote,
	querySetImage,
	querySetVariant,
} from "./queries.js";
import { ApplicationCommandOptionType, Guild, GuildMember } from "discord.js";
import { CommandError } from "../Errors.js";
import { hasWhitelistedImageDomain } from "./hasWhitelistedImageDomain.js";

/** A user attempts to set a new image for an emote */
const setImage = (
	guild: Guild,
	emoteArg: string | undefined,
	imageArg: string | undefined,
	member: GuildMember,
): string => {
	const adminRole = adminRoles.get(guild.id);
	if (!adminRole || !member.roles.cache.has(adminRole))
		throw new CommandError(`Only emote admins can set images on existing emote proposals`);
	if (!emoteArg || !imageArg) throw new CommandError();
	const emote = parseEmote(emoteArg);
	const emoteResult = queryEmote(guild.id, emote);
	if (!emoteResult) throw new CommandError(`There is no emote named ${emote} in the database`);
	querySetImage(guild.id, emote, imageArg);
	clearCachedImage(guild.id, emote);
	return `Set the image for ${emotify(emoteResult.emote, guild.emojis.cache)}`;
};

const commandSetImage: DiscordCommandFunction = (cCall) => {
	cCall.reply(setImage(cCall.message.guild, cCall.args[0], cCall.args[1], cCall.message.member));
};

const slashSetImage: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emoteArg = interaction.options.getString("emote", true);
	const imageArg = interaction.options.getString("image", true);
	return setImage(guild, emoteArg, imageArg, member);
};

addCommand({
	name: "setimage",
	run: commandSetImage,
	category,
	usage: "<emote> <image link>",
	description: "Sets the chosen emote proposal's image to the chosen one. (emote admin only)",
	slash: {
		run: slashSetImage,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "The emote to change the image of",
				required: true,
			},
			{
				name: "image",
				type: ApplicationCommandOptionType.String,
				description: "The link for the emote's new image",
				required: true,
			},
		],
	},
	discordOnly: true,
});

/** A user attempts to add an emote to the database */
const addEmote = (
	guild: Guild,
	emoteArg: string | undefined,
	imageArg: string | undefined,
	member: GuildMember,
): string => {
	const votingRole = votingRoles.get(guild.id);
	if (!votingRole || !member.roles.cache.has(votingRole))
		throw new CommandError(`Only users with 'known' role can propose emotes`);
	if (!emoteArg) throw new CommandError();
	const { emote, id } = parseEmoteWithId(emoteArg);
	if (/\W/.test(emote)) throw new CommandError(`"${emote}" is not a valid emote name`);
	const existingEmote = queryEmote(guild.id, emote);
	if (existingEmote)
		throw new CommandError(
			`An emote named ${existingEmote.emote} already exists in the database`,
		);
	const image = imageArg ?? (id ? `https://cdn.discordapp.com/emojis/${id}.png` : undefined);
	if (!image)
		throw new CommandError(
			`When the emote argument is not passed in emote form, an image link must be supplied`,
		);
	if (!hasWhitelistedImageDomain(image))
		throw new CommandError("The image link is not on a whitelist domain");

	queryAddEmote(guild.id, emote, image);
	queryAddVote(guild.id, emote, member.id, 1);
	return `${emotify(emote, guild.emojis.cache)} has been added to the database.`;
};

const commandAddEmote: DiscordCommandFunction = (cCall) => {
	cCall.reply(addEmote(cCall.message.guild, cCall.args[0], cCall.args[1], cCall.message.member));
};

const slashAddEmote: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emoteArg = interaction.options.getString("emote", true);
	const imageArg = interaction.options.getString("image") ?? undefined;
	return addEmote(guild, emoteArg, imageArg, member);
};

addCommand({
	name: "addemote",
	run: commandAddEmote,
	category,
	usage: "<emote> <[image]>",
	description: "Adds a given emote to the database as a proposal.",
	slash: {
		run: slashAddEmote,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "An emote, or the name of an emote",
				required: true,
			},
			{
				name: "image",
				type: ApplicationCommandOptionType.String,
				description:
					"The link for the new emote's image. Required if the emote argument is just a name.",
			},
		],
	},
	discordOnly: true,
});

/** A user attempts to delete an emote from the database */
const deleteEmote = (guild: Guild, emoteArg: string | undefined, member: GuildMember) => {
	const adminRole = adminRoles.get(guild.id);
	if (!adminRole || !member.roles.cache.has(adminRole))
		throw new CommandError(`Only emote moderators can delete emote proposals`);
	if (!emoteArg) throw new CommandError();
	const emote = parseEmote(emoteArg);
	const emoteResult = queryEmote(guild.id, emote);
	if (!emoteResult) throw new CommandError(`There is no emote named ${emote} in the database`);

	queryDeleteEmote(guild.id, emote);
	clearCachedImage(guild.id, emote);
	return `${member.user.toString()} deleted ${emotify(
		emoteResult.emote,
		guild.emojis.cache,
	)} from the database. ${emoteResult.upvotes}👍 ${emoteResult.downvotes}👎 ${emoteResult.image}`;
};

const commandDeleteEmote: DiscordCommandFunction = (cCall) => {
	cCall.reply(deleteEmote(cCall.message.guild, cCall.args[0], cCall.message.member));
};

const slashDeleteEmote: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emoteArg = interaction.options.getString("emote", true);
	return deleteEmote(guild, emoteArg, member);
};

addCommand({
	name: "deleteemote",
	run: commandDeleteEmote,
	category,
	usage: "<emote>",
	description: "Deletes the chosen emote from the database. (emote admin only)",
	slash: {
		run: slashDeleteEmote,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "The emote to delete from the database",
				required: true,
			},
		],
	},
	discordOnly: true,
});

/** A user attempts to change an emote's name */
const renameEmote = (
	guild: Guild,
	emoteArg: string | undefined,
	newNameArg: string | undefined,
	member: GuildMember,
): string => {
	const adminRole = adminRoles.get(guild.id);
	if (!adminRole || !member.roles.cache.has(adminRole))
		throw new CommandError(`Only emote moderators can rename emote proposals`);
	if (!emoteArg || !newNameArg) throw new CommandError();
	const emote = parseEmote(emoteArg);
	const emoteResult = queryEmote(guild.id, emote);
	if (!emoteResult) throw new CommandError(`There is no emote named ${emote} in the database`);
	const newName = parseEmote(newNameArg);
	if (/\W/.test(newName)) throw new CommandError(`"${emote}" is not a valid emote name`);
	const oldName = emoteResult.emote;
	if (oldName === newName)
		throw new CommandError(
			`${emotify(emoteResult.emote, guild.emojis.cache)} already has that name`,
		);

	if (oldName.toLowerCase() === newName.toLowerCase()) {
		queryRenameEmote(guild.id, oldName, newName);
		return `${member.user.toString()} changed capitalization on ${emotify(
			emoteResult.emote,
			guild.emojis.cache,
		)} to ${newName}.`;
	}

	const newNameResult = queryEmote(guild.id, newName);
	if (newNameResult)
		throw new CommandError(
			`There is already an emote by the name ${newNameResult.emote} in the database`,
		);
	renameCachedImage(guild.id, oldName, newName);
	queryRenameEmote(guild.id, oldName, newName);
	return `${member.user.toString()} renamed ${emotify(
		emoteResult.emote,
		guild.emojis.cache,
	)} to ${newName}.`;
};

const commandRenameEmote: DiscordCommandFunction = (cCall) => {
	cCall.reply(
		renameEmote(cCall.message.guild, cCall.args[0], cCall.args[1], cCall.message.member),
	);
};

const slashRenameEmote: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emoteArg = interaction.options.getString("emote", true);
	const newNameArg = interaction.options.getString("newname", true);
	return renameEmote(guild, emoteArg, newNameArg, member);
};

addCommand({
	name: "renameemote",
	run: commandRenameEmote,
	category,
	usage: "<emote> <new name>",
	description: "Renames the chosen emote proposal to the chosen name. (emote admin only)",
	slash: {
		run: slashRenameEmote,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "The emote to rename in the database",
				required: true,
			},
			{
				name: "newname",
				type: ApplicationCommandOptionType.String,
				description: "The new name for the emote",
				required: true,
			},
		],
	},
	discordOnly: true,
});

/** A user attempts to set (create or override) an emote variant image */
const setVariant = (
	guild: Guild,
	emoteArg: string | undefined,
	variantArg: string | undefined,
	imageArg: string | undefined,
	member: GuildMember,
): string => {
	const adminRole = adminRoles.get(guild.id);
	if (!adminRole || !member.roles.cache.has(adminRole))
		throw new CommandError(`Only emote admins can set emote variants`);
	if (!emoteArg || !variantArg || !imageArg) throw new CommandError();
	let variant = variantArg.toLowerCase();
	validateVariantName(variant);
	const emote = parseEmote(emoteArg);
	const emoteResult = queryEmote(guild.id, emote);
	if (!emoteResult) throw new CommandError(`There is no emote named ${emote} in the database`);
	querySetVariant(guild.id, emote, variant, imageArg);
	clearCachedImage(guild.id, emote, variant);
	return `Set the ${variant} image for ${emotify(emoteResult.emote, guild.emojis.cache)}`;
};

const commandSetVariant: DiscordCommandFunction = (cCall) => {
	cCall.reply(
		setVariant(
			cCall.message.guild,
			cCall.args[0],
			cCall.args[1],
			cCall.args[2],
			cCall.message.member,
		),
	);
};

const slashSetVariant: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emoteArg = interaction.options.getString("emote", true);
	const variantArg = interaction.options.getString("variant", true);
	const imageArg = interaction.options.getString("image", true);
	return setVariant(guild, emoteArg, variantArg, imageArg, member);
};

addCommand({
	name: "setvariant",
	run: commandSetVariant,
	category,
	usage: "<emote> <variant> <image link>",
	description:
		"Sets the specified variant of the chosen emote to the specified image. (emote admin only)",
	slash: {
		run: slashSetVariant,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "The emote to change the variant image of",
				required: true,
			},
			{
				name: "variant",
				type: ApplicationCommandOptionType.String,
				description: "The variant of the emote to change",
				required: true,
			},
			{
				name: "image",
				type: ApplicationCommandOptionType.String,
				description: "The link for the emote's new image",
				required: true,
			},
		],
	},
	discordOnly: true,
});

/** A user attempts to remove an emote variant image */
const removeVariant = (
	guild: Guild,
	emoteArg: string | undefined,
	variantArg: string | undefined,
	member: GuildMember,
): string => {
	const adminRole = adminRoles.get(guild.id);
	if (!adminRole || !member.roles.cache.has(adminRole))
		throw new CommandError(`Only emote admins can remove emote variants`);
	if (!emoteArg || !variantArg) throw new CommandError();
	let variant = variantArg.toLowerCase();
	validateVariantName(variant);
	const emote = parseEmote(emoteArg);
	const emoteResult = queryEmote(guild.id, emote);
	if (!emoteResult) throw new CommandError(`There is no emote named ${emote} in the database`);
	queryRemoveVariant(guild.id, emote, variant);
	clearCachedImage(guild.id, emote, variant);
	return `Removed the ${variant} image for ${emotify(emoteResult.emote, guild.emojis.cache)}`;
};

const commandRemoveVariant: DiscordCommandFunction = (cCall) => {
	cCall.reply(
		removeVariant(cCall.message.guild, cCall.args[0], cCall.args[1], cCall.message.member),
	);
};

const slashRemoveVariant: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emoteArg = interaction.options.getString("emote", true);
	const variantArg = interaction.options.getString("variant", true);
	return removeVariant(guild, emoteArg, variantArg, member);
};

addCommand({
	name: "removevariant",
	run: commandRemoveVariant,
	category,
	usage: "<emote> <variant>",
	description: "Removes the specified variant of the chosen emote. (emote admin only)",
	slash: {
		run: slashRemoveVariant,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "The emote to remove the variant image of",
				required: true,
			},
			{
				name: "variant",
				type: ApplicationCommandOptionType.String,
				description: "The variant of the emote to remove",
				required: true,
			},
		],
	},
	discordOnly: true,
});
