import { TextChannel, Message, ChannelType, GuildPremiumTier } from "discord.js";
import { addCommand, DiscordCommandFunction } from "../commands.js";
import { db } from "../db.js";
import discordClient from "../discordclient.js";
import { log } from "../log.js";
import {
	rankChannels,
	adminRoles,
	makeEmoteMap,
	emotify,
	rankMessageIds,
	category,
} from "./common.js";
import { queryAllEmotes } from "./queries.js";

/**
 * Makes or updates the messages showing the full list of emotes by score, in each guild set
 */
export const updateAllRankPosts = (): void => {
	log(
		`Updating rank posts for ${rankChannels.size} Discord guild${
			rankChannels.size === 1 ? "" : "s"
		}`,
	);
	for (const [guild, channel] of rankChannels) {
		log(`Updating rank channel ${channel} for guild ${guild}`);
		void makeRankPostsInternal(guild);
	}
};

/**
 * Sets the channel where the full list of emotes should be posted
 */
const setRankChannel: DiscordCommandFunction = (cCall) => {
	// Set the rank channel
	const qSetRankChannel = `
		UPDATE discord_config
		SET emote_rank_channel = $channel
		WHERE guild = $guild
	`;

	const guild = cCall.message.guild;
	const adminRole = adminRoles.get(guild.id);
	if (!adminRole || !cCall.message.member.roles.cache.has(adminRole)) {
		cCall.reply(`Only emote admins can set the rank channel`);
		return;
	}
	const parameter = cCall.args[0] ?? "";
	const match = /$<#(\d*)>/.exec(parameter) as null | [string, string]; // Channel was supplied as a mention
	const targetChannelId = match ? match[1] : parameter;
	if (!parameter) {
		cCall.replyUsage();
		return;
	}
	const targetChannel = cCall.message.guild.channels.cache.get(targetChannelId);
	if (!targetChannel) {
		cCall.reply(`There is no channel by that ID in this guild`);
		return;
	}
	if (targetChannel.type !== ChannelType.GuildText) {
		cCall.reply(`That channel is not a text channel`);
		return;
	}

	db.prepare(qSetRankChannel).run({
		channel: targetChannelId,
		guild: guild.id,
	});
	rankChannels.set(guild.id, targetChannelId);
	cCall.reply(`<#${targetChannelId}> set as channel to post emote rank overview messages in`);

	// to-do: delete the old rank posts and make new ones right away
};

const makeRankPostsInternal = async (guildId: string) => {
	const guild = await discordClient.guilds.fetch(guildId);

	/** A map of emotes with names as keys and IDs as values */
	const emoteMap: Map<string, string> = makeEmoteMap(guild);

	/** Array of strings that will be sent as individual Discord messages */
	const messages: [string, ...string[]] = [""];
	/** Array of strings that should not be broken up into different Discord messages */
	const scoreMessages = [
		`**All emotes in the database, by score.** Emotes that show up as text are either proposals or former emotes, ` +
			`which also still function as proposals. Use !emoteinfo <emote> to see the image for a proposed emote. If a proposed emote ` +
			`climbs the ranks, it will still need to be manually added, and the low score emotes manually removed.`,
	];
	/** Score of previous emote */
	let lastScore = 0;
	/** Whether the emote limit line has been inserted yet */
	let havePutEmoteLimitLine = false;
	/** How many non-animated emotes the guild can actually have */
	const emoteMax =
		guild.premiumTier === GuildPremiumTier.Tier3
			? 200
			: guild.premiumTier === GuildPremiumTier.Tier2
			? 150
			: guild.premiumTier === GuildPremiumTier.Tier1
			? 100
			: 50;

	const emoteResults = queryAllEmotes(guildId);
	let emoteCount = 0;
	for (const emoteResult of emoteResults) {
		if (emoteResult.score !== lastScore) {
			// If the score of the current one is not the same as the score of the next one
			if (emoteCount >= emoteMax && !havePutEmoteLimitLine) {
				// If past the maximum number of emotes for the guild for the first time
				havePutEmoteLimitLine = true;
				scoreMessages.push(`\n--- ${emoteCount} emotes above this line ---`);
			}
			lastScore = emoteResult.score;
			scoreMessages.push(`\n${emoteResult.score}: `); // start the next line
		}
		scoreMessages[scoreMessages.length - 1] += `${emotify(emoteResult.emote, emoteMap)} `; // add the emote to the current line
		emoteCount++;
	}

	const now = new Date();
	now.setUTCMinutes(now.getUTCMinutes() - now.getTimezoneOffset());
	const hours = now.getUTCHours().toString(10);
	const minutes = now.getUTCMinutes().toString(10);
	// "Last updated" time string
	scoreMessages.push(`\nLast updated: ${hours.padStart(2, "0")}:${minutes.padStart(2, "0")} UTC`);

	for (const scoreMessage of scoreMessages) {
		if (messages[messages.length - 1]!.length + scoreMessage.length > 2000) {
			if (scoreMessage.length > 2000)
				// If one chunk is ever over the message limit (shouldn't happen)
				continue; // skip that one
			messages.push(scoreMessage); // Add a new message
		} else {
			messages[messages.length - 1] += scoreMessage; // Prepend to previous message
		}
	}

	try {
		const rankChannelId = rankChannels.get(guildId);
		if (!rankChannelId) throw new Error();
		const channel = await discordClient.channels.fetch(rankChannelId);
		if (!(channel instanceof TextChannel)) throw new Error();

		const guildRankMessageIds = rankMessageIds.get(guildId) || [];
		const findPromises: Promise<Message>[] = [];
		for (const messageId of guildRankMessageIds) {
			findPromises.push(channel.messages.fetch(messageId));
		}

		const isFulfilledPromise = <T>(
			promise: PromiseSettledResult<T>,
		): promise is PromiseFulfilledResult<T> => {
			return promise.status === "fulfilled";
		};

		const foundMessages = (await Promise.allSettled(findPromises))
			.filter(isFulfilledPromise) // Ignore messages not found
			.map((outcome) => outcome.value) // Make an array of just the messages
			.filter((message) => message.channel === channel) // Only consider messages still in the target channel
			.sort((a, b) => a.createdTimestamp - b.createdTimestamp); // Sort oldest first

		const to_be_processed = Math.max(foundMessages.length, messages.length);

		const sentMessages: Message[] = [];
		// Edit/send all messages
		for (let i = 0; i < to_be_processed; i++) {
			const foundMessage = foundMessages[i];
			const message = messages[i];
			// Found a message and need to send a message
			if (foundMessage !== undefined && message !== undefined) {
				sentMessages.push(await foundMessage.edit(message));
				// No more messages but still need to send a message
			} else if (message !== undefined) {
				sentMessages.push(await channel.send(message));
				// No messages to send but found more messages
			} else if (foundMessage !== undefined) {
				await foundMessage.delete();
			}
		}

		rankMessageIds.set(
			guildId,
			sentMessages.map((sentMessage) => sentMessage.id),
		);
		updateRankMessages(guildId);
		console.log(rankMessageIds.get(guildId));
	} catch (error) {
		if (error instanceof Error) console.log(error.stack);
	}
};

/**
 * Makes or updates the messages showing the full list of emotes by score
 */
const makeRankPosts: DiscordCommandFunction = (cCall) => {
	const guild = cCall.message.guild;
	const adminrole = adminRoles.get(guild.id);
	if (!adminrole || !cCall.message.member.roles.cache.has(adminrole)) {
		cCall.reply(`Only emote admins can trigger posting the rank posts`);
		return;
	}
	if (!rankChannels.has(guild.id)) {
		cCall.reply(`Error: no rank post channel is set`);
		return;
	}
	void makeRankPostsInternal(guild.id);
};

const updateRankMessages = (guild: string) => {
	// Clear any previously-existing rank messages
	const qClearOldRankMessages = `
		DELETE FROM emote_rank_messages
		WHERE guild = $guild
	`;
	// Save the IDs of the rank messages
	const qAddRankMessages = `
		INSERT INTO emote_rank_messages(guild, message_number, message_id)
		VALUES ($guild, $messageNumber, $messageId)
	`;

	db.prepare(qClearOldRankMessages).run({ guild });
	const qpAddRankMessages = db.prepare(qAddRankMessages);
	const guildRankMessageIds = rankMessageIds.get(guild) || [];
	for (let i = 0; i < guildRankMessageIds.length; i++) {
		qpAddRankMessages.run({
			guild,
			messageNumber: i,
			messageId: guildRankMessageIds[i],
		});
	}
};

addCommand({
	name: "emoterankchannel",
	run: setRankChannel,
	category,
	usage: "<channel>",
	description:
		"Sets the given channel as the channel to post emote rank messages in (emote admin only).",
	discordOnly: true,
});
addCommand({
	name: "emoterankposts",
	run: makeRankPosts,
	category,
	usage: "",
	description:
		"Makes the posts with the full emote proposal list in the set channel (emote admin only).",
	discordOnly: true,
});
