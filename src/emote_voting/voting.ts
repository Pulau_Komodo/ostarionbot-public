import { log } from "console";
import {
	Guild,
	GuildMember,
	Message,
	ActionRowBuilder,
	AttachmentBuilder,
	ButtonBuilder,
	MessageComponentInteraction,
	TextChannel,
	ThreadChannel,
	ApplicationCommandOptionType,
	ButtonStyle,
	ComponentType,
} from "discord.js";
import { DiscordCommandFunction, addCommand, SlashCommandFunction } from "../commands.js";
import { db } from "../db.js";
import { CommandError } from "../Errors.js";
import { regexTwemoji } from "../misc/emoji_regex_tests.js";
import { canSendAttachment } from "../permission checks.js";
import { sleep } from "../promise_util.js";
import { isNonEmptyArray } from "../type_guards.js";
import {
	votingRoles,
	makeEmoteMap,
	emotify,
	parseEmote,
	downvotePower,
	category,
	getGuildAndMember,
	getImage,
} from "./common.js";
import { emotePreview } from "./emotePreview.js";
import { hasWhitelistedImageDomain } from "./hasWhitelistedImageDomain.js";
import {
	queryEmote,
	queryVote,
	queryAddVote,
	queryChangeVote,
	queryCountVotes,
	queryMultipleEmotes,
	queryMultipleVotes,
	queryAddMultipleVotes,
	queryChangeMultipleVotes,
	queryRemoveMultipleVotes,
	queryCountAndUpdateMultipleTallies,
} from "./queries.js";

/**
 * A user votes (or removes vote) on an emote or multiple emotes
 *
 * vote is -1 for downvote, 0 for clear vote, and 1 for vote
 */
const vote = (guild: Guild, member: GuildMember, emotesArg: string, vote: -1 | 0 | 1): string => {
	const votingRole = votingRoles.get(guild.id);
	if (!votingRole || !member.roles.cache.has(votingRole))
		throw new CommandError(`Only users with 'known' role can vote`);
	if (!emotesArg) throw new CommandError();

	/** This should match all <:name:id> emotes, all :emotes:, and all emotes written as plain words not attached to anything else */
	const emoteMatches = emotesArg.matchAll(
		/<:(\w+):\d+>|:(\w+):|(?:^| )(\w+)(?=(?:$| ))/g,
	) as unknown as IterableIterator<
		[string, string] | [string, undefined, string] | [string, undefined, undefined, string]
	>;
	/** A map of strings presumed emotes provided by the user */
	const inputEmotes = new Map<string, string>();
	for (const match of emoteMatches) {
		for (const group of match.slice(1)) {
			if (group !== undefined) {
				const emoteKey = group.toLowerCase();
				if (!inputEmotes.has(emoteKey)) inputEmotes.set(emoteKey, group);
				break;
			}
		}
	}
	if (inputEmotes.size === 0) {
		const emojis = [...emotesArg.matchAll(regexTwemoji)] as [string][];
		if (isNonEmptyArray(emojis)) {
			if (emojis.length === 1)
				throw new CommandError(`${emojis[0][0]} is not a custom emote`);
			throw new CommandError(`Those are not custom emotes`);
		}
		throw new CommandError(`Nothing looked like custom emotes or custom emote names`);
	}
	const emoteResults = queryMultipleEmotes(guild.id, [...inputEmotes.values()]);
	const emotesNonExistent: string[] = [];
	const emotesExistent: string[] = [];
	for (const { emote, existent } of emoteResults) {
		if (existent) {
			emotesExistent.push(emote);
		} else {
			emotesNonExistent.push(emote);
		}
	}
	const emotesPreviouslyUp: string[] = [];
	const emotesPreviouslyDown: string[] = [];
	const emotesPreviouslyUnvoted: string[] = [];
	const emoteVoteResults = queryMultipleVotes(guild.id, member.id, emotesExistent);
	for (const result of emoteVoteResults) {
		if (result.vote === 1) {
			emotesPreviouslyUp.push(result.emote);
		} else if (result.vote === -1) {
			emotesPreviouslyDown.push(result.emote);
		} else {
			emotesPreviouslyUnvoted.push(result.emote);
		}
	}

	if (vote === 1) {
		queryAddMultipleVotes(guild.id, member.id, emotesPreviouslyUnvoted, 1);
		queryChangeMultipleVotes(guild.id, member.id, emotesPreviouslyDown, 1);
		queryCountAndUpdateMultipleTallies(
			guild.id,
			emotesPreviouslyUnvoted.concat(emotesPreviouslyDown),
		);
	} else if (vote === -1) {
		queryAddMultipleVotes(guild.id, member.id, emotesPreviouslyUnvoted, 0);
		queryChangeMultipleVotes(guild.id, member.id, emotesPreviouslyUp, 0);
		queryCountAndUpdateMultipleTallies(
			guild.id,
			emotesPreviouslyUnvoted.concat(emotesPreviouslyUp),
		);
	} else {
		const allVotes = emotesPreviouslyUp.concat(emotesPreviouslyDown);
		queryRemoveMultipleVotes(guild.id, member.id, allVotes);
		queryCountAndUpdateMultipleTallies(guild.id, allVotes);
	}

	/** A map of emotes with names as keys and IDs as values */
	const emoteMap: Map<string, string> = makeEmoteMap(guild);
	return makeVoteOutput(
		vote,
		emotesNonExistent,
		emotesPreviouslyUnvoted,
		emotesPreviouslyUp,
		emotesPreviouslyDown,
		emoteMap,
	);
};

const commandUpvote: DiscordCommandFunction = (cCall) => {
	cCall.reply(vote(cCall.message.guild, cCall.message.member, cCall.argstring, 1));
};
const commandDownvote: DiscordCommandFunction = (cCall) => {
	cCall.reply(vote(cCall.message.guild, cCall.message.member, cCall.argstring, -1));
};
const commandClearvote: DiscordCommandFunction = (cCall) => {
	cCall.reply(vote(cCall.message.guild, cCall.message.member, cCall.argstring, 0));
};
const slashUpvote: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emotesArg = interaction.options.getString("emotes", true);
	return vote(guild, member, emotesArg, 1);
};
const slashDownvote: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emotesArg = interaction.options.getString("emotes", true);
	return vote(guild, member, emotesArg, -1);
};
const slashClearvote: SlashCommandFunction = (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const emotesArg = interaction.options.getString("emotes", true);
	return vote(guild, member, emotesArg, 0);
};

addCommand({
	name: "upvote",
	run: commandUpvote,
	category,
	usage: "<emote> [emote2] [emote3] [etc]",
	description: "Upvotes the chosen emotes.",
	slash: {
		run: slashUpvote,
		options: [
			{
				name: "emotes",
				type: ApplicationCommandOptionType.String,
				description: "The emote or emotes to upvote",
				required: true,
			},
		],
	},
	discordOnly: true,
});
addCommand({
	name: "downvote",
	run: commandDownvote,
	category,
	usage: "<emote> [emote2] [emote3] [etc]",
	description: "Downvotes the chosen emotes.",
	slash: {
		run: slashDownvote,
		options: [
			{
				name: "emotes",
				type: ApplicationCommandOptionType.String,
				description: "The emote or emotes to downvote",
				required: true,
			},
		],
	},
	discordOnly: true,
});
addCommand({
	name: "clearvote",
	run: commandClearvote,
	category,
	usage: "<emote> [emote2] [emote3] [etc]",
	description: "Clears the vote on the chosen emotes.",
	slash: {
		run: slashClearvote,
		options: [
			{
				name: "emotes",
				type: ApplicationCommandOptionType.String,
				description: "The emote or emotes to clear the vote from",
				required: true,
			},
		],
	},
	discordOnly: true,
});

/**
 * Processes the arrays generated by Vote() into an output text message
 * @param vote -1 if user downvoted, 0 if user cleared and 1 if user upvoted
 * @param nonExistent All names that didn't exist as emotes
 * @param prevNon Names of emotes processed that previously had no vote by the user
 * @param prevUp Names of emotes processed that previously had an upvote by the user
 * @param prevDown Names of emotes processed that previously had a downvote by the user
 * @param emoteMap A Collection of emotes with names as keys and IDs as values
 */
const makeVoteOutput = (
	vote: -1 | 0 | 1,
	nonExistent: string[],
	prevNon: string[],
	prevUp: string[],
	prevDown: string[],
	emoteMap: Map<string, string>,
): string => {
	/** The text that will be output via callback */
	let outputText = ``;

	let categories = 0;
	if (nonExistent.length > 0) categories++;
	if (prevNon.length > 0) categories++;
	if (prevUp.length > 0) categories++;
	if (prevDown.length > 0) categories++;
	const addCategoryToOutput = (array: string[], prefix: string) => {
		const firstItem = array[0];
		if (firstItem !== undefined) {
			outputText += `${prefix}${emotify(firstItem, emoteMap)}`;
			for (const emote of array.slice(1)) outputText += ` ${emotify(emote, emoteMap)}`;
			if (categories > 1) {
				outputText += `, `;
				categories--;
			}
		}
	};

	// Multiple emotes
	if (nonExistent.length + prevNon.length + prevUp.length + prevDown.length > 1) {
		if (vote === 1) {
			addCategoryToOutput(prevNon, "👍");
			addCategoryToOutput(prevDown, "👎➡👍");
			addCategoryToOutput(prevUp, "🤔");
		} else if (vote === -1) {
			addCategoryToOutput(prevNon, "👎");
			addCategoryToOutput(prevUp, "👍➡👎");
			addCategoryToOutput(prevDown, "🤔");
		} else {
			addCategoryToOutput(prevUp, "♻👍");
			addCategoryToOutput(prevDown, "♻👎");
			addCategoryToOutput(prevNon, "🤔");
		}
		addCategoryToOutput(nonExistent, "🚫");
		// Single emote
	} else {
		if (vote === 1) {
			if (isNonEmptyArray(prevUp))
				outputText = `User had already upvoted ${emotify(prevUp[0], emoteMap)}`;
			else if (isNonEmptyArray(prevDown))
				outputText = `Changed downvote to upvote on ${emotify(prevDown[0], emoteMap)}`;
			else if (isNonEmptyArray(prevNon))
				outputText = `Upvoted ${emotify(prevNon[0], emoteMap)}`;
		} else if (vote === -1) {
			if (isNonEmptyArray(prevUp))
				outputText = `Changed upvote to downvote ${emotify(prevUp[0], emoteMap)}`;
			else if (isNonEmptyArray(prevDown))
				outputText = `User had already downvoted ${emotify(prevDown[0], emoteMap)}`;
			else if (isNonEmptyArray(prevNon))
				outputText = `Downvoted ${emotify(prevNon[0], emoteMap)}`;
		} else {
			if (isNonEmptyArray(prevUp))
				outputText = `Cleared upvote on ${emotify(prevUp[0], emoteMap)}`;
			else if (isNonEmptyArray(prevDown))
				outputText = `Cleared downvote on ${emotify(prevDown[0], emoteMap)}`;
			else if (isNonEmptyArray(prevNon))
				outputText = `User had no vote on ${emotify(prevNon[0], emoteMap)}`;
		}
		if (isNonEmptyArray(nonExistent)) {
			outputText = `There is no emote named ${nonExistent[0]} in the database`;
		}
	}
	return outputText;
};

const makeVoteButtonRow = (
	upvotes: number,
	downvotes: number,
	clearvotes: number,
	disabled = false,
): ActionRowBuilder<ButtonBuilder> => {
	return new ActionRowBuilder<ButtonBuilder>().addComponents(
		new ButtonBuilder()
			.setCustomId("👍")
			.setLabel(`${upvotes}👍`)
			.setStyle(ButtonStyle.Success)
			.setDisabled(disabled),
		new ButtonBuilder()
			.setCustomId("👎")
			.setLabel(`${downvotes}👎`)
			.setStyle(ButtonStyle.Danger)
			.setDisabled(disabled),
		new ButtonBuilder()
			.setCustomId("🤷")
			.setLabel(`${clearvotes}🤷`)
			.setStyle(ButtonStyle.Secondary)
			.setDisabled(disabled),
	);
};

const onCooldownMessages = new Set<string>();
const queuedUpdateMessages = new Set<string>();

const updateCallVoteMessage = async (
	message: Message,
	upvoters: Set<string>,
	downvoters: Set<string>,
	clearvoters: Set<string>,
) => {
	if (onCooldownMessages.has(message.id)) {
		queuedUpdateMessages.add(message.id);
		return;
	}
	onCooldownMessages.add(message.id);
	do {
		await message.edit({
			components: [makeVoteButtonRow(upvoters.size, downvoters.size, clearvoters.size)],
		});
		queuedUpdateMessages.delete(message.id);
		await sleep(5 * 1000);
	} while (queuedUpdateMessages.has(message.id));
	onCooldownMessages.delete(message.id);
};

const activeVotes = new Set<string>();
const votingTime = 5 * 60 * 1000; // 5 minutes

const callVote = async (
	guild: Guild,
	channel: DiscordGuildTextChannel,
	member: GuildMember,
	emoteArg: string | undefined,
	canAttach: boolean,
): Promise<void> => {
	if (!emoteArg) throw new CommandError();
	const emote = parseEmote(emoteArg);
	const votingRole = votingRoles.get(guild.id);
	if (!votingRole || !member.roles.cache.has(votingRole))
		throw new CommandError(`Only users with 'known' role can call votes`);
	if (activeVotes.has(channel.id))
		throw new CommandError(`A vote is already in progress in this channel`);
	activeVotes.add(channel.id);
	try {
		const emoteResult = queryEmote(guild.id, emote);
		if (!emoteResult)
			throw new CommandError(`There is no emote named ${emote} in the database`);
		const score = emoteResult.upvotes - downvotePower * emoteResult.downvotes;
		let message = `For the next 5 minutes, users can vote on ${emotify(
			emoteResult.emote,
			guild.emojis.cache,
		)} using buttons! `;
		message += `Current totals: **${emoteResult.upvotes}**👍 **${emoteResult.downvotes}**👎 **${score}**📈`;
		let attachment: AttachmentBuilder | undefined;
		if (emoteResult.image) {
			if (canAttach && hasWhitelistedImageDomain(emoteResult.image)) {
				const image = await getImage(guild.id, emoteResult.emote);
				if (image !== undefined) attachment = await emotePreview(image);
			}
			if (!attachment) message += ` <${emoteResult.image}>`;
		}
		const buttonRow = makeVoteButtonRow(0, 0, 0);

		const messageContent = {
			content: message,
			components: [buttonRow],
			files: attachment ? [attachment] : [],
		};
		const voteMessage = await channel.send(messageContent);
		const upvoters = new Set<string>();
		const downvoters = new Set<string>();
		const clearvoters = new Set<string>();
		const filter = (interaction: MessageComponentInteraction) =>
			interaction.customId === "👍" ||
			interaction.customId === "👎" ||
			interaction.customId === "🤷";
		const collector = voteMessage.createMessageComponentCollector<ComponentType.Button>({
			filter,
			time: votingTime,
		});
		collector.on("collect", (interaction) => {
			interaction.deferUpdate().catch(log);
			if (interaction.customId === "👍") {
				upvoters.add(interaction.user.id);
				downvoters.delete(interaction.user.id);
				clearvoters.delete(interaction.user.id);
			} else if (interaction.customId === "👎") {
				upvoters.delete(interaction.user.id);
				downvoters.add(interaction.user.id);
				clearvoters.delete(interaction.user.id);
			} else if (interaction.customId === "🤷") {
				upvoters.delete(interaction.user.id);
				downvoters.delete(interaction.user.id);
				clearvoters.add(interaction.user.id);
			}
			updateCallVoteMessage(voteMessage, upvoters, downvoters, clearvoters).catch(log);
		});
		collector.on("end", (_) => {
			queuedUpdateMessages.delete(voteMessage.id);
			processCalledVote(
				emoteResult.emote,
				voteMessage,
				attachment !== undefined,
				upvoters,
				downvoters,
				clearvoters,
			).catch(log);
		});
		setTimeout(() => activeVotes.delete(channel.id), votingTime);
	} catch (error) {
		activeVotes.delete(channel.id);
		throw error;
	}
};

const commandCallVote: DiscordCommandFunction = async (cCall) => {
	const {
		args,
		message: { guild, member, channel },
	} = cCall;
	await callVote(guild, channel, member, args[0], canSendAttachment(channel));
};

const slashCallVote: SlashCommandFunction = async (interaction) => {
	const [guild, member] = getGuildAndMember(interaction);
	const channel = interaction.channel;
	if (!(channel instanceof TextChannel || channel instanceof ThreadChannel))
		throw new CommandError("Could not get channel");
	const emoteArg = interaction.options.getString("emote", true);
	await callVote(guild, channel, member, emoteArg, canSendAttachment(channel));
	return { content: "Vote called" };
};

addCommand({
	name: "callvote",
	run: commandCallVote,
	category,
	usage: "<emote>",
	description: "Calls a vote on the chosen emote, allowing users to vote with reactions.",
	heat: { fixed: 2 },
	slash: {
		run: slashCallVote,
		options: [
			{
				name: "emote",
				type: ApplicationCommandOptionType.String,
				description: "The emote to call a vote on",
				required: true,
			},
		],
	},
	discordOnly: true,
});

/**
 * Applies one user vote for one emote
 * @returns [upvote change, downvote change]
 */
const voteInternal = (
	guildId: string,
	userId: string,
	emote: string,
	value: -1 | 0 | 1,
): [-1 | 0 | 1, -1 | 0 | 1] => {
	const currentVote = queryVote(guildId, emote, userId);
	if (currentVote === undefined) {
		if (value === 0) return [0, 0];
		const up = value === 1 ? 1 : 0;
		queryAddVote(guildId, emote, userId, up);
		if (value === 1) return [1, 0];
		else return [0, 1];
	} else if (value === 0) {
		queryRemoveMultipleVotes(guildId, userId, [emote]);
		if (currentVote === 0) return [1, 0];
		else return [0, 1];
	} else if (value === 1) {
		if (currentVote === 1) return [0, 0];
		queryChangeVote(guildId, emote, userId, 1);
		return [1, -1];
	} else {
		if (currentVote === 0) return [0, 0];
		queryChangeVote(guildId, emote, userId, 0);
		return [-1, 1];
	}
};

const processCalledVote = async (
	emote: string,
	voteMessage: Message,
	hadAttachment: boolean,
	upvoters: Set<string>,
	downvoters: Set<string>,
	clearvoters: Set<string>,
) => {
	// Update vote tallies in the database
	const qUpdateTallies = `
		UPDATE emotes
		SET upvotes = $upvotes, downvotes = $downvotes
		WHERE guild = $guild AND emote = $emote
	`;

	const guild = voteMessage.guild;
	if (!guild) throw new Error();
	const channel = voteMessage.channel;
	if (!channel.isSendable()) {
		throw new CommandError();
	}
	let rejectedUsers = 0;
	const votingRole = votingRoles.get(guild.id);
	if (!votingRole) {
		await channel.send("Guild has no voting role set");
		return;
	}
	for (const user of upvoters) {
		const member = await guild.members.fetch(user).catch(console.error);
		if (member && member.roles.cache.has(votingRole)) {
			if (downvoters.has(user)) {
				downvoters.delete(user);
				upvoters.delete(user);
			}
		} else {
			upvoters.delete(user);
			rejectedUsers++;
		}
	}
	for (const user of downvoters) {
		const member = await guild.members.fetch(user).catch(console.error);
		if (!member || !member.roles.cache.has(votingRole)) {
			downvoters.delete(user);
			rejectedUsers++;
		}
	}

	// Apply votes
	for (const user of upvoters) {
		voteInternal(guild.id, user, emote, 1);
	}
	for (const user of downvoters) {
		voteInternal(guild.id, user, emote, -1);
	}
	for (const user of clearvoters) {
		voteInternal(guild.id, user, emote, 0);
	}

	const buttonRow = makeVoteButtonRow(upvoters.size, downvoters.size, clearvoters.size, true);

	const handleCalledVoteEmoteMissing = (): void => {
		voteMessage
			.edit({
				content: `Reaction voting on ${emotify(emote, guild.emojis.cache)} has failed.`,
				components: [buttonRow],
			})
			.catch(console.log);
		channel
			.send(
				`Error: ${emotify(
					emote,
					guild.emojis.cache,
				)} was missing from the database at the end of the vote. Votes were not processed.`,
			)
			.catch(console.log);
	};

	// Get a new emoteresult in case votes have happened while waiting
	const emoteResult = queryEmote(guild.id, emote);
	if (!emoteResult) {
		handleCalledVoteEmoteMissing();
		return;
	}
	// Update tallies
	const voteResults = queryCountVotes(guild.id, emoteResult.emote);
	if (!voteResults) {
		handleCalledVoteEmoteMissing();
		return;
	}
	const upvotes = voteResults.upvotes;
	const downvotes = voteResults.downvotes;
	db.prepare(qUpdateTallies).run({
		guild: guild.id,
		emote: emoteResult.emote,
		upvotes,
		downvotes,
	});

	const oldScore = emoteResult.upvotes - downvotePower * emoteResult.downvotes;
	const newScore = upvotes - downvotePower * downvotes;
	const scoreChange = newScore - oldScore;
	const newUpvotes = upvotes - emoteResult.upvotes;
	const newDownvotes = downvotes - emoteResult.downvotes;

	// Construct edit message
	let editText = `Button voting on ${emotify(emoteResult.emote, guild.emojis.cache)} has closed.`;
	if (!hadAttachment && emoteResult.image) {
		// Add an image link if image wasn't added as attachment
		editText += ` <${emoteResult.image}>`;
	}
	// Construct final output message
	let updateText = ``;
	if (rejectedUsers > 0)
		updateText += `${rejectedUsers} user${
			rejectedUsers === 1 ? "" : "s"
		} tried voting, but did not have the 'known' role. `;
	const upvoteText =
		newUpvotes === 0
			? `**${upvotes}**👍`
			: newUpvotes > 0
			? `${emoteResult.upvotes}+${newUpvotes}=**${upvotes}**👍`
			: `${emoteResult.upvotes}${newUpvotes}=**${upvotes}**👍`;
	const downvotetext =
		newDownvotes === 0
			? `**${downvotes}**👎`
			: newDownvotes > 0
			? `${emoteResult.downvotes}+${newDownvotes}=**${downvotes}**👎`
			: `${emoteResult.downvotes}${newDownvotes}=**${downvotes}**👎`;
	const scoretext =
		scoreChange === 0
			? `**${newScore}**📈`
			: scoreChange > 0
			? `${oldScore}+${scoreChange}=**${newScore}**📈`
			: `${oldScore}${scoreChange}=**${newScore}**📈`;
	updateText += `New totals for ${emotify(
		emoteResult.emote,
		guild.emojis.cache,
	)}: ${upvoteText} ${downvotetext} ${scoretext}.`;

	// Edit and send
	voteMessage.edit({ content: editText, components: [buttonRow] }).catch(log);
	channel.send(updateText).catch(log);
};
