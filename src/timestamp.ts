/** Outputs a timestamp, by default in hh:mm format */
export const timestamp = (format?: string): string => {
	const now = new Date();

	const timeTokens = {
		"{y}": "year",
		"{mo}": "month",
		"{d}": "day",
		"{h}": "hours",
		"{m}": "minutes",
		"{s}": "seconds",
		"{ms}": "milliseconds",
	} as const;

	const components = {
		get year() {
			return now.getUTCFullYear().toString(10).padStart(4, "0");
		},
		get month() {
			return now.getUTCMonth().toString(10).padStart(2, "0");
		},
		get day() {
			return now.getUTCDate().toString(10).padStart(2, "0");
		},
		get hours() {
			return now.getUTCHours().toString(10).padStart(2, "0");
		},
		get minutes() {
			return now.getUTCMinutes().toString(10).padStart(2, "0");
		},
		get seconds() {
			return now.getUTCSeconds().toString(10).padStart(2, "0");
		},
		get milliseconds() {
			return now.getUTCMilliseconds().toString(10).padStart(3, "0");
		},
	} as const;

	if (format !== undefined) {
		let output = format;
		for (const timeToken of Object.keys(timeTokens) as (keyof typeof timeTokens)[]) {
			if (output.includes(timeToken)) {
				output = output.replace(timeToken, components[timeTokens[timeToken]]);
			}
		}
		return output;
	} else {
		return `${components.hours}:${components.minutes}`;
	}
};
