export const stringifyDateTime = (time: Date): string => {
	const milliseconds = time.getUTCMilliseconds();
	if (milliseconds >= 500) time.setUTCSeconds(time.getUTCSeconds() + 1);
	const year = time.getUTCFullYear();
	const month = time.getUTCMonth() + 1;
	const date = time.getUTCDate();
	const hours = time.getUTCHours();
	const minutes = time.getUTCMinutes();
	const seconds = time.getUTCSeconds();
	const secondsStr = seconds.toString(10).padStart(2, "0");
	const minutesStr = minutes.toString(10).padStart(2, "0");
	const hoursStr = hours.toString(10).padStart(2, "0");
	const dateStr = date.toString(10).padStart(2, "0");
	const monthStr = month.toString(10).padStart(2, "0");

	return `${year}-${monthStr}-${dateStr} ${hoursStr}:${minutesStr}:${secondsStr}`;
};

export default stringifyDateTime;
