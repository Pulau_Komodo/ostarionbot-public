import { NBH, NBSP, WJ } from "./characters.js";
import { regexTwemoji } from "./misc/emoji_regex_tests.js";

/**
 * Replaces characters that allow line breaking with alternatives that don't, and inserts word joiners between characters that allow line breaks.
 *
 * Does not touch characters explicitly meant to force or allow line breaks.
 *
 * Not complete. Realized it would be excessively expensive to check for emoji pairs.
 */
/*const makeUnbreakable = (text: string): string => {
	return text.replace(/[- ]|(.)(q(?=.))/g, (char, first, second) => {
		if (char === " ") {
			return NBSP;
		} else if (char === "-") {
			return NBH;
		}
		if (regexTwemoji.test(first) && regexTwemoji.test(second)) {
			return `${first}${WJ}${second}`;
		}
		return `${first}${second}`;
	});
};*/

const substitutions: Record<string, string> = {
	" ": NBSP,
	"-": NBH,
};

/** Replaces spaces and hyphens with non-breaking versions */
export const makeUnbreakable = (text: string): string => {
	return text.replace(/[- ]/g, (char: string): string => {
		return substitutions[char] ?? "";
	});
};

/**
 * Applies digit grouping with spaces to a number.
 *
 * Will round and will not make any changes to numbers `1e21` or higher.
 */
export const groupDigits = (number: number): string => {
	number = Math.round(number);
	const text = number.toString();
	if (number > 1e21 || isNaN(number)) {
		return text;
	}
	let formatted = "";
	for (let i = 0; i < text.length - 1; i++) {
		formatted += text[i];
		if ((text.length - i) % 3 === 1) {
			formatted += " ";
		}
	}
	formatted += text[text.length - 1];
	return formatted;
};
